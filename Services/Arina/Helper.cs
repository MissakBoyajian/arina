﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Data;
using System.ComponentModel;
using DocumentFormat.OpenXml.Packaging;
using System.Web;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;

namespace Arina
{
    public static class Helper
    {
        
        public static string ToSerializedXml(this object obj)
        {
            XmlSerializer serializer = new XmlSerializer(obj.GetType());
            using (StringWriter stringwriter = new StringWriter())
            {
                serializer.Serialize(stringwriter, obj);
                return stringwriter.ToString();
            }
        }

        public static T GetObjectFromXMLString<T>(this string str, T defaultValue) where T : class
        {
            try
            {
                return str.GetObjectFromXMLString<T>();
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }
        public static T GetObjectFromXMLString<T>(this string str) where T : class
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            byte[] byteArray = Encoding.ASCII.GetBytes(str);
            using (MemoryStream stream = new MemoryStream(byteArray))
            {
                T retVal = serializer.Deserialize(stream) as T;
                return retVal;
            }
        }

        public static byte[] ExportDataTableToExcel(DataTable table, string path)
        {
            string fileName = path;

            using (SpreadsheetDocument workbook = SpreadsheetDocument.Create(fileName, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart workbookPart = workbook.AddWorkbookPart();
                workbook.WorkbookPart.Workbook = new Workbook();
                workbook.WorkbookPart.Workbook.Sheets = new Sheets();

                var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new SheetData();
                sheetPart.Worksheet = new Worksheet(sheetData);

                Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);
                uint sheetId = 1;
                if (sheets.Elements<Sheet>().Count() > 0)
                    sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;


                Sheet sheet = new Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                sheets.Append(sheet);

                Row headerRow = new Row();

                List<string> columns = new List<string>();
                foreach (DataColumn column in table.Columns)
                {
                    columns.Add(column.ColumnName);

                    Cell cell = new Cell();
                    cell.DataType = CellValues.String;
                    cell.CellValue = new CellValue(column.ColumnName);
                    headerRow.AppendChild(cell);
                }


                sheetData.AppendChild(headerRow);

                foreach (DataRow dsrow in table.Rows)
                {
                    Row newRow = new Row();
                    foreach (string col in columns)
                    {
                        Cell cell = new Cell();
                        cell.DataType = CellValues.String;
                        cell.CellValue = new CellValue(dsrow[col].ToString());
                        newRow.AppendChild(cell);
                    }

                    sheetData.AppendChild(newRow);
                }

                workbook.Save();
            }


            byte[] filebytes = File.ReadAllBytes(fileName);
                File.Delete(fileName);
                return filebytes;
            }

        public static DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        public static byte[] ReadToEnd(this System.IO.Stream stream)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Truncates the first number of cahracers
        /// </summary>
        /// <param name="str">string to truncate</param>
        /// <param name="showFirst">number of characrer</param>
        /// <returns>Truncated String</returns>
        public static string TruncateAt(this string str, int showFirst)
        {
            if (str == null)
                return string.Empty;
            else if (str.Length < showFirst)
                return str;
            else
                return str.Substring(0, showFirst) + "...";
        }
        public static string KeepLast(this string str, int last)
        {
            if (str == null)
                return string.Empty;
            if (str.Length < last)
                return str;

            int total = str.Length;
            string retVal = string.Empty;
            for (int i = 0; i < total - last; i++)
                retVal = retVal + "*";
            string sub = str.Substring(str.Length - last, last);
            retVal = retVal + sub;
            return retVal;
        }
        public static double distanceTo(double lat1, double lon1, double lat2, double lon2)
        {
            double rlat1 = Math.PI * lat1 / 180;
            double rlat2 = Math.PI * lat2 / 180;
            double rlon1 = Math.PI * lon1 / 180;
            double rlon2 = Math.PI * lon2 / 180;
            double theta = lon1 - lon2;
            double rtheta = Math.PI * theta / 180;
            double dist = Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) * Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            dist = dist * 1.609344;
            return dist;
        }

        public static string ToConcatinatedStringString(this List<string> ids)
        {
            StringBuilder str = new StringBuilder();
            if (ids != null)
            {
                foreach (string item in ids)
                {
                    if (item != string.Empty)
                    {
                        str.Append(item + "#@#");
                    }
                }
            }

            return str.ToString();
        }
        public static List<string> ToStringList(this string concatinatedString)
        {
            List<string> retVal = new List<string>();
            if (concatinatedString != null)
            {
                string[] arrStrings = concatinatedString.Split("#@#".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                foreach (string item in arrStrings)
                {
                    retVal.Add(item.EnsureString());
                }
            }

            return retVal;
        }
        public static byte[] GetBytes(this Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }
    }
}
