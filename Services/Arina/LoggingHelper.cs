﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Arina
{
    public static class LoggingHelper
    {
        public static void Error(Exception ex, string format, params object[] args)
        {
            Error(ex);
        }

        public static void Error(Exception ex)
        {
            Error(ex.Message + "\n" + ex.StackTrace);
        }

        public static void Error(string error)
        {
            string eventSource = CreateEventSource("FieldForce");
            System.Diagnostics.EventLog appLog = new System.Diagnostics.EventLog();
            appLog.Source = eventSource;
            appLog.WriteEntry(error, EventLogEntryType.Error);
        }

        private static string CreateEventSource(string currentAppName)
        {
            string eventSource = currentAppName;
            bool sourceExists;
            try
            {
                sourceExists = EventLog.SourceExists(eventSource);
                if (!sourceExists)
                    EventLog.CreateEventSource(eventSource, "Application");

            }
            catch (SecurityException)
            {
                eventSource = "Application";
            }

            return eventSource;
        }
    }
}
