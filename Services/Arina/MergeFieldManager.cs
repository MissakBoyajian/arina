﻿
using System.Collections.Generic;
using System.Text;
namespace Arina
{
    /// <summary>
    /// Returns the field after being replaced with the values
    /// </summary>
    public class MergeFieldManager
    {
        /// <summary>
        /// Gets the string.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="obj">The obj.</param>
        /// <returns>the text with the placeholders replaced</returns>
        public static string GetString(string content, object obj)
        {
            List<MergeField> all = MergeField.GetAllMergeFields(content);
            StringBuilder retVal = new StringBuilder(content);
             
            foreach (MergeField item in all)
            {
                retVal.Replace(item.AllString, item.Evaluate(obj));
            }

            return retVal.ToString();
        }
    }
}
