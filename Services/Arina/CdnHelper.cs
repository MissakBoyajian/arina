﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arina
{
    public static class CdnHelper
    {
        public static string UploadToCdn(byte[] content, string fileName)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
               CloudConfigurationManager.GetSetting("CdnStorageConnectionString"));

            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            CloudBlobContainer container = blobClient.GetContainerReference("files");

            container.CreateIfNotExists();

            container.SetPermissions(new BlobContainerPermissions{ PublicAccess = BlobContainerPublicAccessType.Blob });

            CloudBlockBlob blockBlob = GetCorrespondingBlob(container, fileName);

            using (var fileStream = new MemoryStream(content, true))
            {
                blockBlob.UploadFromStream(fileStream);
            }

            return blockBlob.Uri.ToString();
          
        }

        private static CloudBlockBlob GetCorrespondingBlob(CloudBlobContainer container, string fileName)
        {
            CloudBlockBlob currentBlockBlob = container.GetBlockBlobReference(fileName);
            if (currentBlockBlob.Exists())
            {
                int i = 1;
                while (true)
                {
                    CloudBlockBlob blobFile = container.GetBlockBlobReference(fileName + i.ToString());
                    if (blobFile.Exists())
                    {
                        i++;
                        continue;
                    }
                    else
                        return blobFile;
                }
            }
            return currentBlockBlob;
        }

    }
}
