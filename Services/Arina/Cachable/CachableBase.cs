﻿namespace Arina
{
    using System;
    using System.Collections.Generic;

    public abstract class Cachable : IDisposable
    {
        private static object cacheObjectsLOCK = new object();
        private static List<Cachable> cacheObjects = new List<Cachable>();

        public Cachable()
        {
            lock (cacheObjectsLOCK)
            {
                if (!cacheObjects.Contains(this))
                    cacheObjects.Add(this);
            }
        }

        public static void ClearAllCache()
        {
            foreach (Cachable cacheObj in cacheObjects)
            {
                try
                {
                    cacheObj.Clear();
                }
                catch
                {
                    // Just SKIP
                }
            }
        }

        public abstract void Clear();

        public void Dispose()
        {
            lock (cacheObjectsLOCK)
            {
                if (cacheObjects.Contains(this))
                    cacheObjects.Remove(this);
            }
        }
    }
}
