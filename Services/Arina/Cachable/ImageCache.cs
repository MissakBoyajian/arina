﻿namespace Arina
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;

    public class ImageCache : Cachable
    {
        private const string ImageCacheFileExtension = ".ImageCache";
        private static string imageCacheFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "assets\\imagecache");

        public delegate byte[] RetrieveImage(Guid id);


        public ImageCache()
        {
        }             

        public byte[] GetImage(string prefix, Guid id, int width, int height, double? ratio, RetrieveImage retrieveFromStore)
        {
            if (ratio.HasValue)
                return GetImage(prefix, id, ratio.Value, retrieveFromStore);
            else if (width == 0 && height == 0)
                return GetImage(prefix, id, retrieveFromStore);

            else
                return GetImage(prefix,id, width, height, retrieveFromStore);

        }

        protected byte[] GetImage(string prefix, Guid id, RetrieveImage retrieveFromStore)
        {
            string fileName = prefix + "-" + id.ToString() + ImageCacheFileExtension;
            if (File.Exists(Path.Combine(imageCacheFolder, fileName)))
            {
                return File.ReadAllBytes(Path.Combine(imageCacheFolder, fileName));
            }
            else
            {
                byte[] fileBytes = retrieveFromStore(id);
                File.WriteAllBytes(Path.Combine(imageCacheFolder, fileName), fileBytes ?? new byte[0]);
                return fileBytes;
            }
        }
        protected byte[] GetImage(string prefix, Guid id, double ratio, RetrieveImage retrieveFromStore)
        {
            byte[] retval = null;

            string fileName = string.Format("{0}-{1}_R{2}_{3}", prefix, id.ToString(), ratio.ToString("0.00"), ImageCacheFileExtension);
            string fileNameOriginalSize = prefix + "-" + id.ToString() + ImageCacheFileExtension;

            if (File.Exists(Path.Combine(imageCacheFolder, fileName)))
            {
                retval = File.ReadAllBytes(Path.Combine(imageCacheFolder, fileName));
            }
            else
            {
                byte[] image = GetImage(prefix, id, retrieveFromStore);
                byte[] imageThumbnail = null;


                imageThumbnail = ThumbnailImage(image, ratio);

                if (imageThumbnail == null)
                {
                    retval = image;
                }
                else
                {
                    File.WriteAllBytes(Path.Combine(imageCacheFolder, fileName), imageThumbnail);
                    retval = imageThumbnail;
                }
            }

            return retval;

        }
        protected byte[] GetImage(string prefix, Guid id, int width, int height, RetrieveImage retrieveFromStore)
        {
            string fileName = string.Format("{0}-{1}_w={2}h={3}{4}",prefix, id.ToString(), width, height, ImageCacheFileExtension);
            string fileNameOriginalSize = id.ToString() + ImageCacheFileExtension;

            if (File.Exists(Path.Combine(imageCacheFolder, fileName)) && width > 0 && height > 0)
            {
                return File.ReadAllBytes(Path.Combine(imageCacheFolder, fileName));
            }
            else
            {
                byte[] image = GetImage(prefix, id, retrieveFromStore);
                byte[] imageThumbnail = ThumbnailImage(image, width, height);
                if (imageThumbnail != null)
                {
                    File.WriteAllBytes(Path.Combine(imageCacheFolder, fileName), imageThumbnail);

                    return imageThumbnail;
                }
                else
                {
                    return image;
                }
            }
        }

        public override void Clear()
        {
            ClearAll();
        }

        public static void ClearAll()
        {
            string[] allFiles = Directory.GetFiles(imageCacheFolder, "*" + ImageCacheFileExtension);
            if (allFiles != null)
            {
                foreach (string item in allFiles)
                {
                    try
                    {
                        File.Delete(item);
                    }
                    catch
                    {
                        // Just skip the error and skip it
                    }
                }
            }
        }

        private static byte[] ThumbnailImage(byte[] image, double ratio)
        {
            if (image == null)
                return image;

            if (ratio == 0)
                return image;

            using (MemoryStream stream = new MemoryStream(image))
            {
                // Creating the image object
                System.Drawing.Image mainImage = System.Drawing.Image.FromStream(stream);
                double sourceWidth = mainImage.Width;
                double sourceHeight = mainImage.Height;

                double sourceRatio = sourceWidth / sourceHeight;
                if (ratio > sourceRatio)
                    return ThumbnailImage(image, Convert.ToInt32(sourceWidth), Convert.ToInt32(sourceWidth / ratio));
                else
                    return ThumbnailImage(image, Convert.ToInt32(ratio * sourceHeight), Convert.ToInt32(sourceHeight));
            }
        }
        private static byte[] ThumbnailImage(byte[] image, int width, int height)
        {
            if (image == null)            
                return image;
            
            if (width == 0 && height == 0)            
                return image;
            
            double TargetWidth = width;
            double TargetHeight = height;

            double SourceWidth;
            double SourceHeight;

            double ResizeWidth;
            double ResizeHeight;

            using (MemoryStream stream = new MemoryStream(image))
            {
                // Creating the image object
                System.Drawing.Image mainImage = System.Drawing.Image.FromStream(stream);
                SourceWidth = mainImage.Width;
                SourceHeight = mainImage.Height;

                if (TargetWidth == 0)
                    TargetWidth = (TargetHeight * SourceWidth) / SourceHeight;

                if (TargetHeight == 0)
                    TargetHeight = (TargetWidth * SourceHeight) / SourceWidth;

                double sourceApectRation = SourceWidth / SourceHeight;
                double targetApectRation = TargetWidth / TargetHeight;

                if (sourceApectRation > targetApectRation)
                {
                    ResizeHeight = TargetHeight;
                    ResizeWidth = SourceWidth * ResizeHeight / SourceHeight;
                }
                else
                {
                    ResizeWidth = TargetWidth;
                    ResizeHeight = SourceHeight * ResizeWidth / SourceWidth;
                }

                // The Get Thumbnail
                // Bitmap thumbnailImage = (Bitmap)mainImage.GetThumbnailImage(ResizeWidth.EnsureInt(), ResizeHeight.EnsureInt(), () => false, IntPtr.Zero);
                Bitmap thumbnailImage = new Bitmap(ResizeWidth.EnsureInt(), ResizeHeight.EnsureInt());
                using (Graphics gr = Graphics.FromImage(thumbnailImage))
                {
                    gr.SmoothingMode = SmoothingMode.HighQuality;
                    gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    gr.DrawImage(mainImage, new Rectangle(0, 0, ResizeWidth.EnsureInt(), ResizeHeight.EnsureInt()));
                }

                // Crop
                int recX = 0;
                if (thumbnailImage.Width > TargetWidth)                
                    recX = (thumbnailImage.Width - TargetWidth.EnsureInt()) / 2;
                
                int recY = 0;
                if (thumbnailImage.Height > TargetHeight)                
                    recY = (thumbnailImage.Height - TargetHeight.EnsureInt()) / 2;
                
                Bitmap croppedImage = thumbnailImage.Clone(new Rectangle(recX, recY, TargetWidth.EnsureInt(), TargetHeight.EnsureInt()), thumbnailImage.PixelFormat);
                using (MemoryStream thumbStream = new MemoryStream())
                {
                    croppedImage.Save(thumbStream, mainImage.RawFormat);
                    return thumbStream.ToArray();
                }
            }
        }
    }
}