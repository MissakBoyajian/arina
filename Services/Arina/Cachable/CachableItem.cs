﻿namespace Arina
{
    using System;

    public class Cachable<T> : Cachable where T : class
    {
        private Func<T> objectNeededDelegate;
        private T cachedValue;
        private object lockObj = new object();

        public Cachable(Func<T> objectNeeded)
        {
            if (objectNeeded == null)            
                throw new ArgumentNullException("objectNeeded");            

            this.objectNeededDelegate = objectNeeded;
        }

        public T Value
        {
            get
            {
                lock (this.lockObj)
                {
                    if (this.cachedValue == null)                    
                        this.cachedValue = this.objectNeededDelegate();                    

                    return this.cachedValue;
                }
            }
        }

        public override void Clear()
        {
            lock (this.lockObj)
            {
                if (this.cachedValue is IDisposable)                
                    ((IDisposable)this.cachedValue).Dispose();                

                this.cachedValue = null;
            }
        }
    }
}
