﻿namespace Arina
{
    using System;
    using System.Collections.Generic;

    public class Cachable<K, T> : Cachable where T : class
    {
        private Func<K, T> objectNeededDelegate;             
        private Dictionary<K, CacheItem<K, T>> cacheDictionary = new Dictionary<K, CacheItem<K, T>>();
        
        private object lockObj = new object();
        
        public Cachable(Func<K, T> objectNeeded)
        {
            if (objectNeeded == null)            
                throw new ArgumentNullException("objectNeeded");

            this.objectNeededDelegate = objectNeeded;
        }

        public T this[K index]
        {
            get
            {
                lock (this.lockObj)
                {
                    if (this.cacheDictionary.ContainsKey(index))
                    {
                        return this.cacheDictionary[index].Value;
                    }
                    else
                    {
                        T newVal = this.objectNeededDelegate(index);
                        CacheItem<K, T> item = new CacheItem<K, T>(index, newVal);
                        if (this.cacheDictionary.ContainsKey(index))                        
                            this.cacheDictionary[index] = item;                        
                        else                        
                            this.cacheDictionary.Add(index, item);                        

                        return newVal;
                    }
                }
            }
        }

        public override void Clear()
        {
            lock (this.lockObj)
            {
                foreach (KeyValuePair<K, CacheItem<K, T>> item in this.cacheDictionary)
                {
                    if (item.Value is IDisposable)
                        ((IDisposable)item.Value).Dispose();                    
                }

                this.cacheDictionary.Clear();
            }
        }

        private class CacheItem<TKey, TValue> : IDisposable
        {
            public CacheItem(TKey key, TValue value)
            {
                this.Key = key;
                this.Value = value;
            }

            public TKey Key { get; private set; }
            public TValue Value { get; private set; }

            public void Dispose()
            {
                if (this.Value is IDisposable)                
                    ((IDisposable)this.Value).Dispose();            
            }            
        }
    }
}
