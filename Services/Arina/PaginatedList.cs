﻿using System.Collections.Generic;

namespace Arina
{
    public class PaginatedList<T> : List<T>
    {
        public int CurrentPage {set;get;}
        public int TotalPages { set; get; }

    }
}
