﻿
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;
namespace Arina
{
    /// <summary>
    /// Is responsible for replacing the fields with their values
    /// </summary>
    public class MergeField
    {
        /// <summary>
        /// Regular expression for merge fields
        /// </summary>
        private const string RegexWfeString = "<# [^>]*>(.*?)";

        /// <summary>
        /// Initializes a new instance of the <see cref="MergeField"/> class.
        /// </summary>
        /// <param name="allString">All string.</param>
        private MergeField(string allString)
        {
            this.AllString = allString;
            this.PropertyName = allString.Replace("<#", string.Empty).Replace("/>", string.Empty).Replace(" ", string.Empty);
        }

        /// <summary>
        /// Gets all string.
        /// </summary>
        /// <value>All string.</value> 
        public string AllString { get; private set; }

        /// <summary>
        /// Gets the name of the property.
        /// </summary>
        /// <value>The name of the property.</value>
        public string PropertyName { get; private set; }

        /// <summary>
        /// Gets all merge fields.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>list of all merge fields</returns>
        public static List<MergeField> GetAllMergeFields(string text)
        {
            List<MergeField> retVal = new List<MergeField>();

            Regex regex = new Regex(RegexWfeString);
            MatchCollection match = regex.Matches(text);

            foreach (Match item in match)
            {
                retVal.Add(new MergeField(item.Value));
            }

            return retVal;
        }

        /// <summary>
        /// Evaluates the specified obj.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>value of the object</returns>
        public string Evaluate(object obj)
        {
            try
            {
                string[] allProps = this.PropertyName.Split('.');
                object thisObj = obj;
                int index = 0;

                foreach (string prop in allProps)
                {
                    Type type = thisObj.GetType();
                    PropertyInfo propertyInfo;

                    if (prop.StartsWith("[") && prop.EndsWith("]") && int.TryParse(prop.TrimEnd(']').TrimStart('['), out index))
                    {
                        propertyInfo = type.GetProperty("Item");
                        thisObj = propertyInfo.GetValue(obj, new object[] { index });
                    }
                    else
                    {
                        propertyInfo = type.GetProperty(prop);
                        thisObj = propertyInfo.GetValue(thisObj, null);
                    }

                    index++;
                }

                if (thisObj == null)
                {
                    return string.Empty;
                }

                if (thisObj is DateTime)
                {
                    DateTime dt = (DateTime)thisObj;
                    return dt.ToString("dd-MMM-yyyy");
                }
                else
                {
                    return thisObj.ToString();
                }
            }
            catch (Exception)
            {
                return this.AllString;
            }
        }
    }
}
