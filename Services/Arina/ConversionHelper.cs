﻿namespace Arina
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Text;
    using System.Web;
    using System.Threading;
    using System.Runtime.Serialization.Formatters.Binary;

    /// <summary>
    /// Represent a helper class to convert objects
    /// </summary>
    public static class ConversionHelper
    {
        private static readonly char[] allowedChars = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z','A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
        public static string TrimSpecialCharacters(this string str)
        {
            string retVal = string.Empty;

            char[] all = str.ToCharArray();
            foreach (char item in all)
            {
                int index = Array.IndexOf(allowedChars, item);
                if (index >= 0)
                    retVal = retVal + item;
            }

            return retVal;
        }

        /// <summary>
        /// Ensures the string.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>string representation of the object</returns>
        public static string EnsureString(this object obj)
        {
            if (obj == null)
            {
                return string.Empty;
            }
            else
            {
                return obj.ToString();
            }
        }

        /// <summary>
        /// Convert the array.
        /// </summary>
        /// <param name="str">The STR.</param>
        /// <returns>string array</returns>
        public static string[] ToArray(this string str)
        {
            string concat = str.EnsureString();
            string[] retVal = str.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            return retVal;
        }

        /// <summary>
        /// Joins the specified ar.
        /// </summary>
        /// <param name="ar">The ar.</param>
        /// <returns></returns>
        public static string Join(this string[] ar)
        {
            string retVal = string.Empty;
            foreach (string item in ar)
            {
                retVal = retVal + item + ";"; 
            }

            return retVal;
        }

        /// <summary>
        /// To the int.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>The object as int</returns>
        public static int EnsureInt(this object obj)
        {
            if (obj == null)
            {
                return 0;
            }

            if (obj == System.DBNull.Value)
            {
                return 0;
            }

            try
            {
                int retVal = Convert.ToInt32(obj);
                return retVal;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Spaces the on capital.
        /// </summary>
        /// <param name="str">The STR.</param>
        /// <returns>String with Spaces</returns>
        public static string SpaceOnCapital(this string str)
        {
            char[] letters = str.EnsureString().ToCharArray();
            string retVal = string.Empty;

            foreach (char letter in letters)
            {
                if (char.IsLower(letter))
                {
                    retVal = retVal + letter;
                }
                else
                {
                    retVal = retVal + " " +letter;
                }
            }

            return retVal;
        }

        /// <summary>
        /// Ensures the bool.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>Returns an object of type boolean</returns>
        public static bool EnsureBool(this object obj)
        {
            bool retVal;

            if (obj.EnsureString().ToLower() == "true" || obj.EnsureString().ToLower() == "yes" || obj.EnsureInt() == 1)
            {
                return true;
            }
            else if (obj.EnsureString().ToLower() == "no" || obj.EnsureInt() == 0)
            {
                return false;
            }
            else if (bool.TryParse(obj.EnsureString(), out retVal))
            {
                return retVal;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// To the datetime
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>The object as datetime</returns>
        public static DateTime EnsureDate(this object obj)
        {
            DateTime retVal;
            if (DateTime.TryParse(obj.EnsureString(), out retVal))
            {
                return retVal;
            }
            else
            {
                return DateTime.Now;
            }
        }

        /// <summary>
        /// Ensures the GUID.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>Returns a guid</returns>
        public static Guid EnsureGuid(this object obj)
        {
            Guid retVal;
            if (Guid.TryParse(obj.EnsureString(), out retVal))
            {
                return retVal;
            }
            else
            {
                return Guid.Empty;
            }
        }

        /// <summary>
        /// Ensures the decimal.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>Returns a decimal</returns>
        public static decimal EnsureDecimal(this object obj)
        {
            decimal retVal;
            if (decimal.TryParse(obj.EnsureString(), out retVal))
            {
                return retVal;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Ensures the float.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns></returns>
        public static float EnsureFloat(this object obj)
        {
            float retVal;
            if (float.TryParse(obj.EnsureString(), out retVal))
            {
                return retVal;
            }
            else
            {
                return 0f;
            }
        }

        /// <summary>
        /// Ensures the double.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns></returns>
        public static double EnsureDouble(this object obj)
        {
            double retVal;
            if (double.TryParse(obj.EnsureString(), out retVal))
            {
                return retVal;
            }
            else
            {
                return 0f;
            }
        }

        /// <summary>
        /// Ensures the enum.
        /// </summary>
        /// <typeparam name="T">Enum Type</typeparam>
        /// <param name="obj">The obj.</param>
        /// <returns>Enum Value</returns>
        public static T EnsureEnum<T>(this object obj) where T : struct
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            string str = obj.EnsureString();
            str = str.Replace(" ", string.Empty);
            T retVal = (T)Enum.Parse(typeof(T), str, true);
            return retVal;
        }

        public static T EnsureEnum<T>(this object obj, T defaultValue) where T : struct
        {
            try
            {
                return obj.EnsureEnum<T>();
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }
        public static T GetCustomAttribute<T>(this System.Reflection.MemberInfo member)
        {
            Type attributeType = typeof(T);
            object[] allAttributes = member.GetCustomAttributes(attributeType, false);
            if (allAttributes != null && allAttributes.Length > 0)
            {
                return (T)allAttributes[0];
            }
            else
            {
                return default(T);
            }
        }

        public static T EnsureObject<T>(this byte[] data, T defaultValue)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream(data))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    T retVal = (T)formatter.Deserialize(ms);
                    if (retVal == null)
                        return defaultValue;
                    return retVal;
                }
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }
        public static byte[] GetBytes(this object obj)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(ms, obj);
                    return ms.GetBytes();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
         
        public static TimeSpan GetTimeSpan(this DateTime date)
        {
            return new TimeSpan(date.Hour, date.Minute, 0);
        }
    }
}
