using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Arina.Business;
using Arina.Business.Data;
using Arina.Business.Data.Sql;

namespace Arina.Data.Sql
{
    public class MemberDataService : IMemberDataService
    {
        public Member LoadById(Guid memberId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Members_SelectById", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, memberId));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    List<Member> all = Load(ds.Tables[0]);
                    if (all.Count == 0)
                        return null;
                    else
                        return all[0];
                }
            }
        }

        public List<Member> Find(Guid DealerSiteId, string Territory, string Status, string Roles, string Criteria)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Members_Find", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new CustomSqlParameter("DealerSiteId", SqlDbType.UniqueIdentifier, DealerSiteId));
                    cmd.Parameters.Add(new CustomSqlParameter("Territory", SqlDbType.NVarChar, Territory));
                    cmd.Parameters.Add(new CustomSqlParameter("Criteria", SqlDbType.NVarChar, Criteria));
                    cmd.Parameters.Add(new CustomSqlParameter("Status", SqlDbType.NVarChar, Status));
                    cmd.Parameters.Add(new CustomSqlParameter("Roles", SqlDbType.NVarChar, Roles));

                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    List<Member> Members = new List<Member>();
                    if (dt != null)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {

                            Member member = Load(dr);
                            Members.Add(member);
                        }
                    }
                    return Members;
                }
            }
        }

        public Member FindCustomerByPhone(string Phone)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Members_FindByPhone", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("Phone", SqlDbType.NVarChar, Phone));

                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);

                    List<Member> all = Load(ds.Tables[0]);

                    if (all.Count == 0)
                        return null;
                    else
                        return all[0];
                }
            }
        }

        public List<Member> FindAll(string criteria, string DealerSiteId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Members_FindAll", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("criteria", SqlDbType.NVarChar, "%" + criteria + "%"));
                    //cmd.Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId));
                    cmd.Parameters.Add(new CustomSqlParameter("DealerSiteId", SqlDbType.UniqueIdentifier, DealerSiteId.EnsureGuid()));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return Load(dt);
                }
            }
        }

        public PaginatedList<Member> FindMyCustomers(string Criteria, int PageNumber)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Customers_GetByMemberId", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId));
                    cmd.Parameters.Add(new CustomSqlParameter("PageNumber", SqlDbType.NVarChar, PageNumber));
                    cmd.Parameters.Add(new CustomSqlParameter("Criteria", SqlDbType.Int, Criteria));

                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);

                    PaginatedList<Member> MyCustomers = new PaginatedList<Member>();
                    MyCustomers.AddRange(Load(dt));

                    if (MyCustomers.Count > 0)
                    {
                        MyCustomers.CurrentPage = PageNumber;
                        MyCustomers.TotalPages = dt.Rows[0]["TotalPages"].EnsureInt();
                    }
                    return MyCustomers;
                }
            }
        }

        public List<Member> FindAllManagers()
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Members_FindAllManagers", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.Add(new CustomSqlParameter("DealerSiteId", SqlDbType.UniqueIdentifier, DealerSiteId.EnsureGuid()));
                    cmd.Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId));

                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return Load(dt);
                }
            }
        }

        public int Delete(Guid memberId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Members_Delete", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, memberId));
                    return cmd.ExecuteNonQuery();
                }
            }
        }

        public Member Load(string email)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Members_SelectByEmail", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("Email", SqlDbType.NVarChar, email));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    List<Member> all = Load(ds.Tables[0]);
                    if (all.Count == 0)
                        throw new NotFoundException();
                    else
                        return all[0];
                }
            }
        }

        public Member Load(string email, string password)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Members_SelectByEmailPassword", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("Email", SqlDbType.NVarChar, email));
                    cmd.Parameters.Add(new CustomSqlParameter("Password", SqlDbType.NVarChar, password));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    List<Member> all = Load(ds.Tables[0]);
                    if (all.Count == 0)
                        throw new NotFoundException();
                    else
                        return all[0];
                }
            }
        }

        public void Save(Member member)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Members_Save", connection))
                {
                    if (member.MemberId == Guid.Empty)
                    {
                        member.MemberId = Guid.NewGuid();
                        member.Password = System.Web.Security.Membership.GeneratePassword(8, 3).Replace('[', '9');
                    }

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, member.MemberId));
                    cmd.Parameters.Add(new CustomSqlParameter("FirstName", SqlDbType.NVarChar, member.FirstName));
                    cmd.Parameters.Add(new CustomSqlParameter("LastName", SqlDbType.NVarChar, member.LastName));
                    cmd.Parameters.Add(new CustomSqlParameter("Email", SqlDbType.NVarChar, member.Email));
                    cmd.Parameters.Add(new CustomSqlParameter("Phone", SqlDbType.NVarChar, member.Phone));
                    cmd.Parameters.Add(new CustomSqlParameter("LastLoginDate", SqlDbType.DateTime, member.LastLoginDate));

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void SaveCustomer(Member member)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Customers_Save", connection))
                {

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new CustomSqlParameter("CustomerId", SqlDbType.UniqueIdentifier, member.MemberId.EnsureGuid()));
                    cmd.Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId.EnsureGuid()));
                    cmd.Parameters.Add(new CustomSqlParameter("FirstName", SqlDbType.NVarChar, member.FirstName.EnsureString()));
                    cmd.Parameters.Add(new CustomSqlParameter("LastName", SqlDbType.NVarChar, member.LastName.EnsureString()));
                    cmd.Parameters.Add(new CustomSqlParameter("Phone", SqlDbType.NVarChar, member.Phone.EnsureString()));

                    cmd.ExecuteNonQuery();
                }
            }
        }


        private static List<Member> Load(DataTable dt)
        {
            List<Member> retVal = new List<Member>();
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                    retVal.Add(Load(dr));
            }
            return retVal;
        }

        public bool ValidateQuestion(string Email, Guid QuestionId, string reply)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Members_ValidateQuestion", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("MemberEmail", SqlDbType.NVarChar, Email));
                    cmd.Parameters.Add(new CustomSqlParameter("QuestionId", SqlDbType.UniqueIdentifier, QuestionId));
                    cmd.Parameters.Add(new CustomSqlParameter("reply", SqlDbType.NVarChar, reply));

                    if (int.Parse(cmd.ExecuteScalar().ToString()) > 0)
                        return true;
                    else
                        return false;
                }
            }
        }

        //Modify stored Procedure Members_UpdatePassword

        public void UpdatePassword(string NewPassword, string Email, Guid ResetPasswordToken)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Members_UpdatePassword", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("Email", SqlDbType.NVarChar, Email));
                    cmd.Parameters.Add(new CustomSqlParameter("Password", SqlDbType.NVarChar, NewPassword));
                    cmd.Parameters.Add(new CustomSqlParameter("ResetPasswordToken", SqlDbType.UniqueIdentifier, ResetPasswordToken));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private static Member Load(DataRow dr)
        {
            Member retVal = new Member()
            {
                MemberId = dr["MemberId"].EnsureGuid(),
                Email = dr["Email"].EnsureString(),
                FirstName = dr["FirstName"].EnsureString(),
                LastName = dr["LastName"].EnsureString(),
                IsDisabled = dr["IsDisabled"].EnsureBool(),
                Phone = dr["Phone"].EnsureString(),
                AvailableMFrom = dr["AvailableMFrom"].EnsureInt(),
                AvailableMTo = dr["AvailableMTo"].EnsureInt(),
                AvailableTFrom = dr["AvailableTFrom"].EnsureInt(),
                AvailableTTo = dr["AvailableTTo"].EnsureInt(),
                AvailableWFrom = dr["AvailableWFrom"].EnsureInt(),
                AvailableWTo = dr["AvailableWTo"].EnsureInt(),
                AvailableThFrom = dr["AvailableThFrom"].EnsureInt(),
                AvailableThTo = dr["AvailableThTo"].EnsureInt(),
                AvailableFFrom = dr["AvailableFFrom"].EnsureInt(),
                AvailableFTo = dr["AvailableFTo"].EnsureInt(),
                AvailableSaFrom = dr["AvailableSaFrom"].EnsureInt(),
                AvailableSaTo = dr["AvailableSaTo"].EnsureInt(),
                AvailableSuFrom = dr["AvailableSuFrom"].EnsureInt(),
                AvailableSuTo = dr["AvailableSuTo"].EnsureInt(),
                Rank = dr["AvailableSuTo"].EnsureInt(),
                Rating= dr["Rating"].EnsureInt(),
                Wins= dr["Wins"].EnsureInt(),
                WinStreak= dr["WinStreak"].EnsureInt(),
                Position= dr["Position"].EnsureInt(),
                Skill= dr["Skill"].EnsureInt()
            };

            retVal.Role = dr.Table.Columns.Contains("RoleName") ? new Role()
            {
                RoleId = dr["RoleId"].EnsureGuid(),
                RoleName = dr["RoleName"].EnsureString(),
            } : null;

            return retVal;
        }

    }
}
