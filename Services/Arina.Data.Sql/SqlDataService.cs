using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Arina.Business;
using Arina.Business.Data;
using Arina.Business.Data.Sql;

namespace Arina.Data.Sql
{
    public static class SqlDataService
    {

        public static DataTable ExecuteQuery(String ProcedureName, List<CustomSqlParameter> Parameters)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand(ProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    foreach (SqlParameter item in Parameters)
                    {
                        cmd.Parameters.Add(item);
                    }
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    return ds.Tables[0];
                }
            }

        }

        public static int ExecuteNonQuery(String ProcedureName, List<CustomSqlParameter> Parameters)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand(ProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter item in Parameters)
                    {
                        cmd.Parameters.Add(item);
                    }

                    return cmd.ExecuteNonQuery();
                }
            }
        }

            

    }



}

