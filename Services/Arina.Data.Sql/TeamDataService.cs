using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Arina.Business;
using Arina.Business.Data;
using Arina.Business.Data.Sql;

namespace Arina.Data.Sql
{
    public class TeamDataService : ITeamDataService
    {
        public Guid Save(Team team)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Teams_Save", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("teamId", SqlDbType.UniqueIdentifier, team.TeamId == Guid.Empty ?team.TeamId= Guid.NewGuid() : team.TeamId));
                    cmd.Parameters.Add(new CustomSqlParameter("Available", SqlDbType.Bit, team.Available));
                    cmd.Parameters.Add(new CustomSqlParameter("Badge", SqlDbType.Int, team.Badge));
                    cmd.Parameters.Add(new CustomSqlParameter("Comments", SqlDbType.NVarChar, team.Comments));
                    //cmd.Parameters.Add(new CustomSqlParameter("CreatedOn", SqlDbType.DateTimeOffset, team.CreatedOn==null? DateTimeOffset.Now:team.CreatedOn));
                    cmd.Parameters.Add(new CustomSqlParameter("FavStadium", SqlDbType.UniqueIdentifier, team.FavStadium));
                    cmd.Parameters.Add(new CustomSqlParameter("HomeJersey", SqlDbType.VarChar, team.HomeJersey));
                    cmd.Parameters.Add(new CustomSqlParameter("AwayJersey", SqlDbType.VarChar, team.AwayJersey));
                    cmd.Parameters.Add(new CustomSqlParameter("NumberOfGames", SqlDbType.Int, team.NumberOfGames));
                    cmd.Parameters.Add(new CustomSqlParameter("Rank", SqlDbType.Decimal, team.Rank));
                    cmd.Parameters.Add(new CustomSqlParameter("Rating", SqlDbType.Decimal, team.Rating));
                    cmd.Parameters.Add(new CustomSqlParameter("ReviewRating", SqlDbType.Decimal, team.ReviewRating));
                    cmd.Parameters.Add(new CustomSqlParameter("Status", SqlDbType.Int, team.Status));
                    cmd.Parameters.Add(new CustomSqlParameter("Wins", SqlDbType.Int, team.Wins));
                    cmd.Parameters.Add(new CustomSqlParameter("WinStreak", SqlDbType.Int, team.WinStreak));
                    cmd.Parameters.Add(new CustomSqlParameter("YellowCard", SqlDbType.Int, team.YellowCard));
                    cmd.Parameters.Add(new CustomSqlParameter("TeamOfSize", SqlDbType.Int, team.TeamOfSize));
                    cmd.Parameters.Add(new CustomSqlParameter("TeamOfSize1", SqlDbType.Int, team.TeamOfSize1));
                    cmd.Parameters.Add(new CustomSqlParameter("TeamOfSize2", SqlDbType.Int, team.TeamOfSize2));
                    cmd.Parameters.Add(new CustomSqlParameter("AvailableMFrom", SqlDbType.Int, team.AvailableMFrom));
                    cmd.Parameters.Add(new CustomSqlParameter("AvailableMTo", SqlDbType.Int, team.AvailableMTo));
                    cmd.Parameters.Add(new CustomSqlParameter("AvailableTFrom", SqlDbType.Int, team.AvailableTFrom));
                    cmd.Parameters.Add(new CustomSqlParameter("AvailableTTo", SqlDbType.Int, team.AvailableTTo));
                    cmd.Parameters.Add(new CustomSqlParameter("AvailableWFrom", SqlDbType.Int, team.AvailableWFrom));
                    cmd.Parameters.Add(new CustomSqlParameter("AvailableWTo", SqlDbType.Int, team.AvailableWTo));
                    cmd.Parameters.Add(new CustomSqlParameter("AvailableThFrom", SqlDbType.Int, team.AvailableThFrom));
                    cmd.Parameters.Add(new CustomSqlParameter("AvailableThTo", SqlDbType.Int, team.AvailableThTo));
                    cmd.Parameters.Add(new CustomSqlParameter("AvailableFFrom", SqlDbType.Int, team.AvailableFFrom));
                    cmd.Parameters.Add(new CustomSqlParameter("AvailableFTo", SqlDbType.Int, team.AvailableFTo));
                    cmd.Parameters.Add(new CustomSqlParameter("AvailableSaFrom", SqlDbType.Int, team.AvailableSaFrom));
                    cmd.Parameters.Add(new CustomSqlParameter("AvailableSaTo", SqlDbType.Int, team.AvailableSaTo));
                    cmd.Parameters.Add(new CustomSqlParameter("AvailableSuFrom", SqlDbType.Int, team.AvailableSuFrom));
                    cmd.Parameters.Add(new CustomSqlParameter("AvailableSuTo", SqlDbType.Int, team.AvailableSuTo));
                    cmd.Parameters.Add(new CustomSqlParameter("Name", SqlDbType.NVarChar, team.Name));
                    cmd.Parameters.Add(new CustomSqlParameter("Captain", SqlDbType.UniqueIdentifier, team.Captain));



                    cmd.ExecuteNonQuery();

                    return team.TeamId;
                }
            }
        }

        public List<Team> GetTeamsByMemberId(Guid memberId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Teams_GetByMemberId", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId));
                    cmd.Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, memberId));

                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    return Load(ds.Tables[0]);
                }
            }
        }

        public List<Team> Get()
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Teams_GetAll", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId));

                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    return Load(ds.Tables[0]);
                }
            }
        }

        public List<Team> GetTeamByTeamId(Guid teamId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Teams_GetById", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("TeamId", SqlDbType.UniqueIdentifier, teamId));

                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    return Load(ds.Tables[0]);
                }
            }
        }

        private static List<Team> Load(DataTable dt)
        {
            List<Team> retVal = new List<Team>();
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                    retVal.Add(Load(dr));
            }
            return retVal;
        }

        private static Team Load(DataRow dr)
        {
            Team retVal = new Team()
            {
                TeamId = dr["TeamId"].EnsureGuid(),
                Name = dr["Name"].EnsureString(),
                Available = dr["Available"].EnsureInt(),
                Badge = dr["Badge"].EnsureInt(),
                Comments = dr["Comments"].EnsureString(),
                CreatedOn = dr["CreatedOn"].EnsureDate(),
                HomeJersey = dr["HomeJersey"].EnsureString(),
                AwayJersey = dr["AwayJersey"].EnsureString(),
                NumberOfGames = dr["NumberOfGames"].EnsureInt(),
                Rank = dr["Rank"].EnsureInt(),
                Rating = dr["Rating"].EnsureDecimal(),
                ReviewRating = dr["ReviewRating"].EnsureInt(),
                Status = dr["Status"].EnsureInt(),
                Wins = dr["Wins"].EnsureInt(),
                WinStreak = dr["WinStreak"].EnsureInt(),
                YellowCard = dr["YellowCard"].EnsureInt(),
                TeamOfSize = dr["TeamOfSize"].EnsureInt(),
                TeamOfSize1 = dr["TeamOfSize1"].EnsureInt(),
                TeamOfSize2 = dr["TeamOfSize2"].EnsureInt(),
                AvailableMFrom = dr["AvailableMFrom"].EnsureInt(),
                AvailableMTo = dr["AvailableMTo"].EnsureInt(),
                AvailableTFrom = dr["AvailableTFrom"].EnsureInt(),
                AvailableTTo = dr["AvailableTTo"].EnsureInt(),
                AvailableWFrom = dr["AvailableWFrom"].EnsureInt(),
                AvailableWTo = dr["AvailableWTo"].EnsureInt(),
                AvailableThFrom = dr["AvailableThFrom"].EnsureInt(),
                AvailableThTo = dr["AvailableThTo"].EnsureInt(),
                AvailableFFrom = dr["AvailableFFrom"].EnsureInt(),
                AvailableFTo = dr["AvailableFTo"].EnsureInt(),
                AvailableSaFrom = dr["AvailableSaFrom"].EnsureInt(),
                AvailableSaTo = dr["AvailableSaTo"].EnsureInt(),
                AvailableSuFrom = dr["AvailableSuFrom"].EnsureInt(),
                AvailableSuTo = dr["AvailableSuTo"].EnsureInt(),
                NumberOfPlayers = dr["NumberOfPlayers"].EnsureInt()
            };

            retVal.FavStadium = new Stadium();
            retVal.FavStadium.Name = dr["StadiumName"].EnsureString();
            retVal.FavStadium.StadiumId = dr["FavStadium"].EnsureGuid();

            return retVal;
        }

        
    }



}

