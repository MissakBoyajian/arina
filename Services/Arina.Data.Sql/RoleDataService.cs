using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Arina.Business;
using Arina.Business.Data;
using Arina.Business.Data.Sql;

namespace Arina.Data.Sql
{
	public class RoleDataService : IRoleDataService
	{
		public void Save(Role role)
		{
			using (CustomSqlConnection connection = new CustomSqlConnection())
			{
				using (CustomSqlCommand cmd = new CustomSqlCommand("Roles_Save", connection))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new CustomSqlParameter("RoleId", SqlDbType.UniqueIdentifier, role.RoleId));
					cmd.Parameters.Add(new CustomSqlParameter("RoleName", SqlDbType.NVarChar, role.RoleName));
					cmd.ExecuteNonQuery();
				}
			}
		}
		public List<Role> Find(string criteria)
		{
			using (CustomSqlConnection connection = new CustomSqlConnection())
			{
				using (CustomSqlCommand cmd = new CustomSqlCommand("Roles_Find", connection))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new CustomSqlParameter("criteria", SqlDbType.NVarChar,  "%" + criteria + "%"));
					CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
					DataTable dt = new DataTable();
					adapter.Fill(dt);
					return Load(dt);
				}
			}
		}
		public Role Load(Guid roleid)
		{
			using (CustomSqlConnection connection = new CustomSqlConnection())
			{
				using (CustomSqlCommand cmd = new CustomSqlCommand("Roles_SelectById", connection))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new CustomSqlParameter("RoleId", SqlDbType.UniqueIdentifier, roleid));
					CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
					DataSet ds = new DataSet();
					adapter.Fill(ds);
					List<Role> all = Load(ds.Tables[0]);
                    if (all.Count == 0)
                        throw new NotFoundException();
                    else
                        return all[0];
				}
			}
		}
		public int Delete(Guid roleid)
		{
			using (CustomSqlConnection connection = new CustomSqlConnection())
			{
				using (CustomSqlCommand cmd = new CustomSqlCommand("Roles_Delete", connection))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new CustomSqlParameter("RoleId", SqlDbType.UniqueIdentifier, roleid));
					return cmd.ExecuteNonQuery();
				}
			}
		}
		private static List<Role> Load(DataTable dt)
		{
			List<Role> retVal = new List<Role>();
			if (dt != null)
			{
				foreach (DataRow dr in dt.Rows)
					retVal.Add(Load(dr));
			}
			return retVal;
		}
		private static Role Load(DataRow dr)
		{
			Role retVal = new Role()
			{
				RoleId = dr["RoleId"].EnsureGuid(),
				RoleName = dr["RoleName"].EnsureString(),
			};
			return retVal;
		}
	}
}
