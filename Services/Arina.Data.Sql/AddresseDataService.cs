using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Arina.Business;
using Arina.Business.Data;
using Arina.Business.Data.Sql;

namespace Arina.Data.Sql
{
	public class AddresseDataService : IAddresseDataService
	{
		public void Save(Addresse addresse)
		{
			using (CustomSqlConnection connection = new CustomSqlConnection())
			{
				using (CustomSqlCommand cmd = new CustomSqlCommand("Addresses_Save", connection))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new CustomSqlParameter("AddressId", SqlDbType.UniqueIdentifier, addresse.AddressId.EnsureGuid() == Guid.Empty ? Guid.NewGuid() : addresse.AddressId));
					cmd.Parameters.Add(new CustomSqlParameter("ReferrerId", SqlDbType.UniqueIdentifier, addresse.ReferrerId));
					cmd.Parameters.Add(new CustomSqlParameter("Lat", SqlDbType.Float, addresse.Lat));
					cmd.Parameters.Add(new CustomSqlParameter("Lon", SqlDbType.Float, addresse.Lon));
					cmd.Parameters.Add(new CustomSqlParameter("AddressLine1", SqlDbType.NVarChar, addresse.AddressLine1));
					cmd.Parameters.Add(new CustomSqlParameter("AddressLine2", SqlDbType.NVarChar, addresse.AddressLine2));
					cmd.Parameters.Add(new CustomSqlParameter("Postcode", SqlDbType.NVarChar, addresse.Postcode));
					cmd.Parameters.Add(new CustomSqlParameter("SuburbId", SqlDbType.UniqueIdentifier, addresse.SuburbId));
					cmd.Parameters.Add(new CustomSqlParameter("StateId", SqlDbType.UniqueIdentifier, addresse.StateId));
					cmd.Parameters.Add(new CustomSqlParameter("IsPrimary", SqlDbType.Bit, addresse.IsPrimary));
					cmd.ExecuteNonQuery();
				}
			}
		}
		public List<Addresse> Find(string criteria)
		{
			using (CustomSqlConnection connection = new CustomSqlConnection())
			{
				using (CustomSqlCommand cmd = new CustomSqlCommand("Addresses_Find", connection))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new CustomSqlParameter("criteria", SqlDbType.NVarChar,  "%" + criteria + "%"));
					CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
					DataTable dt = new DataTable();
					adapter.Fill(dt);
					return Load(dt);
				}
			}
		}
		public Addresse Load(Guid addressid)
		{
			using (CustomSqlConnection connection = new CustomSqlConnection())
			{
				using (CustomSqlCommand cmd = new CustomSqlCommand("Addresses_SelectById", connection))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new CustomSqlParameter("AddressId", SqlDbType.UniqueIdentifier, addressid));
					CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
					DataSet ds = new DataSet();
					adapter.Fill(ds);
					List<Addresse> all = Load(ds.Tables[0]);
					if (all.Count == 0)
						throw new NotFoundException();
					else
						return all[0];
				}
			}
		}

        public Addresse LoadByReferrerId(Guid referrerId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Addresses_SelectByReferrerId", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("ReferrerId", SqlDbType.UniqueIdentifier, referrerId));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    List<Addresse> all = Load(ds.Tables[0]);
                    if (all.Count == 0)
                        return null;
                    else
                        return all[0];
                }
            }
        }

        public int Delete(Guid addressid)
		{
			using (CustomSqlConnection connection = new CustomSqlConnection())
			{
				using (CustomSqlCommand cmd = new CustomSqlCommand("Addresses_Delete", connection))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new CustomSqlParameter("AddressId", SqlDbType.UniqueIdentifier, addressid));
					return cmd.ExecuteNonQuery();
				}
			}
		}
        public void DeleteByReferrerId(Guid CustomerId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Addresses_DeleteByReferrerId", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("@CustomerId", SqlDbType.UniqueIdentifier, CustomerId));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        private static List<Addresse> Load(DataTable dt)
		{
			List<Addresse> retVal = new List<Addresse>();
			if (dt != null)
			{
				foreach (DataRow dr in dt.Rows)
					retVal.Add(Load(dr));
			}
			return retVal;
		}
		private static Addresse Load(DataRow dr)
		{
			Addresse retVal = new Addresse()
			{
				AddressId = dr["AddressId"].EnsureGuid(),
				ReferrerId = dr["ReferrerId"].EnsureGuid(),
				Lat = dr["Lat"].EnsureDouble(),
				Lon = dr["Lon"].EnsureDouble(),
				AddressLine1 = dr["AddressLine1"].EnsureString(),
				AddressLine2 = dr["AddressLine2"].EnsureString(),
				Postcode = dr["Postcode"].EnsureString(),
				SuburbId = dr["SuburbId"].EnsureGuid(),
				StateId = dr["StateId"].EnsureGuid(),
				IsPrimary = dr["IsPrimary"].EnsureBool(),
                //TimeStamp = BitConverter.ToInt64((byte[])dr["Version"], 0)

            };
			return retVal;
		}
	}
}
