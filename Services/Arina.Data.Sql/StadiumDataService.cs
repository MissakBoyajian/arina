﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arina.Business;
using Arina.Business.Data;
using Arina.Business.Data.Sql;

namespace Arina.Data.Sql
{
    class StadiumDataService : IStadiumDataService
    {
        public void Save(Stadium stadium)
        {
            throw new NotImplementedException();
        }

        public List<Stadium> Get()
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Stadiums_GetAll", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    return Load(ds.Tables[0]);
                }
            }
        }

        public List<Stadium> Get(int PageNumber)
        {
            List<CustomSqlParameter> Parameters = new List<CustomSqlParameter>();
            Parameters.Add(new CustomSqlParameter("PageNumber", SqlDbType.Int, PageNumber));



            DataTable Res = (SqlDataService.ExecuteQuery("Stadiums_GetAll", Parameters));
            PaginatedList<Stadium> Stadiums = new PaginatedList<Stadium>();

            Stadiums.AddRange(Load(Res));

            if (Stadiums.Count > 0)
            {
                Stadiums.CurrentPage = PageNumber;
                Stadiums.TotalPages = Res.Rows[0]["TotalPages"].EnsureInt();
            }
            return Stadiums;
        }

        public List<Stadium> GetByMemberId(Guid memberId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Stadiums_GetByMemberId", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("memberId", SqlDbType.UniqueIdentifier, memberId));

                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    return Load(ds.Tables[0]);
                }
            }
        }

        public List<Stadium> GetByMemberId()
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Stadiums_GetByMemberId", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("memberId", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId));

                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    return Load(ds.Tables[0]);
                }
            }
        }

        public List<Stadium> GetById(Guid stadiumId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Stadiums_GetById", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("StadiumId", SqlDbType.UniqueIdentifier, stadiumId));

                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    return Load(ds.Tables[0]);
                }
            }
        }

        private static List<Stadium> Load(DataTable dt)
        {
            List<Stadium> retVal = new List<Stadium>();
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                    retVal.Add(Load(dr));
            }
            return retVal;
        }

        private static Stadium Load(DataRow dr)
        {
            Stadium retVal = new Stadium();

            retVal.StadiumId = dr["StadiumId"].EnsureGuid();
            retVal.Name = dr["Name"].EnsureString();
            retVal.MemberId = dr["MemberId"].EnsureGuid();
            retVal.OpeningHourTime = dr["OpeningHourTime"].EnsureInt();
            retVal.OpeningHourMinutes = dr["OpeningHourMinutes"].EnsureInt();
            retVal.ClosingHourTime = dr["ClosingHourTime"].EnsureInt();
            retVal.ClosingHourMinutes = dr["ClosingHourMinutes"].EnsureInt();
            retVal.RestrictedHourTime = dr["RestrictedHourTime"].EnsureInt();
            retVal.RestrictedHourMinutes = dr["RestrictedHourMinutes"].EnsureInt();
            retVal.Lat = dr["Lat"].EnsureDecimal();
            retVal.Lon = dr["Lon"].EnsureDecimal();
            retVal.CancellationPolicy = dr["CancellationPolicy"].EnsureString();
            retVal.Area = dr["Area"].EnsureString();
            retVal.City = dr["City"].EnsureString();
            retVal.Email = dr["Email"].EnsureString();
            retVal.Rating = dr["Rating"].EnsureDecimal();
            retVal.Telephone = dr["Telephone"].EnsureString();
            retVal.HasWater = dr["HasWater"].EnsureInt();
            retVal.SpecialFeatures = dr["SpecialFeatures"].EnsureString();

            return retVal;
        }
    }
}
