using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Arina.Business;
using Arina.Business.Data;
using Arina.Business.Data.Sql;

namespace Arina.Data.Sql
{
    public class MiniStadiumDataService : IMiniStadiumDataService
    {
        //needs to be implemented
        public void Save(MiniStadium stadium)
        {

            List<CustomSqlParameter> Parameters = new List<CustomSqlParameter>();
            Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId));

            SqlDataService.ExecuteNonQuery("MiniStadiums_Save", Parameters);
        }

        public List<MiniStadium> GetByMemberId()
        {
            List<CustomSqlParameter> Parameters = new List<CustomSqlParameter>();
            Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId));

            return Load(SqlDataService.ExecuteQuery("MiniStadiums_GetByMemberId", Parameters));
        }

        public List<MiniStadium> GetByMemberId(Guid memberId)
        {

            List<CustomSqlParameter> Parameters = new List<CustomSqlParameter>();
            Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, memberId));

            return Load(SqlDataService.ExecuteQuery("MiniStadiums_GetByMemberId", Parameters));

        }

        public List<MiniStadium> Get()
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("MiniStadiums_GetAll", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.Add(new CustomSqlParameter("MiniStadiumId", SqlDbType.UniqueIdentifier, miniStadiumId));

                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    return Load(ds.Tables[0]);
                }
            }
        }

        public PaginatedList<MiniStadium> GetFree(int PageNumber,
            int SortBy,
            int FloorType,
            int StadiumType,
            int DistanceFrom,
            int DistanceTo,
            int PriceFrom,
            int PriceTo,
            decimal Lat,
            decimal Lon,
            DateTimeOffset DateFrom,
            int NumPlayers)
        {
            List<CustomSqlParameter> Parameters = new List<CustomSqlParameter>();
            Parameters.Add(new CustomSqlParameter("PageNumber", SqlDbType.Int, PageNumber));
            Parameters.Add(new CustomSqlParameter("SortBy", SqlDbType.Int, SortBy));
            Parameters.Add(new CustomSqlParameter("FloorType", SqlDbType.Int, FloorType));
            Parameters.Add(new CustomSqlParameter("StadiumType", SqlDbType.Int, StadiumType));
            Parameters.Add(new CustomSqlParameter("DistanceFrom", SqlDbType.Int, DistanceFrom));
            Parameters.Add(new CustomSqlParameter("DistanceTo", SqlDbType.Int, DistanceTo));
            Parameters.Add(new CustomSqlParameter("PriceFrom", SqlDbType.Int, PriceFrom));
            Parameters.Add(new CustomSqlParameter("PriceTo", SqlDbType.Int, PriceTo));
            Parameters.Add(new CustomSqlParameter("MemberLat", SqlDbType.Decimal, Lat));
            Parameters.Add(new CustomSqlParameter("MemberLon", SqlDbType.Decimal, Lon));
            Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId));
            Parameters.Add(new CustomSqlParameter("NumPlayers", SqlDbType.Int, NumPlayers));


            Parameters.Add(new CustomSqlParameter("DatePrevFrom", SqlDbType.DateTimeOffset, DateFrom.AddMinutes(-90)));
            Parameters.Add(new CustomSqlParameter("DatePrevTo", SqlDbType.DateTimeOffset, DateFrom));

            Parameters.Add(new CustomSqlParameter("DateFrom", SqlDbType.DateTimeOffset, DateFrom));
            Parameters.Add(new CustomSqlParameter("DateTo", SqlDbType.DateTimeOffset, DateFrom.AddMinutes(90)));

            Parameters.Add(new CustomSqlParameter("DateNextFrom", SqlDbType.DateTimeOffset, DateFrom.AddMinutes(90)));
            Parameters.Add(new CustomSqlParameter("DateNextTo", SqlDbType.DateTimeOffset, DateFrom.AddMinutes(180)));

            DataTable Res = (SqlDataService.ExecuteQuery("MiniStadiums_GetFree", Parameters));
            PaginatedList<MiniStadium> FreeStadiums = new PaginatedList<MiniStadium>();

            FreeStadiums.AddRange(Load(Res));

            if (FreeStadiums.Count > 0)
            {
                FreeStadiums.CurrentPage = PageNumber;
                FreeStadiums.TotalPages = Res.Rows[0]["TotalPages"].EnsureInt();
            }
            return FreeStadiums;
        }

        public List<MiniStadium> GetById(Guid miniStadiumId)
        {

            List<CustomSqlParameter> Parameters = new List<CustomSqlParameter>();
            Parameters.Add(new CustomSqlParameter("MiniStadiumId", SqlDbType.UniqueIdentifier, miniStadiumId));

            return Load(SqlDataService.ExecuteQuery("MiniStadiums_GetById", Parameters));

        }

        public List<MiniStadium> GetByStadiumId(Guid stadiumId)
        {
            List<CustomSqlParameter> Parameters = new List<CustomSqlParameter>();
            Parameters.Add(new CustomSqlParameter("StadiumId", SqlDbType.UniqueIdentifier, stadiumId));

            return Load(SqlDataService.ExecuteQuery("MiniStadiums_GetByStadiumId", Parameters));

        }

        private static List<MiniStadium> Load(DataTable dt)
        {
            List<MiniStadium> retVal = new List<MiniStadium>();
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                    retVal.Add(Load(dr));
            }
            return retVal;
        }

        private static MiniStadium Load(DataRow dr)
        {
            MiniStadium retVal = new MiniStadium();

            retVal.MiniStadiumId = dr["MiniStadiumId"].EnsureGuid();
            retVal.StadiumId = dr["StadiumId"].EnsureGuid();
            //retVal.MemberId = dr["MemberId"].EnsureGuid();
            retVal.Name = dr["Name"].EnsureString();
            retVal.NumPlayers = dr["NumPlayers"].EnsureInt();
            retVal.NumPlayers1 = dr["NumPlayers1"].EnsureInt();
            retVal.Type = dr["Type"].EnsureEnum<StadiumType>();
            retVal.TypeFloor = dr["TypeFloor"].EnsureEnum<StadiumTypeFloor>();
            retVal.Rating = dr["Rating"].EnsureInt();
            retVal.Price = dr["Price"].EnsureInt();

            retVal.Stadium = new Stadium()
            {
                StadiumId = dr["StadiumId"].EnsureGuid(),
                Name = dr["StadiumName"].EnsureString(),
                City = dr["StadiumCity"].EnsureString()
            };

            return retVal;
        }


    }



}

