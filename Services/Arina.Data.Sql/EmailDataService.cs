﻿using System;
using System.Collections.Generic;
using Arina.Business;
using Arina.Business.Data;
using System.Data;
using System.Data.SqlClient;
using Arina.Business.Service;
using Arina.Business.Data.Sql;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;

namespace Arina.Data.Sql
{
    public class EmailDataService : IEmailDataService
    {
        public EmailInfo CreateEmail(EmailInfo email)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand((email.IsDailyDigest) ? "Email_Insert_DailyDigest" : "Email_Insert", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new CustomSqlParameter("EmailID", SqlDbType.UniqueIdentifier, email.ID));
                    cmd.Parameters.Add(new CustomSqlParameter("To", SqlDbType.NVarChar, string.Join(",", email.TO)));
                    cmd.Parameters.Add(new CustomSqlParameter("CC", SqlDbType.NVarChar, string.Join(",", email.CC)));
                    cmd.Parameters.Add(new CustomSqlParameter("BCC", SqlDbType.NVarChar, string.Join(",", email.BCC)));
                    cmd.Parameters.Add(new CustomSqlParameter("Subject", SqlDbType.NVarChar, email.Subject));
                    cmd.Parameters.Add(new CustomSqlParameter("Attatchemnt ", SqlDbType.VarBinary, email.Attatchemnt));
                    cmd.Parameters.Add(new CustomSqlParameter("Body", SqlDbType.NVarChar, email.Body));
                    cmd.Parameters.Add(new CustomSqlParameter("RetryNumber", SqlDbType.Int, 0));
                    cmd.Parameters.Add(new CustomSqlParameter("SentOn", SqlDbType.DateTime, DateTime.Now));
                    cmd.Parameters.Add(new CustomSqlParameter("Error", SqlDbType.NVarChar, "Pending"));
                    cmd.Parameters.Add(new CustomSqlParameter("IsDelivered", SqlDbType.Bit, false));
                    cmd.ExecuteNonQuery();
                    return email;
                }
            }
        }

        public async Task SendEmail1()
        {

            var apiKey = System.Configuration.ConfigurationSettings.AppSettings["SengGridKey"];
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("arina@goarina.com", "ARINA");
            var subject = "Cabaret gas?";
            var to = new EmailAddress("Arikoftikian@gmail.com", "Example User");
            var plainTextContent = "Shapad Cabaret gas?";
            var htmlContent = "<strong>Shapad Cabaret gas gam voch?</strong> _|_";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            //var response = await client.SendEmailAsync(msg);            
            var response = await client.SendEmailAsync(msg);

        }


        public List<EmailInfo> GetUnsentEmails()
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Email_Select_Unsent", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt, true);
                    return Load(dt);
                }
            }
        }

        public void SetEmailToSent(Guid mailID, bool isDelivere, string error)
        {

            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Email_SetToSent", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new CustomSqlParameter("EmailID", SqlDbType.UniqueIdentifier, mailID));
                    cmd.Parameters.Add(new CustomSqlParameter("Error", SqlDbType.NVarChar, error));
                    cmd.Parameters.Add(new CustomSqlParameter("IsDelivered", SqlDbType.Bit, isDelivere));
                    cmd.ExecuteNonQuery();
                }
            }

        }

        private static List<EmailInfo> Load(DataTable dt)
        {
            List<EmailInfo> retVal = new List<EmailInfo>();
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                    retVal.Add(Load(dr));
            }
            return retVal;
        }

        private static EmailInfo Load(DataRow dr)
        {

            EmailInfo retVal = new EmailInfo();

            retVal.ID = dr["EmailID"].EnsureGuid();
            retVal.TO = new List<string>(dr["TO"].ToString().Split(','));
            retVal.CC = new List<string>(dr["CC"].ToString().Split(','));
            retVal.BCC = new List<string>(dr["BCC"].ToString().Split(','));
            retVal.Subject = dr["Subject"].EnsureString();
            retVal.Body = dr["Body"].EnsureString();
            retVal.RetryNumber = dr["RetryNumber"].EnsureInt();
            retVal.SentOn = dr["SentOn"].EnsureDate();
            retVal.Error = dr["Error"].EnsureString();
            retVal.IsDelivered = dr["IsDelivered"].EnsureBool();
            retVal.Attatchemnt = dr["Attatchemnt"] == DBNull.Value ? null : (byte[])dr["Attatchemnt"];
            return retVal;
        }
    }
}
