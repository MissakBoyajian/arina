﻿using Arina.Business.Data;
using System;
using System.Collections.Generic;
using Arina.Business;
using System.Data.SqlClient;
using System.Data;
using Arina.Business.Data.Sql;

namespace Arina.Data.Sql
{
    public class ResourceDataService : IResroucesDataService
    {
        public int Delete(Guid ResourceId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Resources_Delete", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("ResourceId", SqlDbType.UniqueIdentifier, ResourceId));
                    return cmd.ExecuteNonQuery();
                }
            }
        }

        public List<Resource> Find(Guid DealerGroupId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Resources_Find", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("DealerGroupId", SqlDbType.UniqueIdentifier, DealerGroupId));
                    cmd.Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return Load(dt);
                }
            }
        }
        public List<Resource> Find(string criteria, Guid productId, bool includeEmptyFolders)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Resources_FindByProductId", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("criteria", SqlDbType.NVarChar, "%" + criteria + "%"));
                    cmd.Parameters.Add(new CustomSqlParameter("ProductId", SqlDbType.UniqueIdentifier, productId));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    List<Resource> retval = Load(dt);
                    if (includeEmptyFolders)
                    {
                        return retval;
                    }
                    else
                    {
                        bool stillItemsToRemove = true;
                        while (stillItemsToRemove)
                        {
                            stillItemsToRemove = false;
                            for (int i = 0; i < retval.Count; i++)
                            {
                                Resource current = retval[i];
                                if (current.IsDirectory)
                                {
                                    List<Resource> child = retval.FindAll(x => x.ParentResourceId == current.ResourceId);
                                    if (child.Count == 0)
                                    {
                                        retval.Remove(current);
                                        stillItemsToRemove = true;
                                        break;
                                    }
                                }
                            }

                        }
                        return retval;
                    }
                }
            }
        }

        public Resource Load(Guid ResourceId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Resources_SelectById", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("ResourceId", SqlDbType.UniqueIdentifier, ResourceId));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    List<Resource> all = Load(ds.Tables[0]);
                    if (all.Count == 0)
                        throw new NotFoundException();
                    else
                        return all[0];
                }
            }
        }

        public void Save(Resource resource, Guid? ProductId)
        {
            string cdnUrl = string.Empty;
            if (System.Configuration.ConfigurationManager.AppSettings["UseDocumentCdn"].EnsureBool() && resource.IsDirectory == false && resource.FileContent != null)
            {
                try
                {
                    cdnUrl = CdnHelper.UploadToCdn(resource.FileContent, resource.FileNameWithExtension);
                }
                catch (Exception ex)
                {

                    cdnUrl = string.Empty;
                }

            }
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Resources_Save", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("ResourceId", SqlDbType.UniqueIdentifier, resource.ResourceId));
                    cmd.Parameters.Add(new CustomSqlParameter("ProductId", SqlDbType.UniqueIdentifier, ProductId));
                    cmd.Parameters.Add(new CustomSqlParameter("ParentResourceId", SqlDbType.UniqueIdentifier, resource.ParentResourceId));
                    cmd.Parameters.Add(new CustomSqlParameter("IsOnCdn", SqlDbType.Bit, !string.IsNullOrEmpty(cdnUrl)));
                    cmd.Parameters.Add(new CustomSqlParameter("CdnUrl", SqlDbType.NVarChar, cdnUrl));
                    cmd.Parameters.Add(new CustomSqlParameter("HasThumbnail", SqlDbType.Bit, resource.HasThumbnail));
                    cmd.Parameters.Add(new CustomSqlParameter("FileName", SqlDbType.NVarChar, resource.FileName));
                    cmd.Parameters.Add(new CustomSqlParameter("FileNameWithExtension", SqlDbType.NVarChar, resource.FileNameWithExtension));
                    cmd.Parameters.Add(new CustomSqlParameter("FileType", SqlDbType.Int, resource.FileType));
                    cmd.Parameters.Add(new CustomSqlParameter("FileContent", SqlDbType.Image, resource.FileContent));
                    cmd.Parameters.Add(new CustomSqlParameter("CreatedOn", SqlDbType.DateTime, resource.CreatedOn));
                    cmd.Parameters.Add(new CustomSqlParameter("CreatedBy", SqlDbType.UniqueIdentifier, resource.CreatedBy));
                    cmd.Parameters.Add(new CustomSqlParameter("IsDirectory", SqlDbType.Bit, resource.IsDirectory));
                    cmd.Parameters.Add(new CustomSqlParameter("YoutubeId", SqlDbType.NVarChar, resource.YoutubeId));
                    cmd.Parameters.Add(new CustomSqlParameter("VimeoId", SqlDbType.NVarChar, resource.VimeoId));
                    cmd.Parameters.Add(new CustomSqlParameter("AllowSharing", SqlDbType.Bit, resource.AllowSharing));
                    cmd.Parameters.Add(new CustomSqlParameter("RestrictedView", SqlDbType.Bit, resource.RestrictedView));
                    cmd.Parameters.Add(new CustomSqlParameter("Size", SqlDbType.Int, resource.Size / 1000));

                    cmd.ExecuteNonQuery();
                }

                if (resource.RelatedFolders != null)
                {
                    using (CustomSqlCommand cmd = new CustomSqlCommand("Resources_SaveResourceFolders", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        foreach (var item in resource.RelatedFolders)
                        {
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add(new CustomSqlParameter("ResourceId", SqlDbType.UniqueIdentifier, resource.ResourceId));
                            cmd.Parameters.Add(new CustomSqlParameter("ParentId", SqlDbType.UniqueIdentifier, item));
                            cmd.ExecuteNonQuery();
                        }
                    }
                }

            }
        }

        private static List<Resource> Load(DataTable dt)
        {
            List<Resource> retVal = new List<Resource>();
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                    retVal.Add(Load(dr));
            }
            return retVal;
        }
        private static Resource Load(DataRow dr)
        {
            Resource retVal = new Resource();

            retVal.ResourceId = dr["ResourceId"].EnsureGuid();
            retVal.ParentResourceId = dr["ParentResourceId"].EnsureGuid();
            retVal.CdnUrl = dr["CdnUrl"].EnsureString();
            retVal.IsOnCdn = dr["IsOnCdn"].EnsureBool();
            retVal.HasThumbnail = dr["HasThumbnail"].EnsureBool();
            retVal.FileName = dr["FileName"].EnsureString();
            retVal.FileNameWithExtension = dr["FileNameWithExtension"].EnsureString();
            retVal.FileType = dr["FileType"].EnsureEnum<ResourceFileTypeEnum>();
            retVal.CreatedOn = dr["CreatedOn"].EnsureDate();
            retVal.CreatedBy = dr["CreatedBy"].EnsureGuid();
            retVal.Size = dr["Size"].EnsureInt();

            if (dr.Table.Columns.Contains("CreatedBy"))
            {
                retVal.CreatedByMember = new Member()
                {
                    MemberId = dr["CreatedByMemberId"].EnsureGuid(),
                    FirstName = dr["CreatedByFirstName"].EnsureString(),
                    LastName = dr["CreatedByLastName"].EnsureString(),
                    Email = dr["CreatedByEmail"].EnsureString(),
                    RoleId = dr["CreatedByRoleId"].EnsureGuid(),
                    Role = new Role()
                    {
                        RoleId = dr["CreatedByRoleId"].EnsureGuid(),
                        RoleName = dr["CreatedByRoleName"].EnsureString()
                    }
                };
            }

            retVal.IsDirectory = dr["IsDirectory"].EnsureBool();
            retVal.YoutubeId = dr["YoutubeId"].EnsureString();
            retVal.VimeoId = dr["VimeoId"].EnsureString();
            retVal.DealerSiteId = dr["DealerSiteId"].EnsureGuid();

            retVal.ShareNumber = dr["ShareNumber"].EnsureInt();
            retVal.ViewNumber = dr["ViewNumber"].EnsureInt();
            retVal.Viewed = dr.Table.Columns.Contains("Viewed") ? dr["Viewed"].EnsureInt() : -1;

            retVal.AllowSharing = dr["AllowSharing"].EnsureBool();
            retVal.RestrictedView = dr["RestrictedView"].EnsureBool();

            //retVal.TimeStamp = BitConverter.ToInt64((byte[])dr["Version"], 0);

            if (dr.Table.Columns.Contains("FileContent"))
                retVal.FileContent = dr["FileContent"] == DBNull.Value ? null : (byte[])dr["FileContent"];

            retVal.Media = dr.Table.Columns.Contains("Media") ? MediaManager.LoadFromXml(dr["Media"].EnsureString()) : new List<Media>();

            return retVal;
        }

        public List<Resource> LoadByProduct(Guid ProductId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("ResourcesByProductId_Load", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("ProductId", SqlDbType.UniqueIdentifier, ProductId));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return Load(dt);
                }
            }
        }

        public int IncrementViews(Guid promotionid)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Resources_IncrementViews", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("ResourceId", SqlDbType.UniqueIdentifier, promotionid));
                    cmd.Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId));

                    return cmd.ExecuteNonQuery();
                }
            }
        }

        public void IncrementShares(List<Member> contactPersons, Guid ResourceId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                CustomSqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                transaction = connection.BeginTransaction("IncrementResourceShares");

                command.Connection = connection;
                command.Transaction = transaction;

                try
                {
                    command.CommandText = "Resources_IncrementShares";
                    command.CommandType = CommandType.StoredProcedure;

                    foreach (var item in contactPersons)
                    {
                        if(item.MemberId != Guid.Empty)
                        {
                            command.Parameters.Clear();
                            command.Parameters.Add(new CustomSqlParameter("ContactPersonId", SqlDbType.UniqueIdentifier, item.MemberId));
                            command.Parameters.Add(new CustomSqlParameter("ResourceId", SqlDbType.UniqueIdentifier, ResourceId));
                            command.ExecuteNonQuery();
                        }
                    }

                    command.CommandText = "Resources_IncrementSharesByNumber";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Clear();
                    command.Parameters.Add(new CustomSqlParameter("ResourceId", SqlDbType.UniqueIdentifier, ResourceId));
                    command.Parameters.Add(new CustomSqlParameter("Number", SqlDbType.Int, contactPersons.Count));
                    command.ExecuteNonQuery();


                    transaction.Commit();
                }
                //cannot insert duplicate key salesdataid to be solved
                catch (Exception ex)
                {

                    // Attempt to roll back the transaction.
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        // This catch block will handle any errors that may have occurred
                        // on the server that would cause the rollback to fail, such as
                        // a closed connection.
                        Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType());
                        Console.WriteLine("  Message: {0}", ex2.Message);
                    }
                }


            }
        }

        public int DeleteResourceFolders(Guid ResourceId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Resources_DeleteResourceFolders", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("ResourceId", SqlDbType.UniqueIdentifier, ResourceId));

                    return cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
