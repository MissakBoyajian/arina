﻿using System;
using Arina.Business;
using Arina.Business.Data;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using Arina.Business.Data.Sql;

namespace Arina.Data.Sql
{
    class ConfigDataService : IConfigDataService
    {
        public Config Load()
        {

            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("configuration_GetServerData", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return Load(dt);
                } 
            }
        }

        public void Update(Config config)
        {
        }

        private static Config Load(DataTable dt)
        {
            DataRow dr = dt.Rows[0];
            Config retVal = new Config()
            {
                SMTPServer = dr["SMTPServer"].EnsureString(),
                SMTPPort = dr["SMTPPort"].EnsureInt(),
                SMTPSSL = dr["SMTPSSL"].EnsureBool(),
                SMTPRequirePassword = dr["SMTPRequirePassword"].EnsureBool(),
                SMTPFrom = dr["SMTPFrom"].EnsureString(),
                SMTPPassword = dr["SMTPPassword"].EnsureString(),
                SMTPReplyTo = dr["SMTPReplyTo"].EnsureString(),
                PushInterval = dr["PushInterval"].EnsureDouble()
            };
            return retVal;
        }
    }


    public class ConfigurationDataService : IConfigurationDataService
    {
        public void Save(Configuration configuration)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Configuration_Save", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("ConfigurationId", SqlDbType.UniqueIdentifier, configuration.ConfigurationId));
                    cmd.Parameters.Add(new CustomSqlParameter("Description", SqlDbType.NVarChar, configuration.Description));
                    cmd.Parameters.Add(new CustomSqlParameter("ExternalId", SqlDbType.Int, configuration.ExternalId));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public List<Configuration> Find(string criteria)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Configuration_Find", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("criteria", SqlDbType.NVarChar, "%" + criteria + "%"));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return Load(dt);
                }
            }
        }
        public Configuration Load(Guid configurationid)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Configuration_SelectById", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("ConfigurationId", SqlDbType.UniqueIdentifier, configurationid));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    List<Configuration> all = Load(ds.Tables[0]);
                    if (all.Count == 0)
                        throw new NotFoundException();
                    else
                        return all[0];
                }
            }
        }

        public Configuration Load(ConfigurationEnum externalId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Configuration_SelectByExternalId", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("ExternalId", SqlDbType.Int, externalId));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    List<Configuration> all = Load(ds.Tables[0]);
                    if (all.Count == 0)
                        throw new NotFoundException();
                    else
                        return all[0];
                }
            }
        }
        public void Delete(Guid configurationid)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Configuration_Delete", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("ConfigurationId", SqlDbType.UniqueIdentifier, configurationid));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        private static List<Configuration> Load(DataTable dt)
        {
            List<Configuration> retVal = new List<Configuration>();
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                    retVal.Add(Load(dr));
            }
            return retVal;
        }
        private static Configuration Load(DataRow dr)
        {
            Configuration retVal = new Configuration()
            {
                ConfigurationId = dr["ConfigurationId"].EnsureGuid(),
                Description = dr["Description"].EnsureString(),
                ExternalId = dr["ExternalId"].EnsureEnum<ConfigurationEnum>(),
            };
            return retVal;
        }
    }

}
