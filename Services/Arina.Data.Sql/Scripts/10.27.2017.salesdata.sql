USE [Triton]
GO
/****** Object:  Table [dbo].[SalesData]    Script Date: 10/27/2017 13:03:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalesData](
	[SalesDataId] [uniqueidentifier] NOT NULL,
	[AccountNumber] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
	[BrandName] [nvarchar](max) NULL,
	[Credit] [float] NULL,
	[Year] [int] NULL,
	[Month] [int] NULL,
	[Type] [int] NULL,
	[Day] [int] NULL,
	[TotalAmount] [float] NULL,
 CONSTRAINT [PK_SalesData] PRIMARY KEY CLUSTERED 
(
	[SalesDataId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9cdcf7e6-ec40-4fe4-aecc-0102a6fd0226', N'32', N'Victorian Diesel Services P/l', NULL, -19996.740234375, 2017, 8, 0, NULL, 274896)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'18e9897c-684e-4ae1-a43d-01ba0bf1dcf8', N'1', N'Osbourne Automotive', NULL, -18724.080078125, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'25c10ee9-4355-4199-89c6-023a89f34db2', N'21', N'Porter Plant', NULL, -6340.22998046875, 2017, 8, 0, NULL, 287400)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ae42e4c5-d1d2-420e-afc5-0290c2231e4e', N'21', N'Porter Plant', N'BrandB', -6340.22998046875, 2017, 8, 2, NULL, 286434)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'0309cdd7-961d-4965-9e24-03bd38063f3b', N'7', N'Duke''s Automotive Repairs', NULL, 0, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'71c58dd1-e8ab-4e83-9262-04a5722e33af', N'3', N'Rothenburger Automotive', N'BrandA', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7e3485d3-eb10-4311-84c7-05101c185897', N'10', N'Calder Pallets', N'BrandA', -541.02001953125, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'0d6a8659-3ca3-4c94-8968-05458c00ae07', N'15', N'Openica Logistics Pty Ltd', N'BrandA', -2541.679931640625, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'424974db-b866-4e60-820b-05e8ca968cd1', N'12', N'Agee Panels & Towing Pty Ltd', NULL, -1305.5799560546875, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'4577b433-e9f2-45f9-9129-067f0e609a6c', N'9', N'Veolia Environmental Service', N'BrandD', -29225.48046875, 2017, 10, 2, 1, 1530)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a7b1fb49-0511-43b4-af9e-071537874100', N'9', N'Veolia Environmental Service', NULL, -29225.48046875, 2017, 10, 0, 2, 239429)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'1da786f1-b692-49ee-9494-076f04135d59', N'18', N'Holden Leasing (tvpr Pty Ltd)', N'BrandC', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'd205c27d-31a1-4685-8a3d-077fdd6e5502', N'34', N'Western Truck Repairs', NULL, -181150.8125, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'87e7bf30-309d-4b37-b761-07aba7a6e882', N'16', N'South Eastern Truck Repairs', N'BrandA', -5416.7998046875, 2017, 10, 2, NULL, 3000)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9b888e50-fa96-436f-a173-07f7e12ee638', N'7', N'Combined Horse Transport', NULL, 0, 2017, 10, 0, NULL, 139336)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'52d7991f-1d3a-4ff9-a2e6-091be710657f', N'16', N'Gippsland Fleet Maint Morwell', N'BrandA', -1632.9599609375, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'1ad09624-5acf-41ce-b732-097a06299921', N'12', N'Agee Panels & Towing Pty Ltd', N'BrandC', -1305.5799560546875, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a59cd468-10c3-4324-83cb-0a5d8172918d', N'5', N'Phillip Island Automotive', N'BrandD', -1153.9599609375, 2017, 10, 2, 3, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'68dd2052-6fbf-4d08-8918-0a69f33555c1', N'37', N'Foster Tyres & Service Centre', N'BrandD', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7e89c4e3-f10f-4d67-8968-0b274f9b9d5e', N'14', N'Express Exports', N'BrandB', -3041.02001953125, 2017, 10, 2, 4, 145293)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'57fdc943-ceba-4042-99dd-0bdc6be272bc', N'15', N'Openica Logistics Pty Ltd', N'BrandD', -2541.679931640625, 2017, 9, 2, NULL, 10848)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'bc99d23d-8a09-44e4-9b9c-0e4d41711ffc', N'3', N'Rothenburger Automotive', N'BrandC', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2838064d-3d3f-4911-8631-0e55a9491a65', N'36', N'Seabrook Mechanical Services', NULL, 0, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e86e4099-b0f6-47d7-b255-0ea13895baf2', N'19', N'Apex Steel Pty Ltd', NULL, 0, 2017, 10, 1, 5, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'feb1f02c-1e62-485f-aaef-0f0ef3be16a8', N'15', N'Victorian Carriers Pty Ltd', N'BrandC', 0, 2017, 10, 2, 6, 7605)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'77b4d129-042c-40be-9016-0f346e8ed548', N'20', N'Pony Xpress', N'BrandD', -4818.580078125, 2017, 8, 2, NULL, 71096)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'961dbd2f-36a3-4711-9fa8-0f81e8302dbe', N'15', N'Victorian Carriers Pty Ltd', NULL, 0, 2017, 10, 0, 7, 10231)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c73618b3-c228-47d0-aff2-114bc65a50a6', N'12', N'Metropolitan Express', N'BrandD', 0, 2017, 10, 2, 8, 75)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'28be591b-13f0-4931-ab2f-11802d853b3c', N'12', N'Metropolitan Express', NULL, 0, 2017, 10, 0, 9, 47333)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7d8541a9-0c71-44d4-a61b-11c670014861', N'8', N'Bog Cog Off Road & Auto Sve', NULL, 0, 2017, 10, 1, 10, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'724ccc64-7f26-4fd2-bcb7-122e0d44a195', N'1', N'Osbourne Automotive', N'BrandA', -18724.080078125, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'02afb968-0d6c-48e7-954d-12ac2fe907a6', N'6', N'L P F Automotive', N'BrandD', -1618.199951171875, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7d8a2e42-2574-4264-a208-13277fe1dc70', N'18', N'Holden Leasing (tvpr Pty Ltd)', N'BrandA', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c8f741cd-d111-4c24-89a0-140835c2fd94', N'21', N'Porter Plant', N'BrandC', -6340.22998046875, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3aefc9fc-7f13-4ffc-9cd3-14223655840e', N'11', N'Suburban Mechanical', N'BrandC', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'f0cb2166-7d3f-4659-bf92-1492f986d54e', N'31', N'Cleanaway Operations T/as', N'BrandD', 0, 2017, 8, 2, NULL, 34758)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'd067bb8a-3aef-42fd-97d3-17b720718faa', N'14', N'Livesey''s Motor Works', NULL, 0, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e89a39b4-8501-4d1f-9dee-18145534d850', N'29', N'T Hutchinson Transport', N'BrandC', -3344.639892578125, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'1667c398-b608-4757-a17f-181fdd26028a', N'8', N'Bog Cog Off Road & Auto Sve', NULL, 0, 2017, 10, 0, 12, 746)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9963600a-8515-41ad-b79d-19963244dd8a', N'1', N'Osbourne Automotive', NULL, -18724.080078125, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'f6d298a6-034b-4a4a-8d18-1a09c1c7edfd', N'5', N'Kirchner Trucks', N'BrandC', -1649, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9bebd19f-6ac9-4c0c-868e-1a8904acf886', N'3', N'Dandy Truck Sales', N'BrandC', 0, 2017, 10, 2, 13, 2236)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7a35a617-75d5-41c9-a2b8-1b2bd2441dca', N'7', N'Combined Horse Transport', NULL, 0, 2017, 10, 1, 14, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'5f6f7e47-a140-4d63-b132-1b6bd63c6f2e', N'10', N'Calder Pallets', N'BrandC', -541.02001953125, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'8f808c7f-346f-4124-a16f-1b865c7cba57', N'13', N'Langwarrin Service Centre', NULL, 0, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2773c210-cc55-41df-b630-1c5a95b388f1', N'6', N'L P F Automotive', N'BrandB', -1618.199951171875, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'f918cdf2-4255-4e46-88b5-1c933fc590ea', N'30', N'The Haire Truck & Bus Repairs', NULL, -254.55000305175781, 2017, 8, 0, NULL, 21293)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'1e541aad-b7df-4b5e-a5cd-1ca0ac40b85c', N'30', N'The Haire Truck & Bus Repairs', N'BrandA', -254.55000305175781, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'87f75e9c-5c08-4d89-824c-1da00c040102', N'11', N'Suburban Mechanical', NULL, 0, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'54e1077d-08a1-4a17-8712-1f215a3a3689', N'30', N'The Haire Truck & Bus Repairs', NULL, -254.55000305175781, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'72d0e650-9426-4911-81b3-20170315dca5', N'11', N'Suburban Mechanical', N'BrandD', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9d1d2e26-0d9c-41a8-bd76-20e6eb735993', N'13', N'Langwarrin Service Centre', N'BrandC', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'4db7f462-fcec-4cab-a4d5-212b8d77bd9b', N'5', N'Kirchner Trucks', NULL, -1649, 2017, 9, 0, NULL, 1359)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'25ac5cc7-715c-47b6-8b2c-21f73f02d928', N'1', N'Dyers Transport Pty Ltd', NULL, -13608, 2017, 10, 0, 15, 171993)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'8cc3433a-8a0d-468b-b2d4-21fa8833b3cf', N'16', N'Gippsland Fleet Maint Morwell', NULL, -1632.9599609375, 2017, 9, 0, NULL, 169137)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'05216e32-042d-491d-9dbc-2228b60eebf4', N'18', N'Holden Leasing (tvpr Pty Ltd)', NULL, 0, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'602a80a7-6ba6-4544-b533-23ed27dbbec6', N'2', N'Monash University', N'BrandA', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'f265297c-ef9b-4394-90f1-24067564ff43', N'10', N'Calder Pallets', N'BrandD', -541.02001953125, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9f6c0ae4-44e6-4d67-9dc0-247472e00731', N'27', N'Stenaust P/l', NULL, 0, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9847b27e-6038-4542-a56e-24923dcb66cf', N'25', N'Shepparton Transit', N'BrandC', 0, 2017, 8, 2, NULL, 25348)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'42ce98dd-f565-47be-b53b-263397f2ac42', N'17', N'Benalla Auto Industries', NULL, 0, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'93fba165-817b-407d-ba4c-26adc89d90f5', N'3', N'Rothenburger Automotive', NULL, 0, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'881c8fcb-1209-4dbb-83c0-26b40423e8fa', N'26', N'Spotless (Berkley Challenge)', N'BrandD', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'864853a0-2311-4a0a-8214-27ad22c05119', N'13', N'Langwarrin Service Centre', N'BrandD', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e6216206-8655-479a-87c6-280edb39e6bb', N'31', N'Cleanaway Operations T/as', NULL, 0, 2017, 8, 0, NULL, 52692)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'cd373449-bd53-4683-825e-28fe0ecfb663', N'14', N'Express Exports', N'BrandA', -3041.02001953125, 2017, 10, 2, 16, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9717dd21-b31d-490e-a3bd-2a61646240bb', N'35', N'Coburg Truck Parts VIC P/l', N'BrandA', -21741.099609375, 2017, 8, 2, NULL, 27)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'20bc35ec-8aad-457d-b575-2ac785938239', N'14', N'Livesey''s Motor Works', N'BrandD', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'5e1ba2e2-ce19-4544-ba11-2b4173a96dfe', N'17', N'Benalla Auto Industries', N'BrandD', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'84218794-ff59-4819-aea1-2b957c371d34', N'11', N'Suburban Mechanical', N'BrandB', 0, 2017, 9, 2, NULL, 16796)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'71243f83-a73f-42aa-8db4-2bd09bf608b8', N'4', N'Edgar Motors', NULL, 0, 2017, 10, 1, 17, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'bd84b6a2-084b-4e65-a321-2cdff6ac21f0', N'6', N'L P F Automotive', N'BrandD', -1618.199951171875, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'dfec5b3b-1bb3-4b06-875a-2ce6437fa927', N'7', N'Duke''s Automotive Repairs', N'BrandC', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'bfb5f7fe-06d4-4340-9020-2ee8f126105b', N'8', N'Bp Clarke Road S/stn', NULL, -412.32000732421875, 2017, 7, 0, NULL, 2611)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2c72f640-6daf-49ed-9718-2f07d6d15db2', N'17', N'Benalla Auto Industries', NULL, 0, 2017, 7, 0, NULL, 9254)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'22f45e4f-ea34-4bd2-a406-2feec1716801', N'11', N'Borg Fleet Management Pty Ltd', N'BrandB', 0, 2017, 10, 2, 18, 4979)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ea94363c-9902-4221-a932-303cfb392737', N'9', N'Direct Freight', N'BrandD', -42047.44921875, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'748e8f3d-b663-4e32-9861-30671bfe6838', N'17', N'Delux Rodz Pty Ltd', N'BrandC', 0, 2017, 10, 2, 19, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a87c9905-5978-4464-b8e6-3072feb2632e', N'22', N'Qantas Airways Ltd', N'BrandD', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'6d1aca62-300e-44ba-9b1f-30aeb93959de', N'7', N'Combined Horse Transport', N'BrandC', 0, 2017, 10, 2, 20, 139203)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'57727a6f-7bbd-4efb-bbf0-321bd5c4b215', N'5', N'Kirchner Trucks', NULL, -1649, 2017, 7, 0, NULL, 1359)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c900b98b-6078-45e5-a7a3-32b435c6daa4', N'22', N'Qantas Airways Ltd', NULL, 0, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'33589180-60ef-43f3-b1e0-32e1dfda110a', N'14', N'Livesey''s Motor Works', N'BrandB', 0, 2017, 7, 2, NULL, 626)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a235283c-52b4-4cc0-a0e3-33af2e9acaff', N'16', N'South Eastern Truck Repairs', NULL, -5416.7998046875, 2017, 10, 1, 21, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'1a01fc0d-ba2f-41db-a15b-34922bdab22d', N'7', N'Duke''s Automotive Repairs', NULL, 0, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'80ce711f-84b7-4680-9174-34b23c2822d6', N'32', N'Victorian Diesel Services P/l', N'BrandB', -19996.740234375, 2017, 8, 2, NULL, 28129)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7a7dd7ef-f744-4c63-acae-3529105c81a1', N'8', N'Bp Clarke Road S/stn', N'BrandB', -412.32000732421875, 2017, 7, 2, NULL, 2611)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'53ec78e9-2db2-4178-afea-3548b306b108', N'5', N'Kirchner Trucks', NULL, -1649, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'10272887-b1f8-44b0-9bde-368a32957751', N'4', N'Edgar Motors', N'BrandC', 0, 2017, 10, 2, 21, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7a23733b-564d-4649-86a7-36b07dfcd7a2', N'27', N'Stenaust P/l', N'BrandC', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3ecabb14-2afc-429e-9334-37a7b6bdb402', N'26', N'Spotless (Berkley Challenge)', N'BrandA', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'4ecc5b3b-ae42-4665-b957-37adfcf8f620', N'29', N'T Hutchinson Transport', N'BrandB', -3344.639892578125, 2017, 8, 2, NULL, 3693)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'459b83dc-7bb7-4036-8432-38485e51d18f', N'4', N'Edgar Motors', N'BrandA', 0, 2017, 10, 2, 22, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ef2138dc-e94a-4999-8ebd-38bc4e8db520', N'17', N'Benalla Auto Industries', N'BrandA', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'0b2f8811-c513-4025-9cec-39822053780a', N'9', N'Veolia Environmental Service', N'BrandA', -29225.48046875, 2017, 10, 2, 23, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'aeb9c518-fdb0-40f4-84b3-3acb3c4d1bdd', N'3', N'Dandy Truck Sales', N'BrandB', 0, 2017, 10, 2, 24, 4924)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7c557236-3957-44eb-81f3-3c15abff4847', N'17', N'Benalla Auto Industries', NULL, 0, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'dcb1abcd-3e0a-476b-a4ff-3ce431172140', N'8', N'Bp Clarke Road S/stn', N'BrandD', -412.32000732421875, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'61101183-2b1e-4e1a-86de-3d72d515560f', N'29', N'T Hutchinson Transport', NULL, -3344.639892578125, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e9634ee5-0044-42fc-9896-3de8cff54ca3', N'26', N'Spotless (Berkley Challenge)', N'BrandC', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e265ff9f-455f-4fd7-bbd7-40d9b1d0340b', N'33', N'G & M Waldie P/l', N'BrandB', -1859.5799560546875, 2017, 8, 2, NULL, 68271)
GO
print 'Processed 100 total records'
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'22e3281c-5386-45dd-b8ff-422f3c3b4a16', N'35', N'Coburg Truck Parts VIC P/l', N'BrandB', -21741.099609375, 2017, 8, 2, NULL, 126815)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'8c1a0085-6ec5-4e66-a970-43ffc2f49a5a', N'10', N'Calder Pallets', NULL, -541.02001953125, 2017, 7, 0, NULL, 1262)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'25de469e-57ab-468b-9fd1-44f44c1e1cdf', N'19', N'Kenworth Melbourne, Daf Melbourne', N'BrandD', -274.60000610351562, 2017, 8, 2, NULL, 5951)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c002c781-a70a-4ecd-8dfc-454075befc29', N'5', N'Kirchner Trucks', N'BrandB', -1649, 2017, 9, 2, NULL, 881)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'94499b97-bf9a-4d93-a348-4643ac14b9a3', N'1', N'Osbourne Automotive', N'BrandB', -18724.080078125, 2017, 9, 2, NULL, 162482)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3de0827e-27bb-47ac-8a74-465d74729256', N'7', N'Duke''s Automotive Repairs', NULL, 0, 2017, 9, 0, NULL, 8993)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e3eaf13c-29fb-4e9e-998b-466284bd96ed', N'31', N'Cleanaway Operations T/as', NULL, 0, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2572beb0-333e-492b-a010-46a7651e807e', N'31', N'Cleanaway Operations T/as', N'BrandB', 0, 2017, 8, 2, NULL, 17934)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'8994e4b7-4712-46e8-912e-47457d0acb8e', N'10', N'Calder Pallets', N'BrandD', -541.02001953125, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'194e3711-b04a-4bf1-ab0c-48f15b96664d', N'12', N'Metropolitan Express', N'BrandC', 0, 2017, 10, 2, 25, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'04ec1be8-74c5-4e19-a7a3-4a40f22587ff', N'7', N'Duke''s Automotive Repairs', N'BrandD', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e3bdcea7-0fe1-4572-9738-4b4adbcca881', N'6', N'L P F Automotive', N'BrandA', -1618.199951171875, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'189ff82b-2bb7-4843-8991-4bccc3ac3c8c', N'18', N'Holden Leasing (tvpr Pty Ltd)', NULL, 0, 2017, 7, 0, NULL, 858)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'724a4336-d97c-44ed-b124-4bd06982b8c7', N'16', N'South Eastern Truck Repairs', N'BrandD', -5416.7998046875, 2017, 10, 2, NULL, 65347)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'8f37bdbd-5c09-41d0-8404-4be5fcd893fc', N'12', N'Agee Panels & Towing Pty Ltd', N'BrandA', -1305.5799560546875, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'591dd2a8-b270-45db-9cf8-4d336bf2612d', N'37', N'Foster Tyres & Service Centre', N'BrandA', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'0db4d942-184e-4acd-98ac-4ea7041b2534', N'9', N'Veolia Environmental Service', N'BrandB', -29225.48046875, 2017, 10, 2, 25, 237899)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a43bef26-7b3e-4528-a631-4ec4b94d1c3c', N'2', N'Mayos Body Shop', NULL, 0, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a35ad01b-5368-45d5-a6af-4ed9cf2ef021', N'19', N'Apex Steel Pty Ltd', N'BrandA', 0, 2017, 10, 2, 25, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9e6b1fc3-bc51-49d1-98cb-505204e390b2', N'12', N'Agee Panels & Towing Pty Ltd', NULL, -1305.5799560546875, 2017, 7, 0, NULL, 111100)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'b176cf62-99d9-4c9a-8b1b-524756d68df7', N'19', N'Kenworth Melbourne, Daf Melbourne', N'BrandB', -274.60000610351562, 2017, 8, 2, NULL, 31256)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'48eb83c4-5d1a-4c6a-8a5d-5276b4d36f1f', N'16', N'Gippsland Fleet Maint Morwell', N'BrandC', -1632.9599609375, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'279fe38e-7846-408e-a478-532c99e8a0c9', N'34', N'Western Truck Repairs', NULL, -181150.8125, 2017, 8, 0, NULL, 821213)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ee29ec6b-55a4-4414-ac43-53e2a1bfdbe1', N'15', N'Openica Logistics Pty Ltd', NULL, -2541.679931640625, 2017, 7, 0, NULL, 293285)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'49bd8c07-368b-44de-a0d2-540a6f76cc59', N'9', N'Direct Freight', NULL, -42047.44921875, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'283ec66c-39ed-46fc-9c8f-545ebafdd749', N'6', N'L P F Automotive', N'BrandB', -1618.199951171875, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7ce573c6-66fb-487f-afdb-547648047643', N'21', N'Porter Plant', N'BrandA', -6340.22998046875, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'd8f8ef76-be78-4b84-aac5-548db5b1fc58', N'23', N'Roadmech P/l', N'BrandA', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'90305778-a34d-4338-a4fd-54fe462a9f21', N'1', N'Dyers Transport Pty Ltd', NULL, -13608, 2017, 10, 1, 25, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2d0b90e7-32e2-4ea3-8e2e-5505c03f6971', N'18', N'Holden Leasing (tvpr Pty Ltd)', N'BrandD', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7f9f9a5f-07b7-497b-ae80-57a17b0e543b', N'2', N'Mayos Body Shop', NULL, 0, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c9644952-5d46-4ba0-80bd-589005533c78', N'11', N'Borg Fleet Management Pty Ltd', NULL, 0, 2017, 10, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a704f559-13f3-4f1a-8a7f-591cc5684338', N'23', N'Roadmech P/l', N'BrandB', 0, 2017, 8, 2, NULL, 4338)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'48b9c24a-8f7d-4e3c-9e75-599f016e2c0b', N'4', N'Edgar Motors', N'BrandB', 0, 2017, 10, 2, 25, 801)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ad9b8556-bbcf-4973-a3e5-59bcbb47fdaf', N'4', N'M & D O''Hehir Mechanical', N'BrandC', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'08ccfdc4-a351-4a4c-8ef1-5a873f2b976b', N'8', N'Bog Cog Off Road & Auto Sve', N'BrandB', 0, 2017, 10, 2, 1, 746)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9960fea3-e437-4d8b-9d59-5acd3a404e89', N'23', N'Roadmech P/l', N'BrandC', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'0524390b-ac55-4fee-b405-5ad01d4f680a', N'8', N'Bog Cog Off Road & Auto Sve', N'BrandC', 0, 2017, 10, 2, 2, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'b4d2f431-1d06-457a-b515-5b2551237fce', N'24', N'Seymour & District Diesel Rep', N'BrandB', 0, 2017, 8, 2, NULL, 1947)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'b7bb61e9-4643-4de3-8797-5b9842dad7e5', N'2', N'Mayos Body Shop', N'BrandB', 0, 2017, 7, 2, NULL, 927)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'0b343ef3-c980-4f29-b43d-5d51764e5939', N'33', N'G & M Waldie P/l', N'BrandD', -1859.5799560546875, 2017, 8, 2, NULL, 25904)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'19fab305-7323-4c51-8ab3-5d5d005b8ef7', N'2', N'Mayos Body Shop', N'BrandD', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'b9416689-690b-497e-8bbd-5d827703743c', N'19', N'Kenworth Melbourne, Daf Melbourne', N'BrandA', -274.60000610351562, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'6805f4f0-73f1-4c64-8815-5d9b66e7bad2', N'12', N'Agee Panels & Towing Pty Ltd', N'BrandB', -1305.5799560546875, 2017, 9, 2, NULL, 111100)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7066a2f2-917c-459f-8b62-5ea73c90b1c1', N'3', N'Rothenburger Automotive', N'BrandC', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'16294709-3796-4f76-b605-5f3a9d482ea1', N'10', N'Atoc Auto Repairs', N'BrandD', 0, 2017, 10, 2, 3, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'f32fdd0d-37e9-4c7e-991f-5f73bb0752f6', N'2', N'Mayos Body Shop', NULL, 0, 2017, 9, 0, NULL, 927)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'17e3489e-e3bb-49d0-84c6-60d92c152293', N'12', N'Agee Panels & Towing Pty Ltd', N'BrandD', -1305.5799560546875, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9dcd551c-02a4-4ffd-b2c4-6173079f4fa1', N'8', N'Bp Clarke Road S/stn', NULL, -412.32000732421875, 2017, 9, 0, NULL, 2611)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3ee74e5e-1a38-40ba-bb3b-61f10055bcbe', N'10', N'Calder Pallets', N'BrandC', -541.02001953125, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'f7c948bd-ba7d-4ea0-8255-620e4260f2fe', N'13', N'Langwarrin Service Centre', N'BrandA', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3a9d429e-b296-4acd-8d53-6325303cd40b', N'7', N'Duke''s Automotive Repairs', N'BrandB', 0, 2017, 9, 2, NULL, 8993)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'1fb6baa1-c2f2-4dfc-8299-6399f450b384', N'33', N'G & M Waldie P/l', NULL, -1859.5799560546875, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7d54ca1d-5dbf-4288-81cf-63ea745fa356', N'8', N'Bp Clarke Road S/stn', NULL, -412.32000732421875, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'730f1629-779a-46f1-b41d-64028010ddaf', N'5', N'Kirchner Trucks', N'BrandA', -1649, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a16fb5db-da5f-4b3e-abdd-64a71e031571', N'31', N'Cleanaway Operations T/as', N'BrandC', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ec368f10-e29f-4843-9743-64f3f217ee46', N'29', N'T Hutchinson Transport', N'BrandA', -3344.639892578125, 2017, 8, 2, NULL, 7556)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'6bf40772-36ad-4c69-85f4-653db074b866', N'10', N'Atoc Auto Repairs', NULL, 0, 2017, 10, 0, 4, 3773)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'36ec74a6-5468-45f5-b962-654fca40c484', N'8', N'Bp Clarke Road S/stn', N'BrandD', -412.32000732421875, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'fbf79baf-df3b-4390-9597-65b9cfe42277', N'19', N'Kenworth Melbourne, Daf Melbourne', NULL, -274.60000610351562, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7e464580-b61e-4aab-9fc9-66782ee796d1', N'9', N'Veolia Environmental Service', N'BrandC', -29225.48046875, 2017, 10, 2, 5, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'62296587-0606-4cf5-9819-6687856f06eb', N'30', N'The Haire Truck & Bus Repairs', N'BrandD', -254.55000305175781, 2017, 8, 2, NULL, 4781)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2809ab48-7f50-4f01-b6a1-6687a97266e5', N'12', N'Agee Panels & Towing Pty Ltd', NULL, -1305.5799560546875, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9d2035db-222b-473f-9663-678d35af3a4a', N'5', N'Kirchner Trucks', NULL, -1649, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3469cd45-0153-4b3e-8f58-69fa8874b4b5', N'3', N'Dandy Truck Sales', NULL, 0, 2017, 10, 0, 6, 18615)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'db397520-2f97-489d-9d91-6a10979fac3e', N'13', N'Suttons Motors Arncliffe', N'BrandC', 0, 2017, 10, 2, 7, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'04eeef55-6a1e-4995-96b1-6a96fc75a35a', N'9', N'Direct Freight', N'BrandA', -42047.44921875, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'8a501732-d10e-47da-b53e-6bd6b7809e3d', N'11', N'Borg Fleet Management Pty Ltd', N'BrandC', 0, 2017, 10, 2, 8, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'cb3017f4-f200-4e98-bc55-6bf6d3e9bb91', N'29', N'T Hutchinson Transport', NULL, -3344.639892578125, 2017, 8, 0, NULL, 16979)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'bb665080-d133-41c7-a8a2-6c9b089ae1f4', N'2', N'Monash University', NULL, 0, 2017, 10, 0, 9, 159)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3fbe163e-22e8-4dc9-86dd-6d613d948e6f', N'20', N'Pony Xpress', NULL, -4818.580078125, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ca04f3cb-529c-4ca8-afc9-6d69ae9d5022', N'1', N'Osbourne Automotive', N'BrandC', -18724.080078125, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ff3b8acd-341f-4f54-823d-6d79feaf4253', N'35', N'Coburg Truck Parts VIC P/l', N'BrandC', -21741.099609375, 2017, 8, 2, NULL, 780)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'8fc71261-e012-4ce8-86a1-6ed4364f69b3', N'16', N'Gippsland Fleet Maint Morwell', N'BrandD', -1632.9599609375, 2017, 7, 2, NULL, 2268)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'b5edea4a-8715-4e37-a53c-6ef0b9a0f200', N'6', N'L P F Automotive', N'BrandC', -1618.199951171875, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'0f943779-6afb-4785-ad22-6f3ba63d7170', N'19', N'Apex Steel Pty Ltd', N'BrandB', 0, 2017, 10, 2, 12, 122)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'607839d2-e68c-4e8f-a374-6f4bf6da05f2', N'15', N'Openica Logistics Pty Ltd', N'BrandC', -2541.679931640625, 2017, 7, 2, NULL, 277197)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'f3c2f3f2-9cbd-4de7-8650-6f598e2b1295', N'36', N'Seabrook Mechanical Services', N'BrandA', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'de2d289b-c055-4a6f-a964-6fcd28183ff6', N'11', N'Borg Fleet Management Pty Ltd', N'BrandD', 0, 2017, 10, 2, 13, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3b1f4257-df66-4e69-b64f-6fe410ddf971', N'18', N'Holden Leasing (tvpr Pty Ltd)', NULL, 0, 2017, 9, 0, NULL, 858)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'301450dd-0b4d-4663-b6ae-70da87bc3989', N'27', N'Stenaust P/l', N'BrandD', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'4a45ea73-ee20-4643-a655-717deae1591e', N'30', N'The Haire Truck & Bus Repairs', N'BrandC', -254.55000305175781, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'6a462089-7966-4a0e-8dae-72e862c1bdbe', N'11', N'Suburban Mechanical', NULL, 0, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'74698931-5e1b-4bb1-b9e7-732dc8678f42', N'19', N'Apex Steel Pty Ltd', N'BrandC', 0, 2017, 10, 2, 14, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'de375587-85ac-49e8-8c87-7470f9d3e726', N'5', N'Phillip Island Automotive', N'BrandC', -1153.9599609375, 2017, 10, 2, 15, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2653b152-9b0a-422f-a248-74daebe4399b', N'33', N'G & M Waldie P/l', N'BrandC', -1859.5799560546875, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e6d22df3-affd-45f0-a1d3-75097cfebd2d', N'14', N'Livesey''s Motor Works', N'BrandD', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'8268acd0-9158-4929-846a-7534eebcc47a', N'28', N'Suckling Holdings P/l', NULL, 0, 2017, 8, 0, NULL, 777)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'be40c6ae-2e6a-409a-8cbb-761b12791af5', N'25', N'Shepparton Transit', N'BrandB', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'887d24cc-34f6-49d6-a0b4-766d1b8f9f03', N'4', N'Edgar Motors', N'BrandD', 0, 2017, 10, 2, 16, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'753188f9-f102-4f6b-9b92-777030b2b171', N'1', N'Osbourne Automotive', NULL, -18724.080078125, 2017, 7, 0, NULL, 200575)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a58f111e-a4eb-4336-88a8-78d02c768d23', N'4', N'M & D O''Hehir Mechanical', NULL, 0, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'dbc0d319-cfa4-4143-80a8-78e19cfbffee', N'13', N'Suttons Motors Arncliffe', N'BrandA', 0, 2017, 10, 2, 17, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'519fcbe8-fea1-4868-bc9b-799eca7a7691', N'15', N'Openica Logistics Pty Ltd', N'BrandC', -2541.679931640625, 2017, 9, 2, NULL, 277197)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'10caf398-f9d0-49da-81b9-79d4829d5d21', N'13', N'Langwarrin Service Centre', NULL, 0, 2017, 9, 0, NULL, 4996)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'29dab066-ee56-476c-a2b4-7a62b82c22d5', N'17', N'Delux Rodz Pty Ltd', N'BrandD', 0, 2017, 10, 2, 18, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e31d3327-0d96-4ee2-8abf-7abfa246c9f0', N'16', N'Gippsland Fleet Maint Morwell', N'BrandA', -1632.9599609375, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'5021a433-822b-4ccb-be96-7bc596ebe2ca', N'17', N'Benalla Auto Industries', NULL, 0, 2017, 9, 0, NULL, 9254)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a0c327d1-4235-4630-ac17-7bf1ad4b90a7', N'5', N'Kirchner Trucks', N'BrandD', -1649, 2017, 9, 2, NULL, 478)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'11e17af8-7415-464d-b574-7c12f6f52bdc', N'25', N'Shepparton Transit', N'BrandA', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'f546cb8f-431a-45c5-9386-7cac9aeb0fda', N'9', N'Direct Freight', N'BrandB', -42047.44921875, 2017, 9, 2, NULL, 42047)
GO
print 'Processed 200 total records'
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ae0f8ac9-6179-4e69-add9-7d0a4956d788', N'20', N'Pony Xpress', NULL, -4818.580078125, 2017, 8, 0, NULL, 71096)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7460b0d4-6e1b-4e7c-842a-7d962c7eb34c', N'11', N'Suburban Mechanical', N'BrandD', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'f96c9ffa-6098-41c4-9748-7e4c8bb57897', N'4', N'M & D O''Hehir Mechanical', N'BrandB', 0, 2017, 9, 2, NULL, 23287)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'13118c9c-dadb-44c5-b1ed-7e86a8372582', N'15', N'Victorian Carriers Pty Ltd', NULL, 0, 2017, 10, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'1512fbf9-2917-4560-b2b3-7ed0bcf24fd0', N'16', N'South Eastern Truck Repairs', N'BrandC', -5416.7998046875, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9807d363-4b35-4011-95da-7f6a860240aa', N'20', N'Pony Xpress', N'BrandA', -4818.580078125, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e7357494-a1a3-422c-a2e8-80a04541fb08', N'26', N'Spotless (Berkley Challenge)', N'BrandB', 0, 2017, 8, 2, NULL, 12513)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'b51c1104-7264-4cc7-8401-80c812f10e39', N'17', N'Benalla Auto Industries', N'BrandA', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'4a76ce34-3efd-4acb-81eb-822e688d40b5', N'11', N'Suburban Mechanical', N'BrandC', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'8b43d0e2-d236-4b85-8567-8275d9a53947', N'13', N'Langwarrin Service Centre', N'BrandB', 0, 2017, 7, 2, NULL, 4996)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3420b87e-e34d-454f-944a-82878ab93cb5', N'21', N'Porter Plant', N'BrandD', -6340.22998046875, 2017, 8, 2, NULL, 966)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9640eac6-5651-498d-9d33-82f895939967', N'5', N'Phillip Island Automotive', NULL, -1153.9599609375, 2017, 10, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3dd15170-1564-44d7-99ee-8313994caf35', N'28', N'Suckling Holdings P/l', N'BrandB', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ee173520-f0c1-419e-9e15-8338fdb65e32', N'24', N'Seymour & District Diesel Rep', NULL, 0, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'616f7824-bea3-4041-8c87-836e8bdca676', N'32', N'Victorian Diesel Services P/l', N'BrandC', -19996.740234375, 2017, 8, 2, NULL, 220331)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2effdcdf-4a14-4774-b13a-837bb4a4ff33', N'26', N'Spotless (Berkley Challenge)', NULL, 0, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'cd18e291-f6f5-4ec1-8aeb-83bcbd074928', N'9', N'Direct Freight', NULL, -42047.44921875, 2017, 9, 0, NULL, 42047)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a5b650a5-b286-4365-b71e-83ed5cfab813', N'13', N'Langwarrin Service Centre', N'BrandB', 0, 2017, 9, 2, NULL, 4996)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'72e3ccda-72b1-4aaa-b302-84a1ca352d56', N'3', N'Rothenburger Automotive', N'BrandB', 0, 2017, 9, 2, NULL, 686)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'25815d6d-9aaf-46de-b1cf-857257ae2a8b', N'2', N'Monash University', N'BrandC', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'71cf498a-88ab-488b-a695-85847015c4e2', N'13', N'Suttons Motors Arncliffe', NULL, 0, 2017, 10, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c060a5f8-7aa2-487b-9bb3-85b21b24e180', N'15', N'Openica Logistics Pty Ltd', NULL, -2541.679931640625, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e89ea1e9-9625-4867-adf4-85d583f6b133', N'14', N'Livesey''s Motor Works', NULL, 0, 2017, 9, 0, NULL, 626)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7f6d9264-db17-4142-9539-8672bbe908e3', N'15', N'Victorian Carriers Pty Ltd', N'BrandB', 0, 2017, 10, 2, NULL, 913)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'191b8710-5143-451a-8d7a-86c2ec47d7df', N'11', N'Borg Fleet Management Pty Ltd', NULL, 0, 2017, 10, 0, NULL, 4979)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'4a1e0422-da78-40ea-b7cd-870931caf6d6', N'5', N'Phillip Island Automotive', N'BrandB', -1153.9599609375, 2017, 10, 2, NULL, 1493)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'0b3273eb-9987-470e-842f-87b41e2bced7', N'17', N'Benalla Auto Industries', N'BrandD', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'5a945c08-4933-48f8-b896-892221c26435', N'15', N'Openica Logistics Pty Ltd', N'BrandB', -2541.679931640625, 2017, 7, 2, NULL, 5240)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3d0d2590-285b-441f-9ef6-8958de891e64', N'8', N'Bp Clarke Road S/stn', N'BrandA', -412.32000732421875, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'95819768-15f2-4873-8c48-8a1603af7725', N'16', N'Gippsland Fleet Maint Morwell', N'BrandB', -1632.9599609375, 2017, 9, 2, NULL, 166869)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'0d2931cf-4fe0-40b6-9a87-8ac50fac0d55', N'7', N'Duke''s Automotive Repairs', NULL, 0, 2017, 7, 0, NULL, 8993)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7ac230be-262f-4a80-b23f-8ade2675ea19', N'3', N'Dandy Truck Sales', NULL, 0, 2017, 10, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'feddeb0f-8201-4225-a0c6-8ae628219b28', N'20', N'Pony Xpress', N'BrandB', -4818.580078125, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9bd26ff9-2183-4586-8f11-8afecca8b337', N'31', N'Cleanaway Operations T/as', N'BrandA', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7e66b613-f0c6-4ab6-b151-8b793d963180', N'27', N'Stenaust P/l', N'BrandA', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'94ab47af-4ee1-4743-ad40-8be073fffe46', N'35', N'Coburg Truck Parts VIC P/l', NULL, -21741.099609375, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e62180ff-fa30-4473-aaa8-8c30ed2f1dbb', N'19', N'Apex Steel Pty Ltd', N'BrandD', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3f4f98fb-c883-4265-bc03-8db37a65109c', N'23', N'Roadmech P/l', NULL, 0, 2017, 8, 0, NULL, 4338)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7a01f5f8-1b4b-451c-ad9c-8df0ee4bfd4b', N'11', N'Suburban Mechanical', N'BrandA', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'199df13a-0a60-4e5f-a1bd-8df4611fdde2', N'36', N'Seabrook Mechanical Services', N'BrandD', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'b3f9f68d-1211-42cd-a216-8e4c539e7f35', N'18', N'Holden Leasing (tvpr Pty Ltd)', N'BrandA', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'635abb37-0e71-4e85-8fb9-8e8e93e327c9', N'2', N'Mayos Body Shop', N'BrandC', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'586da589-2626-4125-abdb-9057e3c08344', N'2', N'Mayos Body Shop', NULL, 0, 2017, 7, 0, NULL, 927)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2103a4de-ffb6-4678-83bb-9128abcbc43b', N'14', N'Livesey''s Motor Works', NULL, 0, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'acdeefc5-06d5-4140-81f6-91620013c243', N'4', N'M & D O''Hehir Mechanical', N'BrandB', 0, 2017, 7, 2, NULL, 23287)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9bf01c85-87ef-4557-b273-91b37beea19e', N'15', N'Openica Logistics Pty Ltd', N'BrandB', -2541.679931640625, 2017, 9, 2, NULL, 5240)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'bbc169c9-6111-4965-a24a-924c315c5885', N'16', N'Gippsland Fleet Maint Morwell', NULL, -1632.9599609375, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ef5c73e7-a5a1-4254-bfe8-93511f70727f', N'7', N'Combined Horse Transport', N'BrandD', 0, 2017, 10, 2, NULL, 133)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'890d1d29-7c35-45a4-b486-93cc2475766a', N'10', N'Atoc Auto Repairs', N'BrandC', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'29dd4d61-bab8-49e9-b57f-9472a404e5a1', N'36', N'Seabrook Mechanical Services', NULL, 0, 2017, 8, 0, NULL, 52)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e323ae0c-689d-4019-a5a5-94bb0f92b289', N'12', N'Metropolitan Express', N'BrandA', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'6b73302d-e489-4ada-bb95-94e5d7fd07da', N'25', N'Shepparton Transit', NULL, 0, 2017, 8, 0, NULL, 25348)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c46aedc2-52eb-4eb8-8abd-96490209d533', N'15', N'Openica Logistics Pty Ltd', N'BrandA', -2541.679931640625, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'30f4f520-6e9c-4a40-a9f2-9712c08d4e7d', N'3', N'Rothenburger Automotive', N'BrandD', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'707c0c51-a910-41e5-aa31-976d0b3f4ea1', N'11', N'Suburban Mechanical', NULL, 0, 2017, 7, 0, NULL, 16796)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c972130f-ea18-4f66-8986-97a4bccab4b5', N'7', N'Duke''s Automotive Repairs', N'BrandA', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c2a8284f-fc1a-44fe-8e51-9922f27050e8', N'25', N'Shepparton Transit', N'BrandD', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'b3464d40-873f-4896-9d85-9a3e60c71273', N'9', N'Direct Freight', NULL, -42047.44921875, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2af91f48-545d-48a8-b4d6-9a86389820ae', N'1', N'Osbourne Automotive', N'BrandB', -18724.080078125, 2017, 7, 2, NULL, 162482)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'78cca853-1bb8-433a-afff-9b5bb8e0fcac', N'10', N'Calder Pallets', N'BrandB', -541.02001953125, 2017, 9, 2, NULL, 1262)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'0a2515fe-70aa-419a-9faa-9c14385f78e6', N'4', N'M & D O''Hehir Mechanical', N'BrandC', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'd2c80adf-a5e7-4418-989a-9c7a4ffb454c', N'7', N'Duke''s Automotive Repairs', N'BrandC', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'04848a3f-4d51-4c31-9610-9d367e1745d8', N'10', N'Atoc Auto Repairs', NULL, 0, 2017, 10, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'b62a6d3a-4d02-4041-90fb-9da49ec4de9e', N'13', N'Suttons Motors Arncliffe', NULL, 0, 2017, 10, 0, NULL, 10200)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'278df13d-fba6-4862-9b1a-9e0c5e404402', N'13', N'Langwarrin Service Centre', NULL, 0, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ef82a6aa-1a98-4198-ae4b-9e188e6e72e5', N'5', N'Kirchner Trucks', N'BrandA', -1649, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'86f26926-2ba3-4978-8b41-9e37e2d3ecc6', N'8', N'Bp Clarke Road S/stn', N'BrandC', -412.32000732421875, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c6e06094-74a0-4356-b0cb-9e43fc93bf02', N'37', N'Foster Tyres & Service Centre', N'BrandC', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9fdaa40d-44ab-4a32-8595-9e599e78af2a', N'12', N'Agee Panels & Towing Pty Ltd', N'BrandB', -1305.5799560546875, 2017, 7, 2, NULL, 111100)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'48c957cd-ff4b-473d-9134-9e61762bc36c', N'17', N'Benalla Auto Industries', N'BrandB', 0, 2017, 7, 2, NULL, 9254)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ee25cd4b-c6d3-4231-8de0-9f05481e54c8', N'2', N'Monash University', N'BrandD', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'1cd3fba4-a6e1-4cee-94ac-a0cb41f19b61', N'2', N'Mayos Body Shop', N'BrandB', 0, 2017, 9, 2, NULL, 927)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'92fc2311-56ec-4d65-916f-a143204ab078', N'13', N'Suttons Motors Arncliffe', N'BrandB', 0, 2017, 10, 2, NULL, 10200)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'8d1a4d4d-8173-4d00-bb7a-a15e3ba4dcda', N'24', N'Seymour & District Diesel Rep', NULL, 0, 2017, 8, 0, NULL, 1947)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'5c9eac00-71a6-4ff9-837f-a18183c65a75', N'28', N'Suckling Holdings P/l', N'BrandD', 0, 2017, 8, 2, NULL, 777)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e6dee935-e777-4e77-85e4-a1cd6e507a82', N'1', N'Osbourne Automotive', N'BrandD', -18724.080078125, 2017, 7, 2, NULL, 38093)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'64ed84f0-8ed2-4187-85c2-a27ecd74f483', N'2', N'Monash University', NULL, 0, 2017, 10, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'56ee6eab-5f33-400c-8e9b-a3dfef093532', N'18', N'Holden Leasing (tvpr Pty Ltd)', N'BrandB', 0, 2017, 9, 2, NULL, 858)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'eb8036dd-efd8-41f5-8576-a3fe8329e373', N'3', N'Dandy Truck Sales', N'BrandD', 0, 2017, 10, 2, NULL, 11455)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'937e4b0c-d0a0-49c8-a252-a4eacbbdce07', N'22', N'Qantas Airways Ltd', NULL, 0, 2017, 8, 0, NULL, 12620)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'087ea8e5-97ec-421a-abd6-a4f3d34874cc', N'1', N'Dyers Transport Pty Ltd', N'BrandC', -13608, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3f47606b-ea9c-4f93-8a3b-a510767ac4b0', N'4', N'M & D O''Hehir Mechanical', N'BrandA', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a84ad530-5ec7-4c5e-b5f6-a5b53c0b6fbe', N'16', N'Gippsland Fleet Maint Morwell', N'BrandD', -1632.9599609375, 2017, 9, 2, NULL, 2268)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'b40d06ee-18ed-4d15-924a-a5cd015dc158', N'14', N'Livesey''s Motor Works', N'BrandA', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'5fcf0ddd-fdab-4501-9029-a5d0fc43a8a5', N'32', N'Victorian Diesel Services P/l', N'BrandD', -19996.740234375, 2017, 8, 2, NULL, 25587)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'5188bc07-50a3-4b9f-bef5-a67990c951c9', N'24', N'Seymour & District Diesel Rep', N'BrandD', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9d415daa-fbf9-4ab2-aa26-a6e8bbc63ebc', N'7', N'Duke''s Automotive Repairs', N'BrandD', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c1f890b5-98f1-4eef-b147-a76623185f78', N'18', N'Holden Leasing (tvpr Pty Ltd)', N'BrandC', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'b13d07e3-b289-48bc-9c19-a8e1f15d0f98', N'6', N'L P F Automotive', NULL, -1618.199951171875, 2017, 7, 0, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'29b3f2f8-6989-4d0c-a6b0-a90645980e55', N'1', N'Osbourne Automotive', N'BrandD', -18724.080078125, 2017, 9, 2, NULL, 38093)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'd0405f88-ead7-4af4-85fa-a93a4fff1296', N'13', N'Langwarrin Service Centre', N'BrandC', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'74df381d-27c2-44f5-a706-aad4a20cf0b6', N'34', N'Western Truck Repairs', N'BrandB', -181150.8125, 2017, 8, 2, NULL, 302243)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'01d4433d-6115-4bf7-83eb-ab80a5c4af10', N'28', N'Suckling Holdings P/l', NULL, 0, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'39325206-52cc-469f-9fe3-abc1fda72dc4', N'3', N'Rothenburger Automotive', N'BrandB', 0, 2017, 7, 2, NULL, 686)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'14b0a555-5b6d-4ecf-84cd-ac07e48a1c55', N'18', N'Mgm Panelbeaters Pty Ltd', N'BrandD', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'5789e3a3-8192-4c4f-844b-ac2327efaef6', N'35', N'Coburg Truck Parts VIC P/l', N'BrandD', -21741.099609375, 2017, 8, 2, NULL, 68403)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'1b845dd7-470d-4a4b-9d11-ac6bb1b9a3c7', N'15', N'Openica Logistics Pty Ltd', NULL, -2541.679931640625, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a5ec23f2-526f-4b9d-9296-acbaea2c3687', N'11', N'Suburban Mechanical', N'BrandA', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'db0d8992-9434-49fc-b8a9-ad64419a2ee0', N'19', N'Apex Steel Pty Ltd', NULL, 0, 2017, 10, 0, NULL, 122)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'0406b7a9-0dec-4ce0-9f6d-af4a6e805481', N'9', N'Direct Freight', N'BrandC', -42047.44921875, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'5f11cfac-d298-44f0-941b-af5c12a433f7', N'4', N'Edgar Motors', NULL, 0, 2017, 10, 0, NULL, 801)
GO
print 'Processed 300 total records'
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'4096cd0a-8330-400d-9bc5-b02197d85fb4', N'17', N'Benalla Auto Industries', N'BrandB', 0, 2017, 9, 2, NULL, 9254)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'245954c3-2f19-4199-8336-b068f4e5438e', N'9', N'Direct Freight', N'BrandA', -42047.44921875, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'57109c5a-087c-4609-b146-b07fc551915e', N'3', N'Rothenburger Automotive', N'BrandD', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'b4e16459-4a00-4c3c-88fc-b146db7ee1b5', N'35', N'Coburg Truck Parts VIC P/l', NULL, -21741.099609375, 2017, 8, 0, NULL, 196025)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'324d7620-ee51-4b14-a3d4-b261abc4c967', N'26', N'Spotless (Berkley Challenge)', NULL, 0, 2017, 8, 0, NULL, 12513)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'39ef7ca2-e427-466a-b4f0-b382d0f8e621', N'7', N'Combined Horse Transport', N'BrandA', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'57326e8e-330c-436a-bf07-b38e9d0cf4b1', N'24', N'Seymour & District Diesel Rep', N'BrandC', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'781760e9-43cc-4462-9acc-b3bbafd58881', N'29', N'T Hutchinson Transport', N'BrandD', -3344.639892578125, 2017, 8, 2, NULL, 5730)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ab3fadaa-ac6d-4095-9f73-b4617d436816', N'14', N'Express Exports', N'BrandD', -3041.02001953125, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'443f885f-39e1-4102-bdf9-b4a05d33390c', N'37', N'Foster Tyres & Service Centre', NULL, 0, 2017, 8, 0, NULL, 422)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e389fbd4-3fe1-4c2f-8eff-b505e7359fde', N'12', N'Metropolitan Express', NULL, 0, 2017, 10, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e58667fb-94ef-4c3a-b26d-b52128311e02', N'28', N'Suckling Holdings P/l', N'BrandC', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3ca845c2-e5d8-47a1-a4af-b586494a0504', N'12', N'Metropolitan Express', N'BrandB', 0, 2017, 10, 2, NULL, 47258)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c725e745-2821-47e6-8e72-b68c57841d80', N'1', N'Dyers Transport Pty Ltd', N'BrandB', -13608, 2017, 10, 2, NULL, 171993)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7c04f8ef-ee06-4d63-82c8-b6c702146754', N'14', N'Express Exports', NULL, -3041.02001953125, 2017, 10, 0, NULL, 145293)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'28c25fbd-e9b7-4ad7-9c01-b805d3aa2146', N'10', N'Calder Pallets', NULL, -541.02001953125, 2017, 9, 0, NULL, 1262)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'caf55990-da9e-4612-ae27-b88a5fb626e5', N'1', N'Osbourne Automotive', N'BrandA', -18724.080078125, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'08983817-2017-43c0-b949-bac1ea6730cc', N'9', N'Direct Freight', N'BrandB', -42047.44921875, 2017, 7, 2, NULL, 42047)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'492183fa-2736-4184-9a73-baf5d8d552ef', N'34', N'Western Truck Repairs', N'BrandC', -181150.8125, 2017, 8, 2, NULL, 415120)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'842e6713-96fd-44ea-9a46-bb2780f4c5dd', N'4', N'M & D O''Hehir Mechanical', N'BrandD', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'4159dd62-337a-450b-9a14-bc80d825030b', N'9', N'Direct Freight', N'BrandD', -42047.44921875, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2b1626f0-26ff-42af-aa37-bca45cbad7ec', N'5', N'Kirchner Trucks', N'BrandB', -1649, 2017, 7, 2, NULL, 881)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'002f3d68-30c2-4ebf-bc9e-be74abff6aab', N'23', N'Roadmech P/l', NULL, 0, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ffc9fee5-7a0a-4d1d-aff5-be948ea5d8f1', N'16', N'South Eastern Truck Repairs', NULL, -5416.7998046875, 2017, 10, 0, NULL, 143800)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7f21ad8e-fd80-48d1-83af-bf961851de97', N'36', N'Seabrook Mechanical Services', N'BrandB', 0, 2017, 8, 2, NULL, 52)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'f2a52988-7ba4-4f20-85fd-c02d9fe1d454', N'8', N'Bp Clarke Road S/stn', N'BrandA', -412.32000732421875, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'594dc4ff-2944-418d-a6c6-c08f51343fc6', N'20', N'Pony Xpress', N'BrandC', -4818.580078125, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'62c7d59a-e6cf-4031-a355-c36c23225c23', N'16', N'Gippsland Fleet Maint Morwell', NULL, -1632.9599609375, 2017, 7, 0, NULL, 169137)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'69ff2d31-6457-48bb-aa0e-c429875c95bd', N'8', N'Bp Clarke Road S/stn', N'BrandB', -412.32000732421875, 2017, 9, 2, NULL, 2611)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'396e3d16-d165-40ae-9385-c45ce1f451f5', N'3', N'Rothenburger Automotive', NULL, 0, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ba6e5dbf-65af-4f5b-849c-c595169e65fd', N'2', N'Mayos Body Shop', N'BrandA', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'fd647b4e-d23e-4046-b37c-c5a94b687372', N'27', N'Stenaust P/l', NULL, 0, 2017, 8, 0, NULL, 66616)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'57cdbbd3-234a-4868-8dc0-c5d90d80221d', N'14', N'Livesey''s Motor Works', N'BrandB', 0, 2017, 9, 2, NULL, 626)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ce127075-1c18-43c9-8ffd-c645d38cb025', N'10', N'Calder Pallets', N'BrandA', -541.02001953125, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'd3518568-a353-4127-b1e1-c6be361aba91', N'10', N'Calder Pallets', N'BrandB', -541.02001953125, 2017, 7, 2, NULL, 1262)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'50b4d733-d5a1-49d8-b94b-c8088295bc97', N'15', N'Openica Logistics Pty Ltd', N'BrandD', -2541.679931640625, 2017, 7, 2, NULL, 10848)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'17d57e4b-0f1c-46c3-a373-c821fc66ed8b', N'4', N'M & D O''Hehir Mechanical', N'BrandA', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e9576d7f-1b93-435e-8d83-c8ef0b63b077', N'10', N'Atoc Auto Repairs', N'BrandA', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c46786ba-5fa4-446d-98c4-c996c67f3e7b', N'5', N'Phillip Island Automotive', NULL, -1153.9599609375, 2017, 10, 0, NULL, 1493)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c8d9a3ef-9f41-4f39-a865-ca828a54d889', N'36', N'Seabrook Mechanical Services', N'BrandC', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'77fa49f4-176a-4f8b-b74f-cb6704735339', N'8', N'Bp Clarke Road S/stn', NULL, -412.32000732421875, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9041b5f1-b628-4c39-9dfc-cba04ba75674', N'22', N'Qantas Airways Ltd', N'BrandA', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'1c1bbdd9-6c89-469b-b987-cc412542206a', N'13', N'Langwarrin Service Centre', NULL, 0, 2017, 7, 0, NULL, 4996)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'441231e5-417a-43d4-a4e7-ccda0abd7f64', N'16', N'South Eastern Truck Repairs', N'BrandB', -5416.7998046875, 2017, 10, 2, NULL, 75453)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'239b2342-7fde-47c1-9644-cd43ca0c0d61', N'30', N'The Haire Truck & Bus Repairs', N'BrandB', -254.55000305175781, 2017, 8, 2, NULL, 16512)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2c8f2774-d845-4256-b21b-ce2fa25f40e7', N'9', N'Veolia Environmental Service', NULL, -29225.48046875, 2017, 10, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3fd69ecc-b23a-4070-8c65-ce85ff867fd4', N'4', N'M & D O''Hehir Mechanical', NULL, 0, 2017, 7, 0, NULL, 23287)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'd42ed63c-5555-4fa5-8f9d-cf58e753e7fa', N'14', N'Livesey''s Motor Works', N'BrandC', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'cfb87ea6-573b-417f-8fa8-d0377f35b2ca', N'15', N'Openica Logistics Pty Ltd', NULL, -2541.679931640625, 2017, 9, 0, NULL, 293285)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'039baf50-9297-4e3c-a718-d1aa6481113e', N'11', N'Borg Fleet Management Pty Ltd', N'BrandA', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c6519240-1c13-4101-9d00-d3aa960888c8', N'13', N'Langwarrin Service Centre', N'BrandD', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'0765d36a-d016-4444-aaba-d43205d372a5', N'3', N'Rothenburger Automotive', NULL, 0, 2017, 7, 0, NULL, 686)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9cb77b44-24de-4aa6-99c9-d4537d2f6486', N'21', N'Porter Plant', NULL, -6340.22998046875, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'003ec3ff-a6c1-496f-86ee-d47adf656bfd', N'17', N'Benalla Auto Industries', N'BrandC', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'34288317-a387-4522-9a93-d47c881c06d6', N'6', N'L P F Automotive', NULL, -1618.199951171875, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ff62c3d7-9567-4ae2-ba7d-d537bbc0dafe', N'34', N'Western Truck Repairs', N'BrandD', -181150.8125, 2017, 8, 2, NULL, 103850)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'f726e67a-3801-4c61-9e8f-d65096d05918', N'8', N'Bog Cog Off Road & Auto Sve', N'BrandA', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'd6b37f5c-a63b-4d2d-a434-d7636d6e8553', N'6', N'L P F Automotive', NULL, -1618.199951171875, 2017, 9, 0, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'31429b4d-b86f-4dcf-965f-d7692acd68bb', N'16', N'Gippsland Fleet Maint Morwell', NULL, -1632.9599609375, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7e361c17-0b47-46b3-8428-d7bf115fca98', N'5', N'Phillip Island Automotive', N'BrandA', -1153.9599609375, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'4fd4c794-397b-4379-83d7-d86e33ed6acd', N'6', N'L P F Automotive', NULL, -1618.199951171875, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'8a9d5236-fa16-44aa-9efb-dbe41f2e7b1d', N'3', N'Rothenburger Automotive', NULL, 0, 2017, 9, 0, NULL, 686)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7a6e45a6-41da-4838-8227-dc2e74844775', N'7', N'Combined Horse Transport', N'BrandB', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'5e18f04d-2de2-4099-9d24-dc3e84b7ddbb', N'23', N'Roadmech P/l', N'BrandD', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2158e08e-a071-410b-9928-dc860715774d', N'10', N'Calder Pallets', NULL, -541.02001953125, 2017, 7, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'57750bcf-10a6-4793-85d5-dcaf9e248182', N'1', N'Dyers Transport Pty Ltd', N'BrandA', -13608, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'5a55ffff-bfe7-4795-aa5a-dcc4e33f54f3', N'3', N'Dandy Truck Sales', N'BrandA', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'5b0101be-2018-48ec-9ef3-dccbde24b20e', N'17', N'Delux Rodz Pty Ltd', NULL, 0, 2017, 10, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'aacb9fc1-6333-46cf-96ae-dd6fc5f0dc4e', N'1', N'Dyers Transport Pty Ltd', N'BrandD', -13608, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a0bb4b4c-8b30-476a-b343-dd798fc91d1a', N'11', N'Suburban Mechanical', NULL, 0, 2017, 9, 0, NULL, 16796)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'23149c9e-0713-48f5-a980-dd8f2f304bdd', N'15', N'Victorian Carriers Pty Ltd', N'BrandA', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c0eca03c-bed0-4eb9-a098-de3259d18c5e', N'24', N'Seymour & District Diesel Rep', N'BrandA', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'4f1265f9-7aa9-448d-a9a7-de66ebb452bb', N'8', N'Bog Cog Off Road & Auto Sve', N'BrandD', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'f5f1c3fd-66d5-42a9-9472-dea2fe8e350b', N'25', N'Shepparton Transit', NULL, 0, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'12737600-4cbf-49eb-89b4-dfbfb8156133', N'10', N'Calder Pallets', NULL, -541.02001953125, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'daef16e1-a76b-4328-b730-dffdec0074b8', N'16', N'Gippsland Fleet Maint Morwell', N'BrandB', -1632.9599609375, 2017, 7, 2, NULL, 166869)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'be002c9b-d377-4a49-ba64-e12e44918d93', N'14', N'Livesey''s Motor Works', NULL, 0, 2017, 7, 0, NULL, 626)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'ba045b11-1784-479b-865b-e192990be528', N'32', N'Victorian Diesel Services P/l', NULL, -19996.740234375, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e63868a9-b0db-4d2e-960e-e1b0f187e91a', N'17', N'Benalla Auto Industries', N'BrandC', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'fd1c52c8-8a3e-4383-a07b-e260f7ad2f4e', N'3', N'Rothenburger Automotive', N'BrandA', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'20db016d-7bd8-444e-ac0a-e265df65fcdd', N'2', N'Mayos Body Shop', N'BrandA', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'0a42bd88-4b38-4d71-88d7-e2dbcaf7ac69', N'4', N'M & D O''Hehir Mechanical', NULL, 0, 2017, 9, 0, NULL, 23287)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'65d3cd4f-42ac-4207-996d-e2e9adebb5b6', N'2', N'Mayos Body Shop', N'BrandC', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'49a4f923-ee93-40a8-9dad-e42fa7d0c376', N'18', N'Holden Leasing (tvpr Pty Ltd)', N'BrandD', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'b5dd414f-5782-4761-9f9e-e50db7c7cca9', N'16', N'Gippsland Fleet Maint Morwell', N'BrandC', -1632.9599609375, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'6dd28bc4-d14f-44bf-b2af-e53ca500b491', N'9', N'Direct Freight', NULL, -42047.44921875, 2017, 7, 0, NULL, 42047)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3cdaa985-6e92-4451-8692-e5bd22efff69', N'14', N'Livesey''s Motor Works', N'BrandA', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'b65a6fdf-1e6d-4e16-9938-e641ab91b6f9', N'6', N'L P F Automotive', N'BrandA', -1618.199951171875, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2c1c4e7a-b6b2-4338-8648-e66e5d28b1e3', N'5', N'Kirchner Trucks', N'BrandD', -1649, 2017, 7, 2, NULL, 478)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2d730862-c229-4eb0-92cd-e6cf5d1f992e', N'4', N'M & D O''Hehir Mechanical', NULL, 0, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2c04c6a4-5b5a-40d2-b786-e706f89edc2e', N'19', N'Kenworth Melbourne, Daf Melbourne', NULL, -274.60000610351562, 2017, 8, 0, NULL, 37207)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'95a52153-839c-47a1-8952-e72b5f965138', N'7', N'Duke''s Automotive Repairs', N'BrandA', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e21dcae4-d18d-44d6-9776-e7fe904ac450', N'13', N'Langwarrin Service Centre', N'BrandA', 0, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'209d7b6b-fce2-446d-b8c8-e85284dcf6bd', N'6', N'L P F Automotive', N'BrandC', -1618.199951171875, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2fe83599-3062-4c91-be13-e8806eacc78f', N'33', N'G & M Waldie P/l', NULL, -1859.5799560546875, 2017, 8, 0, NULL, 94175)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'6ea17b53-5e64-4f80-aef5-e8ba22266812', N'14', N'Express Exports', NULL, -3041.02001953125, 2017, 10, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'cde2b7a1-202e-4aef-8455-e92b52b3ab0c', N'12', N'Agee Panels & Towing Pty Ltd', N'BrandA', -1305.5799560546875, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'5f69c14c-cdb5-4455-b7cc-e988093f96a0', N'22', N'Qantas Airways Ltd', N'BrandC', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'1ee759cd-75bc-4a98-88e1-e9c1808bd550', N'32', N'Victorian Diesel Services P/l', N'BrandA', -19996.740234375, 2017, 8, 2, NULL, 849)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'b9347b18-70df-4f12-a469-eca23a16ee83', N'18', N'Mgm Panelbeaters Pty Ltd', N'BrandA', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7b0fd433-9c03-4e3c-b66a-edbb4dc5a083', N'13', N'Suttons Motors Arncliffe', N'BrandD', 0, 2017, 10, 2, NULL, NULL)
GO
print 'Processed 400 total records'
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'09c8f484-10e3-4f35-873b-edc61de29308', N'1', N'Osbourne Automotive', NULL, -18724.080078125, 2017, 9, 0, NULL, 200575)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c18a09b3-363a-4411-b295-edcbe22947d3', N'11', N'Suburban Mechanical', N'BrandB', 0, 2017, 7, 2, NULL, 16796)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'e0fc5b7e-5654-4311-80f3-ee5003be853b', N'22', N'Qantas Airways Ltd', N'BrandB', 0, 2017, 8, 2, NULL, 12620)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'8383a72e-b05b-4deb-acb3-eefb48530aa3', N'33', N'G & M Waldie P/l', N'BrandA', -1859.5799560546875, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'4bd5369b-09ea-4491-b59b-ef5fa47f607a', N'12', N'Agee Panels & Towing Pty Ltd', NULL, -1305.5799560546875, 2017, 9, 0, NULL, 111100)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'7458ee8e-8acc-4d7f-9093-f0d6b29fe385', N'28', N'Suckling Holdings P/l', N'BrandA', 0, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2b9fd838-2635-484e-a4c9-f12181a25c1a', N'1', N'Osbourne Automotive', N'BrandC', -18724.080078125, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'd7778945-84c8-46b2-b7cf-f1dcf5afe7c5', N'10', N'Atoc Auto Repairs', N'BrandB', 0, 2017, 10, 2, NULL, 3773)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'2f132754-e1c3-456a-b131-f288a1697fab', N'37', N'Foster Tyres & Service Centre', NULL, 0, 2017, 8, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'37fffc0f-62ae-4d14-9697-f2fe95b7d42d', N'14', N'Livesey''s Motor Works', N'BrandC', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'70778742-8837-4e2a-9989-f37746c3c3a2', N'18', N'Holden Leasing (tvpr Pty Ltd)', NULL, 0, 2017, 9, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'3cbe3ba0-108b-4b9e-acfb-f3e5bff9d5c0', N'18', N'Mgm Panelbeaters Pty Ltd', NULL, 0, 2017, 10, 0, NULL, 28103)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'6f3c70e3-06b5-4de7-abd7-f41366a9160a', N'8', N'Bp Clarke Road S/stn', N'BrandC', -412.32000732421875, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'c3c089fc-774e-4782-846a-f4cb188da89c', N'12', N'Agee Panels & Towing Pty Ltd', N'BrandC', -1305.5799560546875, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'1f79f067-8133-4fbd-9f1a-f4d8dea1d16e', N'17', N'Delux Rodz Pty Ltd', N'BrandB', 0, 2017, 10, 2, NULL, 212)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9de6ef7d-35c1-40ac-bf44-f5b6a1caf601', N'19', N'Kenworth Melbourne, Daf Melbourne', N'BrandC', -274.60000610351562, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'419a368f-fa8e-4aa7-a71e-f5d4617e81d8', N'5', N'Kirchner Trucks', N'BrandC', -1649, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'402a9d66-fd6a-4ccf-8b83-f6b95c8f21e5', N'27', N'Stenaust P/l', N'BrandB', 0, 2017, 8, 2, NULL, 66616)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'59625392-6716-44ad-849c-f803dfe2e371', N'18', N'Mgm Panelbeaters Pty Ltd', N'BrandC', 0, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'5ae395bb-762b-4317-b86c-f84bf7433173', N'12', N'Agee Panels & Towing Pty Ltd', N'BrandD', -1305.5799560546875, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'dbc2577a-7de4-48c1-9186-f95e56060ca2', N'17', N'Delux Rodz Pty Ltd', NULL, 0, 2017, 10, 0, NULL, 212)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'60198198-53f6-4225-9a87-f9825c3ddacb', N'7', N'Duke''s Automotive Repairs', N'BrandB', 0, 2017, 7, 2, NULL, 8993)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'13d1316b-d8f6-4f40-9a95-f9d54e8f9ba7', N'37', N'Foster Tyres & Service Centre', N'BrandB', 0, 2017, 8, 2, NULL, 422)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a9a1d0d6-afb2-451c-87d4-fa71c3633429', N'18', N'Holden Leasing (tvpr Pty Ltd)', N'BrandB', 0, 2017, 7, 2, NULL, 858)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'1c78ac70-bae7-4555-88e4-fb7c279ba790', N'15', N'Victorian Carriers Pty Ltd', N'BrandD', 0, 2017, 10, 2, NULL, 1713)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'fd45f564-2f6d-4d27-8030-fc5d9995ebf6', N'18', N'Mgm Panelbeaters Pty Ltd', NULL, 0, 2017, 10, 1, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'63cda66b-30dc-49f5-8fd0-fd44c621b377', N'14', N'Express Exports', N'BrandC', -3041.02001953125, 2017, 10, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a7476e1c-8872-4a96-8177-fe46ff150fc4', N'4', N'M & D O''Hehir Mechanical', N'BrandD', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'456df455-6338-4b35-bf97-fefe39ac0543', N'18', N'Mgm Panelbeaters Pty Ltd', N'BrandB', 0, 2017, 10, 2, NULL, 28103)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'9c0f6128-413c-4c8c-a674-ff76fcd4d2e6', N'2', N'Mayos Body Shop', N'BrandD', 0, 2017, 9, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'a0d550d2-afea-4719-8bdb-ff8ba1cb2888', N'2', N'Monash University', N'BrandB', 0, 2017, 10, 2, NULL, 159)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'de8f1673-ae65-4370-a692-ffb434c8cb93', N'34', N'Western Truck Repairs', N'BrandA', -181150.8125, 2017, 8, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'0c0b7a5f-9ff7-40a7-a678-ffca2b8f6df7', N'9', N'Direct Freight', N'BrandC', -42047.44921875, 2017, 7, 2, NULL, NULL)
INSERT [dbo].[SalesData] ([SalesDataId], [AccountNumber], [Name], [BrandName], [Credit], [Year], [Month], [Type], [Day], [TotalAmount]) VALUES (N'8a23fe35-6e5a-4a49-9446-ffd02f9cc10e', N'17', N'Delux Rodz Pty Ltd', N'BrandA', 0, 2017, 10, 2, NULL, NULL)
/****** Object:  StoredProcedure [dbo].[SalesData_SelectById]    Script Date: 10/27/2017 13:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SalesData_SelectById]
	@SalesDataId uniqueidentifier 
AS
BEGIN
	SELECT * FROM SalesData WHERE SalesDataId=@SalesDataId
END
GO
/****** Object:  StoredProcedure [dbo].[SalesData_Save]    Script Date: 10/27/2017 13:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SalesData_Save]
	@SalesDataId uniqueidentifier,
	@AccountNumber int,
	@Name nvarchar(max),
	@BrandName nvarchar(max),
	@Credit float,
	@Year int,
	@Month int,
	@Type int,
	@Day int,
	@TotalAmount float
	
	
AS
BEGIN
	IF (SELECT COUNT(*)FROM SalesData WHERE SalesDataId=@SalesDataId)=0
		BEGIN
			INSERT INTO SalesData(SalesDataId,AccountNumber,Name,BrandName,Credit,[Year],[Month],[Type],[Day],TotalAmount)
			VALUES (@SalesDataId,@AccountNumber,@Name,@BrandName,@Credit,@Year,@Month,@Type,@Day,@TotalAmount)
		END
	ELSE
		BEGIN
			UPDATE SalesData
			SET
				SalesDataId=@SalesDataId,
				AccountNumber=@AccountNumber,
				Name=@Name,
				BrandName=@BrandName,
				Credit=@Credit,
				[Year]=@Year,
				[Month]=@Month,
				[Type]=@Type,
				[Day]=@Day,
				TotalAmount=@TotalAmount
			WHERE
				SalesDataId=@SalesDataId
		END
END
GO
/****** Object:  StoredProcedure [dbo].[SalesData_Find]    Script Date: 10/27/2017 13:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SalesData_Find]
	@Criteria nvarchar(max)
AS
BEGIN
	SELECT * FROM SalesData WHERE Name LIKE @Criteria 
	OR BrandName LIKE @Criteria 
END
GO
/****** Object:  StoredProcedure [dbo].[SalesData_DeleteByDate]    Script Date: 10/27/2017 13:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SalesData_DeleteByDate]
	@Year int,
	@Month int 
AS
BEGIN
	DELETE FROM SalesData WHERE ([Year]=@Year and [Month]=@Month)
END
GO
/****** Object:  StoredProcedure [dbo].[SalesData_Delete]    Script Date: 10/27/2017 13:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SalesData_Delete]
	@SalesDataId uniqueidentifier 
AS
BEGIN
	DELETE FROM SalesData WHERE SalesDataId=@SalesDataId
END
GO
/****** Object:  StoredProcedure [dbo].[Sales_TotalAmountPerDay]    Script Date: 10/27/2017 13:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sales_TotalAmountPerDay]
As
BEGIN
	DECLARE @CurrentDate DATE = GETDATE()

	select Sum(TotalAmount) as SalesToDate,[Day]
	from SalesData where [Month] = MONTH(@CurrentDate) and [Year] = YEAR(@CurrentDate) and Day is not null
	group by [Day]
	order by [Day] asc
END
GO
/****** Object:  StoredProcedure [dbo].[Sales_MonthToDateForBrands]    Script Date: 10/27/2017 13:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sales_MonthToDateForBrands]
AS
BEGIN
	DECLARE @CurrentDate DATE = GETDATE()

	select SalesData.BrandName,SUM(TotalAmount)as MonthToDate,(select SUM(Credit) from SalesData where [Month] = MONTH(@CurrentDate) and [Year] = YEAR(@CurrentDate) )  as CreditsReturn
	from SalesData where [Month] = MONTH(@CurrentDate) and [Year] = YEAR(@CurrentDate)
	group by SalesData.BrandName
	order by SUM(TotalAmount) desc
END
GO
/****** Object:  StoredProcedure [dbo].[Sales_RollingAverageForBrands]    Script Date: 10/27/2017 13:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sales_RollingAverageForBrands]

AS
BEGIN  

	DECLARE @Day int = DAY(GETDATE()) 
	DECLARE @MonthPriorOneDays int
	DECLARE @MonthPriorTwoDays int
	DECLARE @MonthPriorThreeDays int
	DECLARE @StartOfMonth DATE = DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0)
	DECLARE @CurrentDate DATE = GETDATE()
	DECLARE @WeekendDays int
	
-- subtract weekends untill current date

	SELECT @WeekendDays = 
					  (DATEDIFF(wk, @StartOfMonth, @CurrentDate) * 2)
					  +(CASE WHEN DATEPART (dw, @StartOfMonth) = 1 THEN 1 ELSE 0 END)
					  +(CASE WHEN DATEPART (dw, @CurrentDate) = 7 THEN 1 ELSE 0 END)
  
	SET @Day= @Day-@WeekendDays

--get work days for each month
	
	SET @MonthPriorOneDays = DAY(DATEADD(DD,-1,DATEADD(mm, DATEDIFF(mm, 0, @CurrentDate), 0)))-8
	SET @MonthPriorTwoDays = DAY(DATEADD(DD,-1,DATEADD(mm, DATEDIFF(mm, 0, @CurrentDate)-1, 0)))-8
	SET @MonthPriorThreeDays = DAY(DATEADD(DD,-1,DATEADD(mm, DATEDIFF(mm, 0, @CurrentDate)-2, 0)))-8
	
-- create Temporary table and insert [Sales_MonthToDateForCustomers] proc to it
	CREATE TABLE #Temp(
		BrandName NVARCHAR(MAX),
		MonthToDate FLOAT,
		CreditsReturn FLOAT
	)
	INSERT INTO #Temp EXEC dbo.[Sales_MonthToDateForBrands]
	
-- query sales for the past 3 month and calculate rolling average

	SELECT SalesData.BrandName,#Temp.MonthToDate,@Day * SUM(TotalAmount) /(@MonthPriorOneDays+@MonthPriorTwoDays+@MonthPriorThreeDays) as RollingAverage,#Temp.CreditsReturn
	FROM SalesData INNER JOIN #Temp ON #Temp.BrandName = SalesData.BrandName
	WHERE DATEADD(MONTH, 0, Cast(SalesData.[Year] as nvarchar(50))+'-'+Cast(SalesData.[Month] as nvarchar(50))+'-1') between DATEADD(MONTH, -3, @StartOfMonth) AND DATEADD(MONTH, 0, @StartOfMonth)
	GROUP BY SalesData.BrandName,#Temp.MonthToDate,#Temp.CreditsReturn
	ORDER BY MonthToDate DESC
END
GO
/****** Object:  StoredProcedure [dbo].[Sales_MonthToDateForCustomers]    Script Date: 10/27/2017 13:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sales_MonthToDateForCustomers]
AS
BEGIN
	DECLARE @CurrentDate DATE = GETDATE()

	select Top 20 SalesData.AccountNumber,SUM(Credit) as CreditReturns,SUM(TotalAmount)as MonthToDate,Customers.BusinessName,Customers.CustomerId
	from SalesData inner join Customers on Customers.AccountNumber = SalesData.AccountNumber where [Month] = MONTH(@CurrentDate) and [Year] = YEAR(@CurrentDate)
	group by SalesData.AccountNumber,Customers.BusinessName,Customers.CustomerId
	order by SUM(TotalAmount) desc
END
GO
/****** Object:  StoredProcedure [dbo].[Sales_RollingAverageForCustomers]    Script Date: 10/27/2017 13:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sales_RollingAverageForCustomers]

AS
BEGIN  

	DECLARE @Day int = DAY(GETDATE()) 
	DECLARE @MonthPriorOneDays int
	DECLARE @MonthPriorTwoDays int
	DECLARE @MonthPriorThreeDays int
	DECLARE @StartOfMonth DATE = DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0)
	DECLARE @CurrentDate DATE = GETDATE()
	DECLARE @WeekendDays int
	
-- subtract weekends untill current date

	SELECT @WeekendDays = 
					  (DATEDIFF(wk, @StartOfMonth, @CurrentDate) * 2)
					  +(CASE WHEN DATEPART (dw, @StartOfMonth) = 1 THEN 1 ELSE 0 END)
					  +(CASE WHEN DATEPART (dw, @CurrentDate) = 7 THEN 1 ELSE 0 END)
  
	SET @Day= @Day-@WeekendDays

--get work days for each month
	
	SET @MonthPriorOneDays = DAY(DATEADD(DD,-1,DATEADD(mm, DATEDIFF(mm, 0, @CurrentDate), 0)))-8
	SET @MonthPriorTwoDays = DAY(DATEADD(DD,-1,DATEADD(mm, DATEDIFF(mm, 0, @CurrentDate)-1, 0)))-8
	SET @MonthPriorThreeDays = DAY(DATEADD(DD,-1,DATEADD(mm, DATEDIFF(mm, 0, @CurrentDate)-2, 0)))-8
	
-- create Temporary table and insert [Sales_MonthToDateForCustomers] proc to it
	CREATE TABLE #Temp(
		AccountNumber NVARCHAR(MAX),
		CreditReturns FLOAT,
		MonthToDate FLOAT,
		BusinessName NVARCHAR(MAX),
		CustomerId uniqueidentifier
	)
	INSERT INTO #Temp EXEC dbo.[Sales_MonthToDateForCustomers]
	
-- query sales for the past 3 month and calculate rolling average

	SELECT SalesData.AccountNumber,#Temp.BusinessName,#Temp.MonthToDate,#Temp.CreditReturns,@Day * SUM(TotalAmount) /(@MonthPriorOneDays+@MonthPriorTwoDays+@MonthPriorThreeDays) as RollingAverage,(select Count(*) from Calls where CustomerId=#Temp.CustomerId) as CallsCount
	FROM SalesData INNER JOIN #Temp ON #Temp.AccountNumber = SalesData.AccountNumber
	WHERE DATEADD(MONTH, 0, Cast(SalesData.[Year] as nvarchar(50))+'-'+Cast(SalesData.[Month] as nvarchar(50))+'-1') between DATEADD(MONTH, -3, @StartOfMonth) AND DATEADD(MONTH, 0, @StartOfMonth)
	GROUP BY SalesData.AccountNumber,#Temp.BusinessName,#Temp.MonthToDate,#Temp.CreditReturns,#Temp.CustomerId
	ORDER BY MonthToDate DESC
END
GO

update Configuration set DbVersion = DbVersion +1