USE [Triton]
GO
/****** Object:  StoredProcedure [dbo].[Calls_GetMyOpenCallsNum]    Script Date: 10/27/2017 14:01:12 ******/
DROP PROCEDURE [dbo].[Calls_GetMyOpenCallsNum]
GO
/****** Object:  StoredProcedure [dbo].[Calls_GetMyScheduledCallsNum]    Script Date: 10/27/2017 14:01:12 ******/
DROP PROCEDURE [dbo].[Calls_GetMyScheduledCallsNum]
GO
/****** Object:  StoredProcedure [dbo].[Issues_GetMyOpenIssuesNum]    Script Date: 10/27/2017 14:01:12 ******/
DROP PROCEDURE [dbo].[Issues_GetMyOpenIssuesNum]
GO
/****** Object:  StoredProcedure [dbo].[Tasks_FindMyTasks]    Script Date: 10/27/2017 14:01:12 ******/
DROP PROCEDURE [dbo].[Tasks_FindMyTasks]
GO
/****** Object:  StoredProcedure [dbo].[Tasks_GetMyOpenTasksNum]    Script Date: 10/27/2017 14:01:12 ******/
DROP PROCEDURE [dbo].[Tasks_GetMyOpenTasksNum]
GO
/****** Object:  StoredProcedure [dbo].[Tasks_GetMyTasksNum]    Script Date: 10/27/2017 14:01:12 ******/
DROP PROCEDURE [dbo].[Tasks_GetMyTasksNum]
GO
/****** Object:  StoredProcedure [dbo].[CustomersName]    Script Date: 10/27/2017 14:01:12 ******/
DROP PROCEDURE [dbo].[CustomersName]
GO
/****** Object:  StoredProcedure [dbo].[Members_GetCustomersPermissions]    Script Date: 10/27/2017 14:01:12 ******/
DROP PROCEDURE [dbo].[Members_GetCustomersPermissions]
GO
/****** Object:  StoredProcedure [dbo].[Members_SelectByEmailPassword]    Script Date: 10/27/2017 14:01:12 ******/
DROP PROCEDURE [dbo].[Members_SelectByEmailPassword]
GO
/****** Object:  StoredProcedure [dbo].[Resources_Find]    Script Date: 10/27/2017 14:01:12 ******/
DROP PROCEDURE [dbo].[Resources_Find]
GO
/****** Object:  StoredProcedure [dbo].[Resources_Find]    Script Date: 10/27/2017 14:01:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Resources_Find]
	@Criteria nvarchar(max)= null
AS
BEGIN
	SELECT  [ResourceId]
      ,[IsOnCdn]
      ,[CdnUrl]
      ,[HasThumbnail]
      ,[FileName]
      ,[FileNameWithExtension]
      ,[FileType]
      ,[CreatedOn]
      ,[CreatedBy]
      ,[IsDirectory]
      ,[ParentResourceId]
      ,YoutubeId
      ,VimeoId FROM Resources
      order by IsDirectory DESC,FileName
END
GO
/****** Object:  StoredProcedure [dbo].[Members_SelectByEmailPassword]    Script Date: 10/27/2017 14:01:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Members_SelectByEmailPassword]
@Email nvarchar(50),
@Password nvarchar(max)
as
begin 
SELECT * FROM Members
	left join Roles on Members.RoleId= roles.RoleId
	left join Zones on Members.ResponsibilityZoneId = zones.ZoneId
	left join RegistrationQuestions as ques on ques.QuestionId= Members.QuestionId 
	 where Email = @Email and [Password] = @Password and IsDisabled=0
end
GO
/****** Object:  StoredProcedure [dbo].[Members_GetCustomersPermissions]    Script Date: 10/27/2017 14:01:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Members_GetCustomersPermissions]
	@MemberId uniqueidentifier
As
Begin
	DECLARE @Result int = 0

	DECLARE @CurrentPermission int
	
	DECLARE ZoneMemberCursor CURSOR LOCAL FOR 
		SELECT Zones.Permission FROM MemberZones
		inner join Zones ON MemberZones.ZoneId = Zones.ZoneId 
		WHERE MemberZones.MemberId=@MemberId
			
		OPEN ZoneMemberCursor
		FETCH NEXT FROM ZoneMemberCursor INTO @CurrentPermission  
			
		WHILE @@FETCH_STATUS = 0  
			BEGIN 
				SET @Result = @Result  | @CurrentPermission
				FETCH NEXT FROM ZoneMemberCursor INTO @CurrentPermission
			END
				
	CLOSE  ZoneMemberCursor
	DEALLOCATE ZoneMemberCursor
			 
	SELECT Customers.CustomerId from Customers join Zones  on Customers.ZoneId = Zones.ZoneId
	WHERE Zones.PermissionId & @Result > 0 AND IsDeleted = 0
		
End
GO
/****** Object:  StoredProcedure [dbo].[CustomersName]    Script Date: 10/27/2017 14:01:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[CustomersName]
@MemberId uniqueidentifier

as 
begin
	Declare @customerIds table(CustomerId uniqueidentifier)
	insert into @customerIds  exec Members_GetCustomersPermissions @MemberId
	
select CustomerId,BusinessName from Customers
where Customers.IsDeleted = 0 
			And (Customers.CustomerId in (select CustomerId from @customerIds))
			order by BusinessName asc

end
GO
/****** Object:  StoredProcedure [dbo].[Tasks_GetMyTasksNum]    Script Date: 10/27/2017 14:01:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Tasks_GetMyTasksNum]
	@AssignedTo uniqueidentifier,
	@MemberId uniqueidentifier
AS
BEGIN
Declare @customerIds table(CustomerId uniqueidentifier)
insert into @customerIds  exec Members_GetCustomersPermissions @MemberId

	select COUNT(*) from Tasks 
	LEFT JOIN Customers on Tasks.CustomerId = Customers.CustomerId
	where Tasks.Status = 0 AND
	AssignStatus = 1 and AssignedTo = @AssignedTo
	AND (Tasks.CustomerId in (select CustomerId from @customerIds) OR Tasks.CustomerId is Null)
END
GO
/****** Object:  StoredProcedure [dbo].[Tasks_GetMyOpenTasksNum]    Script Date: 10/27/2017 14:01:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Tasks_GetMyOpenTasksNum]
	@AssignedTo uniqueidentifier,
	@MemberId uniqueidentifier
AS
BEGIN
Declare @customerIds table(CustomerId uniqueidentifier)
	insert into @customerIds  exec Members_GetCustomersPermissions @MemberId
	
	select COUNT(*) from Tasks
	LEFT JOIN Customers on Tasks.CustomerId = Customers.CustomerId
	where Tasks.Status = 0 and Tasks.AssignStatus=0 and AssignedTo = @AssignedTo
	AND Tasks.CustomerId in (select CustomerId from @customerIds)
END
GO
/****** Object:  StoredProcedure [dbo].[Tasks_FindMyTasks]    Script Date: 10/27/2017 14:01:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Tasks_FindMyTasks]

--	@AssignedTo uniqueidentifier
--AS
--BEGIN
--	SELECT *
--	FROM Tasks
--	WHERE AssignedTo = @AssignedTo
--	AND [Status]=0 AND AssignStatus <>2
--	ORDER BY DueDate ASC
--END

@PageNumber INT,
@criteria nvarchar(max)=null,
@CreatedFrom datetime=null,
@CreatedTo datetime=null,
@DueFrom datetime=null,
@DueTo datetime=null,
@AssignedTo uniqueidentifier =null,
@CustomerId uniqueidentifier = null,
@MemberId uniqueidentifier,
@Status INT,
@AssignStatus INT
As
Begin
Declare @customerIds table(CustomerId uniqueidentifier)
insert into @customerIds  exec Members_GetCustomersPermissions @MemberId
SELECT *
FROM (
SELECT TotalPages = Ceiling(cast((count(*) OVER ()) as float)/20),*,ROW_NUMBER() OVER (ORDER BY TaskId) AS RowNum
FROM Tasks
where 
			(@Criteria IS NULL OR Title LIKE @Criteria OR [Description] LIKE @Criteria)
			AND
			(@AssignedTo is null OR AssignedTo = @AssignedTo)
			AND
			(@CreatedFrom is null OR  CreatedOn >= @CreatedFrom)
			AND
			(@CreatedTo is null OR CreatedOn <= @CreatedTo)
			AND
			(@DueFrom is null OR DueDate >= @DueFrom)
			AND
			(@DueTo is null OR DueDate <= @DueTo)
						AND
			(@CustomerId is null OR CustomerId = @CustomerId)
			AND
			 [Status]=0
			AND
			(CustomerId in (select CustomerId from @customerIds) OR CustomerId is Null)
			AND
			(@Status is null OR [Status] = @Status)
			AND
			(@AssignStatus is null OR AssignStatus = @AssignStatus)
			AND (Tasks.CustomerId in (select CustomerId from @customerIds) OR Tasks.CustomerId is Null)
			
			

) AS SOD
WHERE SOD.RowNum BETWEEN ((@PageNumber-1)*20)+1
AND 20*(@PageNumber)
ORDER BY DueDate ASC
End
GO
/****** Object:  StoredProcedure [dbo].[Issues_GetMyOpenIssuesNum]    Script Date: 10/27/2017 14:01:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Issues_GetMyOpenIssuesNum]
	@AssignedTo uniqueidentifier,
	@MemberId uniqueidentifier
AS
BEGIN
Declare @customerIds table(CustomerId uniqueidentifier)
	insert into @customerIds  exec Members_GetCustomersPermissions @MemberId
	
	select COUNT(*) from Issues
	LEFT JOIN Customers on Issues.CustomerId = Customers.CustomerId
	where Issues.Status = 0 and Issues.AssignStatus=0 and AssignedTo = @AssignedTo
	AND Issues.CustomerId in (select CustomerId from @customerIds)
	AND Customers.IsDeleted = 0
END
GO
/****** Object:  StoredProcedure [dbo].[Calls_GetMyScheduledCallsNum]    Script Date: 10/27/2017 14:01:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Calls_GetMyScheduledCallsNum]
	@AssignedTo uniqueidentifier,
	@MemberId uniqueidentifier
AS
BEGIN
Declare @customerIds table(CustomerId uniqueidentifier)
	insert into @customerIds  exec Members_GetCustomersPermissions @MemberId
	
	select COUNT(*) from Calls
	LEFT JOIN Customers on Calls.CustomerId = Customers.CustomerId
	where Calls.Status = 0 and AssignedTo = @AssignedTo
	AND Calls.CustomerId in (select CustomerId from @customerIds)
	AND Customers.IsDeleted = 0
END
GO
/****** Object:  StoredProcedure [dbo].[Calls_GetMyOpenCallsNum]    Script Date: 10/27/2017 14:01:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Calls_GetMyOpenCallsNum]
	@AssignedTo uniqueidentifier,
	@MemberId uniqueidentifier
AS
BEGIN
Declare @customerIds table(CustomerId uniqueidentifier)
	insert into @customerIds  exec Members_GetCustomersPermissions @MemberId
	
	select COUNT(*) from Calls
	LEFT JOIN Customers on Calls.CustomerId = Customers.CustomerId
	where Calls.Status = 0 and Calls.AssignStatus=0 and AssignedTo = @AssignedTo
	AND Calls.CustomerId in (select CustomerId from @customerIds)
	AND Customers.IsDeleted = 0
END
GO

update Configuration set DbVersion = DbVersion+1
