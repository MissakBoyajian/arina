USE [Triton]
GO
/****** Object:  StoredProcedure [dbo].[Members_SelectByEmail]    Script Date: 11/14/2017 09:09:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[RegistrationQuestions_SelectByEmail]
@Email nvarchar(50)
as
begin 
SELECT Members.QuestionId,QuestionText
	 FROM Members
	inner join RegistrationQuestions as ques on ques.QuestionId= Members.QuestionId 
	 where Email = @Email
end

