create proc OfflineMode_GetCalls
@MemberId uniqueidentifier,
@TimeStamp timestamp
As
Begin
Declare @customerIds table(CustomerId uniqueidentifier)
insert into @customerIds  exec Members_GetCustomersPermissions @MemberId

Select * 
from Calls 
where Calls.CustomerId in (select CustomerId from @customerIds)
end

create proc OfflineMode_GetTasks
@MemberId uniqueidentifier,
@TimeStamp timestamp
As
Begin
Declare @customerIds table(CustomerId uniqueidentifier)
insert into @customerIds  exec Members_GetCustomersPermissions @MemberId

Select * 
from tasks 
where tasks.CustomerId in (select CustomerId from @customerIds)
end

create proc OfflineMode_GetIssues
@MemberId uniqueidentifier,
@TimeStamp timestamp
As
Begin
Declare @customerIds table(CustomerId uniqueidentifier)
insert into @customerIds  exec Members_GetCustomersPermissions @MemberId

Select * 
from issues 
where issues.CustomerId in (select CustomerId from @customerIds)
end

create proc OfflineMode_GetProducts
@MemberId uniqueidentifier,
@TimeStamp timestamp
As
Begin
	DECLARE @MemberPermission int
	SET @MemberPermission=dbo.GetMemberPermissionToken (@MemberId)

Select * 
from products p
where (dbo.GetProductPermissionToken(p.ProductId)&@MemberPermission)>0
end

create proc OfflineMode_GetPromotions
@MemberId uniqueidentifier,
@TimeStamp timestamp
As
Begin
	DECLARE @MemberPermission int
	SET @MemberPermission=dbo.GetMemberPermissionToken (@MemberId)

Select * 
from Promotions 
where (dbo.GetPromotionPermissionToken(Promotions.PromotionId)&@MemberPermission)>0
end

create proc OfflineMode_GetPromotions
@MemberId uniqueidentifier,
@TimeStamp timestamp
As
Begin
	DECLARE @MemberPermission int
	SET @MemberPermission=dbo.GetMemberPermissionToken (@MemberId)

Select * 
from Promotions 
where (dbo.GetPromotionPermissionToken(Promotions.PromotionId)&@MemberPermission)>0
end

create proc OfflineMode_GetResources
@TimeStamp timestamp
As
Begin
Select * 
from Resources 
end

create proc OfflineMode_GetCustomers
@MemberId uniqueidentifier,
@TimeStamp timestamp
As
Begin
Declare @customerIds table(CustomerId uniqueidentifier)
insert into @customerIds  exec Members_GetCustomersPermissions @MemberId

Select * 
from Customers 
where Customers.CustomerId in (select CustomerId from @customerIds)
end

create proc OfflineMode_GetMembers
@MemberId uniqueidentifier,
@TimeStamp timestamp
As
Begin
	Declare @MembersIds table(MemberId uniqueidentifier)
	insert into @MembersIds  exec Members_GetMembersPermissions @MemberId

Select * 
from members 
where Members.MemberId in (select MemberId from @MembersIds)
end

update Configuration set DbVersion=DbVersion+1
