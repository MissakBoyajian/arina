USE [Triton]
GO
/****** Object:  StoredProcedure [dbo].[Members_SelectByEmailPassword]    Script Date: 10/12/2017 10:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[Members_SelectByEmailPassword]
@Email nvarchar(50),
@Password nvarchar(max)
as
begin 
SELECT * FROM Members
	left join Roles on Members.RoleId= roles.RoleId
	left join Zones on Members.ResponsibilityZoneId = zones.ZoneId
	left join RegistrationQuestions as ques on ques.QuestionId= Members.QuestionId 
	 where Email = @Email and [Password] = @Password and IsDisabled=0
end

update Configuration set DbVersion=2