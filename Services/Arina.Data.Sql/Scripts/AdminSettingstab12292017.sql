USE [Triton]
GO
/****** Object:  Table [dbo].[Territories]    Script Date: 12/29/2017 08:04:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Territories](
	[TerritoryId] [uniqueidentifier] NOT NULL,
	[TerritoryName] [nvarchar](max) NULL,
	[Percentage] [float] NULL,
 CONSTRAINT [PK_Territories] PRIMARY KEY CLUSTERED 
(
	[TerritoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Territories] ([TerritoryId], [TerritoryName], [Percentage]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f71', N'TerritoryA', 20)
INSERT [dbo].[Territories] ([TerritoryId], [TerritoryName], [Percentage]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f72', N'TerritoryB', 20)
INSERT [dbo].[Territories] ([TerritoryId], [TerritoryName], [Percentage]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f73', N'TerritoryC', 20)
INSERT [dbo].[Territories] ([TerritoryId], [TerritoryName], [Percentage]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f74', N'TerritoryD', 20)
INSERT [dbo].[Territories] ([TerritoryId], [TerritoryName], [Percentage]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f75', N'TerritoryE', 20)
INSERT [dbo].[Territories] ([TerritoryId], [TerritoryName], [Percentage]) VALUES (N'75a482ed-928b-4421-b1b7-6c33042f9bb4', N'TerritoryNEWWW', 20)
INSERT [dbo].[Territories] ([TerritoryId], [TerritoryName], [Percentage]) VALUES (N'2814c650-1873-4fcc-88a4-876269bc53d1', N'edhdg', 40)
/****** Object:  Table [dbo].[MembersTerritories]    Script Date: 12/29/2017 08:04:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MembersTerritories](
	[MemberId] [uniqueidentifier] NOT NULL,
	[TerritoryId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_MembersTerritories] PRIMARY KEY CLUSTERED 
(
	[MemberId] ASC,
	[TerritoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DealerGroups]    Script Date: 12/29/2017 08:04:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DealerGroups](
	[DealerGroupId] [uniqueidentifier] NOT NULL,
	[BusinessName] [nvarchar](max) NULL,
 CONSTRAINT [PK_DealerGroups] PRIMARY KEY CLUSTERED 
(
	[DealerGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[DealerGroups] ([DealerGroupId], [BusinessName]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f70', N'GroupZ11')
INSERT [dbo].[DealerGroups] ([DealerGroupId], [BusinessName]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f71', N'GroupB')
INSERT [dbo].[DealerGroups] ([DealerGroupId], [BusinessName]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f72', N'GroupA')
INSERT [dbo].[DealerGroups] ([DealerGroupId], [BusinessName]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f73', N'GroupD')
INSERT [dbo].[DealerGroups] ([DealerGroupId], [BusinessName]) VALUES (N'1cd6cf58-5715-4cf9-8664-29be93eccc81', N'GroupKusa')
INSERT [dbo].[DealerGroups] ([DealerGroupId], [BusinessName]) VALUES (N'bcc31a66-ee39-4dea-ae7e-304589751d70', N'GroupK')
INSERT [dbo].[DealerGroups] ([DealerGroupId], [BusinessName]) VALUES (N'54e6cfb1-ee37-40e0-837e-41cd774134db', N'GroupStart')
INSERT [dbo].[DealerGroups] ([DealerGroupId], [BusinessName]) VALUES (N'dc728986-6606-499e-a955-89e6f5a8f33c', N'trash1')
INSERT [dbo].[DealerGroups] ([DealerGroupId], [BusinessName]) VALUES (N'a234b44c-6913-4b58-adb2-9927bbb3b130', N'trash')
INSERT [dbo].[DealerGroups] ([DealerGroupId], [BusinessName]) VALUES (N'9d9ca508-d4db-4807-8507-e6a840818104', N'Group88')
INSERT [dbo].[DealerGroups] ([DealerGroupId], [BusinessName]) VALUES (N'f6c91181-8823-4503-821a-e9bf750d4bd8', N'GroupN00')
/****** Object:  Table [dbo].[DealerSiteCategories]    Script Date: 12/29/2017 08:04:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DealerSiteCategories](
	[DealerSiteId] [uniqueidentifier] NOT NULL,
	[CategoryId] [uniqueidentifier] NOT NULL,
	[Percentage] [float] NULL,
 CONSTRAINT [PK_DealerSiteCategories] PRIMARY KEY CLUSTERED 
(
	[DealerSiteId] ASC,
	[CategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DealerSiteBrands]    Script Date: 12/29/2017 08:04:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DealerSiteBrands](
	[BrandId] [uniqueidentifier] NOT NULL,
	[DealerSiteId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_DealerSiteBrands] PRIMARY KEY CLUSTERED 
(
	[BrandId] ASC,
	[DealerSiteId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[DealerSiteCategories_Delete]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DealerSiteCategories_Delete]
	@CategoryId uniqueidentifier,
	@DealerSiteId  uniqueidentifier
AS
BEGIN
	DELETE FROM DealerSiteCategories
    WHERE CategoryId=@CategoryId and @DealerSiteId=DealerSiteId
END
GO
/****** Object:  StoredProcedure [dbo].[DealerGroups_SelectById]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[DealerGroups_SelectById]
	@DealerGroupId uniqueidentifier 
AS
BEGIN
	SELECT * FROM DealerGroups WHERE DealerGroupId=@DealerGroupId
END
GO
/****** Object:  StoredProcedure [dbo].[DealerGroups_Save]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DealerGroups_Save]
	@DealerGroupId uniqueidentifier,
	@BusinessName nvarchar(max)

AS
BEGIN
	IF (SELECT COUNT(*)FROM DealerGroups WHERE DealerGroupId=@DealerGroupId)=0
		BEGIN
			INSERT INTO DealerGroups(DealerGroupId,BusinessName)
			VALUES (@DealerGroupId,@BusinessName)
		END
	ELSE
		BEGIN
			UPDATE DealerGroups
			SET
				BusinessName=@BusinessName
			WHERE
				DealerGroupId=@DealerGroupId
		END
END
GO
/****** Object:  StoredProcedure [dbo].[DealerGroups_Find]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DealerGroups_Find]
	@Criteria nvarchar(max)
AS
BEGIN
	SELECT * FROM DealerGroups
	where BusinessName like @Criteria 
END
GO
/****** Object:  StoredProcedure [dbo].[DealerGroups_Delete]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[DealerGroups_Delete]
	@DealerGroupId uniqueidentifier 
AS
BEGIN
	DELETE FROM DealerGroups
    WHERE DealerGroupId=@DealerGroupId
END
GO
/****** Object:  Table [dbo].[DealerSites]    Script Date: 12/29/2017 08:04:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DealerSites](
	[DealerSiteId] [uniqueidentifier] NOT NULL,
	[DealerGroupId] [uniqueidentifier] NULL,
	[BusinessName] [nvarchar](max) NULL,
 CONSTRAINT [PK_DealerSite] PRIMARY KEY CLUSTERED 
(
	[DealerSiteId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[DealerSites] ([DealerSiteId], [DealerGroupId], [BusinessName]) VALUES (N'4b65c437-df3b-427e-ae42-02e28d87b8c2', N'39a2b5e6-c1bd-4396-b703-11e9e0220f71', N'SiteUU')
INSERT [dbo].[DealerSites] ([DealerSiteId], [DealerGroupId], [BusinessName]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f71', N'39a2b5e6-c1bd-4396-b703-11e9e0220f70', N'SiteMD')
INSERT [dbo].[DealerSites] ([DealerSiteId], [DealerGroupId], [BusinessName]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f72', N'39a2b5e6-c1bd-4396-b703-11e9e0220f70', N'SiteB')
INSERT [dbo].[DealerSites] ([DealerSiteId], [DealerGroupId], [BusinessName]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f73', N'39a2b5e6-c1bd-4396-b703-11e9e0220f71', N'SiteC')
INSERT [dbo].[DealerSites] ([DealerSiteId], [DealerGroupId], [BusinessName]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f74', N'39a2b5e6-c1bd-4396-b703-11e9e0220f71', N'SiteD')
INSERT [dbo].[DealerSites] ([DealerSiteId], [DealerGroupId], [BusinessName]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f75', N'39a2b5e6-c1bd-4396-b703-11e9e0220f72', N'SiteFF')
INSERT [dbo].[DealerSites] ([DealerSiteId], [DealerGroupId], [BusinessName]) VALUES (N'584d730d-8a61-480f-8566-2c244558dcdf', N'39a2b5e6-c1bd-4396-b703-11e9e0220f70', N'SiteLiko')
INSERT [dbo].[DealerSites] ([DealerSiteId], [DealerGroupId], [BusinessName]) VALUES (N'3e0b79e0-bdd3-4fbb-8bc7-3c20ff185c92', N'39a2b5e6-c1bd-4396-b703-11e9e0220f70', N'SiteMOM')
INSERT [dbo].[DealerSites] ([DealerSiteId], [DealerGroupId], [BusinessName]) VALUES (N'd35d98df-e296-48fa-8c98-3ebc5d8d08b2', N'39a2b5e6-c1bd-4396-b703-11e9e0220f71', N'SiteWWWW1')
INSERT [dbo].[DealerSites] ([DealerSiteId], [DealerGroupId], [BusinessName]) VALUES (N'60f9dbe2-ee95-46ac-97b6-73d44f718c7a', N'39a2b5e6-c1bd-4396-b703-11e9e0220f70', N'SiteKusa')
INSERT [dbo].[DealerSites] ([DealerSiteId], [DealerGroupId], [BusinessName]) VALUES (N'b4209060-966e-4ebd-8d9b-7e77aa31bd04', N'39a2b5e6-c1bd-4396-b703-11e9e0220f70', N'SiteHAS')
INSERT [dbo].[DealerSites] ([DealerSiteId], [DealerGroupId], [BusinessName]) VALUES (N'f862e204-1ac6-40d5-9e0e-d610603f2bcf', N'39a2b5e6-c1bd-4396-b703-11e9e0220f71', N'SiteQ')
INSERT [dbo].[DealerSites] ([DealerSiteId], [DealerGroupId], [BusinessName]) VALUES (N'351d30dc-1130-446b-b3cb-edb8222d90e2', N'1cd6cf58-5715-4cf9-8664-29be93eccc81', N'SiteSupperKusa')
INSERT [dbo].[DealerSites] ([DealerSiteId], [DealerGroupId], [BusinessName]) VALUES (N'6a5a2f5f-e1ae-41b6-8a61-fd0566619283', N'54e6cfb1-ee37-40e0-837e-41cd774134db', N'SiteStart')
/****** Object:  StoredProcedure [dbo].[Territory_SelectById]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Territory_SelectById]
	@TerritoryId uniqueidentifier 
AS
BEGIN
	SELECT * FROM Territories WHERE TerritoryId=@TerritoryId
END
GO
/****** Object:  StoredProcedure [dbo].[Territories_Save]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Territories_Save]
	@TerritoryId uniqueidentifier,
	@TerritoryName nvarchar(max),
	@Percentage float
AS
BEGIN
	IF (SELECT COUNT(*)FROM Territories WHERE TerritoryId=@TerritoryId)=0
		BEGIN
			INSERT INTO Territories(TerritoryId,TerritoryName,Percentage)
			VALUES (@TerritoryId,@TerritoryName,@Percentage)
		END
	ELSE
		BEGIN
			UPDATE Territories
			SET
				TerritoryName=@TerritoryName,
				Percentage=@Percentage
			WHERE
				TerritoryId=@TerritoryId
		END
END
GO
/****** Object:  StoredProcedure [dbo].[Territories_Find]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Territories_Find]
	@Criteria nvarchar(max)
AS
BEGIN
	SELECT * FROM Territories
	where TerritoryName like @Criteria 
END
GO
/****** Object:  StoredProcedure [dbo].[Territories_Delete]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Territories_Delete]
	@TerritoryId uniqueidentifier 
AS
BEGIN
	DELETE FROM Territories
    WHERE TerritoryId=@TerritoryId
END
GO
/****** Object:  StoredProcedure [dbo].[DealerSiteCategories_Save]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DealerSiteCategories_Save]
	@CategoryId uniqueidentifier,
	@DealerSiteId uniqueidentifier,
	@Percentage float
AS
BEGIN
	IF (SELECT COUNT(*)FROM DealerSiteCategories WHERE DealerSiteId=@DealerSiteId and CategoryId=@CategoryId)=0
		BEGIN
				INSERT INTO DealerSiteCategories(CategoryId,DealerSiteId,Percentage)
			VALUES (@CategoryId,@DealerSiteId,@Percentage)
		END
	ELSE
		BEGIN
			UPDATE DealerSiteCategories
			SET
				Percentage=@Percentage
			WHERE
				DealerSiteId=@DealerSiteId and CategoryId=@CategoryId
		END
END
GO
/****** Object:  StoredProcedure [dbo].[DealerSiteCategories_PercentSumPerDealerSite]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[DealerSiteCategories_PercentSumPerDealerSite]
	@DealerSiteId uniqueidentifier
AS
BEGIN

	DECLARE @StartOfMonth DATE = DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0)

	select SUM(Percentage)		
	 
			FROM DealerSiteCategories 
			WHERE DealerSiteId=@DealerSiteId
END
GO
/****** Object:  StoredProcedure [dbo].[DealerSiteCategories_Find]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DealerSiteCategories_Find]
	@DealerSiteId uniqueidentifier
AS
BEGIN
	SELECT *
	FROM DealerSiteCategories 
	inner join Categories on categories.CategoryId = DealerSiteCategories.CategoryId
	where DealerSiteId=@DealerSiteId 
END
GO
/****** Object:  Table [dbo].[DealerSitesTerritories]    Script Date: 12/29/2017 08:04:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DealerSitesTerritories](
	[TerritoryId] [uniqueidentifier] NOT NULL,
	[DealerSiteId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_DealerSitesTerritories] PRIMARY KEY CLUSTERED 
(
	[TerritoryId] ASC,
	[DealerSiteId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[DealerSitesTerritories] ([TerritoryId], [DealerSiteId]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f71', N'39a2b5e6-c1bd-4396-b703-11e9e0220f71')
INSERT [dbo].[DealerSitesTerritories] ([TerritoryId], [DealerSiteId]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f71', N'39a2b5e6-c1bd-4396-b703-11e9e0220f72')
INSERT [dbo].[DealerSitesTerritories] ([TerritoryId], [DealerSiteId]) VALUES (N'39a2b5e6-c1bd-4396-b703-11e9e0220f74', N'39a2b5e6-c1bd-4396-b703-11e9e0220f72')
INSERT [dbo].[DealerSitesTerritories] ([TerritoryId], [DealerSiteId]) VALUES (N'01c6c364-e2b6-464f-9cd6-1e672fe61181', N'39a2b5e6-c1bd-4396-b703-11e9e0220f72')
INSERT [dbo].[DealerSitesTerritories] ([TerritoryId], [DealerSiteId]) VALUES (N'83d757e3-d100-4992-a9ab-1f99f45371a3', N'39a2b5e6-c1bd-4396-b703-11e9e0220f72')
INSERT [dbo].[DealerSitesTerritories] ([TerritoryId], [DealerSiteId]) VALUES (N'32dcf9cf-0691-46b0-bff8-4b7459ee473d', N'39a2b5e6-c1bd-4396-b703-11e9e0220f72')
INSERT [dbo].[DealerSitesTerritories] ([TerritoryId], [DealerSiteId]) VALUES (N'75a482ed-928b-4421-b1b7-6c33042f9bb4', N'39a2b5e6-c1bd-4396-b703-11e9e0220f72')
INSERT [dbo].[DealerSitesTerritories] ([TerritoryId], [DealerSiteId]) VALUES (N'2814c650-1873-4fcc-88a4-876269bc53d1', N'39a2b5e6-c1bd-4396-b703-11e9e0220f72')
INSERT [dbo].[DealerSitesTerritories] ([TerritoryId], [DealerSiteId]) VALUES (N'cdd4aab7-4a08-43cd-bd0d-c938865341e5', N'39a2b5e6-c1bd-4396-b703-11e9e0220f72')
INSERT [dbo].[DealerSitesTerritories] ([TerritoryId], [DealerSiteId]) VALUES (N'ac94bed8-71b9-4d74-9a80-eb26811a9514', N'39a2b5e6-c1bd-4396-b703-11e9e0220f72')
INSERT [dbo].[DealerSitesTerritories] ([TerritoryId], [DealerSiteId]) VALUES (N'04c72862-bd12-404f-ba4a-f03fbfbc8d27', N'39a2b5e6-c1bd-4396-b703-11e9e0220f72')
/****** Object:  StoredProcedure [dbo].[DealerSites_SelectById]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DealerSites_SelectById]
	@DealerSiteId uniqueidentifier 
AS
BEGIN
	SELECT * FROM DealerSites WHERE DealerSiteId=@DealerSiteId
END
GO
/****** Object:  StoredProcedure [dbo].[DealerSites_SelectByGroup]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DealerSites_SelectByGroup]
	@DealerGroupId uniqueidentifier 
AS
BEGIN
select * from DealerSites where DealerGroupId=@DealerGroupId
END
GO
/****** Object:  StoredProcedure [dbo].[DealerSites_Save]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DealerSites_Save]
	@DealerSiteId uniqueidentifier,
	@BusinessName nvarchar(max),
	@DealerGroupId uniqueidentifier
AS
BEGIN
	IF (SELECT COUNT(*)FROM DealerSites WHERE DealerSiteId=@DealerSiteId)=0
		BEGIN
			INSERT INTO DealerSites(DealerSiteId,BusinessName,DealerGroupId)
			VALUES (@DealerSiteId,@BusinessName,@DealerGroupId)
		END
	ELSE
		BEGIN
			UPDATE DealerSites
			SET
				BusinessName=@BusinessName,
				DealerGroupId=@DealerGroupId
			WHERE
				DealerSiteId=@DealerSiteId
		END
END
GO
/****** Object:  StoredProcedure [dbo].[DealerSites_Find]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DealerSites_Find]
	@Criteria nvarchar(max)
AS
BEGIN
	SELECT * FROM DealerSites
	where BusinessName like @Criteria 
END
GO
/****** Object:  StoredProcedure [dbo].[DealerSites_Delete]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DealerSites_Delete]
	@DealerSiteId uniqueidentifier 
AS
BEGIN
	DELETE FROM DealerSites
    WHERE @DealerSiteId=@DealerSiteId
END
GO
/****** Object:  StoredProcedure [dbo].[DealerSiteTerritories_Save]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[DealerSiteTerritories_Save]
@DealerSiteId uniqueidentifier,
@TerritoryId uniqueidentifier

as
begin
insert into DealerSitesTerritories(TerritoryId,DealerSiteId) values(@TerritoryId,@DealerSiteId)
end
GO
/****** Object:  StoredProcedure [dbo].[DealerSiteTerritories_Delete]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[DealerSiteTerritories_Delete]
@DealerSiteId uniqueidentifier,
@TerritoryId uniqueidentifier

as
begin
delete from DealerSitesTerritories where DealerSiteId=@DealerSiteId and TerritoryId=@TerritoryId
end
GO
/****** Object:  StoredProcedure [dbo].[Territories_SelectByDealerSite]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Territories_SelectByDealerSite]
	@DealerSiteId uniqueidentifier
AS
BEGIN

	DECLARE @StartOfMonth DATE = DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0)

	select te.*,
	 	   (select COUNT(*)
			FROM Members as m
			inner join
			MembersTerritories as mt on m.MemberId=mt.MemberId
			inner join 
			Territories as t on t.TerritoryId = mt.TerritoryId 
			WHERE t.TerritoryId=te.TerritoryId and RoleId='3af83181-9582-4f87-9960-37d01c8a5b29') as ManagersCount,
			
		   (select COUNT(*)
			FROM Members as m
			inner join
			MembersTerritories as mt on m.MemberId=mt.MemberId
			inner join 
			Territories as t on t.TerritoryId = mt.TerritoryId 
			WHERE t.TerritoryId=te.TerritoryId and RoleId='fcc0b8fe-d94f-4022-807f-f3cd5377e98e') as RepsCount,
			
		   (select Count(*)
			FROM customers 
			WHERE TerritoryId=te.TerritoryId) as CustomersCount,
			
		   (select Count(*)
			FROM DealerSitesTerritories 
			WHERE TerritoryId=te.TerritoryId) as DealerSiteCount,
    
		   (select Count(*) from Calls where CallDate >= @StartOfMonth and  CustomerId in 
		   (select CustomerId
			FROM customers 
			WHERE TerritoryId=te.TerritoryId)) as CallsCount		
	 
			FROM DealerSites ds
			inner join  
			DealerSitesTerritories as dst on ds.DealerSiteId = dst.DealerSiteId
			inner join
			Territories as te on te.TerritoryId = dst.TerritoryId 
			WHERE ds.DealerSiteId=@DealerSiteId
END
GO
/****** Object:  StoredProcedure [dbo].[Members_SelectByDealerSite]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Members_SelectByDealerSite]
	@DealerSiteId  uniqueidentifier
AS
BEGIN
	Select  m.MemberId,
			m.FirstName,
			m.LastName,
			m.Email,
			m.Mobile,
			r.RoleName, 
			(select COUNT(*) from calls where AssignedTo=m.MemberId) as CallsCount
	
	from Members as m inner join Roles as r on m.RoleId=r.RoleId 
	where DealerSiteId=@DealerSiteId and IsDisabled=0
END
GO
/****** Object:  StoredProcedure [dbo].[DealerSiteCategories_SelectByDealerSite]    Script Date: 12/29/2017 08:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DealerSiteCategories_SelectByDealerSite]
	@DealerSiteId uniqueidentifier
AS
BEGIN
	DECLARE @StartOfMonth DATE = DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0)

	SELECT dsc.*,cc.CategoryName,
		   (select Count(*)
			FROM CustomersCategories 
			WHERE CategoryId=cc.CategoryId) as CustomersCount,
    
		   (select Count(*) from Calls where CallDate >= @StartOfMonth and  CustomerId in 
		   (select CustomerId
			FROM CustomersCategories 
			WHERE CategoryId=cc.CategoryId)) as CallsCount
			
	FROM Categories as cc
	inner join 
	DealerSiteCategories as dsc on cc.CategoryId = dsc.CategoryId
	inner join
	DealerSites as ds on dsc.DealerSiteId = ds.DealerSiteId
	where ds.DealerSiteId=@DealerSiteId 
END
GO
/****** Object:  ForeignKey [FK_DealerSiteGroup]    Script Date: 12/29/2017 08:04:49 ******/
ALTER TABLE [dbo].[DealerSites]  WITH CHECK ADD  CONSTRAINT [FK_DealerSiteGroup] FOREIGN KEY([DealerGroupId])
REFERENCES [dbo].[DealerGroups] ([DealerGroupId])
GO
ALTER TABLE [dbo].[DealerSites] CHECK CONSTRAINT [FK_DealerSiteGroup]
GO
/****** Object:  ForeignKey [FK_DealerSiteId]    Script Date: 12/29/2017 08:04:49 ******/
ALTER TABLE [dbo].[DealerSitesTerritories]  WITH CHECK ADD  CONSTRAINT [FK_DealerSiteId] FOREIGN KEY([DealerSiteId])
REFERENCES [dbo].[DealerSites] ([DealerSiteId])
GO
ALTER TABLE [dbo].[DealerSitesTerritories] CHECK CONSTRAINT [FK_DealerSiteId]
GO
/****** Object:  ForeignKey [FK_TerritoryId]    Script Date: 12/29/2017 08:04:49 ******/
ALTER TABLE [dbo].[Territories]  WITH CHECK ADD  CONSTRAINT [FK_TerritoryId] FOREIGN KEY([TerritoryId])
REFERENCES [dbo].[Territories] ([TerritoryId])
GO
ALTER TABLE [dbo].[Territories] CHECK CONSTRAINT [FK_TerritoryId]
GO

update Configuration set DbVersion=DbVersion+1