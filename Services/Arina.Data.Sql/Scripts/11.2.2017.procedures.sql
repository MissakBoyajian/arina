USE NetLube
GO
/****** Object:  StoredProcedure [dbo].[Products_Find]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[Products_Find]
GO
/****** Object:  StoredProcedure [dbo].[Promotions_Find]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[Promotions_Find]
GO
/****** Object:  StoredProcedure [dbo].[Issues_FindMyOpenIssues]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[Issues_FindMyOpenIssues]
GO
/****** Object:  StoredProcedure [dbo].[Issues_FindMySolvedIssues]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[Issues_FindMySolvedIssues]
GO
/****** Object:  StoredProcedure [dbo].[Calls_FindMyPublishedCalls]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[Calls_FindMyPublishedCalls]
GO
/****** Object:  StoredProcedure [dbo].[Calls_FindMyScheduledCalls]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[Calls_FindMyScheduledCalls]
GO
/****** Object:  StoredProcedure [dbo].[Calls_FindMyToReviewCalls]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[Calls_FindMyToReviewCalls]
GO
/****** Object:  StoredProcedure [dbo].[Customers_Pagination]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[Customers_Pagination]
GO
/****** Object:  StoredProcedure [dbo].[CustomersName]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[CustomersName]
GO
/****** Object:  StoredProcedure [dbo].[Tasks_FindCompletedTasks]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[Tasks_FindCompletedTasks]
GO
/****** Object:  StoredProcedure [dbo].[Tasks_FindMyTasks]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[Tasks_FindMyTasks]
GO
/****** Object:  StoredProcedure [dbo].[News_Save]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[News_Save]
GO
/****** Object:  StoredProcedure [dbo].[Promotions_Save]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[Promotions_Save]
GO
/****** Object:  StoredProcedure [dbo].[Sales_RollingAverageForBrands]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[Sales_RollingAverageForBrands]
GO
/****** Object:  StoredProcedure [dbo].[Sales_YTD]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[Sales_YTD]
GO
/****** Object:  StoredProcedure [dbo].[Sales_Credits]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[Sales_Credits]
GO
/****** Object:  StoredProcedure [dbo].[Sales_MonthToDateForBrands]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[Sales_MonthToDateForBrands]
GO
/****** Object:  StoredProcedure [dbo].[Sales_QTD]    Script Date: 11/02/2017 08:24:03 ******/
DROP PROCEDURE [dbo].[Sales_QTD]
GO
/****** Object:  StoredProcedure [dbo].[Sales_QTD]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sales_QTD]
AS
BEGIN

	DECLARE @CurrentDate DATE = GETDATE()
	
	if MONTH(@CurrentDate) =1 or MONTH(@CurrentDate) =2  or MONTH(@CurrentDate) = 3
	BEGIN
		SELECT SUM(TotalAmount)  as QTD
		FROM SalesData 
		WHERE [MONTH] = 1 or [MONTH] = 2 or [MONTH] = 3 and [YEAR] = YEAR(@CurrentDate)
	END
	
	else if MONTH(@CurrentDate) =4 or MONTH(@CurrentDate) =5  or MONTH(@CurrentDate) = 6
	BEGIN
		SELECT SUM(TotalAmount)  as QTD
		FROM SalesData 
		WHERE [MONTH] = 4 or [MONTH] = 5 or [MONTH] = 6 and [YEAR] = YEAR(@CurrentDate)
	END
	
	else if MONTH(@CurrentDate) =7 or MONTH(@CurrentDate) =8  or MONTH(@CurrentDate) = 9
	BEGIN
		SELECT SUM(TotalAmount)  as QTD
		FROM SalesData 
		WHERE [MONTH] = 7 or [MONTH] = 8 or [MONTH] = 9 and [YEAR] = YEAR(@CurrentDate)
	END
	
	else if MONTH(@CurrentDate) =10 or MONTH(@CurrentDate) =11  or MONTH(@CurrentDate) = 12
	BEGIN
		SELECT SUM(TotalAmount)  as QTD
		FROM SalesData 
		WHERE [MONTH] = 10 or [MONTH] = 11 or [MONTH] = 12 and [YEAR] = YEAR(@CurrentDate)
	END
	
END
GO
/****** Object:  StoredProcedure [dbo].[Sales_MonthToDateForBrands]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sales_MonthToDateForBrands]
AS
BEGIN
	DECLARE @CurrentDate DATE = GETDATE()
	SELECT SalesData.BrandName,
		   SUM(TotalAmount)AS MonthToDate
	FROM SalesData 
	
	WHERE [Month] = MONTH(@CurrentDate) AND [Year] = YEAR(@CurrentDate) and BrandName is not null
	GROUP BY SalesData.BrandName
	ORDER BY SUM(TotalAmount) DESC
END
GO
/****** Object:  StoredProcedure [dbo].[Sales_Credits]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sales_Credits]
AS
BEGIN
	DECLARE @CurrentDate DATE = GETDATE()
	SELECT SUM(Credit)  as CreditsReturn FROM SalesData WHERE [Month] = MONTH(@CurrentDate) AND [Year] = YEAR(@CurrentDate)
END
GO
/****** Object:  StoredProcedure [dbo].[Sales_YTD]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sales_YTD]
AS
BEGIN
	DECLARE @CurrentDate DATE = GETDATE()
SELECT SUM(TotalAmount) as YearlyIncome FROM SalesData WHERE [Year] = YEAR(@CurrentDate) 
END
GO
/****** Object:  StoredProcedure [dbo].[Sales_RollingAverageForBrands]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sales_RollingAverageForBrands]

AS
BEGIN  

	DECLARE @Day int = DAY(GETDATE()) 
	DECLARE @MonthPriorOneDays int
	DECLARE @MonthPriorTwoDays int
	DECLARE @MonthPriorThreeDays int
	DECLARE @StartOfMonth DATE = DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0)
	DECLARE @CurrentDate DATE = GETDATE()
	DECLARE @WeekendDays int
	
-- subtract weekends untill current date for this month

	SELECT @WeekendDays = 
					  (DATEDIFF(wk, @StartOfMonth, @CurrentDate) * 2)
					  +(CASE WHEN DATEPART (dw, @StartOfMonth) = 1 THEN 1 ELSE 0 END)
					  +(CASE WHEN DATEPART (dw, @CurrentDate) = 7 THEN 1 ELSE 0 END)
  
	SET @Day= @Day-@WeekendDays

--get working days for each month
	
	SET @MonthPriorOneDays = DAY(DATEADD(DD,-1,DATEADD(mm, DATEDIFF(mm, 0, @CurrentDate), 0)))-8
	SET @MonthPriorTwoDays = DAY(DATEADD(DD,-1,DATEADD(mm, DATEDIFF(mm, 0, @CurrentDate)-1, 0)))-8
	SET @MonthPriorThreeDays = DAY(DATEADD(DD,-1,DATEADD(mm, DATEDIFF(mm, 0, @CurrentDate)-2, 0)))-8
	
-- create Temporary table and insert [Sales_MonthToDateForCustomers] proc to it
	CREATE TABLE #Temp(
		BrandName NVARCHAR(MAX),
		MonthToDate FLOAT
	)
	INSERT INTO #Temp EXEC dbo.[Sales_MonthToDateForBrands]
	
-- query sales for the past 3 month and calculate rolling average

	SELECT  SalesData.BrandName,
			#Temp.MonthToDate,
			@Day * SUM(TotalAmount) /(@MonthPriorOneDays+@MonthPriorTwoDays+@MonthPriorThreeDays) as RollingAverage
	FROM SalesData INNER JOIN #Temp ON #Temp.BrandName = SalesData.BrandName
	WHERE DATEADD(MONTH, 0, Cast(SalesData.[Year] as nvarchar(50))+'-'+Cast(SalesData.[Month] as nvarchar(50))+'-1') between DATEADD(MONTH, -3, @StartOfMonth) AND DATEADD(MONTH, 0, @StartOfMonth)
	GROUP BY SalesData.BrandName,#Temp.MonthToDate
	ORDER BY MonthToDate DESC
END
GO
/****** Object:  StoredProcedure [dbo].[Promotions_Save]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Promotions_Save]
	@PromotionId uniqueidentifier,
	@CategoryId uniqueidentifier,
	@Title nvarchar(max),
	@StartsOn datetime,
	@EndsOn datetime,
	@Summary ntext,
	@Terms ntext,
	@Order int,
	@ResourceId uniqueidentifier
AS
BEGIN

	IF (SELECT COUNT(*)FROM Promotions WHERE PromotionId=@PromotionId)=0
		BEGIN
		
			declare @MaxOrder int;
			Select @MaxOrder = MAX(pp.[Order]) from Promotions as pp
			SET @MaxOrder = @MaxOrder+1;
			
			
				INSERT INTO Promotions(PromotionId,CategoryId,Title,StartsOn,EndsOn,Summary,Terms,[Order],ResourceId,ViewNumber)
				VALUES (@PromotionId,@CategoryId,@Title,@StartsOn,@EndsOn,@Summary,@Terms,@MaxOrder,@ResourceId,1)
		END
	ELSE
		BEGIN
			UPDATE Promotions
			SET
				CategoryId=@CategoryId,
				Title=@Title,
				StartsOn=@StartsOn,
				EndsOn=@EndsOn,
				Summary=@Summary,
				Terms=@Terms,
				[Order]=@Order,
				ResourceId=@ResourceId
			WHERE
				PromotionId=@PromotionId
		END
END
GO
/****** Object:  StoredProcedure [dbo].[News_Save]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[News_Save]
	@NewsId uniqueidentifier,
	@CategoryId uniqueidentifier,
	@Subject nvarchar(max),
	@ShortDescription nvarchar(max),
	@Description nvarchar(max),
	@CreatedOn datetime,
	@CreatedBy uniqueidentifier,
	@Order int,
	@ResourceId uniqueidentifier
AS
BEGIN
	IF (SELECT COUNT(*)FROM News WHERE NewsId=@NewsId)=0
		BEGIN
		
			declare @MaxOrder int;
			Select @MaxOrder = MAX(pp.[Order]) from news as pp
			SET @MaxOrder = @MaxOrder+1;
			
			INSERT INTO News(NewsId,CategoryId,Subject,ShortDescription,Description,CreatedOn,CreatedBy,[Order],ResourceId)
			VALUES (@NewsId,@CategoryId,@Subject,@ShortDescription,@Description,@CreatedOn,@CreatedBy,@MaxOrder,@ResourceId)
		END
	ELSE
		BEGIN
			UPDATE News
			SET
				CategoryId=@CategoryId,
				Subject=@Subject,
				ShortDescription=@ShortDescription,
				Description=@Description,
				CreatedOn=@CreatedOn,
				[Order]=@Order,
				ResourceId =@ResourceId
			WHERE
				NewsId=@NewsId
		END
END
GO
/****** Object:  StoredProcedure [dbo].[Tasks_FindMyTasks]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Tasks_FindMyTasks]

--	@AssignedTo uniqueidentifier
--AS
--BEGIN
--	SELECT *
--	FROM Tasks
--	WHERE AssignedTo = @AssignedTo
--	AND [Status]=0 AND AssignStatus <>2
--	ORDER BY DueDate ASC
--END

@PageNumber INT,
@criteria nvarchar(max)=null,
@CreatedFrom datetime=null,
@CreatedTo datetime=null,
@DueFrom datetime=null,
@DueTo datetime=null,
@AssignedTo uniqueidentifier =null,
@CustomerId uniqueidentifier = null,
@MemberId uniqueidentifier,
@Status INT,
@AssignStatus INT
As
Begin
Declare @customerIds table(CustomerId uniqueidentifier)
insert into @customerIds  exec Members_GetCustomersPermissions @MemberId
SELECT *
FROM (
SELECT TotalPages = Ceiling(cast((count(*) OVER ()) as float)/20),*,ROW_NUMBER() OVER (ORDER BY TaskId) AS RowNum
FROM Tasks
where 
			(@Criteria IS NULL OR Title LIKE @Criteria OR Title LIKE ('% '+@Criteria) OR [Description] LIKE @Criteria)
			AND
			(@AssignedTo is null OR AssignedTo = @AssignedTo)
			AND
			(@CreatedFrom is null OR  CreatedOn >= @CreatedFrom)
			AND
			(@CreatedTo is null OR CreatedOn <= @CreatedTo)
			AND
			(@DueFrom is null OR DueDate >= @DueFrom)
			AND
			(@DueTo is null OR DueDate <= @DueTo)
						AND
			(@CustomerId is null OR CustomerId = @CustomerId)
			AND
			 [Status]=0
			AND
			(CustomerId in (select CustomerId from @customerIds) OR CustomerId is Null)
			AND
			(@Status is null OR [Status] = @Status)
			AND
			(@AssignStatus is null OR AssignStatus = @AssignStatus)
			AND (Tasks.CustomerId in (select CustomerId from @customerIds) OR Tasks.CustomerId is Null)
			
			

) AS SOD
WHERE SOD.RowNum BETWEEN ((@PageNumber-1)*20)+1
AND 20*(@PageNumber)
ORDER BY DueDate ASC
End
GO
/****** Object:  StoredProcedure [dbo].[Tasks_FindCompletedTasks]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Tasks_FindCompletedTasks]

--	@AssignedTo uniqueidentifier,
--	@CreatedBy uniqueidentifier

	
--AS
--BEGIN
--	SELECT *
--	FROM Tasks
--	WHERE (AssignedTo = @AssignedTo OR CreatedBy = @CreatedBy) AND [status] = 1
--	ORDER BY CreatedOn DESC
--END

@PageNumber INT,
@criteria nvarchar(max) =null,
@CreatedFrom datetime=null,
@CreatedTo datetime=null,
@DueFrom datetime=null,
@DueTo datetime=null,
@AssignedTo uniqueidentifier =null,
@CreatedBy uniqueidentifier =null,
@CustomerId uniqueidentifier = null,
@MemberId uniqueidentifier

As
Begin
Declare @customerIds table(CustomerId uniqueidentifier)
insert into @customerIds  exec Members_GetCustomersPermissions @MemberId
SELECT *
FROM (
SELECT TotalPages = Ceiling(cast((count(*) OVER ()) as float)/20),*,ROW_NUMBER() OVER (ORDER BY TaskId) AS RowNum
FROM Tasks
where 
			(@Criteria IS NULL OR Title LIKE @Criteria OR Title LIKE ('% '+@Criteria) OR [Description] LIKE @Criteria)
			AND
			(@AssignedTo is null OR AssignedTo = @AssignedTo)
			AND
			( @CreatedFrom is null OR CreatedOn >= @CreatedFrom)
			AND
			( @CreatedTo is null OR CreatedOn <= @CreatedTo)
			AND
			( @DueFrom is null OR DueDate >= @DueFrom)
			AND
			( @DueTo is null OR DueDate <= @DueTo)
						AND
			(@CustomerId is null OR CustomerId = @CustomerId)
			AND
			((@AssignedTo is null OR AssignedTo = @AssignedTo) OR (@CreatedBy is null OR CreatedBy = @CreatedBy))
		    AND 
		    [status] = 1
		    			AND
			(CustomerId in (select CustomerId from @customerIds) OR CustomerId is Null)

) AS SOD
WHERE SOD.RowNum BETWEEN ((@PageNumber-1)*20)+1
AND 20*(@PageNumber)
ORDER BY CreatedOn DESC
End
GO
/****** Object:  StoredProcedure [dbo].[CustomersName]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[CustomersName]
@PageNumber INT,
@Criteria nvarchar(max) = NULL,
@MemberId uniqueidentifier

As
Begin

	Declare @customerIds table(CustomerId uniqueidentifier)
	insert into @customerIds  exec Members_GetCustomersPermissions @MemberId

	SELECT * FROM (
		SELECT  TotalPages = Ceiling(cast((count(*) OVER ()) as float)/50),
				Customers.CustomerId,
				Customers.BusinessName,
				Customers.Email,
				ROW_NUMBER() OVER (
				ORDER BY 
				Customers.BusinessName) AS RowNum
				
		FROM Customers 
		WHERE @Criteria IS NULL OR BusinessName LIKE @Criteria
		and customers.Email is not null
			
	) AS SOD	
WHERE
	SOD.RowNum BETWEEN ((@PageNumber-1)*50)+1
	AND 50*(@PageNumber)
End
GO
/****** Object:  StoredProcedure [dbo].[Customers_Pagination]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Customers_Pagination]
@PageNumber INT,
@Criteria nvarchar(max) = NULL,
@CategoryId nvarchar(max) = NULL,
@Lat float = NULL,
@Long float = NULL,
@CustomerTypeId nvarchar(max) = NULL,
@MemberId uniqueidentifier,
@State nvarchar(max) = NULL,
@SortBy int,
@Order bit = 0
As
Begin

	Declare @customerIds table(CustomerId uniqueidentifier)
	insert into @customerIds  exec Members_GetCustomersPermissions @MemberId

	SELECT * FROM (
		SELECT  TotalPages = Ceiling(cast((count(*) OVER ()) as float)/20),
				cus.*,
				ct.CustomerTypeName,
				at.AccountTypeName,
				cat.*,
				parcat.CategoryId as ParentCategory,
				parcat.CategoryName as ParentCategoryName,
				parcat.CategoryType as ParentCategoryType,
				adr.*,
				lastcall.calldate as LastCallDate,
				lastcall.id as LastCallId,
				firstcall.calldate as NextCallDate,
				firstcall.id as NextCallId,
				ROW_NUMBER() OVER (
				ORDER BY 
				CASE WHEN @SortBy=0 AND @Order=1 THEN BusinessName END ASC, 
				CASE WHEN @SortBy=0 AND @Order=0 THEN BusinessName END DESC,
				CASE WHEN @SortBy=1 AND @Order=1 THEN cat.CategoryName END ASC,
				CASE WHEN @SortBy=1 AND @Order=0 THEN cat.CategoryName END DESC) AS RowNum
		FROM Customers cus
			left join CustomerTypes ct on  ct.CustomerTypeId = cus.CustomerTypeId
			left join AccountTypes at on at.AccountTypeId = cus.AccountTypeId
			--left join CustomersCategories cuscat on  cuscat.CustomerId = cus.CustomerId
			--Left join Addresses adr on adr.ReferrerId = cus.CustomerId
			outer apply (select top 1 Calls.CallDate, 
			                          Calls.CallId from Calls 
			                          where Calls.CustomerId = cus.CustomerId
			                          and Calls.CallDate < getdate() order by Calls.CallDate desc)
			                          lastcall(calldate, id)
			
			outer apply (select top 1 Calls.calldate, 
									  Calls.CallId from Calls
									  where Calls.CustomerId = cus.CustomerId
									  and Calls.calldate >= getdate() order by calldate asc)
									  firstcall(calldate, id)

			outer apply (Select top 1 * FROM Addresses where ReferrerId=cus.CustomerId)adr
			outer apply (Select top 1 * FROM Categories WHERE CategoryId in (SELECT CategoryId FROM CustomersCategories WHERE CustomerId=cus.CustomerId)) cat
			outer apply (Select top 1 * FROM Categories WHERE CategoryId = cat.ParentCategoryId) parcat
			
			
			
		WHERE ((
			(@Criteria IS NULL OR BusinessName LIKE @Criteria OR BusinessName LIKE ('% '+@Criteria) OR AccountNumber LIKE @Criteria OR ABN LIKE @Criteria))
			AND(len(@CategoryId) <10 OR  @CategoryId like ('%' +convert(nvarchar(50), cat.CategoryId) + '%') or cat.CategoryId in (SELECT CategoryId from Categories where @CategoryId like ('%' +convert(nvarchar(50), ParentCategoryId) + '%')))
			AND((len(@CustomerTypeId) <10 OR  @CustomerTypeId like ('%' +convert(nvarchar(50), cus.CustomerTypeId) + '%')))
			--AND(@Lat IS NULL OR adr.Lat = @Lat)
			--AND (@Long IS NULL OR adr.Lon = @Long)
			And (cus.CustomerId in (select CustomerId from @customerIds)))
			and cus.IsDeleted=0
			and (@State IS NULL OR  (@State like '%' + convert(nvarchar(50),PhysicalState) + '%' AND LTRIM(RTRIM(PhysicalState))<>''))
	) AS SOD	
WHERE
	SOD.RowNum BETWEEN ((@PageNumber-1)*20)+1
	AND 20*(@PageNumber)

End
GO
/****** Object:  StoredProcedure [dbo].[Calls_FindMyToReviewCalls]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Calls_FindMyToReviewCalls]


@PageNumber INT,
@criteria nvarchar(max) =null,
@CreatedFrom datetime=null,
@CreatedTo datetime=null,
@AssignedTo uniqueidentifier =null,
@CustomerId uniqueidentifier = null,
@MemberId uniqueidentifier
As
Begin
Declare @customerIds table(CustomerId uniqueidentifier)
insert into @customerIds  exec Members_GetCustomersPermissions @MemberId
SELECT *
FROM (
SELECT TotalPages = Ceiling(cast((count(*) OVER ()) as float)/20),Calls.*,ROW_NUMBER() OVER (ORDER BY CallId) AS RowNum
FROM Calls
LEFT JOIN Customers on Calls.CustomerId = Customers.CustomerId
where 
			(@Criteria IS NULL OR Customers.BusinessName LIKE @Criteria OR Customers.BusinessName LIKE ('% '+@Criteria))
			AND
			(@AssignedTo is null OR AssignedTo = @AssignedTo)
			AND
			(@CreatedFrom is null OR CallDate >= @CreatedFrom)
			AND
			(@CustomerId is null OR Calls.CustomerId = @CustomerId)
			AND
			(@CreatedTo is null OR CallDate <= @CreatedTo)
			AND
			Calls.[Status]=1
			AND
			Calls.CustomerId in (select CustomerId from @customerIds)
			AND
			Customers.IsDeleted = 0
) AS SOD
WHERE SOD.RowNum BETWEEN ((@PageNumber-1)*20)+1
AND 20*(@PageNumber)
ORDER BY CallDate ASC
End
GO
/****** Object:  StoredProcedure [dbo].[Calls_FindMyScheduledCalls]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Calls_FindMyScheduledCalls]

--	@AssignedTo uniqueidentifier
--AS
--BEGIN
--	SELECT *
--	FROM Calls
--	WHERE AssignedTo = @AssignedTo
--	AND [Status]=0 AND AssignStatus <>2
--	ORDER BY CallDate ASC
--END


@PageNumber INT,
@criteria nvarchar(max) =null,
@CreatedFrom datetime=null,
@CreatedTo datetime=null,
@AssignedTo uniqueidentifier =null,
@MemberId uniqueidentifier,
@CustomerId uniqueidentifier,
@Status INT,
@AssignStatus INT

As
Begin
Declare @customerIds table(CustomerId uniqueidentifier)
insert into @customerIds  exec Members_GetCustomersPermissions @MemberId
SELECT *
FROM (
SELECT TotalPages = Ceiling(cast((count(*) OVER ()) as float)/20),Calls.*,ROW_NUMBER() OVER (ORDER BY CallId) AS RowNum
FROM Calls LEFT JOIN Customers on Calls.CustomerId = Customers.CustomerId
where 
			(@Criteria IS NULL OR Customers.BusinessName LIKE @Criteria OR Customers.BusinessName LIKE ('% '+@Criteria))
			AND
			(@AssignedTo is null OR AssignedTo = @AssignedTo)
			AND
			(@CreatedFrom is null OR CallDate >= @CreatedFrom)
			AND
			(@CreatedTo is null OR CallDate <= @CreatedTo)
			AND
			(@CustomerId is null OR Calls.CustomerId = @CustomerId)
			AND
			 Calls.[Status]=0
		    AND 
		    AssignStatus <>2
		    AND
			 Calls.CustomerId in (select CustomerId from @customerIds)
			AND
			(@Status is null OR Calls.[Status] = @Status)
			AND
			(@AssignStatus is null OR AssignStatus = @AssignStatus)
						AND
			Customers.IsDeleted = 0
) AS SOD
WHERE SOD.RowNum BETWEEN ((@PageNumber-1)*20)+1
AND 20*(@PageNumber)
ORDER BY CallDate DESC
End
GO
/****** Object:  StoredProcedure [dbo].[Calls_FindMyPublishedCalls]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Calls_FindMyPublishedCalls]

@PageNumber INT,
@criteria nvarchar(max) =null,
@CreatedFrom datetime=null,
@CreatedTo datetime=null,
@AssignedTo uniqueidentifier =null,
@MemberId uniqueidentifier,
@CustomerId uniqueidentifier = null

As
Begin

Declare @customerIds table(CustomerId uniqueidentifier)
insert into @customerIds  exec Members_GetCustomersPermissions @MemberId

SELECT *
FROM (
SELECT TotalPages = Ceiling(cast((count(*) OVER ()) as float)/20),Calls.*,ROW_NUMBER() OVER (ORDER BY CallId) AS RowNum
FROM Calls 
LEFT JOIN Customers on Calls.CustomerId = Customers.CustomerId
where 
			(@Criteria IS NULL OR Customers.BusinessName LIKE @Criteria OR Customers.BusinessName LIKE ('% '+@Criteria))
			AND
			(@AssignedTo is null OR AssignedTo = @AssignedTo)
			AND
			(@CreatedFrom is null OR CallDate >= @CreatedFrom)
			AND
			(@CreatedTo is null OR CallDate <= @CreatedTo)
			AND
			(@CustomerId is null OR Calls.CustomerId = @CustomerId)
			AND
			Calls.[Status]=2
			AND
			Calls.CustomerId in (select CustomerId from @customerIds)
			AND
			Customers.IsDeleted = 0

) AS SOD
WHERE SOD.RowNum BETWEEN ((@PageNumber-1)*20)+1
AND 20*(@PageNumber)
ORDER BY CallDate ASC
End
GO
/****** Object:  StoredProcedure [dbo].[Issues_FindMySolvedIssues]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Issues_FindMySolvedIssues]

@PageNumber INT,
@Priority int =null,
@criteria nvarchar(max) =null,
@CreatedFrom datetime=null,
@CreatedTo datetime=null,
@AssignedTo uniqueidentifier =null,
@CustomerId uniqueidentifier = null,
@MemberId uniqueidentifier
As
Begin
Declare @customerIds table(CustomerId uniqueidentifier)
insert into @customerIds  exec Members_GetCustomersPermissions @MemberId
SELECT *
FROM (
SELECT TotalPages = Ceiling(cast((count(*) OVER ()) as float)/20),*,ROW_NUMBER() OVER (ORDER BY IssueId) AS RowNum
FROM Issues
where 
			(@Criteria IS NULL OR Title LIKE @Criteria OR Title LIKE ('% '+@Criteria) OR [Description] LIKE @Criteria OR [Description] LIKE ('% '+@Criteria))
			AND
			(@AssignedTo is null OR AssignedTo = @AssignedTo)
			AND
			(@CreatedFrom is null OR CreatedOn >= @CreatedFrom)
			AND
			(@CreatedTo is null OR CreatedOn <= @CreatedTo)
			AND
			(@CustomerId is null OR CustomerId = @CustomerId)
			AND
			(@Priority is null OR Priority=@Priority)
			AND
			 [Status]=2
			AND
			CustomerId in (select CustomerId from @customerIds)

) AS SOD
WHERE SOD.RowNum BETWEEN ((@PageNumber-1)*20)+1
AND 20*(@PageNumber)
ORDER BY CreatedOn ASC
End
GO
/****** Object:  StoredProcedure [dbo].[Issues_FindMyOpenIssues]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Issues_FindMyOpenIssues]
--	@AssignedTo uniqueidentifier
--AS
--BEGIN
--	SELECT * 
--	FROM Issues 
--	WHERE AssignedTo = @AssignedTo
--		AND [Status]=0
--	    AND AssignStatus <>2
--	ORDER BY CreatedOn ASC
--END


@PageNumber INT,
@Priority int =null,
@criteria nvarchar(max) =null,
@CreatedFrom datetime=null,
@CreatedTo datetime=null,
@AssignedTo uniqueidentifier =null,
@CustomerId uniqueidentifier = null,
@MemberId uniqueidentifier,
@Status INT,
@AssignStatus INT

As
Begin
Declare @customerIds table(CustomerId uniqueidentifier)
insert into @customerIds  exec Members_GetCustomersPermissions @MemberId
SELECT *
FROM (
SELECT TotalPages = Ceiling(cast((count(*) OVER ()) as float)/20),*,ROW_NUMBER() OVER (ORDER BY IssueId) AS RowNum
FROM Issues
where 
			(@Criteria IS NULL OR Title LIKE @Criteria OR Title LIKE ('% '+@Criteria) OR [Description] LIKE @Criteria OR [Description] LIKE ('% '+@Criteria))
			AND
			(@AssignedTo is null OR AssignedTo = @AssignedTo)
			AND
			(@CreatedFrom is null OR CreatedOn >= @CreatedFrom)
			AND
			(@CreatedTo is null OR CreatedOn <= @CreatedTo)
			AND
			(@CustomerId is null OR CustomerId = @CustomerId)
			AND 
			(@Priority is null OR Priority=@Priority)
			AND
			[Status]=0
			AND
			 AssignStatus <>2
			AND
			CustomerId in (select CustomerId from @customerIds)
			AND
			(@Status is null OR [Status] = @Status)
			AND
			(@AssignStatus is null OR AssignStatus = @AssignStatus)

) AS SOD
WHERE SOD.RowNum BETWEEN ((@PageNumber-1)*20)+1
AND 20*(@PageNumber)
ORDER BY CreatedOn ASC
End
GO
/****** Object:  StoredProcedure [dbo].[Promotions_Find]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Promotions_Find]
	@Criteria nvarchar(max),
	@MemberId uniqueidentifier,
	@CategoryId nvarchar(max),
	@Status nvarchar(max),
	@CustomerId uniqueidentifier,
	@RowsPerPage int = 20,
	@PageNumber int
AS
BEGIN

	DECLARE @MemberPermission int
	SET @MemberPermission=dbo.GetMemberPermissionToken(@MemberId)
	
	DECLARE @TodayDate Datetime
	set @TodayDate = GETDATE()

SELECT *
FROM (
SELECT Promotions.*,TotalPages = Ceiling(cast((count(*) OVER ()) as float)/@RowsPerPage),ROW_NUMBER() OVER (ORDER BY Promotions.[Order] ASC,Promotions.Title ASC) AS RowNum,
	case when CustomerId is not null         
	then 'true'  
	else 'false'
	end as Marked,
	(select COUNT (*) from Promotions_MarkInterest where Promotions_MarkInterest.PromotionId = Promotions.PromotionId) as NumberOfMarks,
	
	case when Promotions.StartsOn > @TodayDate then 1
	     when Promotions.EndsOn < @TodayDate then 2
	     when Promotions.StartsOn < @TodayDate AND Promotions.EndsOn > @TodayDate then 0
	     else 0
	     END
	     AS Status
	   
FROM Promotions left join Promotions_MarkInterest on Promotions.PromotionId = Promotions_MarkInterest.PromotionId
AND Promotions_MarkInterest.CustomerId = @CustomerId
WHERE 
	Title LIKE @Criteria OR Title LIKE ('% '+@Criteria)
	AND((len(@CategoryId) <10 OR  @CategoryId like ('%' +convert(nvarchar(50), Promotions.CategoryId) + '%')))
	--AND((@Status = '' OR  @Status like ('%' +convert(nvarchar(50), PromotionStatus) + '%')))
	AND (dbo.GetPromotionPermissionToken(Promotions.PromotionId)&@MemberPermission)>0
	
	
)
 AS SOD
 WHERE SOD.RowNum BETWEEN ((@PageNumber-1)*@RowsPerPage)+1
AND @RowsPerPage*(@PageNumber)
ORDER BY Status ASC,EndsOn ASC

End
GO
/****** Object:  StoredProcedure [dbo].[Products_Find]    Script Date: 11/02/2017 08:24:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Products_Find]
	@MemberId uniqueidentifier,
	@CategoryId nvarchar(max) = NULL,
	@PageNumber INT,
	@criteria nvarchar(max)
As
Begin
	DECLARE @MemberPermission int
	SET @MemberPermission=dbo.GetMemberPermissionToken (@MemberId)
	
	SELECT * FROM 
	(
		SELECT TotalPages = 
			Ceiling(cast((count(*) OVER ()) as float)/20),
			Products.*,
			Categories.CategoryName,
			Categories.CategoryType,
			Categories.ParentCategoryId,
			Categories.CalculatorId,
			ROW_NUMBER() OVER (ORDER BY Products.[Order] ASC,Products.Name ASC) AS RowNum
		FROM 
			Products inner join Categories on Products.CategoryId=Categories.CategoryId
		WHERE
		(
			
			((len(@CategoryId) <10 OR  @CategoryId like ('%' +convert(nvarchar(50), Products.CategoryId) + '%') 
			 or Products.CategoryId in (SELECT CategoryId from Categories where @CategoryId like ('%' +convert(nvarchar(50), ParentCategoryId) + '%')))))
			 
		AND(@Criteria IS NULL OR Name LIKE @criteria OR Name LIKE ('% '+@Criteria) OR SubName LIKE @criteria  OR SubName LIKE ('% '+@Criteria))
		AND (dbo.GetProductPermissionToken(Products.ProductId)&@MemberPermission)>0
	) AS SOD
	WHERE SOD.RowNum BETWEEN ((@PageNumber-1)*20)+1 AND 20*(@PageNumber)
End


update Configuration set DbVersion = DbVersion +1

GO
