ALTER table zones
Add [Version] RowVersion

ALTER table valuecalculators
Add [Version] RowVersion

ALTER table tasks
Add [Version] RowVersion

ALTER table Salesdata
Add [Version] RowVersion

ALTER table Resources
Add [Version] RowVersion

ALTER table RegistrationQuestions
Add [Version] RowVersion

ALTER table QuestionTypes
Add [Version] RowVersion

ALTER table QuestionSortOrders
Add [Version] RowVersion

ALTER table Questions
Add [Version] RowVersion

ALTER table QuestionOptions
Add [Version] RowVersion

ALTER table Promotions
Add [Version] RowVersion

ALTER table PromotionEmails
Add [Version] RowVersion

ALTER table ProductSizes
Add [Version] RowVersion

ALTER table ProductPeople
Add [Version] RowVersion

ALTER table ProductInsights
Add [Version] RowVersion

ALTER table News
Add [Version] RowVersion

ALTER table Members
Add [Version] RowVersion

ALTER table Media
Add [Version] RowVersion

ALTER table IssueTypes
Add [Version] RowVersion

ALTER table IssuesHistory
Add [Version] RowVersion

ALTER table Issues
Add [Version] RowVersion

ALTER table Feedbacks
Add [Version] RowVersion

ALTER table Addresses
Add [Version] RowVersion

ALTER table FeedbackCategory_Ref
Add [Version] RowVersion

ALTER table CustomerValueCalculators
Add [Version] RowVersion

ALTER table CustomerTypes
Add [Version] RowVersion

ALTER table Customers
Add [Version] RowVersion

ALTER table CustomerOffers
Add [Version] RowVersion

ALTER table CustomerOfferItems
Add [Version] RowVersion

ALTER table ContactPersons
Add [Version] RowVersion

ALTER table Categories
Add [Version] RowVersion

ALTER table CallTypes
Add [Version] RowVersion

ALTER table Calls
Add [Version] RowVersion

ALTER table CallNotes
Add [Version] RowVersion

ALTER table CallAnswersPublished
Add [Version] RowVersion

ALTER table Addresses
Add [Version] RowVersion

ALTER table AccountTypes
Add [Version] RowVersion


update Configuration set DbVersion= DbVersion +1

