GO
/****** Object:  StoredProcedure [dbo].[Issues_GetMyIssuesNum]    Script Date: 10/10/2017 07:31:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Issues_GetMyIssuesNum]
	@AssignedTo uniqueidentifier,
	@MemberId uniqueidentifier
AS
BEGIN
	Declare @customerIds table(CustomerId uniqueidentifier)
	insert into @customerIds  exec Members_GetCustomersPermissions @MemberId
		select COUNT(*) from Issues where Status = 0 and AssignedTo = @AssignedTo
		AND CustomerId in (select CustomerId from @customerIds) OR CustomerId is Null
END


GO
ALTER PROCEDURE [dbo].[Tasks_GetMyTasksNum]
	@AssignedTo uniqueidentifier,
	@MemberId uniqueidentifier
AS
BEGIN
Declare @customerIds table(CustomerId uniqueidentifier)
insert into @customerIds  exec Members_GetCustomersPermissions @MemberId

	select COUNT(*) from Tasks where Status = 0 and AssignedTo = @AssignedTo
	AND (Tasks.CustomerId in (select CustomerId from @customerIds) OR CustomerId is Null)
END

GO
/****** Object:  StoredProcedure [dbo].[Calls_GetMyScheduledCallsNum]    Script Date: 10/10/2017 07:35:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Calls_GetMyScheduledCallsNum]
	@AssignedTo uniqueidentifier,
	@MemberId uniqueidentifier
AS
BEGIN
Declare @customerIds table(CustomerId uniqueidentifier)
	insert into @customerIds  exec Members_GetCustomersPermissions @MemberId
	
	select COUNT(*) from Calls
	LEFT JOIN Customers on Calls.CustomerId = Customers.CustomerId
	where Calls.Status = 0 and AssignedTo = @AssignedTo
	AND Calls.CustomerId in (select CustomerId from @customerIds)
	AND Customers.IsDeleted = 0
END


GO
/****** Object:  StoredProcedure [dbo].[Customers_Pagination]    Script Date: 10/10/2017 08:15:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[Customers_Pagination]
@PageNumber INT,
@Criteria nvarchar(max) = NULL,
@CategoryId nvarchar(max) = NULL,
@Lat float = NULL,
@Long float = NULL,
@CustomerTypeId nvarchar(max) = NULL,
@MemberId uniqueidentifier,
@State nvarchar(max) = NULL,
@SortBy int,
@Order bit = 0
As
Begin

	Declare @customerIds table(CustomerId uniqueidentifier)
	insert into @customerIds  exec Members_GetCustomersPermissions @MemberId

	SELECT * FROM (
		SELECT  TotalPages = Ceiling(cast((count(*) OVER ()) as float)/20),
				cus.*,
				ct.CustomerTypeName,
				at.AccountTypeName,
				cat.*,
				parcat.CategoryId as ParentCategory,
				parcat.CategoryName as ParentCategoryName,
				parcat.CategoryType as ParentCategoryType,
				adr.*,
				lastcall.calldate as LastCallDate,
				lastcall.id as LastCallId,
				firstcall.calldate as NextCallDate,
				firstcall.id as NextCallId,
				ROW_NUMBER() OVER (
				ORDER BY 
				CASE WHEN @SortBy=0 AND @Order=1 THEN BusinessName END ASC, 
				CASE WHEN @SortBy=0 AND @Order=0 THEN BusinessName END DESC,
				CASE WHEN @SortBy=1 AND @Order=1 THEN cat.CategoryName END ASC,
				CASE WHEN @SortBy=1 AND @Order=0 THEN cat.CategoryName END DESC) AS RowNum
		FROM Customers cus
			left join CustomerTypes ct on  ct.CustomerTypeId = cus.CustomerTypeId
			left join AccountTypes at on at.AccountTypeId = cus.AccountTypeId
			--left join CustomersCategories cuscat on  cuscat.CustomerId = cus.CustomerId
			--Left join Addresses adr on adr.ReferrerId = cus.CustomerId
			outer apply (select top 1 Calls.CallDate, 
			                          Calls.CallId from Calls 
			                          where Calls.CustomerId = cus.CustomerId
			                          and Calls.CallDate < getdate() order by Calls.CallDate desc)
			                          lastcall(calldate, id)
			
			outer apply (select top 1 Calls.calldate, 
									  Calls.CallId from Calls
									  where Calls.CustomerId = cus.CustomerId
									  and Calls.calldate >= getdate() order by calldate asc)
									  firstcall(calldate, id)

			outer apply (Select top 1 * FROM Addresses where ReferrerId=cus.CustomerId)adr
			outer apply (Select top 1 * FROM Categories WHERE CategoryId in (SELECT CategoryId FROM CustomersCategories WHERE CustomerId=cus.CustomerId)) cat
			outer apply (Select top 1 * FROM Categories WHERE CategoryId = cat.ParentCategoryId) parcat
			
			
			
		WHERE ((
			(@Criteria IS NULL OR BusinessName LIKE @Criteria OR AccountNumber LIKE @Criteria OR ABN LIKE @Criteria))
			AND(len(@CategoryId) <10 OR  @CategoryId like ('%' +convert(nvarchar(50), cat.CategoryId) + '%') or cat.CategoryId in (SELECT CategoryId from Categories where @CategoryId like ('%' +convert(nvarchar(50), ParentCategoryId) + '%')))
			AND((len(@CustomerTypeId) <10 OR  @CustomerTypeId like ('%' +convert(nvarchar(50), cus.CustomerTypeId) + '%')))
			--AND(@Lat IS NULL OR adr.Lat = @Lat)
			--AND (@Long IS NULL OR adr.Lon = @Long)
			And (cus.CustomerId in (select CustomerId from @customerIds)))
			and cus.IsDeleted=0
			and (@State IS NULL OR  (@State like '%' + convert(nvarchar(50),PhysicalState) + '%' AND LTRIM(RTRIM(PhysicalState))<>''))
	) AS SOD	
WHERE
	SOD.RowNum BETWEEN ((@PageNumber-1)*20)+1
	AND 20*(@PageNumber)

End


GO
/****** Object:  StoredProcedure [dbo].[Promotions_Find]    Script Date: 10/10/2017 13:27:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Promotions_Find]
	@Criteria nvarchar(max),
	@MemberId uniqueidentifier,
	@CategoryId nvarchar(max),
	@Status nvarchar(max),
	@CustomerId uniqueidentifier,
	@RowsPerPage int = 20,
	@PageNumber int
AS
BEGIN

	DECLARE @MemberPermission int
	SET @MemberPermission=dbo.GetMemberPermissionToken(@MemberId)
	
	DECLARE @TodayDate Datetime
	set @TodayDate = GETDATE()

SELECT *
FROM (
SELECT Promotions.*,TotalPages = Ceiling(cast((count(*) OVER ()) as float)/@RowsPerPage),ROW_NUMBER() OVER (ORDER BY Promotions.[Order] ASC,Promotions.Title ASC) AS RowNum,
	case when CustomerId is not null         
	then 'true'  
	else 'false'
	end as Marked,
	(select COUNT (*) from Promotions_MarkInterest where Promotions_MarkInterest.PromotionId = Promotions.PromotionId) as NumberOfMarks,
	
	case when Promotions.StartsOn > @TodayDate then 1
	     when Promotions.EndsOn < @TodayDate then 2
	     when Promotions.StartsOn < @TodayDate AND Promotions.EndsOn > @TodayDate then 0
	     else 0
	     END
	     AS Status
	   
FROM Promotions left join Promotions_MarkInterest on Promotions.PromotionId = Promotions_MarkInterest.PromotionId
AND Promotions_MarkInterest.CustomerId = @CustomerId
WHERE 
	Title LIKE @Criteria
	AND((len(@CategoryId) <10 OR  @CategoryId like ('%' +convert(nvarchar(50), Promotions.CategoryId) + '%')))
	--AND((@Status = '' OR  @Status like ('%' +convert(nvarchar(50), PromotionStatus) + '%')))
	AND (dbo.GetPromotionPermissionToken(Promotions.PromotionId)&@MemberPermission)>0
	
	
)
 AS SOD
 WHERE SOD.RowNum BETWEEN ((@PageNumber-1)*@RowsPerPage)+1
AND @RowsPerPage*(@PageNumber)
ORDER BY Status ASC,EndsOn ASC

End



GO
/****** Object:  StoredProcedure [dbo].[Promotions_Save]    Script Date: 10/10/2017 14:45:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Promotions_Save]
	@PromotionId uniqueidentifier,
	@CategoryId uniqueidentifier,
	@Title nvarchar(max),
	@StartsOn datetime,
	@EndsOn datetime,
	@Summary ntext,
	@Terms ntext,
	@Order int
AS
BEGIN

	IF (SELECT COUNT(*)FROM Promotions WHERE PromotionId=@PromotionId)=0
		BEGIN
		
			declare @MaxOrder int;
			Select @MaxOrder = MAX(pp.[Order]) from Promotions as pp
			SET @MaxOrder = @MaxOrder+1;
			
			
				INSERT INTO Promotions(PromotionId,CategoryId,Title,StartsOn,EndsOn,Summary,Terms,[Order],ViewNumber)
				VALUES (@PromotionId,@CategoryId,@Title,@StartsOn,@EndsOn,@Summary,@Terms,@MaxOrder,1)
		END
	ELSE
		BEGIN
			UPDATE Promotions
			SET
				CategoryId=@CategoryId,
				Title=@Title,
				StartsOn=@StartsOn,
				EndsOn=@EndsOn,
				Summary=@Summary,
				Terms=@Terms,
				[Order]=@Order
			WHERE
				PromotionId=@PromotionId
		END
END

GO
/****** Object:  StoredProcedure [dbo].[Promotions_SelectById]    Script Date: 10/10/2017 14:58:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Promotions_SelectById]
	@PromotionId uniqueidentifier ,
	@MemberId uniqueidentifier
AS
BEGIN
	DECLARE @MemberPermission int
	SET @MemberPermission=dbo.GetMemberPermissionToken(@MemberId)
	
	DECLARE @TodayDate Datetime
	set @TodayDate = GETDATE()
	
	SELECT Promotions.*,
		case when CustomerId is not null         
		then 'true'  
		else 'false'
		end as Marked,
		
		case when Promotions.StartsOn > @TodayDate then 1
	     when Promotions.EndsOn < @TodayDate then 2
	     when Promotions.StartsOn < @TodayDate AND Promotions.EndsOn > @TodayDate then 0
	     else 0
	     END
	     AS Status,
		
		
	   (select COUNT (*) from Promotions_MarkInterest where Promotions_MarkInterest.PromotionId = Promotions.PromotionId) as NumberOfMarks 
	FROM Promotions left join Promotions_MarkInterest on Promotions.PromotionId = Promotions_MarkInterest.PromotionId	
	WHERE Promotions.PromotionId=@PromotionId
  
END

UPDATE Configuration set DbVersion = 1