using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Arina.Business;
using Arina.Business.Data;
using Arina.Business.Data.Sql;

namespace Arina.Data.Sql
{
	public class ApplicationModuleDataService : IApplicationModuleDataService
	{
		public void Save(ApplicationModule applicationmodule)
		{
			using (CustomSqlConnection connection = new CustomSqlConnection())
			{
				using (CustomSqlCommand cmd = new CustomSqlCommand("ApplicationModules_Save", connection))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new CustomSqlParameter("ApplicationModuleId", SqlDbType.UniqueIdentifier, applicationmodule.ApplicationModuleId));
					cmd.Parameters.Add(new CustomSqlParameter("Name", SqlDbType.UniqueIdentifier, applicationmodule.Name));
					cmd.ExecuteNonQuery();
				}
			}
		}
		public List<ApplicationModule> Find(string criteria)
		{
			using (CustomSqlConnection connection = new CustomSqlConnection())
			{
				using (CustomSqlCommand cmd = new CustomSqlCommand("ApplicationModules_Find", connection))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new CustomSqlParameter("criteria", SqlDbType.NVarChar,  "%" + criteria + "%"));
					CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
					DataTable dt = new DataTable();
					adapter.Fill(dt);
					return Load(dt);
				}
			}
		}
		public ApplicationModule Load(Guid applicationmoduleid)
		{
			using (CustomSqlConnection connection = new CustomSqlConnection())
			{
				using (CustomSqlCommand cmd = new CustomSqlCommand("ApplicationModules_SelectById", connection))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new CustomSqlParameter("ApplicationModuleId", SqlDbType.UniqueIdentifier, applicationmoduleid));
					CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
					DataSet ds = new DataSet();
					adapter.Fill(ds);
					List<ApplicationModule> all = Load(ds.Tables[0]);
					if (all.Count == 0)
						throw new NotFoundException();
					else
						return all[0];
				}
			}
		}
		public int Delete(Guid applicationmoduleid)
		{
			using (CustomSqlConnection connection = new CustomSqlConnection())
			{
				using (CustomSqlCommand cmd = new CustomSqlCommand("ApplicationModules_Delete", connection))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new CustomSqlParameter("ApplicationModuleId", SqlDbType.UniqueIdentifier, applicationmoduleid));
					return cmd.ExecuteNonQuery();
				}
			}
		}
		private static List<ApplicationModule> Load(DataTable dt)
		{
			List<ApplicationModule> retVal = new List<ApplicationModule>();
			if (dt != null)
			{
				foreach (DataRow dr in dt.Rows)
					retVal.Add(Load(dr));
			}
			return retVal;
		}
		private static ApplicationModule Load(DataRow dr)
		{
			ApplicationModule retVal = new ApplicationModule()
			{
				ApplicationModuleId = dr["ApplicationModuleId"].EnsureGuid(),
				Name = dr["Name"].EnsureString(),
			};
			return retVal;
		}
	}
}
