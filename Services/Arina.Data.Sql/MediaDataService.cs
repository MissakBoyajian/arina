﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Arina.Business;
using Arina.Business.Data;
using Arina.Business.Data.Sql;

namespace Arina.Data.Sql
{
    class MediaDataService : IMediaDataService
    {
        public void Save(Media media)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Media_Save", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("MediaId", SqlDbType.UniqueIdentifier, media.MediaId));
                    cmd.Parameters.Add(new CustomSqlParameter("MediaType", SqlDbType.Int, media.MediaType));
                    cmd.Parameters.Add(new CustomSqlParameter("ReffererId", SqlDbType.UniqueIdentifier, media.ReffererId));
                    cmd.Parameters.Add(new CustomSqlParameter("Caption", SqlDbType.NVarChar, media.Caption));
                    cmd.Parameters.Add(new CustomSqlParameter("MediaContent", SqlDbType.Image, media.MediaContent));
                    cmd.Parameters.Add(new CustomSqlParameter("IsOnCdn", SqlDbType.Bit, media.IsOnCdn));
                    cmd.Parameters.Add(new CustomSqlParameter("CdnUrl", SqlDbType.NVarChar, media.CdnUrl));
                    cmd.Parameters.Add(new CustomSqlParameter("CreatedBy", SqlDbType.UniqueIdentifier, media.CreatedBy));
                    cmd.Parameters.Add(new CustomSqlParameter("CreatedOn", SqlDbType.DateTime, media.CreatedOn));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public List<Media> Find(string criteria)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Media_Find", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("criteria", SqlDbType.NVarChar, "%" + criteria + "%"));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return Load(dt);
                }
            }
        }

        public Media Load(Guid mediaid)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Media_SelectById", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("MediaId", SqlDbType.UniqueIdentifier, mediaid));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    List<Media> all = Load(ds.Tables[0]);
                    if (all.Count == 0)
                        throw new NotFoundException();
                    else
                        return all[0];
                }
            }
        }

        public List<Media> GetMediaByReffererId(Guid reffererid)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Media_SelectByReffererId", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("ReffererId", SqlDbType.UniqueIdentifier, reffererid));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    return Load(ds.Tables[0]);
                }
            }
        }
        public int Delete(Guid mediaid)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Media_Delete", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("MediaId", SqlDbType.UniqueIdentifier, mediaid));
                    return cmd.ExecuteNonQuery();
                }
            }
        }
        private static List<Media> Load(DataTable dt)
        {
            List<Media> retVal = new List<Media>();
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                    retVal.Add(Load(dr));
            }
            return retVal;
        }
        private static Media Load(DataRow dr)
        {
            Media retVal = new Media()
            {
                MediaId = dr["MediaId"].EnsureGuid(),
                MediaType = dr["MediaType"].EnsureEnum<MediaTypeEnum>(),
                ReffererId = dr["ReffererId"].EnsureGuid(),
                Caption = dr["Caption"].EnsureString(),
                CdnUrl = dr["CdnUrl"].EnsureString(),
                IsOnCdn = dr["IsOnCdn"].EnsureBool(),
                CreatedBy = dr["CreatedBy"].EnsureGuid(),
                CreatedOn = dr["CreatedOn"].EnsureDate(),
                //TimeStamp = BitConverter.ToInt64((byte[])dr["Version"], 0)

            };

            if (dr.Table.Columns.Contains("MediaContent"))
                retVal.MediaContent = dr["MediaContent"] as byte[];
            return retVal;
        }
    }
}
