﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arina.Business;
using Arina.Business.Data;
using Arina.Business.Data.Sql;

namespace Arina.Data.Sql
{
    class FeedbacksDataService: IFeedbacksDataService
    {
        public void Save(Stadium stadium)
        {
            throw new NotImplementedException();
        }

        public void Save(string Feedback)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("Feedbacks_Save", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new CustomSqlParameter("Description", SqlDbType.NVarChar, Feedback));
                    cmd.Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId));

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<Feedbacks> Get()
        {
            throw new NotImplementedException();
        }

        //private static Stadium Load(DataRow dr)
        //{
        //    Stadium retVal = new Stadium();

        //    retVal.StadiumId = dr["StadiumId"].EnsureGuid();
        //    retVal.Name = dr["Name"].EnsureString();
        //    retVal.MemberId = dr["MemberId"].EnsureGuid();
        //    retVal.OpeningHourTime = dr["OpeningHourTime"].EnsureInt();
        //    retVal.OpeningHourMinutes = dr["OpeningHourMinutes"].EnsureInt();
        //    retVal.ClosingHourTime = dr["ClosingHourTime"].EnsureInt();
        //    retVal.ClosingHourMinutes = dr["ClosingHourMinutes"].EnsureInt();
        //    retVal.RestrictedHourTime = dr["RestrictedHourTime"].EnsureInt();
        //    retVal.RestrictedHourMinutes = dr["RestrictedHourMinutes"].EnsureInt();
        //    retVal.Lat = dr["Lat"].EnsureDecimal();
        //    retVal.Lon = dr["Lon"].EnsureDecimal();
        //    retVal.CancellationPolicy = dr["CancellationPolicy"].EnsureString();
        //    retVal.Area = dr["Area"].EnsureString();
        //    retVal.City = dr["City"].EnsureString();
        //    retVal.Email = dr["Email"].EnsureString();
        //    retVal.Rating = dr["Rating"].EnsureDecimal();
        //    retVal.Telephone = dr["Telephone"].EnsureString();
        //    retVal.HasWater = dr["HasWater"].EnsureInt();
        //    retVal.SpecialFeatures = dr["SpecialFeatures"].EnsureString();

        //    return retVal;
        //}


    }
}
