﻿using Arina.Business;
using Arina.Business.Data;
using Arina.Business.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace Arina.Data.Sql
{
    public class IssuesHistoryDataService : IIssuesHistoryDataService
    {
        public Guid Save(IssueHistory issueHistory)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("IssuesHistory_Save", connection))
                {
                    Guid issuehistoryid = Guid.NewGuid();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("IssueHistoryId", SqlDbType.UniqueIdentifier, issueHistory.IssueHistoryId == Guid.Empty ? issuehistoryid : issueHistory.IssueHistoryId));
                    cmd.Parameters.Add(new CustomSqlParameter("IssueId", SqlDbType.UniqueIdentifier, issueHistory.IssueId));
                    cmd.Parameters.Add(new CustomSqlParameter("Operation", SqlDbType.Int, (int)issueHistory.Operation));
                    cmd.Parameters.Add(new CustomSqlParameter("Status", SqlDbType.Int, (int)issueHistory.Status));
                    cmd.Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId));
                    cmd.Parameters.Add(new CustomSqlParameter("ReassignedId", SqlDbType.UniqueIdentifier, issueHistory.ReassignedId));
                    cmd.Parameters.Add(new CustomSqlParameter("CommentText", SqlDbType.NVarChar, issueHistory.CommentText));
                    cmd.Parameters.Add(new CustomSqlParameter("NewPriority", SqlDbType.Int, issueHistory.Priority));
                    cmd.Parameters.Add(new CustomSqlParameter("NewType", SqlDbType.Int, issueHistory.Type));
                    cmd.Parameters.Add(new CustomSqlParameter("Date", SqlDbType.DateTime, issueHistory.Date));
                    cmd.Parameters.Add(new CustomSqlParameter("IsViewed", SqlDbType.Bit, issueHistory.IsViewed));

                    return cmd.ExecuteNonQuery() != 0 ? issuehistoryid : Guid.Empty;
                }
            }
        }


        public List<IssueHistory> LoadByIssue(Guid IssueId)
        {

            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("IssuesHistory_SelectByIssueId", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("IssueId", SqlDbType.UniqueIdentifier, IssueId));

                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return Load(dt);
                }
            }
        }

        private static List<IssueHistory> Load(DataTable dt)
        {
            List<IssueHistory> retVal = new List<IssueHistory>();
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                    retVal.Add(Load(dr));
            }
            return retVal;
        }
        private static IssueHistory Load(DataRow dr)
        {
            IssueHistory retVal = new IssueHistory();

            retVal.IssueHistoryId = dr["IssueHistoryId"].EnsureGuid();
            retVal.IssueId = dr["IssueId"].EnsureGuid();
            retVal.Operation = (OperationTypeEnum)dr["Operation"].EnsureInt();
            retVal.MemberId = dr["MemberId"].EnsureGuid();
            retVal.Priority = dr["NewPriority"].EnsureInt();
            retVal.Type = (IssueTypeEnum)dr["NewType"].EnsureInt();
            retVal.CommentText = dr["CommentText"].EnsureString();
            retVal.ReassignedId = dr["ReassignedId"].EnsureGuid();
            retVal.Status = (IssuesStatusEnum)dr["Status"].EnsureInt();
            retVal.Date = dr["Date"].EnsureDate();
            retVal.IsViewed = dr["IsViewed"].EnsureBool();

            retVal.issue = new Issue()
            {
                IssueId = dr["IssueId"].EnsureGuid(),
                CustomerId = dr["CustomerId"].EnsureGuid(),
                Title = dr["Title"].EnsureString(),
                Description = dr["Description"].EnsureString(),
                Status = dr["Status"].EnsureEnum<IssuesStatusEnum>(),

                CreatedBy = dr["CreatedBy"].EnsureGuid(),
                AssignedTo = dr["AssignedTo"].EnsureGuid(),
                CallId = dr["CallId"].EnsureGuid(),

                AssignedToMember = dr.Table.Columns.Contains("UploadedToMemberId") ? new Member()
                {
                    MemberId = dr["UploadedToMemberId"].EnsureGuid(),
                    FirstName = dr["UploadedToFirstName"].EnsureString(),
                    LastName = dr["UploadedToLastName"].EnsureString(),
                    Email = dr["UploadedToEmail"].EnsureString(),
                    RoleId = dr["UploadedToRoleId"].EnsureGuid()
                } : null,


                CreatedOn = dr["CreatedOn"].EnsureDate(),
                UpdatedOn = dr["UpdatedOn"].EnsureDate(),
                AssignStatus = dr["AssignStatus"].EnsureEnum<IssuesAssignStatusEnum>(),
                IssueNo = dr["IssueNo"].EnsureInt(),
                Priority = (IssuesPriorityEnum)dr["Priority"].EnsureInt(),
                IssueType = dr["IssueType"].EnsureEnum<IssueTypeEnum>(),

            };

            if (dr["ReassignedMemberId"].EnsureGuid() != Guid.Empty)
            {
                retVal.ReassignedMember = new Member()
                {
                    MemberId = dr["ReassignedMemberId"].EnsureGuid(),
                    FirstName = dr["ReassignedMemberFirstName"].EnsureString(),
                    LastName = dr["ReassignedMemberLastName"].EnsureString(),
                    Email = dr["ReassignedMemberEmail"].EnsureString(),
                    RoleId = dr["ReassignedMemberRoleId"].EnsureGuid(),
                    Role = new Role()
                    {
                        RoleId = dr["ReassignedMemberRoleId"].EnsureGuid(),
                        RoleName = dr["ReassignedMemberRoleName"].EnsureString()
                    }
                };
            }

            retVal.Media = dr.Table.Columns.Contains("Media") ? MediaManager.LoadFromXml(dr["Media"].EnsureString()) : new List<Media>();

            return retVal;
        }

        public List<IssueHistory> Find()
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("IssuesHistory_Find", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("LoggedInMember", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return Load(dt);
                }
            }
        }

        public void Save(List<IssueHistory> issueHistories)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("IssuesHistory_Save", connection))
                {

                    Guid issuehistoryid = Guid.NewGuid();
                    cmd.CommandType = CommandType.StoredProcedure;
                    foreach (IssueHistory issueHistory in issueHistories)
                    {
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new CustomSqlParameter("IssueHistoryId", SqlDbType.UniqueIdentifier, issuehistoryid));
                        cmd.Parameters.Add(new CustomSqlParameter("IssueId", SqlDbType.UniqueIdentifier, issueHistory.IssueId));
                        cmd.Parameters.Add(new CustomSqlParameter("Operation", SqlDbType.Int, (int)issueHistory.Operation));
                        cmd.Parameters.Add(new CustomSqlParameter("Status", SqlDbType.Int, (int)issueHistory.Status));
                        cmd.Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, issueHistory.MemberId));
                        cmd.Parameters.Add(new CustomSqlParameter("ReassignedId", SqlDbType.UniqueIdentifier, issueHistory.ReassignedId));
                        cmd.Parameters.Add(new CustomSqlParameter("CommentText", SqlDbType.NVarChar, issueHistory.CommentText));
                        cmd.Parameters.Add(new CustomSqlParameter("NewPriority", SqlDbType.Int, issueHistory.Priority));
                        cmd.Parameters.Add(new CustomSqlParameter("Date", SqlDbType.DateTime, DateTime.Now));

                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }
    }
}
