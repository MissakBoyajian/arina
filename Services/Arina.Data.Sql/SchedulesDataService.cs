using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Arina.Business;
using Arina.Business.Data;
using Arina.Business.Data.Sql;

namespace Arina.Data.Sql
{
    public class SchedulesDataService : ISchedulesDataService
    {

        public List<Schedules> Get(DateTime Date)
        {

            List<CustomSqlParameter> Parameters = new List<CustomSqlParameter>();

            Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId));
            Parameters.Add(new CustomSqlParameter("Date", SqlDbType.DateTime, Date));

            return Load(SqlDataService.ExecuteQuery("Schedules_GetByDay", Parameters));
        }

        public List<Schedules> GetPreviousBookings(Guid MemberId, int PageNumber)
        {

            List<CustomSqlParameter> Parameters = new List<CustomSqlParameter>();

            Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, MemberId));
            Parameters.Add(new CustomSqlParameter("PageNumber", SqlDbType.Int, PageNumber));

            return Load(SqlDataService.ExecuteQuery("Members_GetPreviousBookings", Parameters));
        }

        public List<Schedules> GetCurrentBookings(Guid MemberId, int PageNumber)
        {

            List<CustomSqlParameter> Parameters = new List<CustomSqlParameter>();

            Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, MemberId));
            Parameters.Add(new CustomSqlParameter("PageNumber", SqlDbType.Int, PageNumber));

            return Load(SqlDataService.ExecuteQuery("Members_GetUpcomingBookings", Parameters));
        }

        public void Save(Schedules Schedule)
        {
            List<CustomSqlParameter> Parameters = new List<CustomSqlParameter>();

            if (Schedule.ScheduleId != Guid.Empty)
            {
                Parameters.Add(new CustomSqlParameter("ScheduleId", SqlDbType.UniqueIdentifier, Schedule.ScheduleId));
            }

            Parameters.Add(new CustomSqlParameter("MiniStadiumId", SqlDbType.UniqueIdentifier, Schedule.MiniStadiumId.EnsureGuid()));
            Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, Schedule.MemberId.EnsureGuid()));
            Parameters.Add(new CustomSqlParameter("ScheduleDateFrom", SqlDbType.DateTimeOffset, Schedule.ScheduleDateFrom));
            Parameters.Add(new CustomSqlParameter("ScheduleDateTo", SqlDbType.DateTimeOffset, Schedule.ScheduleDateFrom.AddMinutes(Schedule.Duration)));
            Parameters.Add(new CustomSqlParameter("Recurring", SqlDbType.Int, Schedule.Recurring.EnsureInt()));
            Parameters.Add(new CustomSqlParameter("Price", SqlDbType.Int, Schedule.Price.EnsureInt()));

            SqlDataService.ExecuteNonQuery("Schedules_Save", Parameters);
        }

        public void Delete(Guid ScheduleId, Boolean Recurring)
        {

            List<CustomSqlParameter> Parameters = new List<CustomSqlParameter>();

            Parameters.Add(new CustomSqlParameter("ScheduleId", SqlDbType.UniqueIdentifier, ScheduleId));
            Parameters.Add(new CustomSqlParameter("Recurring", SqlDbType.Bit, Recurring.EnsureInt()));
            Parameters.Add(new CustomSqlParameter("MemberId", SqlDbType.UniqueIdentifier, ContextInfo.Current.LoggedInUser.MemberId));

            SqlDataService.ExecuteNonQuery("Schedules_Delete", Parameters);
        }


        private static List<Schedules> Load(DataTable dt)
        {
            List<Schedules> retVal = new List<Schedules>();
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                    retVal.Add(Load(dr));
            }
            return retVal;
        }

        private static Schedules Load(DataRow dr)
        {
            Schedules retVal = new Schedules()
            {

                ScheduleId = dr["ScheduleId"].EnsureGuid(),
                ScheduleDateFrom = dr["ScheduleDateFrom"].EnsureDate(),
                ScheduleDateTo = dr["ScheduleDateTo"].EnsureDate(),
                MiniStadiumId = dr["MiniStadiumId"].EnsureGuid(),
                MemberId = dr["MemberId"].EnsureGuid(),
                Price = dr["Price"].EnsureInt(),
                MiniStadium = new MiniStadium()
                {
                    MiniStadiumId = dr["MiniStadiumId"].EnsureGuid(),
                    Name = dr["MiniStadiumName"].EnsureString()
                },
                Member = new Member()
                {
                    MemberId = dr["MemberId"].EnsureGuid(),
                    FirstName = dr["MemberFirstName"].EnsureString(),
                    LastName = dr["MemberLastName"].EnsureString(),
                    Phone = dr["MemberTelephone"].EnsureString(),
                }

            };

            return retVal;
        }


    }
}
