using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Arina.Business;
using Arina.Business.Data;
using Arina.Business.Data.Sql;

namespace Arina.Data.Sql
{
    public class RolesApplicationModuleDataService : IRolesApplicationModuleDataService
    {
        public void Save(RolesApplicationModule rolesapplicationmodule)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("RolesApplicationModules_Save", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("RoleId", SqlDbType.UniqueIdentifier, rolesapplicationmodule.RoleId));
                    cmd.Parameters.Add(new CustomSqlParameter("ApplicationModuleId", SqlDbType.UniqueIdentifier, rolesapplicationmodule.ApplicationModuleId));
                    cmd.Parameters.Add(new CustomSqlParameter("AllowView", SqlDbType.Bit, rolesapplicationmodule.AllowView));
                    cmd.Parameters.Add(new CustomSqlParameter("AllowAdd", SqlDbType.Bit, rolesapplicationmodule.AllowAdd));
                    cmd.Parameters.Add(new CustomSqlParameter("AllowEdit", SqlDbType.Bit, rolesapplicationmodule.AllowEdit));
                    cmd.Parameters.Add(new CustomSqlParameter("AllowDelete", SqlDbType.Bit, rolesapplicationmodule.AllowDelete));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public List<RolesApplicationModule> Find(string criteria)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("RolesApplicationModules_Find", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("criteria", SqlDbType.NVarChar, "%" + criteria + "%"));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return Load(dt);
                }
            }
        }

        public List<RolesApplicationModule> LoadByRoleId(Guid roleId)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("RolesApplicationModules_SelectByRoleId", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("RoleId", SqlDbType.UniqueIdentifier, roleId));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return Load(dt);
                }
            }
        }
        public RolesApplicationModule Load(Guid roleid, Guid applicationmoduleid)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("RolesApplicationModules_SelectById", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("RoleId", SqlDbType.UniqueIdentifier, roleid));
                    cmd.Parameters.Add(new CustomSqlParameter("ApplicationModuleId", SqlDbType.UniqueIdentifier, applicationmoduleid));
                    CustomSqlDataAdapter adapter = new CustomSqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    List<RolesApplicationModule> all = Load(ds.Tables[0]);
                    if (all.Count == 0)
                        throw new NotFoundException();
                    else
                        return all[0];
                }
            }
        }
        public int Delete(Guid roleid, Guid applicationmoduleid)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("RolesApplicationModules_Delete", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("RoleId", SqlDbType.UniqueIdentifier, roleid));
                    cmd.Parameters.Add(new CustomSqlParameter("ApplicationModuleId", SqlDbType.UniqueIdentifier, applicationmoduleid));
                    return cmd.ExecuteNonQuery();
                }
            }
        }
        private static List<RolesApplicationModule> Load(DataTable dt)
        {
            List<RolesApplicationModule> retVal = new List<RolesApplicationModule>();
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                    retVal.Add(Load(dr));
            }
            return retVal;
        }
        private static RolesApplicationModule Load(DataRow dr)
        {
            RolesApplicationModule retVal = new RolesApplicationModule()
            {
                RoleId = dr["RoleId"].EnsureGuid(),
                ApplicationModuleId = dr["ApplicationModuleId"].EnsureGuid(),
                AllowView = dr["AllowView"].EnsureBool(),
                AllowAdd = dr["AllowAdd"].EnsureBool(),
                AllowEdit = dr["AllowEdit"].EnsureBool(),
                AllowDelete = dr["AllowDelete"].EnsureBool(),
                Role = new Role()
                {
                    RoleId = dr["RoleId"].EnsureGuid(),
                    RoleName = dr["RoleName"].EnsureString(),
                },
                ApplicationModule = new ApplicationModule()
                {
                    ApplicationModuleId = dr["ApplicationModuleId"].EnsureGuid(),
                    Name = dr["Name"].EnsureString(),
                }
            };
            return retVal;
        }

     
    }
}
