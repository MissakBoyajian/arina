﻿using Arina.Business;
using Arina.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Arina.Services
{
    public class WebApiApplication : System.Web.HttpApplication
    {

        private EmailService emailService = null;
        private static object emailServiceLock = new object();
        private static object reminderServiceLock = new object();
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //if (emailService == null)
            //{
            //    lock (emailServiceLock)
            //    {
            //        if (emailService == null)
            //        {
            //            emailService = new EmailService();
            //            emailService.Start(BatchJobType.Email);
            //        }
            //    }
            //}
        }
    }
}
