﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Arina.Business;
using System.Security.Claims;
using Newtonsoft.Json;

namespace Arina.Services
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        Member member;
        public override async System.Threading.Tasks.Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async System.Threading.Tasks.Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            ClaimsIdentity identity = new ClaimsIdentity(context.Options.AuthenticationType);
            string username = context.UserName;
            string password = context.Password;

            try
            {
                member = MemberManager.Load(username, password);

                identity.AddClaim(new Claim(ClaimTypes.Name, member.FirstName + " " + member.LastName));
                identity.AddClaim(new Claim(ClaimTypes.Email, member.Email));
                identity.AddClaim(new Claim(ClaimTypes.Role, member.Role.RoleName));
                context.Validated(identity);

                member.LastLoginDate = DateTime.Now;
                MemberManager.Save(member);
            }
            catch (NotFoundException)
            {
                context.SetError("invalid credentials", "Email or password is incorrect");
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
        }

        public override System.Threading.Tasks.Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
          //Return data with the token response
            context.AdditionalResponseParameters.Add("LoggedInMember", JsonConvert.SerializeObject(member));
            //context.AdditionalResponseParameters.Add("Roles", JsonConvert.SerializeObject(RolesApplicationModuleManager.Find("")));

            return System.Threading.Tasks.Task.FromResult<object>(null);
        }
    }
}