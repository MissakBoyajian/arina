﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Arina.Business;

namespace Arina.Services.Controllers
{
    [Authorize]
    public class RolesApplicationModulesController : ApiController
    {
        // GET: api/RolesApplicationModules
        public HttpResponseMessage Get(string criteria = "")
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, RolesApplicationModuleManager.Find(criteria));
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // GET: api/RolesApplicationModules/5
        [HttpGet]
        [Route("api/customerofferitems/{roleid}/{applicationmoduleid}")]
        public HttpResponseMessage Get(Guid roleid, Guid applicationmoduleid)
        {
            try
            {
                try
                {
                    RolesApplicationModule roleapplicationmodule = RolesApplicationModuleManager.Load(roleid, applicationmoduleid);
                    return Request.CreateResponse(HttpStatusCode.OK, roleapplicationmodule);
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, " RoleApplicationModule not found");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // POST: api/RolesApplicationModules
        public HttpResponseMessage Post(RolesApplicationModule roleapplicationmodule)
        {
            try
            {
                RolesApplicationModuleManager.Save(roleapplicationmodule);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, roleapplicationmodule);
                message.Headers.Location = new Uri(Request.RequestUri + "/" + roleapplicationmodule.RoleId.ToString() + "/" + roleapplicationmodule.ApplicationModuleId);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // PUT: api/RolesApplicationModules/5
        public void Put(int id, [FromBody]string value)
        {
        }

    }
}
