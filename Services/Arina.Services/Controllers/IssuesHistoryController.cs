﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Arina.Business;

namespace Arina.Services.Controllers
{
    public class IssuesHistoryController : ApiController
    {
        // GET: api/IssuesHistory
        [HttpGet, Route("api/IssuesHistory")]
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, IssuesHistoryManager.Find());
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // GET: api/IssuesHistory/5
        [HttpGet,Route("api/IssuesHistory/{id}")]
        public HttpResponseMessage Get(Guid id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, IssuesHistoryManager.LoadByIssue(id));
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // POST: api/IssuesHistory
        [HttpPost,Route("api/IssuesHistory")]
        public HttpResponseMessage Post(IssueHistory issueHistory)
        {
            try
            {
                Guid issuehistoryid = IssuesHistoryManager.Save(issueHistory);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, issuehistoryid);
                message.Headers.Location = new Uri(Request.RequestUri + "/" + issueHistory.IssueHistoryId.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        // POST: api/IssuesHistory
        [HttpPost, Route("api/IssuesHistorylist")]
        public HttpResponseMessage Post(List<IssueHistory> issueHistories)
        {
            try
            {
                IssuesHistoryManager.Save(issueHistories);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, issueHistories);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // PUT: api/IssuesHistory/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/IssuesHistory/5
        public void Delete(int id)
        {
        }
    }
}
