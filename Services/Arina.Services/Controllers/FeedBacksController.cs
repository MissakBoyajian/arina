﻿using System.Net.Http;
using System.Web.Http;
using Arina.Business;
using System.Net;
using System;

namespace Arina.Services.Controllers
{
    public class FeedbacksController : ApiController
    {
        [Authorize]
        // GET: api/Issues
        [HttpGet, Route("api/feedbacks")]
        public HttpResponseMessage Get(string Criteria = "", int PageNumber = 1)
        {
            try
            {
                //EmailManager.SendEmail1();
                PaginatedList<Member> Customers = MemberManager.FindMyCustomers(Criteria, PageNumber);
                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Customers = Customers,
                    CurrentPage = Customers.CurrentPage,
                    TotalPages = Customers.TotalPages
                });
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Authorize]
        // POST: api/Issues
        [HttpPost, Route("api/feedbacks")]
        public HttpResponseMessage Post(Feedbacks Feedback)
        {
            try
            {
                FeedbacksManager.Save(Feedback.Feedback);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created);
                //message.Headers.Location = new Uri(Request.RequestUri + "/" + customer.MemberId.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
