﻿using Newtonsoft.Json;
using Arina.Business;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Arina.Services.Controllers
{
    public class ResourcesController : ApiController
    {

        [HttpGet, Route("api/resources")]
        public HttpResponseMessage Get(Guid DealerGroupId)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ResourceManager.Find(DealerGroupId));
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpGet, Route("api/resources/{id}")]
        public HttpResponseMessage GetByProductId(Guid id)
        {
            try
            {
                try
                {
                    Resource resource = ResourceManager.Load(id);
                    return Request.CreateResponse(HttpStatusCode.OK, resource);
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, " resource with ID = " + id.ToString() + " is not found");
                }
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        // GET: api/Resources/5
        [HttpGet, Route("api/resources/GetResourcesByProductId/{productId}")]
        public HttpResponseMessage GetResourcesByProductId(Guid productId)
        {
            try
            {
                try
                {
                    List<Resource> resources = ResourceManager.LoadByProduct(productId);
                    return Request.CreateResponse(HttpStatusCode.OK, resources);
                }
                catch (Exception er)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, " resource with ID = " + productId.ToString() + " is not found");
                }
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        // POST: api/Resources
        [HttpPost, Route("api/resources")]

        public HttpResponseMessage Post(Resource resource, Guid? ProductId = null)
        {
            try
            {
                ResourceManager.Save(resource, ProductId);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, resource);
                message.Headers.Location = new Uri(Request.RequestUri + "/" + resource.ResourceId.ToString());
                return message;
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // DELETE: api/Resources/5

        [HttpDelete, Route("api/resources/{id}")]
        public HttpResponseMessage Delete(Guid id)
        {
            try
            {
                int RowsDeleted = ResourceManager.Delete(id);
                if (RowsDeleted > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, " resource with ID = " + id + " is deleted");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, " resource with ID = " + id.ToString() + " is not found to delete");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Route("")]
        [Route("api/Resources/SaveFileToServer")]
        public HttpResponseMessage PostFileToDB()
        {
            try
            {
                var httpRequest = HttpContext.Current.Request;

                Resource ResourceItem = JsonConvert.DeserializeObject<Resource>(httpRequest.Form["OfflineData"]);

                if (ResourceItem.ResourceId == Guid.Empty)
                {
                    ResourceItem.ResourceId = Guid.NewGuid();
                }

                if (httpRequest.Files.Count > 0)
                {
                    for (int i = 0; i < httpRequest.Files.Count; i++)
                    {
                        Guid loggedInUserId = ContextInfo.Current.LoggedInUser.MemberId;
                        HttpPostedFile postedFile = httpRequest.Files[i];

                        Guid refferrerId = httpRequest.Form["ReffererId"].EnsureGuid();
                        BinaryReader mediaBinaryReader = new BinaryReader(postedFile.InputStream);

                        byte[] mediaBytes = mediaBinaryReader.ReadBytes((Int32)postedFile.InputStream.Length);

                        //foreach (var item in ParentResources)
                        //{

                        ResourceItem.FileContent = mediaBytes;
                        string ext = Path.GetExtension(postedFile.FileName).TrimStart('.');

                        ResourceItem.FileNameWithExtension = postedFile.FileName;
                        ResourceItem.FileType = ext.EnsureEnum<ResourceFileTypeEnum>();
                        ResourceItem.Size = postedFile.ContentLength;
                        ResourceManager.Save(ResourceItem, null);
                        // }

                    }
                }
                else
                {
                    if (ResourceItem.VimeoId.ToLower().Contains("vimeo"))
                    {
                        ResourceItem.VimeoId = ResourceItem.VimeoId.Substring(ResourceItem.VimeoId.LastIndexOf("/") + 1);
                        ResourceItem.FileType = ResourceFileTypeEnum.vimeo;
                        ResourceItem.FileName = ResourceItem.FileName + " - Vimeo";
                        ResourceItem.YoutubeId = "";
                    }
                    else
                        if ((ResourceItem.YoutubeId.ToLower().Contains("youtube")))
                    {
                        ResourceItem.YoutubeId = ResourceItem.YoutubeId.Substring(ResourceItem.YoutubeId.Length - 11);
                        ResourceItem.FileType = ResourceFileTypeEnum.youtube;
                        ResourceItem.FileName = ResourceItem.FileName + " - Youtube";
                        ResourceItem.VimeoId = "";
                    }
                    ResourceManager.Save(ResourceItem, null);
                }


                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, "");
                message.Headers.Location = new Uri(Request.RequestUri.AbsoluteUri);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);

            }
        }

        [Route("")]
        [Route("api/Resources/UpdateResource")]
        public HttpResponseMessage UpdateResource(Resource Resource)
        {
            try
            {
                ResourceManager.Save(Resource, null);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, "");
                message.Headers.Location = new Uri(Request.RequestUri.AbsoluteUri);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);

            }
        }

       
        [HttpPost]
        [Route("api/Resources/IncrementViews/{id}")]
        public HttpResponseMessage Post(Guid id)
        {
            try
            {
                ResourceManager.IncrementViews(id);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, "Resource with " + id + " Is Incremented");
                message.Headers.Location = new Uri(Request.RequestUri + "/" + id.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
