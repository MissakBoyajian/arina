﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.DirectoryServices.AccountManagement;
namespace Arina.Services.Controllers
{
    [Authorize]
    public class WindowsUsersController : ApiController
    {
        // GET: api/WindowsUsers
        [Route("api/windowsusers/{name}")]
        public string Get(string name)
        {
            // set up domain context
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, "SomeServer.Foo.Bar.com");

            // find a user
            UserPrincipal user = UserPrincipal.FindByIdentity(ctx, name);

            // find the group in question
            //GroupPrincipal group = GroupPrincipal.FindByIdentity(ctx, "YourGroupNameHere");

            if (user != null)
            {
                return "success";
                //// check if user is member of that group
                //if (user.IsMemberOf(group))
                //{
                //    // do something.....
                //}
            }
            return "failure";
        }

    }
}
