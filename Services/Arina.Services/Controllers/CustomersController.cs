﻿
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using Arina.Business;
using System.Net;
using System;
using System.Text;
using Newtonsoft.Json;

namespace Arina.Services.Controllers
{
    public class CustomersController : ApiController
    {
        [Authorize]
        // GET: api/Issues
        [HttpGet, Route("api/customers")]
        public HttpResponseMessage Get(string Criteria = "", int PageNumber = 1)
        {
            try
            {
                //EmailManager.SendEmail1();
                PaginatedList<Member> Customers = MemberManager.FindMyCustomers(Criteria, PageNumber);
                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Customers = Customers,
                    CurrentPage = Customers.CurrentPage,
                    TotalPages = Customers.TotalPages
                });
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Authorize]
        // POST: api/Issues
        [HttpPost, Route("api/customers")]
        public HttpResponseMessage Post(Member customer)
        {
            try
            {
                if (customer.MemberId.EnsureGuid() == Guid.Empty)
                {
                    customer.MemberId = Guid.NewGuid();
                }
                MemberManager.SaveCustomer(customer);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, customer);
                message.Headers.Location = new Uri(Request.RequestUri + "/" + customer.MemberId.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Authorize]
        // POST: api/Issues
        [HttpGet, Route("api/customers/phone/{phone}")]
        public HttpResponseMessage Post(string Phone)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, MemberManager.FindCustomerByPhone(Phone));
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
