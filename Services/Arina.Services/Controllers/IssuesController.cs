﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Arina.Business;

namespace Arina.Services.Controllers
{
    //[Authorize(Roles = "Manager,OEM,System Administrator,Sales Rep")]
    public class IssuesController : ApiController
    {
        // GET: api/Issues
        [HttpGet, Route("api/issues")]
        public HttpResponseMessage Get(string criteria = "")
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, IssueManager.Find(criteria));
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Route("api/issues/pendingissuescount")]
        public HttpResponseMessage GetPendingCount()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, IssueManager.PendingIssuesCount());
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Route("api/issues/openissuescount")]
        public HttpResponseMessage GetOpenCount()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, IssueManager.OpenIssuesCount());
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Route("api/issues/today")]
        public HttpResponseMessage GetTodaysCalls()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, IssueManager.FindTodaysIssues());
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        [HttpGet, Route("api/issues/MyOpenIssues")]
        public HttpResponseMessage GetOpenIssues(bool IsMine, int pagenumber = 1,
            string criteria = "",
            IssuesPriorityEnum? priority = null,
            Guid? CustomerId = null,
            int? Status = null,
            int? AssignStatus = null,
            string DealerSiteId = null,
            DateTime? CreatedFrom = null,
            DateTime? CreatedTo = null,
            DateTime? DueFrom = null,
            int Sort = 0,
            bool Order = true,
            string UploadedBy = "",
            string UploadedTo = "",
            string IssueType = "",
            string IssuePriority = "",
            string SalesRep = ""
            )
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, IssueManager.FindMyOpenIssues(
                    IsMine: IsMine,
                    PageNumber: pagenumber,
                    CreatedFrom: CreatedFrom,
                    CreatedTo: CreatedTo,
                    criteria: criteria,
                    priority: priority,
                    CustomerId: CustomerId,
                    Status: Status,
                    AssignStatus: AssignStatus,
                    DealerSiteId: DealerSiteId,
                    DueFrom: DueFrom,
                    Sort: Sort,
                    Order: Order,
                    UploadedBy: UploadedBy,
                    UploadedTo: UploadedTo,
                    IssueType: IssueType,
                    IssuePriority: IssuePriority,
                    SalesRep: SalesRep));
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        [HttpGet, Route("api/issues/MyOnHoldIssues")]

        public HttpResponseMessage GetOnHoldIssues(
            bool IsMine,
            int pagenumber = 1,
            string criteria = "",
            IssuesPriorityEnum? priority = null,
            Guid? CustomerId = null,
            string DealerSiteId = null,
            DateTime? CreatedFrom = null,
            DateTime? CreatedTo = null,
            DateTime? DueFrom = null,
            int Sort = 0,
            bool Order = true,
            string UploadedBy = "",
            string UploadedTo = "",
            string IssueType = "",
            string IssuePriority = "",
            string SalesRep = "")
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, IssueManager.FindMyOnHoldIssues
                    (
                    IsMine: IsMine,
                    PageNumber: pagenumber,
                    CreatedFrom: CreatedFrom,
                    CreatedTo: CreatedTo,
                    criteria: criteria,
                    priority: priority,
                    CustomerId: CustomerId,
                    DealerSiteId: DealerSiteId,
                    DueFrom: DueFrom,
                    Sort: Sort,
                    Order: Order,
                    UploadedBy: UploadedBy,
                    UploadedTo: UploadedTo,
                    IssueType: IssueType,
                    IssuePriority: IssuePriority,
                    SalesRep: SalesRep
                    ));
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        [HttpGet, Route("api/issues/MySolvedIssues")]

        public HttpResponseMessage GetSolvedIssues(bool IsMine,
            int pagenumber = 1,
            string criteria = "",
            IssuesPriorityEnum? priority = null,
            Guid? CustomerId = null,
            string DealerSiteId = null,
            DateTime? CreatedFrom = null,
            DateTime? CreatedTo = null,
            DateTime? DueFrom = null,
            int Sort = 0,
            bool Order = true,
            string UploadedBy = "",
            string UploadedTo = "",
            string IssueType = "",
            string IssuePriority = "",
            string SalesRep = "")
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, IssueManager.FindMySolvedIssues(
                    IsMine: IsMine,
                    PageNumber: pagenumber,
                    CreatedFrom: CreatedFrom,
                    CreatedTo: CreatedTo,
                    criteria: criteria,
                    priority: priority,
                    CustomerId: CustomerId,
                    DealerSiteId: DealerSiteId,
                    DueFrom: DueFrom,
                    Sort: Sort,
                    Order: Order,
                    UploadedBy: UploadedBy,
                    UploadedTo: UploadedTo,
                    IssueType: IssueType,
                    IssuePriority: IssuePriority,
                    SalesRep:SalesRep));
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // GET: api/Issues/5
        [HttpGet, Route("api/issues/{id}")]
        public HttpResponseMessage Get(Guid id)
        {
            try
            {
                try
                {
                    Issue issue = IssueManager.Load(id);
                    return Request.CreateResponse(HttpStatusCode.OK, issue);
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, " Issue with ID = " + id.ToString() + " is not found");
                }
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpGet]
        [Route("")]
        [Route("api/issues/ByCallId")]
        public HttpResponseMessage GetIssuesByCallId(Guid CallId)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, IssueManager.FindIssuesByCallId(CallId));
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // POST: api/Issues
        [HttpPost, Route("api/issues")]
        public HttpResponseMessage Post(Issue issue)
        {
            try
            {
                IssueManager.Save(issue);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, issue);
                message.Headers.Location = new Uri(Request.RequestUri + "/" + issue.IssueId.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // POST: api/Issues
        [HttpPost, Route("api/issueslist")]
        public HttpResponseMessage Post(List<Issue> issues)
        {
            try
            {
                IssueManager.Save(issues);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, issues);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // POST: api/Customer
        [HttpPost]
        [Route("api/issues/IncrementViews/{id}")]
        public HttpResponseMessage Post(Guid id)
        {
            try
            {
                IssueManager.IncrementViews(id);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, "");
                message.Headers.Location = new Uri(Request.RequestUri + "/" + id.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // PUT: api/Issues/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Issues/5
        [HttpDelete, Route("api/issues/{id}")]
        public HttpResponseMessage Delete(Guid id)
        {
            try
            {
                int RowsDeleted = IssueManager.Delete(id);
                if (RowsDeleted > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, " Issue with ID = " + id + " is deleted");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, " Issue with ID = " + id.ToString() + " is not found to delete");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
