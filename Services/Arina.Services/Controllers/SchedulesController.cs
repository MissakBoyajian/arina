﻿using Arina.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Arina.Services.Controllers
{
    [Authorize]
    public class SchedulesController : ApiController
    {

        [HttpGet]
        [Route("api/schedules")]
        public HttpResponseMessage Get(DateTime Date)
        {
            try
            {
                try
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SchedulesManager.Get(Date));
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, " Member is not found");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost, Route("api/schedules")]
        public HttpResponseMessage Post(Schedules schedule)
        {
            try
            {
                SchedulesManager.Save(schedule);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, schedule);
                message.Headers.Location = new Uri(Request.RequestUri + "/" + schedule.ScheduleId.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpDelete, Route("api/schedules/{id}")]
        public HttpResponseMessage Delete(Guid id, Boolean Recurring = false)
        {
            try
            {
                SchedulesManager.Delete(id, Recurring);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.OK);
                message.Headers.Location = new Uri(Request.RequestUri + "/" + id.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("api/schedules/current/{memberId}")]
        // GET: api/Members/{memberId}/teams
        public HttpResponseMessage GetCurrentBookings(Guid MemberId, int PageNumber = 1)
        {
            try
            {
                try
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SchedulesManager.GetCurrentBookings(MemberId, PageNumber));
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Member not found");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("api/schedules/previous/{memberId}")]
        // GET: api/Members/{memberId}/teams
        public HttpResponseMessage GetPreviousBookings(Guid MemberId, int PageNumber = 1)
        {
            try
            {
                try
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SchedulesManager.GetPreviousBookings(MemberId, PageNumber));
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Member not found");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


    }
}
