﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Arina.Business;


namespace Arina.Services.Controllers
{
    [Authorize]
    public class ApplicationModulesController : ApiController
    {
        // GET: api/ApplicationModules
        public HttpResponseMessage Get(string criteria = "")
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ApplicationModuleManager.Find(criteria));
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // GET: api/ApplicationModules/5
        public HttpResponseMessage Get(Guid id)
        {
            try
            {
                try
                {
                    ApplicationModule applicationmodule = ApplicationModuleManager.Load(id);
                    return Request.CreateResponse(HttpStatusCode.OK, applicationmodule);
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, " ApplicationModule with ID = " + id.ToString() + " is not found");
                }

            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // POST: api/ApplicationModules
        public HttpResponseMessage Post(ApplicationModule applicationmodule)
        {
            try
            {
                ApplicationModuleManager.Save(applicationmodule);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, applicationmodule);
                message.Headers.Location = new Uri(Request.RequestUri + "/" + applicationmodule.ApplicationModuleId.ToString());
                return message;
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // PUT: api/ApplicationModules/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ApplicationModules/5
        public HttpResponseMessage Delete(Guid id)
        {
            try
            {
                int RowsDeleted = ApplicationModuleManager.Delete(id);
                if (RowsDeleted > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, " ApplicationModule with ID = " + id + " is deleted");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, " ApplicationModule with ID = " + id.ToString() + " is not found to delete");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
