﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Arina.Business;
using Arina;

namespace Arina.Services.Controllers
{
    public class StadiumController : ApiController
    {
        [HttpGet]
        [Route("api/stadiums")]
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, StadiumManager.Get());
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, " Bad request");
            }
        }

        [HttpGet]
        [Route("api/stadiums")]
        public HttpResponseMessage Get(int PageNumber = 1)
        {
            try
            {
                PaginatedList<Stadium> Stadiums = StadiumManager.Get(PageNumber);
                //return Request.CreateResponse(HttpStatusCode.OK, StadiumManager.Get());
                return Request.CreateResponse(HttpStatusCode.OK,
                    new
                    {
                        Stadiums = Stadiums,
                        CurrentPage = Stadiums.CurrentPage,
                        TotalPages = Stadiums.TotalPages
                    });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, " Bad request");
            }
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}