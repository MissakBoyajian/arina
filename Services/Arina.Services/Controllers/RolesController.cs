﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Arina.Business;

namespace Arina.Services.Controllers
{
 //   [Authorize]
    public class RolesController : ApiController
    {

        // GET: api/Role
     
        public HttpResponseMessage Get(string criteria = "")
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, RoleManager.Find(criteria));
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // GET: api/Role/5
        public HttpResponseMessage Get(Guid id)
        {
            try
            {
                try
                {
                    Role role = RoleManager.Load(id);
                    return Request.CreateResponse(HttpStatusCode.OK, role);
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, " Role with ID = " + id.ToString() + " is not found");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // POST: api/Role
        [HttpPost]
        [Route("api/posts")]
        public HttpResponseMessage Post(Role role)
        {
            try
            {
                RoleManager.Save(role);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, role);
                message.Headers.Location = new Uri(Request.RequestUri + "/" + role.RoleId.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // PUT: api/Role/5
        public void Put(int id, string value)
        {
        }

        // DELETE: api/Role/5
        public HttpResponseMessage Delete(Guid id)
        {
            try
            {
                int RowsDeleted = RoleManager.Delete(id);
                if (RowsDeleted > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, " Role with ID = " + id + " is deleted");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, " Role with ID = " + id.ToString() + " is not found to delete");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}