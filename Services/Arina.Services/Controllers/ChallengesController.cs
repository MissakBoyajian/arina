﻿using Arina.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Arina.Services.Controllers
{
    public class ChallengesController : ApiController
    {

        [Authorize]
        [HttpGet]
        [Route("api/stadium")]
        // GET: api/Members/5
        public HttpResponseMessage Get()
        {
            try
            {
                try
                {
                    return Request.CreateResponse(HttpStatusCode.OK, MiniStadiumManager.Get());
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, " Member is not found");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        
        [HttpPost, Route("api/challenges")]
        public HttpResponseMessage Post(Challenge Challenge)
        {
            try
            {
                ChallengeManager.Save(Challenge);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, Challenge);
                message.Headers.Location = new Uri(Request.RequestUri + "/" + Challenge.ChallengeId.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


    }
}
