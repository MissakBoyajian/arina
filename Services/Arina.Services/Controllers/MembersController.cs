﻿using Arina.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Arina.Services.Controllers
{
    public class MembersController : ApiController
    {

        [Route("")]
        [HttpGet, Route("api/members/ByEmail")]
        public HttpResponseMessage GetByEmail(string Email)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, MemberManager.Load(Email));
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [Authorize]
        [HttpGet]
        [Route("api/members/loggedIn")]
        // GET: api/Members/5
        public HttpResponseMessage Get()
        {
            try
            {
                try
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ContextInfo.Current.LoggedInUser);
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, " Member is not found");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("api/members/{memberId}/teams")]
        // GET: api/Members/{memberId}/teams
        public HttpResponseMessage GetMemberTeams(Guid memberId)
        {
            try
            {
                try
                {
                    return Request.CreateResponse(HttpStatusCode.OK, TeamManager.GetTeamsByMemberId(memberId));
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Member not found");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [Authorize]
        [HttpGet]
        [Route("api/members/{memberId}/ministadiums")]
        // GET: api/Members/{memberId}/teams
        public HttpResponseMessage GetMemberStadiums(Guid memberId)
        {
            try
            {
                try
                {
                    return Request.CreateResponse(HttpStatusCode.OK, MiniStadiumManager.GetByMemberId(memberId));
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Member not found");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }




    }
}
