﻿using Arina.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Arina.Services.Controllers
{

    [Authorize]
    public class MiniStadiumController : ApiController
    {
        [HttpGet]
        [Route("api/ministadiums")]
        // GET: api/Members/5
        public HttpResponseMessage Get()
        {
            try
            {
                try
                {
                    return Request.CreateResponse(HttpStatusCode.OK, MiniStadiumManager.Get());
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, " Bad request");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpGet]
        [Route("api/ministadiums/free")]
        // GET: api/Members/5
        public HttpResponseMessage GetFree(int PageNumber, 
            int SortBy, 
            int FloorType, 
            int StadiumType, 
            int DistanceFrom, 
            int DistanceTo, 
            int PriceFrom, 
            int PriceTo,
            decimal Lat, 
            decimal Lon, 
            DateTimeOffset DateFrom,
            int NumPlayers)
        {

            try
            {
                PaginatedList<MiniStadium> FreeStadiums = MiniStadiumManager.GetFree(PageNumber, 
                    SortBy, 
                    FloorType, 
                    StadiumType, 
                    DistanceFrom, 
                    DistanceTo, 
                    PriceFrom, 
                    PriceTo, 
                    Lat,
                    Lon,
                    DateFrom,
                    NumPlayers);
                return Request.CreateResponse(HttpStatusCode.OK,
                    new
                    {
                        FreeStadiums = FreeStadiums,
                        CurrentPage = FreeStadiums.CurrentPage,
                        TotalPages = FreeStadiums.TotalPages
                    });


            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, " Bad request");
            }

        }

    }
}
