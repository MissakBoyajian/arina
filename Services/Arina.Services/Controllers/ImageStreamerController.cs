﻿using Arina.Business;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Arina.Services.Controllers
{

    public class ImageStreamerController : ApiController
    {
        // GET: api/ImageStreamer
        [HttpGet, Route("api/ImageStreamer")]
        public HttpResponseMessage Get(int w = 0, int h = 0, string r = "", Guid? MediaId = null, Guid? OfferId = null, Guid? ResourceId = null, Guid? PromotionId = null, Guid? UploadSalesErrorFileId = null)
        {
            try
            {
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

                if (MediaId != null)
                {
                    Media media = MediaManager.Load(MediaId.EnsureGuid());
                    result.Content = new ByteArrayContent(media.MediaContent);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");

                }
                if (ResourceId != null)
                {
                    Resource resc = ResourceManager.Load(ResourceId.EnsureGuid());
                    result.Content = new ByteArrayContent(resc.FileContent);
                    string fileName = resc.FileNameWithExtension;
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    result.Content.Headers.ContentDisposition.FileName = fileName;
                    if (resc != null && resc.FileType != ResourceFileTypeEnum.docx)
                    {
                        if (resc.FileType == ResourceFileTypeEnum.mp4)
                            result.Content.Headers.ContentType = new MediaTypeHeaderValue("video/" + resc.FileType.ToString());
                        else
                            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/" + resc.FileType.ToString());
                    }
                    else
                        result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.wordprocessingml.document");

                }

                return result;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }
    }
}

