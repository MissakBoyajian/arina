﻿using Arina.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Arina.Services.Controllers
{
    public class TeamsController : ApiController
    {
        [Authorize]
        // GET api/<controller>
        [HttpGet, Route("api/teams")]
        public HttpResponseMessage Get()
        {
            try
            {
                //EmailManager.SendEmail1();
                return Request.CreateResponse(HttpStatusCode.OK, TeamManager.Get());
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Authorize]
        // GET api/<controller>
        [HttpGet, Route("api/teams/{teamId}")]
        public HttpResponseMessage GetByTeamId(Guid teamId)
        {
            try
            {
                //EmailManager.SendEmail1();
                return Request.CreateResponse(HttpStatusCode.OK, TeamManager.GetTeamByTeamId(teamId));
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        //[Authorize]
        // POST: api/teams
        [HttpPost, Route("api/teams")]
        public HttpResponseMessage Post( Team team)
        {
            try
            {
                var teamid = TeamManager.Save(team);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, team);
                message.Headers.Location = new Uri(Request.RequestUri + "/" + team.TeamId.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}
