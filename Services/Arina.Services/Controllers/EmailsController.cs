﻿
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using Arina.Business;
using System.Net;
using System;
using System.Text;
using Newtonsoft.Json;

namespace Arina.Services.Controllers
{
    public class EmailsController : ApiController
    {
        // GET: api/Emails
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Emails/5
        public string Get(int id)
        {
            return "value";
        }
        [HttpPost]
        [Route("api/Emails/sendEmail")]
        public HttpResponseMessage Post([FromBody]string email)
        {
            try
            {
                EmailManager.SendEmail(EmailType.ForgetPassword, email, null, null, MemberManager.Load(email), null);

                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.OK, email);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        [Route("api/Emails/sendEmailNewPassword")]
        public HttpResponseMessage PostNewPassword([FromBody]string email)
        {
            try
            {
                EmailManager.SendEmail(EmailType.NewPassword, email, null, null, MemberManager.Load(email), null);

                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.OK, email);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        [Route("api/Emails/sendissue")]
        public HttpResponseMessage Post([FromBody]Issue issue)
        {
            try
            {
                EmailManager.SendEmail(EmailType.Issue, issue.AssignedToMember.Email, null, null, issue, null);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.OK);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }



        // PUT: api/Emails/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Emails/5
        public void Delete(int id)
        {
        }
    }
}
