﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace Arina.Web.Common
{
    public class FoundationMenu : Menu
    {
        private const string ONE_LEVEL = "<li><a href='{0}'><i class='{1}'></i>{2}</a></li> ";

        private const string MULTI_LEVEL_HEADER = "<li><a><i class='{0}'></i> {1} <span class='fa fa-chevron-down'></span></a><ul class='nav child_menu'>";
        private const string MULTI_LEVEL_ITEM = "<li><a href='{0}'>{1}</a></li>";
        private const string MULTI_LEVEL_FOOTER = "</ul></li>";
        protected override void OnPreRender(EventArgs e)
        {
        }

       
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            this.PerformDataBinding();

            string html = GetHtml(Items);
            writer.Write(html);
        }

        private string GetHtml(MenuItemCollection items)
        {
            StringBuilder str = new StringBuilder();

            str.Append("<ul class='nav side-menu'>");

            foreach (MenuItem item in items)
            {
                if (item.ChildItems.Count == 0 && item.NavigateUrl.Contains("void"))
                    continue;
                else if (item.ChildItems.Count == 0)
                    str.Append(string.Format(ONE_LEVEL, item.NavigateUrl, item.ToolTip, item.Text));
                else
                {
                    str.Append(string.Format(MULTI_LEVEL_HEADER, item.ToolTip, item.Text));
                    foreach (MenuItem subItem in item.ChildItems)
                        str.Append(string.Format(MULTI_LEVEL_ITEM, subItem.NavigateUrl, subItem.Text));

                    str.Append(MULTI_LEVEL_FOOTER);
                }

            }

            str.Append("</ul>");
            return str.ToString();
        }
    }
}