﻿using Arina.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace Arina.Web.Common
{
    public class SecureButton : Button
    {
        public OperationTypes OperationType { get; set; }
        public Modules Module { get; set; }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            ModuleInfoAttribute moduleInfo = typeof(Modules).GetField(Module.ToString()).GetCustomAttribute<ModuleInfoAttribute>();
            this.Visible = CheckPermissions(moduleInfo.ModuleId);
        }

        private bool CheckPermissions(Guid moduleId)
        {
            RolesApplicationModule roleApplicationModule = RolesApplicationModuleManager.Load(ContextInfo.Current.LoggedInUser.RoleId, moduleId);
            bool result = false;
            switch (this.OperationType)
            {
                case OperationTypes.Add:
                    result = roleApplicationModule.AllowAdd;
                    break;
                case OperationTypes.Edit:
                    result = roleApplicationModule.AllowEdit;
                    break;
                case OperationTypes.View:
                    result = roleApplicationModule.AllowView;
                    break;
                case OperationTypes.Delete:
                    result = roleApplicationModule.AllowDelete;
                    break;
                default:
                    break;
            }
            return result;
        }
    }
}
