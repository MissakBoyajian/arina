﻿using Arina.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Arina.Web.Common
{
    public  class ImageHandler : IHttpHandler
    {

        private static ImageCache imageCache = new ImageCache();

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            int height = context.Request.Params["h"].EnsureInt();
            int width = context.Request.Params["w"].EnsureInt();
            double? ratio = context.Request.Params["r"].EnsureDouble();
            if (ratio == 0)
                ratio = null;

            string url = HttpContext.Current.Request.Url.ToString();
            string[] allParts = url.Split("/".ToCharArray());
            string lastPart = allParts[allParts.Length - 1].ToLower();

            string typeName = lastPart.Substring(0, lastPart.IndexOf("-"));
            string id = lastPart.Replace(typeName + "-", string.Empty);
            string extension = id.Substring(id.LastIndexOf(".") + 1);
            id = id.Replace("." + extension, string.Empty);

            byte[] content = imageCache.GetImage("image", id.EnsureGuid(), width, height, ratio, delegate (Guid mediaId)
              {
                  return MediaManager.Load(mediaId).MediaContent;
              });

            if (extension.Contains("png"))
                context.Response.ContentType = "image/png";
            else if (extension.Contains("jpg"))
                context.Response.ContentType = "image/jpg";

            if (content == null || content.Length == 0)
                context.Response.BinaryWrite(new byte[0]);
            else
                context.Response.BinaryWrite(content);
        }
    }
}