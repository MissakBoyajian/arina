using System;
using System.Collections.Generic;
using Arina.Business.Data;

namespace Arina.Business
{
	public static class RoleManager
	{
		private static IRoleDataService data = DataManager.GetDataImplementation<IRoleDataService>();
		public static void Save(this Role role)
		{
			data.Save(role);
		}
		public static List<Role> Find(string criteria)
		{
			return data.Find(criteria);
		}
		public static Role Load(Guid roleid)
		{
			return data.Load(roleid);
		}
		public static int Delete(Guid roleid)
		{
			return data.Delete(roleid);
		}
	}
}
