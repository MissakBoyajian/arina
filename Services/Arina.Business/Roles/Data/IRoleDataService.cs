using System;
using System.Collections.Generic;

namespace Arina.Business.Data
{
	public interface IRoleDataService
	{
		void Save(Role role);
		List<Role> Find(string criteria);
		Role Load(Guid roleid);
		int Delete(Guid roleid);
	}
}
