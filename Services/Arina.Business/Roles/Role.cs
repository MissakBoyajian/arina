using System;
using System.Collections.Generic;

namespace Arina.Business
{
    public class Role
    {
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }

    }
}
