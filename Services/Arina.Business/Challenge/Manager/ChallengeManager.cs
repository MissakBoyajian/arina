using System;
using System.Collections.Generic;
using Arina.Business.Data;

namespace Arina.Business
{
    public static class ChallengeManager
    {
        private static IChallengeDataService data = DataManager.GetDataImplementation<IChallengeDataService>();


        public static void Save(Challenge stadium)
        {
            data.Save(stadium);
        }

        public static List<Challenge> Get()
        {
            return data.Get();
        }

    }
}
