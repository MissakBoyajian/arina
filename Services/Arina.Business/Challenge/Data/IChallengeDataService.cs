using System;
using System.Collections.Generic;

namespace Arina.Business.Data
{
	public interface IChallengeDataService
    {
		void Save(Challenge Schedule);
		List<Challenge> Get();

    }
}
