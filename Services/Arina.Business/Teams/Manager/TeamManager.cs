using System;
using System.Collections.Generic;
using Arina.Business.Data;

namespace Arina.Business
{
    public static class TeamManager
    {
        private static ITeamDataService data = DataManager.GetDataImplementation<ITeamDataService>();


        public static Guid Save(Team team)
        {
            return data.Save(team);
        }

        public static List<Team> Get()
        {
            return data.Get();
        }

        public static List<Team> GetTeamsByMemberId(Guid memberId)
        {
            return data.GetTeamsByMemberId(memberId);
        }

        public static List<Team> GetTeamByTeamId(Guid teamId)
        {
            return data.GetTeamByTeamId(teamId);
        }

    }
}
