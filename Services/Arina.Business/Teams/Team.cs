using System;
using System.Collections.Generic;

namespace Arina.Business
{

    public class Team
    {
        public Team()
        {

        }

        public Guid TeamId { get; set; }
        public string Name { get; set; }
        public int Available { get; set; }
        public int Badge { get; set; }
        public string Comments { get; set; }
        public DateTime CreatedOn { get; set; }
        public string HomeJersey { get; set; }
        public string AwayJersey { get; set; }
        public int NumberOfGames { get; set; }
        public int Rank { get; set; }
        public decimal Rating { get; set; }
        public int ReviewRating { get; set; }
        public int Status { get; set; }
        public int Wins { get; set; }
        public int WinStreak { get; set; }
        public int YellowCard { get; set; }
        public int TeamOfSize { get; set; }
        public int TeamOfSize1 { get; set; }
        public int TeamOfSize2 { get; set; }
        public int AvailableMFrom { get; set; }
        public int AvailableMTo { get; set; }
        public int AvailableTFrom { get; set; }
        public int AvailableTTo { get; set; }
        public int AvailableWFrom { get; set; }
        public int AvailableWTo { get; set; }
        public int AvailableThFrom { get; set; }
        public int AvailableThTo { get; set; }
        public int AvailableFFrom { get; set; }
        public int AvailableFTo { get; set; }
        public int AvailableSaFrom { get; set; }
        public int AvailableSaTo { get; set; }
        public int AvailableSuFrom { get; set; }
        public int AvailableSuTo { get; set; }
        public int NumberOfPlayers { get; set; }
        public Guid Captain { get; set; }

        public Stadium FavStadium { get; set; }


    }
}
