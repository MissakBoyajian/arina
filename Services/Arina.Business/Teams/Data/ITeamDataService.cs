using System;
using System.Collections.Generic;

namespace Arina.Business.Data
{
	public interface ITeamDataService
    {
		Guid Save(Team team);
		List<Team> Get();
        List<Team> GetTeamsByMemberId(Guid memberId);
        List<Team> GetTeamByTeamId(Guid teamIda);
    }
}
