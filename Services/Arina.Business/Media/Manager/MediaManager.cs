﻿using System;
using System.Collections.Generic;
using Arina.Business.Data;
using System.Xml.Serialization;

namespace Arina.Business
{
   public static class MediaManager
    {
        private static IMediaDataService data = DataManager.GetDataImplementation<IMediaDataService>();
        public static void Save(this Media media)
        {
            data.Save(media);
        }
        public static List<Media> Find(string criteria)
        {
            return data.Find(criteria);
        }
        public static Media Load(Guid newsid)
        {
            return data.Load(newsid);
        }
        public static int Delete(Guid newsid)
        {
            return data.Delete(newsid);
        }

        public static List<Media> GetMediaByReffererId(Guid reffererid) {

            return data.GetMediaByReffererId(reffererid);
        }

        public static List<Media> LoadFromXml(string xml)
        {
            try
            {
                List<Media> retVal = xml == "" ? null : xml.GetObjectFromXMLString<List<Media>>();
                if (retVal == null)
                    return new List<Media>();
                else return retVal;
            }
            catch (Exception ex)
            {
                return new List<Media>();
            }
        }
    }
}
