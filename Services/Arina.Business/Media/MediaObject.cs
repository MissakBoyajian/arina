﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Arina.Business
{
    [Serializable]
    [DataContract]
    public abstract class MediaObject
    {
        private List<Media> _media;
        public abstract Guid ReferrerId { get; }


        [DataMember]
        public List<Media> Media { get
            {
                if (_media == null)
                    return new List<Business.Media>();
                else
                    return _media;
            }
            set
            {
                _media = value;
            }
        }

        [DataMember]
        [XmlIgnore]
        public string MainMediaUrl
        {
            get
            {
                if (Media.Count == 0)
                    return string.Empty;
                else
                    return Media[0].MediaUrl;
            }
            private set { }
        }
    }
}