﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Arina.Business
{
    public enum MediaTypeEnum
    {
        [XmlEnum("0")]
        unknown = 0,

        [XmlEnum("1")]
        image = 1,

        [XmlEnum("2")]
        document = 2,

        [XmlEnum("3")]
        excelfile = 3,

        [XmlEnum("4")]
        wordfile = 4
    }
}
