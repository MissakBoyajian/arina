﻿using System;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace Arina.Business
{
    [DataContract]
    public class Media
    {
        [XmlAttribute]
        [DataMember]
        public Guid MediaId { set; get; }

        [XmlAttribute]
        public MediaTypeEnum MediaType { set; get; }


        [XmlAttribute]
        public Guid ReffererId { set; get; }


        [XmlAttribute]
        public string Caption { set; get; }


        [XmlIgnore]
        public byte[] MediaContent { set; get; }

        [XmlAttribute]
        public long TimeStamp { get; set; }


        [XmlAttribute]
        public bool IsOnCdn { get; set;}

        [XmlAttribute]
        public string CdnUrl { get; set; }

        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public Guid CreatedBy { set; get; }

        [XmlAttribute]
        public DateTime CreatedOn { set; get; }

        [XmlIgnore]
        [DataMember]
        public string MediaUrl
        {
            get
            {
                return  System.Configuration.ConfigurationSettings.AppSettings["BaseUrl"] +"/api/ImageStreamer?MediaId=" + MediaId;
            }
            private set { }
        }


    }
}
