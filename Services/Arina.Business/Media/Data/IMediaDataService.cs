﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arina.Business.Data
{
    public interface IMediaDataService
    {
        void Save(Media media);
        List<Media> Find(string criteria);
        Media Load(Guid mediaid);
        int Delete(Guid mediaid);
        List<Media> GetMediaByReffererId(Guid reffererid);
    }
}
