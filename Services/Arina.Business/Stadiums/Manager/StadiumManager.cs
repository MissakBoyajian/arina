﻿using System;
using System.Collections.Generic;
using Arina.Business.Data;
using Arina;

namespace Arina.Business
{
    public class StadiumManager
    {
        private static IStadiumDataService data = DataManager.GetDataImplementation<IStadiumDataService>();

        public static void Save(Stadium stadium)
        {

        }

        public static List<Stadium> Get()
        {
            return data.Get();
        }

        public static PaginatedList<Stadium> Get(int PageNumber)
        {
            return data.Get();
        }

        public static List<Stadium> GetById(Guid stadiumId)
        {
            return GetById(stadiumId);
        }

        public static List<Stadium> GetByMemberId()
        {
            return GetByMemberId();
        }

        public static List<Stadium> GetByMemberId(Guid memberId)
        {
            return GetByMemberId(memberId);
        }
    }
}
