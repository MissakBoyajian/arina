using System;
using System.Collections.Generic;

namespace Arina.Business
{

    public class Stadium
    {
        public Stadium()
        {
            
        }

        public Stadium( Guid stadiumId, string name, Guid memberId, int openingHourTime, int openingHourMinutes, int closingHourTime, int closingHourMinutes, int restrictedHourTime, int restrictedHourMinutes, decimal lat, decimal lon, string cancellationPolicy, string area, string city, string email, decimal rating, string telephone, int hasWater, string specialFeatures)
        {
            StadiumId = stadiumId;
            Name = name;
            MemberId = memberId;
            OpeningHourTime = openingHourTime;
            OpeningHourMinutes = openingHourMinutes;
            ClosingHourTime = closingHourTime;
            ClosingHourMinutes = closingHourMinutes;
            RestrictedHourTime = restrictedHourTime;
            RestrictedHourMinutes = restrictedHourMinutes;
            Lat = lat;
            Lon = lon;
            CancellationPolicy = cancellationPolicy;
            Area = area;
            City = city;
            Email = email;
            Rating = rating;
            Telephone = telephone;
            HasWater = hasWater;
            SpecialFeatures = specialFeatures;
        }
        
        public Guid StadiumId { get; set; }
        public string Name { get; set; }
        public Guid MemberId { get; set; }

        public int OpeningHourTime { get; set; }
        public int OpeningHourMinutes { get; set; }
        public int ClosingHourTime { get; set; }
        public int ClosingHourMinutes { get; set; }
        public int RestrictedHourTime { get; set; }
        public int RestrictedHourMinutes { get; set; }

        public decimal Lat { get; set; }
        public decimal Lon { get; set; }

        
        public string CancellationPolicy { get; set; }
        public string Area { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public decimal Rating { get; set; }
        public string Telephone { get; set; }
        public int HasWater { get; set; }
        public string SpecialFeatures { get; set; }

    }

    public enum StadiumTypeFloor
    {
        Grass,
        Ground
    }

    public enum StadiumType
    {
        Indoor,
        Outdoor
    }
}
