﻿using System;
using System.Collections.Generic;

namespace Arina.Business.Data
{
    public interface IStadiumDataService
    {
        void Save(Stadium stadium);
        List<Stadium> Get();
        List<Stadium> GetById(Guid stadiumId);
        List<Stadium> GetByMemberId();
        List<Stadium> GetByMemberId(Guid memberId);
    }
}
