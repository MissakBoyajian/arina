using System;
using System.Collections.Generic;

namespace Arina.Business.Data
{
	public interface IApplicationModuleDataService
	{
		void Save(ApplicationModule applicationmodule);
		List<ApplicationModule> Find(string criteria);
		ApplicationModule Load(Guid applicationmoduleid);
		int Delete(Guid applicationmoduleid);
	}
}
