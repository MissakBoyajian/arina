using System;
using System.Collections.Generic;

namespace Arina.Business
{
	public class ApplicationModule
	{
		public Guid ApplicationModuleId  { get; set; }
		public string Name  { get; set; }
	}
}
