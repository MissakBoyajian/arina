﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arina.Business
{
    public enum ApplicationModuleEnum
    {
        Products = 0,
        Calendar,
        CustomersDetails,
        Promotions,
        Issues,
        Resources,
        DashboardDetails,
        CustomersListing,
        Reporting,
        SalesData,
        Administration,
        Calls,
        Tasks,
        Incoming
    }
}
