using System;
using System.Collections.Generic;
using Arina.Business.Data;

namespace Arina.Business
{
	public static class ApplicationModuleManager
	{
		private static IApplicationModuleDataService data = DataManager.GetDataImplementation<IApplicationModuleDataService>();
		public static void Save(this ApplicationModule applicationmodule)
		{
			data.Save(applicationmodule);
		}
		public static List<ApplicationModule> Find(string criteria)
		{
			return data.Find(criteria);
		}
		public static ApplicationModule Load(Guid applicationmoduleid)
		{
			return data.Load(applicationmoduleid);
		}
		public static int Delete(Guid applicationmoduleid)
		{
			return data.Delete(applicationmoduleid);
		}
	}
}
