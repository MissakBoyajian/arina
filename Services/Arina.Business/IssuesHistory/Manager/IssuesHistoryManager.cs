﻿using Arina.Business.Data;
using System;
using System.Collections.Generic;


namespace Arina.Business
{
   public class IssuesHistoryManager
    {
        private static IIssuesHistoryDataService data = DataManager.GetDataImplementation<IIssuesHistoryDataService>();

        public static List<IssueHistory> Find()
        {

            return data.Find();
        }

        public static Guid Save(IssueHistory issueHistory)
        {

           return data.Save(issueHistory);
        }
        public static List<IssueHistory> LoadByIssue(Guid issueId)
        {

            return data.LoadByIssue(issueId);
        }
        public static void Save(List<IssueHistory> issueHistories)
        {
            data.Save(issueHistories);
        }
    }
}
