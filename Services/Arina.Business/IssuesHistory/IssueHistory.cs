﻿using System;
using System.Runtime.Serialization;

namespace Arina.Business
{
    [Serializable]
    [DataContract]
    public class IssueHistory : MediaObject
    {
        Lazy<Member> CurrentMemberObj = new Lazy<Member>();

        public IssueHistory()
        {

            CurrentMemberObj = new Lazy<Member>(delegate ()
            {
                return MemberManager.LoadById(MemberId);
            });

        }

        [DataMember]
        public Guid IssueHistoryId { get; set; }
        [DataMember]
        public Guid IssueId { get; set; }
        [DataMember]
        public OperationTypeEnum Operation { get; set; }
        [DataMember]
        public Guid MemberId { get; set; }
        [DataMember]
        public string CommentText { get; set; }
        [DataMember]
        public Guid ReassignedId { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public IssueTypeEnum Type { get; set; }

        [DataMember]
        public string IssueTypeString
        {
            get
            {
                return Type.ToString();
            }
            private set { }
        }
        [DataMember]
        public IssuesStatusEnum Status { get; set; }
        [DataMember]

        public long TimeStamp { get; set; }
        [DataMember]

        public Issue issue { get; set; }
        [DataMember]

        public bool IsViewed { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string StatusDescription
        {
            get
            {
                return Status.ToString().SpaceOnCapital();
            }
            private set { }
        }
        [DataMember]
        public Member CurrentMember
        {
            get
            {
                return CurrentMemberObj.Value;
            }
            private set { }

        }

        [DataMember]
        public Member ReassignedMember { get; set; }

        [DataMember]
        public override Guid ReferrerId
        {
            get
            {
                return IssueHistoryId;
            }
        }
    }
}