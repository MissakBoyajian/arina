﻿using System;
using System.Collections.Generic;

namespace Arina.Business.Data
{
    public interface IIssuesHistoryDataService
    {
        List<IssueHistory> Find();
        List<IssueHistory> LoadByIssue(Guid IssueId);
        Guid Save(IssueHistory issueHistory);
        void Save(List<IssueHistory> issueHistories);

    }
}
