﻿using System;
using System.Timers;

namespace Arina.Business
{
    public abstract class BatchJobService
    {
        private Timer pushNotificationTimer;

        public void Start(BatchJobType type)
        {
            if (pushNotificationTimer == null)
            {
                pushNotificationTimer = new Timer(type == BatchJobType.Email ? 5000 : 60000);
                pushNotificationTimer.Elapsed += new ElapsedEventHandler(this.BatchWork);
                pushNotificationTimer.Start();
            }
           
        }
        public abstract void DoBatchWork();

        private void BatchWork(object sender, ElapsedEventArgs e)
        {
            pushNotificationTimer.Stop();
            try
            {

                    try
                    {
                        DoBatchWork();
                    }
                    catch (Exception ex)
                    {
                        // Do nothing
                    }
                
            }
            catch (Exception ex)
            {
                // Do nothing
            }
            finally
            {
                pushNotificationTimer.Start();
            }
        }
    }
}
