﻿
using System.Security.Claims;
using System.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;

namespace Arina.Business
{
    public class ContextInfo
    {
        private const string ContextInfoPropertyName = "ContextInfo";
        public static ContextInfo Current
        {
            get
            {
                ContextInfo retVal = HttpContext.Current.Items[ContextInfo.ContextInfoPropertyName] as ContextInfo;
                if (retVal == null)
                {
                    string email = string.Empty;
                    if (HttpContext.Current.User.Identity is FormsIdentity)
                    {
                        email = HttpContext.Current.User.Identity.Name;
                    }
                    else if (HttpContext.Current.User.Identity is ClaimsIdentity)
                    {
                        ClaimsIdentity idendity = (ClaimsIdentity)HttpContext.Current.User.Identity;
                        email = idendity.Claims.FirstOrDefault<Claim>(x => x.Type == ClaimTypes.Email).Value;
                    }


                    InitiateContext(email);
                    retVal = HttpContext.Current.Items[ContextInfo.ContextInfoPropertyName] as ContextInfo;
                }

                return retVal;
            }
        }

        public bool IsUserLoggedIn
        {
            get
            {
                return LoggedInUser != null; ;
            }
        }

        public Member LoggedInUser { get; private set; }

        private ContextInfo()
        {
        }

        public static void InitiateContext(string email)
        {
            Member member = null;
            try
            {
                member = MemberManager.Load(email);
            }
            catch (NotFoundException)
            {
                member = null;
            }

            InitiateContext(member);
        }
        public static void InitiateContext(Member member)
        {
            ContextInfo context = new ContextInfo()
            {
                LoggedInUser = member
            };
            HttpContext.Current.Items.Add(ContextInfo.ContextInfoPropertyName, context);
        }
    }
}
