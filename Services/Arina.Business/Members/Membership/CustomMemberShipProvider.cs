﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;

namespace Arina.Business.Membership
{
    public class CustomMemberShipProvider : System.Web.Security.MembershipProvider
    {
        public CustomMemberShipProvider()
        {}

        public override string ApplicationName
        {
            get
            {
                return CustomRoleProvider.APPLICATION_NAME;
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        public override bool EnablePasswordReset
        {
            get { return false; }
        }
        public override bool EnablePasswordRetrieval
        {
            get { return true; }
        }
        public override int MaxInvalidPasswordAttempts
        {
            get { return 3; }
        }
        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return 0; }
        }
        public override int MinRequiredPasswordLength
        {
            get { return 6; }
        }
        public override int PasswordAttemptWindow
        {
            get { return 0; }
        }
        public override string PasswordStrengthRegularExpression
        {
            get { return string.Empty; }
        }
        public override bool RequiresQuestionAndAnswer
        {
            get { return false; }
        }
        public override bool RequiresUniqueEmail
        {
            get { return false; }
        }
        public override MembershipPasswordFormat PasswordFormat
        {
            get { return MembershipPasswordFormat.Clear; }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            try
            {
                Member loggedUser = MemberManager.Load(username, oldPassword);
                loggedUser.Password = newPassword;
                loggedUser.Save();
                return true;
            }
            catch (NotFoundException)
            {
                return false;
            }
        }        
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            try
            {
                Member loggedUser = MemberManager.Load(username);
                MembershipUser retVal = new MembershipUser(base.Name, loggedUser.Email, loggedUser.Email, string.Empty, string.Empty, string.Empty, true, loggedUser.IsDisabled, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now);
                return retVal;
            }
            catch (NotFoundException)
            {
                return null;
            }
        }
        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            if (providerUserKey == null)
                return null;
            else
                return GetUser(providerUserKey.ToString(), userIsOnline);
        }
        public override bool ValidateUser(string username, string password)
        {
            try
            {
                Member loggedUser = MemberManager.Load(username, password);
                return !loggedUser.IsDisabled;
            }
            catch (NotFoundException)
            {
                return false;
            }
        }
        
        #region Not Supported
        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }
        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }
        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }
        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out System.Web.Security.MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }
        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }
        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }
        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }
        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
