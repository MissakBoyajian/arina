﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arina.Business.Membership
{
    public class CustomRoleProvider : System.Web.Security.RoleProvider
    {
        #region Constants
        internal const string APPLICATION_NAME = "TritonMembers";
        #endregion
        
        public override string ApplicationName
        {
            get
            {
                return APPLICATION_NAME;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string[] GetAllRoles()
        {
            return new List<string>().ToArray();
        }
        public override bool RoleExists(string roleName)
        {
            string[] allRoles = GetAllRoles();
            bool retVal = allRoles.Contains<string>(roleName);
            return retVal;
        }
        public override string[] GetRolesForUser(string username)
        {
            Member broker = MemberManager.Load(username);
            if (broker == null)
                throw new Exception(string.Format("User with ID '{0}' cannot be found", username));
            string[] retVal = GetMemberRoles(broker);
            return retVal;
        }
        private string[] GetMemberRoles(Member broker)
        {
            try
            {
                
                HashSet<string> memberRoles = new HashSet<string>();
                List<RolesApplicationModule> rolesApplicationModules = RolesApplicationModuleManager.LoadByRoleId(broker.RoleId);

                foreach (RolesApplicationModule roleAplicationModules in rolesApplicationModules)
                {
                    if (roleAplicationModules.AllowAdd)
                        memberRoles.Add(roleAplicationModules.ApplicationModule.Name + " Add");
                    if (roleAplicationModules.AllowEdit)
                        memberRoles.Add(roleAplicationModules.ApplicationModule.Name + " Edit");
                    if (roleAplicationModules.AllowView)
                        memberRoles.Add(roleAplicationModules.ApplicationModule.Name + " View");
                    if (roleAplicationModules.AllowDelete)
                        memberRoles.Add(roleAplicationModules.ApplicationModule.Name + " Delete");
                }

                String[] stringArray = new String[memberRoles.Count];
                memberRoles.CopyTo(stringArray);

                return stringArray;
            }
            catch (NotFoundException)
            {
                return new string[0];
            }
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            string[] userRoles = GetRolesForUser(username);
            bool isMember = userRoles.Contains<string>(roleName);
            return isMember;
        }

        #region Not Supported
        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }
        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }
        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }
        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }
        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

}
