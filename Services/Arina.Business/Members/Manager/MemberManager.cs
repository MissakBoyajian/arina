using System;
using System.Collections.Generic;
using Arina.Business.Data;

namespace Arina.Business
{
    public static class MemberManager
    {
        private static IMemberDataService data = DataManager.GetDataImplementation<IMemberDataService>();

        public static void Save(this Member member)
        {
            data.Save(member);
        }

        public static void SaveCustomer(this Member member)
        {
            data.SaveCustomer(member);
        }

        public static PaginatedList<Member> FindMyCustomers(string Criteria = "", int PageNumber = 1)
        {
            return data.FindMyCustomers(Criteria, PageNumber);
        }

        public static Member FindCustomerByPhone(string Phone)
        {
            return data.FindCustomerByPhone(Phone);
        }

        public static List<Member> FindAll(string criteria, string DealerSiteId)
        {
            return data.FindAll(criteria, DealerSiteId);
        }

        public static Member Load(string email)
        {
            return data.Load(email);
        }

        public static Member LoadById(Guid memberId)
        {
            return data.LoadById(memberId);
        }

        public static Member Load(string email, string password)
        {
            return data.Load(email, password);
        }

        public static int Delete(Guid memberId)
        {
            return data.Delete(memberId);
        }
        public static bool ValidateQuestion(string Email, Guid QuestionId, string reply)
        {
            return data.ValidateQuestion(Email, QuestionId, reply);
        }

        public static void UpdatePassword(string NewPassword, string Email, Guid ResetPasswordToken)
        {

            data.UpdatePassword(NewPassword, Email, ResetPasswordToken);
        }

        public static List<Member> LoadFromXml(string xml)
        {
            try
            {
                List<Member> retVal = xml.GetObjectFromXMLString<List<Member>>();
                if (retVal == null)
                    return new List<Member>();
                else return retVal;
            }
            catch (Exception ex)
            {
                return new List<Member>();
            }
        }
    }
}
