using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace Arina.Business
{
    [Serializable]
    [DataContract]
    public class Member
    {

        public Member()
        {

        }

        [DataMember]
        public Guid MemberId { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public long TimeStamp { get; set; }

        [DataMember]
        public Guid CreatedBy { get; set; }

        [DataMember]
        public Member CreatedByMember { get; set; }

        [DataMember]
        public DateTime CreatedOn { get; set; }

        [DataMember]
        public int AvailableMFrom { get; set; }
        [DataMember]
        public int AvailableMTo { get; set; }
        [DataMember]
        public int AvailableTFrom { get; set; }
        [DataMember]
        public int AvailableTTo { get; set; }
        [DataMember]
        public int AvailableWFrom { get; set; }
        [DataMember]
        public int AvailableWTo { get; set; }
        [DataMember]
        public int AvailableThFrom { get; set; }
        [DataMember]
        public int AvailableThTo { get; set; }
        [DataMember]
        public int AvailableFFrom { get; set; }
        [DataMember]
        public int AvailableFTo { get; set; }
        [DataMember]
        public int AvailableSaFrom { get; set; }
        [DataMember]
        public int AvailableSaTo { get; set; }
        [DataMember]
        public int AvailableSuFrom { get; set; }
        [DataMember]
        public int AvailableSuTo { get; set; }
        [DataMember]
        public int Rank { get; set; }
        [DataMember]
        public int Rating { get; set; }
        [DataMember]
        public int Wins { get; set; }
        [DataMember]
        public int WinStreak { get; set; }
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public int Skill { get; set; }

        public string FullName
        {
            get
            {
                if (string.IsNullOrEmpty(FirstName) && string.IsNullOrEmpty(LastName))
                    return Email;
                else if (string.IsNullOrEmpty(FirstName) && !string.IsNullOrEmpty(LastName))
                    return LastName;
                else if (!string.IsNullOrEmpty(FirstName) && string.IsNullOrEmpty(LastName))
                    return FirstName;
                else
                    return FirstName + " " + LastName;
            }
        }

        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public bool IsConfirmed { get; set; }

        public string Password { get; set; }

        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Mobile { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public bool IsDisabled { get; set; }

        [DataMember]
        public Guid RoleId { get; set; }

        [DataMember]
        public Role Role { get; set; }


        public Guid ResetPasswordToken { get; set; }

        public Guid DealerSiteId { get; set; }

        public int SnoozeTime { get; set; }
        [DataMember]
        public MemberStatusEnum Status { get; set; }

        [DataMember]
        public DateTime LastLoginDate { get; set; }

        [DataMember]
        public string StatusString
        {
            get
            {
                return Status.ToString();
            }
        }
        [DataMember]
        public string PhysicalState { get; set; }

        [DataMember]
        public string Suburb { get; set; }

        public string BaseUrl
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["BaseUrl"];
            }
            private set { }
        }
        public bool IsManager { get; set; }
    }
}
