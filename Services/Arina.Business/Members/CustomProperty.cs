﻿using System.Xml.Serialization;

namespace Arina.Business
{
    public class CustomProperty
    {
        [XmlAttribute]
        public string PropertyName { get; set; }
        [XmlAttribute]
        public string PropertyValue { get; set; }
    }
}
