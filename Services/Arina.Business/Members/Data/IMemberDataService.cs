using System;
using System.Collections.Generic;

namespace Arina.Business.Data
{
    public interface IMemberDataService
    {
        bool ValidateQuestion(string Email, Guid QuestionId, string reply);
        void Save(Member member);
        void SaveCustomer(Member member);
        PaginatedList<Member> FindMyCustomers(string Criteria, int PageNumber);
        Member FindCustomerByPhone(string Phone);
        List<Member> FindAll(string criteria, string DealerSiteId);
        Member Load(string email);
        Member LoadById(Guid memberId);
        Member Load(string email, string password);
        int Delete(Guid memberId);
        void UpdatePassword(string NewPassword, string Email, Guid ResetPasswordToken);
    }
}
