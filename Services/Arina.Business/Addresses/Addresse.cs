using System;
using System.Collections.Generic;

namespace Arina.Business
{
	public class Addresse
	{
		public Guid AddressId { get; set; }
		public Guid ReferrerId  { get; set; }
		public Double Lat  { get; set; }
		public Double Lon  { get; set; }
		public string AddressLine1  { get; set; }
		public string AddressLine2  { get; set; }
		public string CountryCode { get; set; }
        public string Postcode  { get; set; }
		public Guid SuburbId  { get; set; }
		public string Suburb  { get; set; }
        public Guid StateId  { get; set; }
		public bool IsPrimary  { get; set; }
        public long TimeStamp { get; set; }

        public string PhysicalState { get; set; }
        


    }
}
