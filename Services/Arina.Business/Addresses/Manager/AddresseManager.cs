using System;
using System.Collections.Generic;
using Arina.Business.Data;

namespace Arina.Business
{
	public static class AddresseManager
	{
		private static IAddresseDataService data = DataManager.GetDataImplementation<IAddresseDataService>();
		public static void Save(this Addresse addresse)
		{
            data.DeleteByReferrerId(addresse.ReferrerId);
			data.Save(addresse);
		}
		public static List<Addresse> Find(string criteria)
		{
			return data.Find(criteria);
		}
		public static Addresse Load(Guid addressid)
		{
			return data.Load(addressid);
		}
		public static int Delete(Guid addressid)
		{
			return data.Delete(addressid);
		}

        public static void DeleteByReferrerId(Guid CustomerId)
        {
            data.DeleteByReferrerId(CustomerId);
        }

        public static Addresse LoadByReferrerId(Guid referrerId)
        {
            return data.LoadByReferrerId(referrerId);
        }
    }
}
