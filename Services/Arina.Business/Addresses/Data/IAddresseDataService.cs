using System;
using System.Collections.Generic;

namespace Arina.Business.Data
{
	public interface IAddresseDataService
	{
		void Save(Addresse addresse);
		List<Addresse> Find(string criteria);
		Addresse Load(Guid addressid);
        Addresse LoadByReferrerId(Guid referrerId);
        int Delete(Guid addressid);
        void DeleteByReferrerId(Guid CustomerId);
    }
}
