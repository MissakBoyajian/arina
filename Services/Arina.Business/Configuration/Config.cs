﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arina.Business
{
    public class Config
    {
        // General
        public string DateFormat { get; set; }
        public string DateTimeFormat { get; set; }
        public int GridPageSize { get; set; }
        public string DateTimeFormatServices
        {
            get
            {
                return "dd-MMM-yyyy HH:mm:ss";
            }
        }

        // Push
        public string AuthorizationToken { get; set; }
        public string IosCertificateFile { get; set; }
        public string IosCertificatePassword { get; set; }
        public double PushInterval { get; set; }

        // HomePage
        public Guid HomePageId { get; set; }

        // About Us
        public Guid AboutUsId { get; set; }
        public string AboutUsTitle { get; set; }
        public string AboutUsDescription { get; set; }

        //Email
        public string SMTPServer { get; set; }
        public int SMTPPort { get; set; }
        public bool SMTPSSL { get; set; }
        public bool SMTPRequirePassword { get; set; }
        public string SMTPFrom { get; set; }
        public string SMTPPassword { get; set; }
        public string SMTPReplyTo { get; set; }

    }
}
