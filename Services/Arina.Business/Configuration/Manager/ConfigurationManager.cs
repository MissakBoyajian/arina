using System;
using System.Collections.Generic;
using Arina.Business.Data;

namespace Arina.Business
{
	public static class ConfigurationManager
	{
		private static IConfigurationDataService data = DataManager.GetDataImplementation<IConfigurationDataService>();
		public static void Save(this Configuration configuration)
		{
			data.Save(configuration);
		}
		public static List<Configuration> Find(string criteria)
		{
			return data.Find(criteria);
		}
		public static Configuration Load(Guid configurationid)
		{
			return data.Load(configurationid);
		}

        public static Configuration Load(ConfigurationEnum externalId)
        {
            return data.Load(externalId);
        }
        public static void Delete(Guid configurationid)
		{
			data.Delete(configurationid);
		}
	}
}
