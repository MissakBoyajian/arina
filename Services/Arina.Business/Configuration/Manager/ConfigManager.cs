﻿
using Arina.Business.Data;

namespace Arina.Business
{
    public static class ConfigManager
    {
        private static IConfigDataService data = DataManager.GetDataImplementation<IConfigDataService>();

        public static Config Current { get; private set; }

        static ConfigManager()
        {
            Current = Load();
        }

        public static Config Load()
        {
            return data.Load();
        }
        public static void Update(this Config config)
        {
            data.Update(config);
        }
    }
}
