using System;
using System.Collections.Generic;

namespace Arina.Business
{
	public class Configuration
	{
		public Guid ConfigurationId  { get; set; }
		public string Description  { get; set; }
		public ConfigurationEnum ExternalId  { get; set; }
	}
}
