using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Randem.Triton.Business;
using Randem.Triton.Business.Data;

namespace Randem.Triton.Data.Sql
{
	public class ConfigurationDataService : IConfigurationDataService
	{
		public void Save(Configuration configuration)
		{
			using (CustomSqlConnection connection = new CustomSqlConnection())
			{
				using (SqlCommand cmd = new SqlCommand("Configuration_Save", connection))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new CustomSqlParameter("ConfigurationId", SqlDbType.UniqueIdentifier, configuration.ConfigurationId));
					cmd.Parameters.Add(new CustomSqlParameter("Description", SqlDbType.NVarChar, configuration.Description));
					cmd.Parameters.Add(new CustomSqlParameter("ExternalId", SqlDbType.Int, configuration.ExternalId));
					cmd.ExecuteNonQuery();
				}
			}
		}
		public List<Configuration> Find(string criteria)
		{
			using (CustomSqlConnection connection = new CustomSqlConnection())
			{
				using (SqlCommand cmd = new SqlCommand("Configuration_Find", connection))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new CustomSqlParameter("criteria", SqlDbType.NVarChar,  "%" + criteria + "%"));
					SqlDataAdapter adapter = new SqlDataAdapter(cmd);
					DataTable dt = new DataTable();
					adapter.Fill(dt);
					return Load(dt);
				}
			}
		}
		public Configuration Load(Guid configurationid)
		{
			using (CustomSqlConnection connection = new CustomSqlConnection())
			{
				using (SqlCommand cmd = new SqlCommand("Configuration_SelectById", connection))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new CustomSqlParameter("ConfigurationId", SqlDbType.UniqueIdentifier, configurationid));
					SqlDataAdapter adapter = new SqlDataAdapter(cmd);
					DataSet ds = new DataSet();
					adapter.Fill(ds);
					List<Configuration> all = Load(ds.Tables[0]);
					if (all.Count == 0)
						throw new NotFoundException();
					else
						return all[0];
				}
			}
		}
		public void Delete(Guid configurationid)
		{
			using (CustomSqlConnection connection = new CustomSqlConnection())
			{
				using (SqlCommand cmd = new SqlCommand("Configuration_Delete", connection))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new CustomSqlParameter("ConfigurationId", SqlDbType.UniqueIdentifier, configurationid));
					cmd.ExecuteNonQuery();
				}
			}
		}
		private static List<Configuration> Load(DataTable dt)
		{
			List<Configuration> retVal = new List<Configuration>();
			if (dt != null)
			{
				foreach (DataRow dr in dt.Rows)
					retVal.Add(Load(dr));
			}
			return retVal;
		}
		private static Configuration Load(DataRow dr)
		{
			Configuration retVal = new Configuration()
			{
				ConfigurationId = dr["ConfigurationId"].EnsureGuid(),
				Description = dr["Description"].EnsureString(),
				ExternalId = dr["ExternalId"].EnsureInt(),
			};
			return retVal;
		}
	}
}
