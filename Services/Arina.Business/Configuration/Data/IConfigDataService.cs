﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arina.Business.Data
{
    public interface IConfigDataService
    {
        Config Load();
        void Update(Config config);
    }
}
