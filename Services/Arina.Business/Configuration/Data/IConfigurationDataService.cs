using System;
using System.Collections.Generic;

namespace Arina.Business.Data
{
	public interface IConfigurationDataService
	{
		void Save(Configuration configuration);
		List<Configuration> Find(string criteria);
		Configuration Load(Guid configurationid);
        Configuration Load(ConfigurationEnum externalId);
        void Delete(Guid configurationid);
	}
}
