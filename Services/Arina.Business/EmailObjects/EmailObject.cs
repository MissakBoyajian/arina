﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arina.Business
{
   public class EmailObject<T>
    {
        public T Object { get; set; }
        public List<Member> ContactPersons { get; set; }

    }
}
