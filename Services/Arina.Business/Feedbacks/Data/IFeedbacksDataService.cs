using System;
using System.Collections.Generic;

namespace Arina.Business.Data
{
    public interface IFeedbacksDataService
    {
        void Save(string Feedback);
        List<Feedbacks> Get();
    }
}
