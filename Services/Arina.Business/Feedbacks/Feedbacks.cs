using System;
using System.Collections.Generic;

namespace Arina.Business
{

    public class Feedbacks
    {
        public Feedbacks()
        {
            
        }

        public Guid MemberId { get; set; }

        public string Feedback { get; set; }

    }
}
