using System;
using System.Collections.Generic;
using Arina.Business.Data;

namespace Arina.Business
{
    public static class FeedbacksManager
    {
        private static IFeedbacksDataService data = DataManager.GetDataImplementation<IFeedbacksDataService>();


        public static void Save(string Feedback)
        {
            data.Save(Feedback);
        }

        public static List<Feedbacks> Get()
        {
            return data.Get();
        }

    }
}
