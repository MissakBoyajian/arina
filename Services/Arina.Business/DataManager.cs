﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Arina.Business
{
    internal static class DataManager
    {
        public static T GetDataImplementation<T>()
        {
            Type retType = typeof(T);
            Assembly assembly = Assembly.Load("Arina.Data.Sql, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null");
            foreach (Type type in assembly.GetTypes())
            {
                bool assignable = retType.IsAssignableFrom(type);
                if (assignable)
                    return (T)Activator.CreateInstance(type);
            }

            throw new Exception("No Implementation found");
        }
    }
}
