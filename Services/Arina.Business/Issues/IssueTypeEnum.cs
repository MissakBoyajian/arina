﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arina.Business
{
    public enum IssueTypeEnum
    {
        General,
        Order,
        Delivery,
        Credit,
        PaymentOrAccount,
        Quote,
        Other,
    }
}
