using System;
using System.Collections.Generic;
using Arina.Business.Data;

namespace Arina.Business
{
    public static class IssueManager
    {
        private static IIssueDataService data = DataManager.GetDataImplementation<IIssueDataService>();
        public static void Save(this Issue issue)
        {
            data.Save(issue);
        }
        public static List<Issue> Find(string criteria)
        {
            return data.Find(criteria);
        }
     
        public static PaginatedList<Issue> FindMyOpenIssues(
            bool IsMine,
            int PageNumber,
            string criteria,
            IssuesPriorityEnum? priority,
            Guid? CustomerId,
            int? Status,
            int? AssignStatus,
            string DealerSiteId,
            DateTime? CreatedFrom,
            DateTime? CreatedTo,
            DateTime? DueFrom,
            int Sort,
            bool Order,
            string UploadedBy,
            string UploadedTo,
            string IssueType,
            string IssuePriority,
            string SalesRep)
        {
            return data.FindMyOpenIssues(
                IsMine: IsMine,
                    PageNumber: PageNumber,
                    CreatedFrom: CreatedFrom,
                    CreatedTo: CreatedTo,
                    criteria: criteria,
                    priority: priority,
                    CustomerId: CustomerId,
                    Status: Status,
                    AssignStatus: AssignStatus,
                    DealerSiteId: DealerSiteId,
                    DueFrom: DueFrom,
                    Sort: Sort,
                    Order: Order,
                    UploadedBy: UploadedBy,
                    UploadedTo: UploadedTo,
                    IssueType: IssueType,
                    IssuePriority: IssuePriority,
                    SalesRep: SalesRep);
        }
        public static PaginatedList<Issue> FindMyOnHoldIssues(
            bool IsMine, 
            int PageNumber, 
            string criteria, 
            IssuesPriorityEnum? priority, 
            Guid? CustomerId, 
            string DealerSiteId,
            DateTime? CreatedFrom,
            DateTime? CreatedTo,
            DateTime? DueFrom,
            int Sort,
            bool Order,
            string UploadedBy,
            string UploadedTo,
            string IssueType,
            string IssuePriority,
            string SalesRep)
        {
            return data.FindMyOnHoldIssues(
                IsMine: IsMine,
                    PageNumber: PageNumber,
                    CreatedFrom: CreatedFrom,
                    CreatedTo: CreatedTo,
                    criteria: criteria,
                    priority: priority,
                    CustomerId: CustomerId,
                    DealerSiteId: DealerSiteId,
                    DueFrom: DueFrom,
                    Sort: Sort,
                    Order: Order,
                    UploadedBy: UploadedBy,
                    UploadedTo: UploadedTo,
                    IssueType: IssueType,
                    IssuePriority: IssuePriority,
                    SalesRep: SalesRep);
        }
        public static PaginatedList<Issue> FindMySolvedIssues(
            bool IsMine, 
            int PageNumber,  
            string criteria, 
            IssuesPriorityEnum? priority, 
            Guid? CustomerId, 
            string DealerSiteId,
            DateTime? CreatedFrom,
            DateTime? CreatedTo,
            DateTime? DueFrom,
            int Sort,
            bool Order,
            string UploadedBy,
            string UploadedTo,
            string IssueType,
            string IssuePriority,
            string SalesRep)
        {
            return data.FindMySolvedIssues(
                IsMine: IsMine,
                    PageNumber: PageNumber,
                    CreatedFrom: CreatedFrom,
                    CreatedTo: CreatedTo,
                    criteria: criteria,
                    priority: priority,
                    CustomerId: CustomerId,
                    DealerSiteId: DealerSiteId,
                    DueFrom: DueFrom,
                    Sort: Sort,
                    Order: Order,
                    UploadedBy: UploadedBy,
                    UploadedTo: UploadedTo,
                    IssueType: IssueType,
                    IssuePriority: IssuePriority,
                    SalesRep: SalesRep
                );
        }

        public static Issue Load(Guid issueid)
        {
            return data.Load(issueid);
        }
        public static int Delete(Guid issueid)
        {
            return data.Delete(issueid);
        }
        public static int PendingIssuesCount()
        {
            return data.PendingIssuesCount();
        }

        public static object FindIssuesByCallId(Guid IssueId)
        {
            return data.FindIssuesByCallId(IssueId);
        }

        public static int IncrementViews(Guid IssueId)
        {
            return data.IncrementViews(IssueId);
        }
        public static List<Issue> FindTodaysIssues()
        {
            return data.FindTodaysIssues();
        }

        public static object OpenIssuesCount()
        {
            return data.OpenIssuesCount();
        }

        public static void Save(List<Issue> issues)
        {
            data.Save(issues);
        }

    }
}
