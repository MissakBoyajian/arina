using System;
using System.Collections.Generic;

namespace Arina.Business.Data
{
	public interface IIssueDataService
	{
		void Save(Issue issue);
        void Save(List<Issue> issues);
		List<Issue> Find(string criteria);
        List<Issue> FindTodaysIssues();
        Issue Load(Guid issueid);
		int Delete(Guid issueid);

        PaginatedList<Issue> FindMyOpenIssues(
            bool IsMine, 
            int PageNumber, 
            string criteria, 
            IssuesPriorityEnum? priority, 
            Guid? CustomerId, 
            int? Status, 
            int? AssignStatus,
            string DealerSiteId,
            DateTime? CreatedFrom,
            DateTime? CreatedTo,
            DateTime? DueFrom,
            int Sort,
            bool Order,
            string UploadedBy,
            string UploadedTo,
            string IssueType,
            string IssuePriority,
            string SalesRep);
        PaginatedList<Issue> FindMyOnHoldIssues(
            bool IsMine, 
            int PageNumber,
            string criteria, 
            IssuesPriorityEnum? priority,
            Guid? CustomerId, 
            string DealerSiteId,
            DateTime? CreatedFrom,
            DateTime? CreatedTo,
            DateTime? DueFrom,
            int Sort,
            bool Order,
            string UploadedBy,
            string UploadedTo,
            string IssueType,
            string IssuePriority,
            string SalesRep);
        PaginatedList<Issue> FindMySolvedIssues(
            bool IsMine, 
            int PageNumber, 
            string criteria, 
            IssuesPriorityEnum? priority, 
            Guid? CustomerId, 
            string DealerSiteId,
            DateTime? CreatedFrom,
            DateTime? CreatedTo,
            DateTime? DueFrom,
            int Sort,
            bool Order,
            string UploadedBy,
            string UploadedTo,
            string IssueType,
            string IssuePriority,
            string SalesRep);
        int PendingIssuesCount();
        List<Issue> FindIssuesByCallId(Guid CallId);

        int IncrementViews(Guid IssueId);
        int OpenIssuesCount();
    }
}
