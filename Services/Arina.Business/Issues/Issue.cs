using System;
using System.Collections.Generic;

namespace Arina.Business
{

    public class Issue
    {
        public Issue()
        {
            
        }
        public Guid IssueId { get; set; }
        public Guid CustomerId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public IssuesStatusEnum Status { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int TotalIssues { get; set; }
        public int Viewed { get; set; }
        public int NumberOfIssues { get; set; }
        public Guid AssignedTo { get; set; }
        public Guid CallId { get; set; }
        public IssuesAssignStatusEnum AssignStatus { get; set; }
        public int IssueNo { get; set; }
        public IssuesPriorityEnum Priority { get; set; }
        public IssueTypeEnum IssueType { get; set; }
        public long TimeStamp { get; set; }

        public string IssuePriority
        {
            get
            {
                return Priority.ToString().SpaceOnCapital();
            }
        }

        public string IssueStatus
        {
            get
            {
                return Status.ToString().SpaceOnCapital();
            }

            private set { }

        }
        public string IssueAssignStatus
        {
            get
            {
                return AssignStatus.ToString().SpaceOnCapital();
            }

            private set { }

        }
        public string IssueTypeString
        {
            get
            {
                return IssueType.ToString();
            }
            private set { }
        }
            
        public Member AssignedToMember { get; set; }
        public Member UpdatedByMember { get; set; }
        public Member CreatedByMember { get; set; }

        public string AssignedToMemberFullName
        {

            get
            {
                if (AssignedToMember != null)
                    return AssignedToMember.FirstName + " " + AssignedToMember.LastName;
                else
                    return "";
            }
        }
        public string CreatedByMemberFullName
        {

            get
            {
                if (CreatedByMember != null)
                    return CreatedByMember.FirstName + " " + CreatedByMember.LastName;
                else
                    return "";
            }
        }

        //for email
        public string ClassName
        {
            get
            {
                return "Issue";
            }
        }
        
        public string CreatedTime
        {
            get
            {

                return CreatedOn.ToShortTimeString();
            }
        }

    }
}
