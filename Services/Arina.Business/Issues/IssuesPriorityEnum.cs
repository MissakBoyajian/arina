﻿

namespace Arina.Business
{
    public enum IssuesPriorityEnum
    {
        none,
        low,
        medium,
        high
    }
}
