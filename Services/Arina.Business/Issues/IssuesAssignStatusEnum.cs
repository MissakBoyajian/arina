﻿
namespace Arina.Business
{
    public enum IssuesAssignStatusEnum
    {
        pending,
        approved,
        rejected
    }
}
