using System;
using System.Collections.Generic;
using Arina.Business.Data;

namespace Arina.Business
{
    public static class RolesApplicationModuleManager
    {
        private static IRolesApplicationModuleDataService data = DataManager.GetDataImplementation<IRolesApplicationModuleDataService>();
        public static void Save(this RolesApplicationModule rolesapplicationmodule)
        {
            data.Save(rolesapplicationmodule);
        }
        public static List<RolesApplicationModule> Find(string criteria)
        {
            return data.Find(criteria);
        }
        public static RolesApplicationModule Load(Guid roleid, Guid applicationmoduleid)
        {
            return data.Load(roleid, applicationmoduleid);
        }
        public static int Delete(Guid roleid, Guid applicationmoduleid)
        {
            return data.Delete(roleid, applicationmoduleid);
        }

        public static List<RolesApplicationModule> LoadByRoleId(Guid roleId)
        {
            return data.LoadByRoleId(roleId);
        }
    }
}
