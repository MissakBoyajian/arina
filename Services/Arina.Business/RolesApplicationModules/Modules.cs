﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arina.Business
{
    public enum Modules
    {
        [ModuleInfo("768AEEB9-C918-41CF-A4A4-01FA72BB60EC")]
        PRODUCTS,

        [ModuleInfo("5406558C-4FD1-4E94-A850-2EC551FC8D55")]
        CALENDAR,

        [ModuleInfo("420D6777-1E4A-452B-A92E-74DEF05CAFCB")]
        CUSTOMERS_DETAILS,

        [ModuleInfo("147392BA-5B53-4E7B-BA41-8AE43489D150")]
        PROMOTIONS,

        [ModuleInfo("A77621EA-868C-489B-AF09-9375FE8BAF89")]
        ISSUES,

        [ModuleInfo("B0C4AD0F-0713-4474-BF00-988E94513B0D")]
        RESOURCES,

        [ModuleInfo("1724EC2B-7CDF-4154-BECF-A531BBF4842C")]
        DASHBOARD_DETAILS,

        [ModuleInfo("6D542D29-5E6B-42E9-B8D4-B54A9CD2F226")]
        CUSTOMERS_LISTING,

        [ModuleInfo("49B1AF53-27D7-4F74-B082-BF79B169F71A")]
        REPORTING,

        [ModuleInfo("38D44C7D-12F3-4FEF-B2D6-DF17AFD0D948")]
        SALES_DATA,

        [ModuleInfo("8AA204AC-4F86-4BC6-A229-E5AA152267C9")]
        ADMINISTRATION,

        [ModuleInfo("1D00E4A8-5914-4AC1-82BF-F35F2D1A3E38")]
        CALLS,

        [ModuleInfo("9123A177-AF9D-40BD-A166-F86C11BB085F")]
        TASKS,

        [ModuleInfo("ACF4661F-7F6C-4F61-B8FF-FE75EE5EB3F4")]
        INCOMING,
    }
}
