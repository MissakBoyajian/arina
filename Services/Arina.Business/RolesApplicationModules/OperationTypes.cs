﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arina.Business
{
    public enum OperationTypes
    {
        Add = 1,
        Edit = 2,
        View = 3,
        Delete = 4
    }
}