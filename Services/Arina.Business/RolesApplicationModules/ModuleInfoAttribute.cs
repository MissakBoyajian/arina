﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arina.Business
{
    public class ModuleInfoAttribute : Attribute
    {
        public Guid ModuleId { get; set; }
        public ModuleInfoAttribute(Guid moduleId)
        {
            this.ModuleId = moduleId;
        }
        public ModuleInfoAttribute(string moduleId) : this(new Guid(moduleId))
        {
        }
    }
}