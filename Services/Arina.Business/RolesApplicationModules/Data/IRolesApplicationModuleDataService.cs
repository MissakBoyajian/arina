using System;
using System.Collections.Generic;

namespace Arina.Business.Data
{
	public interface IRolesApplicationModuleDataService
	{
		void Save(RolesApplicationModule rolesapplicationmodule);
		List<RolesApplicationModule> Find(string criteria);
		RolesApplicationModule Load(Guid roleid,Guid applicationmoduleid);
		int Delete(Guid roleid,Guid applicationmoduleid);
        List<RolesApplicationModule> LoadByRoleId(Guid roleId);
    }
}
