using System;
using System.Collections.Generic;

namespace Arina.Business
{
	public class RolesApplicationModule
	{
		public Guid RoleId  { get; set; }
		public Guid ApplicationModuleId  { get; set; }
		public bool AllowView  { get; set; }
		public bool AllowAdd  { get; set; }
		public bool AllowEdit  { get; set; }
		public bool AllowDelete  { get; set; }
        public Role Role { get; set; }
        public ApplicationModule ApplicationModule { get; set; }
    }
}
