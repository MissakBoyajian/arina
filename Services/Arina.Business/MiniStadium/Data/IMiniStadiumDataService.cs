using System;
using System.Collections.Generic;

namespace Arina.Business.Data
{
	public interface IMiniStadiumDataService
    {
		void Save(MiniStadium stadium);
		List<MiniStadium> Get();
        PaginatedList<MiniStadium> GetFree(int PageNumber, int SortBy, int FloorType, 
            int StadiumType, int DistanceFrom, int DistanceTo, int PriceFrom, 
            int PriceTo, decimal Lat, decimal Lon, DateTimeOffset DateFrom,int NumPlayers);
        List<MiniStadium> GetById(Guid miniStadiumId);
        List<MiniStadium> GetByStadiumId(Guid stadiumId);
        List<MiniStadium> GetByMemberId();
        List<MiniStadium> GetByMemberId(Guid memberId);
        
    }
}
