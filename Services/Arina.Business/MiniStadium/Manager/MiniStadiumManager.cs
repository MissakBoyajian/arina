using System;
using System.Collections.Generic;
using Arina.Business.Data;

namespace Arina.Business
{
    public static class MiniStadiumManager
    {
        private static IMiniStadiumDataService data = DataManager.GetDataImplementation<IMiniStadiumDataService>();


        public static void Save(MiniStadium stadium)
        {
            data.Save(stadium);
        }

        public static List<MiniStadium> Get()
        {
            return data.Get();
        }

        public static PaginatedList<MiniStadium> GetFree(int PageNumber, 
            int SortBy, 
            int FloorType, 
            int StadiumType, 
            int DistanceFrom, 
            int DistanceTo, 
            int PriceFrom, 
            int PriceTo, 
            decimal Lat, 
            decimal Lon, 
            DateTimeOffset DateFrom,
            int NumPlayers)
        {
            return data.GetFree(PageNumber, SortBy, FloorType, StadiumType,DistanceFrom, DistanceTo, PriceFrom, PriceTo, Lat, Lon, DateFrom, NumPlayers);
        }

        public static List<MiniStadium> GetById(Guid miniStadiumId)
        {
            return data.GetById(miniStadiumId);
        }

        public static List<MiniStadium> GetByStadiumId(Guid stadiumId)
        {
            return data.GetByStadiumId(stadiumId);
        }

        public static List<MiniStadium> GetByMemberId()
        {
            return data.GetByMemberId();
        }

        public static List<MiniStadium> GetByMemberId(Guid memberId)
        {
            return data.GetByMemberId(memberId);
        }



    }
}
