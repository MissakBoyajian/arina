using System;
using System.Collections.Generic;

namespace Arina.Business
{

    public class MiniStadium
    {
        public MiniStadium()
        {

        }

        public Guid MiniStadiumId { get; set; }
        public Guid StadiumId { get; set; }
        public string Name { get; set; }
        public Guid MemberId { get; set; }

        public decimal Lat { get; set; }
        public decimal Lon { get; set; }
        public decimal Rating { get; set; }

        public int Price { get; set; }

        public int NumPlayers { get; set; }
        public int NumPlayers1 { get; set; }

        public StadiumType Type { get; set; }
        public StadiumTypeFloor TypeFloor { get; set; }

        public String TypeText
        {
            get
            {
                return this.Type.ToString().SpaceOnCapital();
            }
            private set
            {
            }
        }
        public String TypeFloorText
        {
            get
            {
                return this.TypeFloor.ToString().SpaceOnCapital();
            }
            private set
            {
            }
        }

        public Stadium Stadium { get; set; }


    }

    public class AvailableTimes
    {

        public DateTime DateFrom { get; set; }
        public int HasPromotion { get; set; }
        public int NewPrice { get; set; }

    }
}
