﻿
using Arina.Business.Data;
using System;
using System.Collections.Generic;

namespace Arina.Business
{
    public static class ResourceManager
    {

        private static IResroucesDataService data = DataManager.GetDataImplementation<IResroucesDataService>();
        public static void Save(this Resource resource, Guid? ProductId)
        {
            DeleteResourceFolders(resource.ResourceId);
            data.Save(resource, ProductId);
        }
        public static List<Resource> Find(Guid DealerGroupId)
        {
            return data.Find(DealerGroupId);
        }
        public static List<Resource> Find(string criteria, Guid productId, bool includeEmptyFolders)
        {
            return data.Find(criteria, productId, includeEmptyFolders);
        }
        public static Resource Load(Guid resourceid)
        {
            return data.Load(resourceid);
        }
        public static int Delete(Guid resourceid)
        {
            return data.Delete(resourceid);
        }

        public static List<Resource> LoadByProduct(Guid ProductId)
        {
            List<Resource> ProductResources = data.LoadByProduct(ProductId);

            foreach (var element in ProductResources)
            {
                if (!ProductResources.Exists(x => x.ResourceId == element.ParentResourceId))
                {
                    element.ParentResourceId = Guid.Empty;
                }
            }

            return ProductResources;
        }
        public static int IncrementViews(Guid promotionid)
        {
            return data.IncrementViews(promotionid);
        }

        public static void DeleteResourceFolders(Guid ResourceId)
        {
            data.DeleteResourceFolders(ResourceId);
        }


    }
}
