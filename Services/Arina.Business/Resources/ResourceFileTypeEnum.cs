﻿
using System;
using System.Runtime.Serialization;

namespace Arina.Business
{
    [Serializable]
    [DataContract]
    public enum ResourceFileTypeEnum
    {
        [EnumMember]
        pdf = 0,
        [EnumMember]
        png = 1,
        [EnumMember]
        docx = 2,
        [EnumMember]
        xlsx = 3,
        [EnumMember]
        folder = 4,
        [EnumMember]
        mp4 = 5,
        [EnumMember]
        jpg = 8,
        [EnumMember]
        xls = 9,
        [EnumMember]
        youtube = 10,
        [EnumMember]
        pptx = 11,
        [EnumMember]
        doc = 12,
        [EnumMember]
        ppt = 13,
        [EnumMember]
        vimeo = 14


    }
}
