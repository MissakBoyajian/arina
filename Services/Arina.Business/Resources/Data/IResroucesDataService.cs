﻿
using System;
using System.Collections.Generic;

namespace Arina.Business.Data
{
    public interface IResroucesDataService
    {
        void Save(Resource resource,Guid? ProductId);
        List<Resource> Find(Guid DealerGroupId);
        List<Resource> LoadByProduct(Guid ProductId);
        List<Resource> Find(string criteria, Guid productId, bool includeEmptyFolders);
        Resource Load(Guid ResourceId);
        int Delete(Guid ResourceId);
        int IncrementViews(Guid promotionid);
        int DeleteResourceFolders(Guid ResourceId);

    }
}
