﻿

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Arina.Business
{
    [Serializable]
    [DataContract]
    public class Resource : MediaObject
    {
        [DataMember]
        public Guid ResourceId { get; set; }
        [DataMember]
        public string YoutubeId { get; set; }
        [DataMember]
        public string VimeoId { get; set; }
        [DataMember]
        public Guid ParentResourceId { get; set; }
        [DataMember]
        public Guid DealerSiteId { get; set; }
        [DataMember]
        public int ViewNumber { get; set; }
        [DataMember]
        public int Viewed { get; set; }
        [DataMember]
        public int ShareNumber { get; set; }
        [DataMember]
        public bool AllowSharing { get; set; }

        [DataMember]
        public bool RestrictedView { get; set; }

        [DataMember]
        public bool IsOnCdn { get; set; }
        [DataMember]
        public string CdnUrl { get; set; }
        [DataMember]
        public bool HasThumbnail { get; set; }
        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public int Size { get; set; }

        [DataMember]
        public List<Guid> RelatedFolders { get; set; }

        [DataMember]
        public string FileNameWithExtension { get; set; }
        [DataMember]
        public ResourceFileTypeEnum FileType { get; set; }
        public byte[] FileContent { get; set; }
        [DataMember]
        public DateTime CreatedOn { get; set; }
        [DataMember]
        public Guid CreatedBy { get; set; }

        [DataMember]
        public Member CreatedByMember { get; set; }

        [DataMember]
        public bool IsDirectory { get; set; }
        [DataMember]
        public long TimeStamp { get; set; }

        [DataMember]
        public string MediaUrl
        {
            get
            {
                return IsOnCdn ? CdnUrl : System.Configuration.ConfigurationManager.AppSettings["BaseUrl"] + "/api/ImageStreamer?ResourceId=" + ResourceId.ToString();
            }
            private set { }
        }

        [DataMember]
        public bool HasParent
        {
            get
            {
                return ParentResourceId != Guid.Empty;
            }
            private set { }
        }


        public Member LoggedInMember
        {

            get
            {
                return ContextInfo.Current.LoggedInUser;
            }
        }
        [DataMember]
        public string EmailBody { get; set; }

        public override Guid ReferrerId
        {
            get
            {
                return ResourceId;
            }
        }
    }
}