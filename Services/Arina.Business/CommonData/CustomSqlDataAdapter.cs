﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arina.Business.Data.Sql
{
    public class CustomSqlDataAdapter
    {
        SqlDataAdapter adaptor;
        CustomSqlCommand command;
        public CustomSqlDataAdapter(CustomSqlCommand cmd)
        {
            adaptor = new SqlDataAdapter(cmd);
            command = cmd;
        }

        public int Fill(DataSet dataSet, bool skipTimeLog = false)
        {
            if (skipTimeLog)
                return adaptor.Fill(dataSet);
            else
                using (new SPTimeLogger(command))
                    return adaptor.Fill(dataSet);
        }
        public int Fill(DataTable dt, bool skipTimeLog = false)
        {
            if (skipTimeLog)
                return adaptor.Fill(dt);
            else
                using (new SPTimeLogger(command))
                    return adaptor.Fill(dt);
        }
    }
}
