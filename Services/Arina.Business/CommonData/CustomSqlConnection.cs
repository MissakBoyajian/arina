﻿using System;
using System.Data.SqlClient;

namespace Arina.Business.Data.Sql
{
    public class CustomSqlConnection : IDisposable
    {
        private SqlConnection sqlConn;

        public CustomSqlConnection()
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["main"].ToString();
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new Exception(string.Format("Connection string was not found in config file '{0}'", AppDomain.CurrentDomain.SetupInformation.ConfigurationFile));
            }

            this.sqlConn = new SqlConnection(connectionString);
            this.sqlConn.Open();
        }

        public static implicit operator SqlConnection(CustomSqlConnection connection)
        {
            return connection.sqlConn;
        }

        public void Dispose()
        {
            this.sqlConn.Close();
            this.sqlConn.Dispose();
        }

        public CustomSqlCommand CreateCommand()
        {
            return new CustomSqlCommand(sqlConn.CreateCommand());
        }

        public SqlTransaction BeginTransaction(string transactionName)
        {
            return sqlConn.BeginTransaction(transactionName);
        }
    }
}
