﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arina.Business.Data.Sql
{
    public class SPTimeLogger : IDisposable
    {
        private Stopwatch watch;
        private CustomSqlCommand cmd;
        public SPTimeLogger(CustomSqlCommand command)
        {
            cmd = command;
            watch = new Stopwatch();
            watch.Start();
        }

        public void Dispose()
        {
            watch.Stop();
            if (System.Configuration.ConfigurationSettings.AppSettings["LogSPExecutionTime"].EnsureBool())
                LogTime(Guid.NewGuid(), DateTime.Now, cmd.CommandText, cmd.GetParameterList(), watch.Elapsed.TotalSeconds);
        }

        private static void LogTime(Guid id, DateTime executionDate, string spName, string paramters, double executionTime)
        {
            using (CustomSqlConnection connection = new CustomSqlConnection())
            {
                using (CustomSqlCommand cmd = new CustomSqlCommand("SqlExecutionLog_Save", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new CustomSqlParameter("SqlExecutionLogId", SqlDbType.UniqueIdentifier, id));
                    cmd.Parameters.Add(new CustomSqlParameter("ExecutionDate", SqlDbType.DateTime, executionDate));
                    cmd.Parameters.Add(new CustomSqlParameter("SPName", SqlDbType.NVarChar, spName));
                    cmd.Parameters.Add(new CustomSqlParameter("Parameters", SqlDbType.NVarChar, paramters));
                    cmd.Parameters.Add(new CustomSqlParameter("ExecutionTime", SqlDbType.Float, executionTime));
                    cmd.ExecuteNonQuery(true);
                }
            }
        }
    }
}