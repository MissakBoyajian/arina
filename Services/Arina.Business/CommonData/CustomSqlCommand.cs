﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arina.Business.Data.Sql
{
    public class CustomSqlCommand : IDisposable
    {
        private SqlCommand sqlCommand;

        public string CommandText { get
            {
                return sqlCommand.CommandText;
            }
            set
            {
                sqlCommand.CommandText = value;
            }
        }
        public CommandType CommandType
        {
            get
            {
                return sqlCommand.CommandType;
            }
            set
            {
                sqlCommand.CommandType = value;
            }
        }

        internal string GetParameterList()
        {
            StringBuilder str = new StringBuilder();

            if (sqlCommand != null && sqlCommand.Parameters != null)
            {
                foreach (SqlParameter item in sqlCommand.Parameters)
                {
                    switch (item.DbType)
                    {

                        case DbType.Binary:
                            str.AppendLine(item.ParameterName);
                            break;

                        default:
                            if (item.Value == null)
                                str.AppendLine(item.ParameterName + ": (null)");
                            else
                                str.AppendLine(item.ParameterName + ":" + item.Value.ToString());
                            break;
                    }
                }
            }

            return str.ToString();
        }

        public SqlParameterCollection Parameters
        {
            get
            {
                return sqlCommand.Parameters;
            }
        }

        public SqlConnection Connection
        {
            get
            {
                return sqlCommand.Connection;
            }
            set
            {
                sqlCommand.Connection = value;
            }
        }
        public SqlTransaction Transaction
        {
            get
            {
                return sqlCommand.Transaction;
            }
            set
            {
                sqlCommand.Transaction = value;
            }
        }

        public CustomSqlCommand(SqlCommand cmd)
        {
            sqlCommand = cmd;
        }
        public CustomSqlCommand()
        {
            sqlCommand = new SqlCommand();
        }
        public CustomSqlCommand(string cmdText)
        {
            sqlCommand = new SqlCommand(cmdText);
        }
        public CustomSqlCommand(string cmdText, SqlConnection connection)
        {
            sqlCommand = new SqlCommand(cmdText, connection);
        }
        public CustomSqlCommand(string cmdText, SqlConnection connection, SqlTransaction transaction)
        {
            sqlCommand = new SqlCommand(cmdText, connection, transaction);
        }

        public static implicit operator SqlCommand(CustomSqlCommand connection)
        {
            return connection.sqlCommand;
        }

        public void Dispose()
        {
            this.sqlCommand.Dispose();
        }

        public int ExecuteNonQuery(bool skipTimeLog = false)
        {
            if (skipTimeLog)
                return sqlCommand.ExecuteNonQuery();
            else
            {
                using (new SPTimeLogger(this))
                    return sqlCommand.ExecuteNonQuery();
            }
        }

        public object ExecuteScalar(bool skipTimeLog = false)
        {
            if (skipTimeLog)
                return sqlCommand.ExecuteScalar();
            else
            {
                using (new SPTimeLogger(this))
                    return sqlCommand.ExecuteScalar();
            }
        }
    }
}