using System;
using System.Collections.Generic;

namespace Arina.Business
{

    public class Schedules
    {
        public Schedules()
        {
            
        }

        public Guid ScheduleId { get; set; }
        public Guid MiniStadiumId { get; set; }


        public DateTimeOffset ScheduleDateFrom { get; set; }
        public DateTimeOffset ScheduleDateTo { get; set; }

        public int Price { get; set; }
        public int Duration { get; set; }
        public int Recurring { get; set; }

        public Guid MemberId { get; set; }

        public Member Member { get; set; }
        public Challenge Challenge { get; set; }
        public MiniStadium MiniStadium { get; set; }



    }
}
