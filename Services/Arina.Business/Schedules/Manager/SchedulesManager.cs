using System;
using System.Collections.Generic;
using Arina.Business.Data;

namespace Arina.Business
{
    public static class SchedulesManager
    {
        private static ISchedulesDataService data = DataManager.GetDataImplementation<ISchedulesDataService>();


        public static void Save(Schedules Schedule)
        {
            data.Save(Schedule);
        }

        public static List<Schedules> Get(DateTime Date)
        {
            return data.Get(Date);
        }

        public static void Delete(Guid ScheduleId, Boolean Recurring = false)
        {
            data.Delete(ScheduleId, Recurring);
        }

        public static List<Schedules> GetPreviousBookings(Guid MemberId, int PageNumber)
        {
            return data.GetPreviousBookings(MemberId, PageNumber);
        }

        public static List<Schedules> GetCurrentBookings(Guid MemberId, int PageNumber)
        {
            return data.GetCurrentBookings(MemberId, PageNumber);
        }


    }
}
