using System;
using System.Collections.Generic;

namespace Arina.Business.Data
{
    public interface ISchedulesDataService
    {
        void Save(Schedules Schedule);
        List<Schedules> Get(DateTime Date);

        List<Schedules> GetPreviousBookings(Guid MemberId, int PageNumber);
        List<Schedules> GetCurrentBookings(Guid MemberId, int PageNumber);

        void Delete(Guid ScheduleId, Boolean Recurring);
    }
}
