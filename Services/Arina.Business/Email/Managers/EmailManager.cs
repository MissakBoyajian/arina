﻿
using System;
using System.Collections.Generic;
using System.IO;
using Arina.Business.Data;
using System.Net;

namespace Arina.Business
{
    public static class EmailManager
    {
        private static IEmailDataService data = DataManager.GetDataImplementation<IEmailDataService>();

        public static void SendEmail(EmailType emailType, string to, string cc, string bcc, object relevantClass,byte[] attachment)
        {
            List<string> toS = new List<string>();
            List<string> ccS = new List<string>();
            List<string> bccS = new List<string>();

            if (!string.IsNullOrEmpty(to))
            toS = new List<string>(to.Split(','));

            if (!string.IsNullOrEmpty(cc))
             ccS = new List<string>(cc.Split(','));

            if (!string.IsNullOrEmpty(bcc))
            bccS = new List<string>(bcc.Split(','));



            //if (!string.IsNullOrEmpty(to))
            //    toS.Add(to);

            //if (!string.IsNullOrEmpty(cc))
            //    ccS.Add(cc);

            //if (!string.IsNullOrEmpty(bcc))
            //    bccS.Add(bcc);
            
            SendEmail( emailType, toS, ccS, bccS, relevantClass, attachment);
        }
        public static void SendEmail( EmailType emailType, List<string> to, List<string> cc, List<string> bcc, object relevantClass, byte[] attachment)
        {
            if (to == null)
                throw new Exception("to");

            EmailTemplateAttribute emailInfo = emailType.EmailTemplateInfo();
            if (emailType == EmailType.Promotion || emailType == EmailType.NewsArticle || emailType == EmailType.Resource)
                emailInfo.EmailSubject = ContextInfo.Current.LoggedInUser.FirstName + " " + ContextInfo.Current.LoggedInUser.LastName + " " + emailInfo.EmailSubject;
            string templatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"assets\emailTemplates\" + emailInfo.TemplateName).Replace("\\Services\\", "\\");
            if (File.Exists(templatePath))
            {
                string body = MergeFieldManager.GetString(File.ReadAllText(templatePath), relevantClass);
                string subject = emailInfo.EmailSubject;

                EmailInfo email = new EmailInfo();
                //email.IsDailyDigest = relevantClass is DailyDigest;
                email.ID = Guid.NewGuid();
                email.Subject = subject;
                email.Body = body;
                if (to != null)
                    foreach (string item in to)
                        if (!string.IsNullOrEmpty(item))
                            email.TO.Add(item);

                if (cc != null)
                    foreach (string item in cc)
                        if (!string.IsNullOrEmpty(item))
                            email.CC.Add(item);

                if (bcc != null)
                    foreach (string item in bcc)
                        if (!string.IsNullOrEmpty(item))
                            email.BCC.Add(item);

                if (attachment != null)
                    email.Attatchemnt = attachment;

                data.CreateEmail(email);
            }
            else
                throw new Exception("Email Template not found");

            //#region hack
            //string text;
            //var request = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["BaseUrl"] + "/api/Imagestreamer/Login/Login.aspx");
            //using (var response = request.GetResponse())
            //{
            //    using (var reader = new StreamReader(response.GetResponseStream()))
            //    {
            //        text = reader.ReadToEnd();
            //    }
            //}
            //#endregion
        }

        public static void SendEmail1()
        {
            data.SendEmail1().Wait();
        }

        public static EmailTemplateAttribute EmailTemplateInfo(this EmailType type)
        {
            Type enumType = typeof(EmailType);
            System.Reflection.MemberInfo[] members = enumType.GetMember(type.ToString());
            if (members != null && members.Length > 0)
            {
                EmailTemplateAttribute memberAttribute = members[0].GetCustomAttribute<EmailTemplateAttribute>();
                if (memberAttribute != null)
                {
                    return memberAttribute;
                }
            }

            throw new Exception(string.Format("Enum '{0}' is not decorated with an attribute of type '{1}'.", type.ToString(), typeof(EmailTemplateAttribute).FullName));
        }
    }
}
