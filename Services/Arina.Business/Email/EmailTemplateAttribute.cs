﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arina.Business
{
    [AttributeUsage(AttributeTargets.Field)]
    public class EmailTemplateAttribute : Attribute
    {
        public EmailTemplateAttribute(string templateName, string emailSubject)
        {
            this.TemplateName = templateName;

            this.EmailSubject = emailSubject;
        }

        public string TemplateName { get; private set; }

        public string EmailSubject { get; set; }
    }
}
