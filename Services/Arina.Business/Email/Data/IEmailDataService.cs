﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arina.Business.Data
{
    public interface IEmailDataService
    {
        Task SendEmail1();
        EmailInfo CreateEmail(EmailInfo email);
        List<EmailInfo> GetUnsentEmails();
        void SetEmailToSent(Guid mailID, bool isDelivere, string error);
    }
}
