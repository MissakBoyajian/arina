﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Net.Mail;
using System.IO;

namespace Arina.Business
{
    public class EmailInfo
    {
        public EmailInfo()
        {
            this.TO = new List<string>();
            this.CC = new List<string>();
            this.BCC = new List<string>();
            this.RetryNumber = 0;
            this.IsDelivered = false;
        }

        public Guid ID { get; set; }
        public List<string> TO { get; set; }
        public List<string> CC { get; set; }
        public List<string> BCC { get; set; }
        public string Subject { get; set; }
        public byte[] Attatchemnt { get; set; }
        public string Body { get; set; }
        public int RetryNumber { get; set; }
        public DateTime SentOn { get; set; }
        public string Error { get; set; }
        public bool IsDelivered { get; set; }
        public bool IsDailyDigest { get; set; }
    }
}
