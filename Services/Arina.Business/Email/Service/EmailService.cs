﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using Arina.Business.Data;
using System.IO;

namespace Arina.Business.Service
{
    public class EmailService : BatchJobService
    {
        private static IEmailDataService data = DataManager.GetDataImplementation<IEmailDataService>();

        public override void DoBatchWork()
        {
            List<EmailInfo> all = data.GetUnsentEmails();
            foreach (EmailInfo message in all)
            {
                EmailService.Send(message);
            }
        }

        private static void Send(EmailInfo email)
        {
            try
            {
                SmtpClient smtpServer = new SmtpClient(ConfigManager.Current.SMTPServer, ConfigManager.Current.SMTPPort);
                smtpServer.EnableSsl = ConfigManager.Current.SMTPSSL;
                if (ConfigManager.Current.SMTPRequirePassword)
                {
                    smtpServer.UseDefaultCredentials = false;
                    smtpServer.Credentials = new System.Net.NetworkCredential(ConfigManager.Current.SMTPFrom, ConfigManager.Current.SMTPPassword);
                }

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("triton@myfieldforce.com.au", "FieldForce");
                mail.Subject = email.Subject;
                mail.Body = email.Body;
                mail.IsBodyHtml = true;
                mail.SubjectEncoding = Encoding.UTF8;
                mail.BodyEncoding = Encoding.UTF8;

                if (email.TO != null)
                    foreach (string item in email.TO)
                        if (!string.IsNullOrEmpty(item))
                            mail.To.Add(item);

                if (email.CC != null)
                    foreach (string item in email.CC)
                        if (!string.IsNullOrEmpty(item))
                            mail.CC.Add(item);

                if (email.BCC != null)
                    foreach (string item in email.BCC)
                        if (!string.IsNullOrEmpty(item))
                            mail.Bcc.Add(item);
                if (email.Attatchemnt != null)
                {
                    Attachment att = new Attachment(new MemoryStream(email.Attatchemnt), "Proposal Template");
                    mail.Attachments.Add(att);
                }
                if (!string.IsNullOrEmpty(ConfigManager.Current.SMTPReplyTo))
                    mail.ReplyToList.Add(ConfigManager.Current.SMTPReplyTo);

                smtpServer.Timeout = 600000;
                smtpServer.Send(mail);
                data.SetEmailToSent(email.ID, true, string.Empty);
            }
            catch (Exception ex)
            {
                data.SetEmailToSent(email.ID, false, ex.Message);
            }

        }                
    }
}
