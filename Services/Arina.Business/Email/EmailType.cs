﻿
namespace Arina.Business
{
    public enum EmailType
    {
        [EmailTemplateAttribute("RedemptionApproved.htm", "Redemption Approved")]
        RedemptionApproved = 0,

        [EmailTemplateAttribute("ForgetPassword.htm", "Forget Password")]
        ForgetPassword = 1,

        [EmailTemplateAttribute("Offer.htm", "Your Offer is ready to download")]
        Offer = 2,

        [EmailTemplateAttribute("News.htm", "wants to share an article with you")]
        NewsArticle = 3,

        [EmailTemplateAttribute("NewPassword.htm", "New Password")]
        NewPassword = 4,

        [EmailTemplateAttribute("Promotion.htm", "wants to share an promotion with you")]
        Promotion = 5,

        [EmailTemplateAttribute("Feedback.htm", "Feedback")]
        Feedback = 6,

        [EmailTemplateAttribute("DriveLine.htm", "DriveLine")]
        DriveLine = 7,

        [EmailTemplateAttribute("DieselEngine.htm", "DieselEngine")]
        DieselEngine = 8,

        [EmailTemplateAttribute("Ces.htm", "Ces")]
        Ces = 9,

        [EmailTemplateAttribute("Esoc.htm", "Esoc")]
        Esoc = 10,

        [EmailTemplateAttribute("DailyDigest.htm", "Daily Digest")]
        DailyDigest = 11,

        [EmailTemplateAttribute("Calls.htm", "Calls")]
        Calls = 12,

        [EmailTemplateAttribute("Customer.htm", "Customer")]
        Customer = 13,

        [EmailTemplateAttribute("NewAssignedMail.htm", "New Assigned Task")]
        Task = 14,

        [EmailTemplateAttribute("NewAssignedMail.htm", "New Assigned Call")]
        Call = 15,

        [EmailTemplateAttribute("NewAssignedMail.htm", "New Assigned Issue")]
        Issue = 16,

        [EmailTemplateAttribute("NewMember.htm", "Welcome to FieldForce")]
        NewMember = 17,

        [EmailTemplateAttribute("PasswordChanged.htm", "Your Password Changed")]
        PasswordChanged = 18,

        [EmailTemplateAttribute("Resource.htm", "wants to share a resource with you")]
        Resource = 19,

        [EmailTemplateAttribute("SalesUpload.htm", "Sales File was Uploaded Successfully")]
        SalesUpload = 20,

        [EmailTemplateAttribute("NewContentSubscriber.htm", "New Content Subscriber")]
        ContentSubscriber = 21,

        [EmailTemplateAttribute("NewContentSubscriberUser.htm", "New Content Subscriber User")]
        ContentSubscriberUser = 22,

        [EmailTemplateAttribute("CallPhotos.htm", "Call Photos")]
        CallPhotos = 23,

    }
}