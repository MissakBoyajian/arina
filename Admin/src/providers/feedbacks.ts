import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { Api } from './api';

@Injectable()
export class ProviderFeedbacks {

    constructor(private api: Api) { }

    Save(Feedback: any) {
        return this.api.post('feedbacks', { Feedback: Feedback });
    }
}