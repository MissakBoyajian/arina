import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Settings } from '../app/settings';


/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {

  // url: string = 'http://ec2-54-148-199-97.us-west-2.compute.amazonaws.com:2424/api';
  // tokenurl: string = 'http://ec2-54-148-199-97.us-west-2.compute.amazonaws.com:2424';

  url: string = 'http://localhost:5554/api';
  tokenurl: string = 'http://localhost:5554';

  constructor(public http: HttpClient, public _settings: Settings) {

  }

  get(endpoint: string, params?: any) {
    return this.http.get(this.url + '/' + endpoint, this._settings.reqOpts); 
  }

  post(endpoint: string, body: any) {
    return this.http.post(this.url + '/' + endpoint, body, this._settings.reqOpts);
  }

  PostCredentials(endUrl: string, param: any) {
    return this.http.post(this.tokenurl + '/' + endUrl, param, this._settings.reqOpts);
  }

  put(endpoint: string, body: any) {
    return this.http.put(this.url + '/' + endpoint, body, this._settings.reqOpts);
  }

  delete(endpoint: string) {
    return this.http.delete(this.url + '/' + endpoint, this._settings.reqOpts);
  }

  patch(endpoint: string, body: any) {
    return this.http.patch(this.url + '/' + endpoint, body, this._settings.reqOpts);
  }
}

