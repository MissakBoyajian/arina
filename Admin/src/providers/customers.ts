import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { Api } from './api';

@Injectable()
export class ProviderCustomers {

    constructor(private api: Api) {

    }

    Get(FilterOptions: any = null) {
        return this.api.get('customers' + this.QueryBuilder(FilterOptions));
    }

    GetByPhone(PhoneNumber: any = null) {
        return this.api.get('customers/phone/' + PhoneNumber);
    }

    Save(Customer: any) {
        return this.api.post('customers', Customer);
    }

    Delete(CustomerId: any) {
        return this.api.delete('customers');
    }

    QueryBuilder(FilterOptions: any) {

        return '?pagenumber=' + FilterOptions.PageNumber
            + '&criteria=' + FilterOptions.Criteria
    }
}