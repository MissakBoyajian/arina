import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { Api } from './api';

@Injectable()
export class ProviderSchedules {

    constructor(private api: Api) { }

    GetSchedulesByDate(Date: any) {
        return this.api.get('schedules?Date=' + Date.toISOString());
    }

    Save(Schedule: any) {
        return this.api.post('schedules', Schedule);
    }

    DeleteBookingById(ScheduleId: string, Recurring: boolean = false) {
        return this.api.delete('schedules/' + ScheduleId + this.QueryBuilder(Recurring));
    }


    QueryBuilder(Recurring) {
        return '?Recurring=' + Recurring
    }

}