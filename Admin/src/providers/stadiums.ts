import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { Api } from './api';
import { Settings } from '../app/settings';

@Injectable()
export class ProviderStadiums {

    constructor(private api: Api, private _settings: Settings) {

    }

    Get(FilterOptions: any = null) {
        return this.api.get('members/' + this._settings.CurrentUser.MemberId + '/ministadiums');
    }

    // Save(Promotion: any) {
    //     return this.api.post('promotions', Promotion);
    // }

    BuildQuery(FilterOptions: any) {

    }
}