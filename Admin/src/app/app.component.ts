import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { PageSchedules } from '../pages/schedules/schedules';
import { PageFeedbacks } from '../pages/feedbacks/feedbacks';
import { PageStadiums } from '../pages/stadiums/stadiums';
import { PageCustomers } from '../pages/customers/customers';
import { PagePromotions } from '../pages/promotions/promotions';
import { PageLogin } from '../pages/auth/login/login';
import { OneSignal } from '@ionic-native/onesignal';
import { Settings } from './settings';
import { ThrowStmt } from '@angular/compiler';
import { UserProvider } from '../providers/user';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: Array<{ title: string, component: any, icon: string }>;

  constructor(private oneSignal: OneSignal,
    public _settings: Settings,
    public _user: UserProvider,
    public platform: Platform,
    public statusBar: StatusBar,
    public menuCtrl: MenuController,
    public splashScreen: SplashScreen) {

    this.initializeApp();



  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      if (this.platform.is('cordova')) {

        this.oneSignal.startInit('3c52e01f-0945-4334-aff0-25ff0b5fb7ad', '597359579523');

        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

        this.oneSignal.handleNotificationReceived().subscribe(() => {
          // do something when notification is received
        });

        this.oneSignal.handleNotificationOpened().subscribe(() => {
          // do something when a notification is opened
        });
      }

      if (!this._settings.getValue('token')) {

        this.rootPage = PageLogin;

      } else {

        this._settings.reqOpts.headers['Authorization'] = 'Bearer ' + this._settings.getValue('token');

        this._user.GetLoggedInMember().subscribe(res => {

          if (res != null || res != undefined) {
            this._settings.CurrentUser = res;
            this.rootPage = PageSchedules;
          }

        }, err => {
          this.rootPage = PageLogin;
        })

      }

      // used for an example of ngFor and navigation
      this.pages = [
        { title: 'Schedules', component: PageSchedules, icon: 'calendar' },
        { title: 'Customers', component: PageCustomers, icon: 'people' },
        { title: 'Stadiums', component: PageStadiums, icon: 'home' },
        { title: 'Promotions', component: PagePromotions, icon: 'pricetag' },
        { title: 'Feedback', component: PageFeedbacks, icon: 'call' }
      ];

    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.rootPage = page.component;
  }

  Logout() {

    this._settings.removeValueByKey('token');
    this._settings.removeValueByKey('tokenDate');
    this._settings.removeValueByKey('username');

    this.nav.setRoot(PageLogin);
    this.menuCtrl.close();

  }
}
