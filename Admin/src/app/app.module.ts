import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { PageSchedules } from '../pages/schedules/schedules';
import { PageCustomers } from '../pages/customers/customers';
import { PageBookingAdd } from '../pages/bookingadd/bookingadd';
import { PageStadiums } from '../pages/stadiums/stadiums';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PageFeedbacks } from '../pages/feedbacks/feedbacks';
import { PageSelectCustomer } from '../pages/bookingadd/selectcustomer/selectcustomer';
import { PagePromotions } from '../pages/promotions/promotions';
import { PageAddPromotions } from '../pages/promotions/addpromotions/addpromotions';
import { Api } from '../providers/api';

import { UserProvider } from '../providers/user';
import { LibraryService } from '../helpers/LibraryService';
import { AlertClass } from '../helpers/AlertClass';
import { PageLogin } from '../pages/auth/login/login';
import { HttpClientModule } from '@angular/common/http';
import { Network } from '@ionic-native/network';

import { Settings } from './settings';
import { PageCustomersAdd } from '../pages/customers/customersadd/customersadd';
import { ProviderSchedules } from '../providers/schedules';
import { ProviderCustomers } from '../providers/customers';
import { ProviderFeedbacks } from '../providers/feedbacks';
import { ProviderPromotions } from '../providers/promotions';

//LIBRARIES
import { OneSignal } from '@ionic-native/onesignal';
import { ProviderStadiums } from '../providers/stadiums';
import { PageNewCustomer } from '../pages/bookingadd/newcustomer/newcustomer';

// import {
//   MatButtonModule, MatNativeDateModule,
// } from '@angular/material';

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {
  MatButtonModule, MatCheckboxModule, MatNativeDateModule, DateAdapter,
  MatDateFormats, NativeDateAdapter, MAT_DATE_FORMATS,
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MultiPickerModule } from 'ion-multi-picker';
import { PageEditBooking } from '../pages/schedules/editbooking/editbooking';
import { networkmode } from '../components/networkmode/networkmode';

export const MAT_NATIVE_DATE_FORMATS: MatDateFormats = {
  parse: {
    dateInput: { month: 'long', year: 'numeric' }
  },
  display: {
    // dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
    dateInput: 'input',
    monthYearLabel: { year: 'numeric', month: 'long' },
    dateA11yLabel: { year: 'numeric', month: 'long' },
    monthYearA11yLabel: { year: 'numeric', month: 'long' },
  }
};

export class MyDateAdapter extends NativeDateAdapter {
  constructor() { super('en-US'); }
  format(date: Date, displayFormat: Object): string {

    var monthNames = [
      "Jan", "Feb", "Mar",
      "Apr", "May", "Jun", "Jul",
      "Aug", "Sep", "Oct",
      "Nov", "Dec"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return monthNames[monthIndex] + ' ' + year;

  }
}
@NgModule({
  declarations: [
    MyApp,
    PageSchedules,
    PageCustomers,
    PageStadiums,
    PageBookingAdd,
    PageFeedbacks,
    PageSelectCustomer,
    PagePromotions,
    PageAddPromotions,
    PageLogin,
    PageCustomersAdd,
    PageNewCustomer,
    PageEditBooking,
    networkmode
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    BrowserAnimationsModule,
    MultiPickerModule, //Import MultiPickerModule
    IonicModule.forRoot(MyApp, {
      mode: 'md'
    },
    ),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PageSchedules,
    PageCustomers,
    PageStadiums,
    PageBookingAdd,
    PageFeedbacks,
    PageSelectCustomer,
    PagePromotions,
    PageAddPromotions,
    PageLogin,
    PageCustomersAdd,
    PageNewCustomer,
    PageEditBooking,
    networkmode
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Api,
    ProviderSchedules,
    ProviderCustomers,
    ProviderFeedbacks,
    ProviderPromotions,
    ProviderStadiums,
    UserProvider,
    LibraryService,
    AlertClass,
    Settings,
    OneSignal,
    Network,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: DateAdapter, useClass: MyDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: MAT_NATIVE_DATE_FORMATS }
  ]
})
export class AppModule { }
