import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PageAddPromotions } from './addpromotions/addpromotions';

@Component({
  selector: 'page-promotions',
  templateUrl: 'promotions.html'
})
export class PagePromotions {

  constructor(public navCtrl: NavController) {

  }

  AddPromotion() {
    this.navCtrl.push(PageAddPromotions);
  }

}
