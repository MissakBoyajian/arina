import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { ProviderPromotions } from '../../../providers/promotions';
import { AlertClass } from '../../../helpers/AlertClass';
import { ProviderStadiums } from '../../../providers/stadiums';

@Component({
  selector: 'page-addpromotions',
  templateUrl: 'addpromotions.html'
})
export class PageAddPromotions {

  private Promotion: any = {
    Title: "",
    MiniStadiumId: "",
    OldPrice: "",
    NewPrice: "",
    Discount: "",
    Day: '0',
    StartDate: new Date().toISOString(),
    EndDate: new Date().toISOString(),
    IsRecurring: false
  };

  private Stadiums: any = [];

  constructor(private AlertClass: AlertClass,
    private PromotionService: ProviderPromotions,
    private StadiumService: ProviderStadiums,
    private navCtrl: NavController,
    private viewCtrl: ViewController) {
    this.GetStadiums();
  }

  GetStadiums() {
    this.StadiumService.Get().subscribe(res => {
      this.Stadiums = res;
    })
  }

  SavePromotion() {

    this.PromotionService.Save(this.Promotion).subscribe(res => {
      this.AlertClass.showLongToast("Promotion Saved Successfully");
      this.viewCtrl.dismiss(true);
    }, err => {
      this.AlertClass.showLongToast("Something Went Wrong");
    })

  }

}
