import { Component } from '@angular/core';
import { NavParams, ViewController, AlertController, NavController } from 'ionic-angular';
import { ProviderCustomers } from '../../../providers/customers';
import { AlertClass } from '../../../helpers/AlertClass';
import { ProviderSchedules } from '../../../providers/schedules';
import { PageBookingAdd } from '../../bookingadd/bookingadd';

@Component({
  selector: 'page-editbooking',
  templateUrl: 'editbooking.html'
})
export class PageEditBooking {

  private CurrentBooking: any = null;

  constructor(public navParams: NavParams,
    public viewCtrl: ViewController,
    public _alertClass: AlertClass,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public _customersService: ProviderCustomers,
    public _scheduleService: ProviderSchedules) {

    this.CurrentBooking = this.navParams.get('Booking');
    console.log(this.CurrentBooking);

  }

  CloseScreen() {
    this.viewCtrl.dismiss();
  }

  EditBooking(Booking: any = null, FromPage: any = null) {
    this.viewCtrl.dismiss('edit');
  }

  CancelBooking(Booking: any = null, Recurring: boolean = false) {

    let alert = this.alertCtrl.create({
      //title: 'Confirm',
      title: 'Are you sure you want to delete this booking?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Yes',
          handler: (res) => {
            this._scheduleService.DeleteBookingById(Booking.ScheduleId, Recurring).subscribe(res => {

              this._alertClass.showLongToast("Booking Deleted Successfully");
              this.viewCtrl.dismiss('edit');

            }, err => {
              this._alertClass.showLongToast("Something Went Wrong!");
            });
          }
        }
      ]
    });
    alert.present();
  }

}
