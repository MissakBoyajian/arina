import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { PageBookingAdd } from '../bookingadd/bookingadd';
import { ProviderSchedules } from '../../providers/schedules';
import { PageEditBooking } from './editbooking/editbooking';

import * as _ from 'lodash';
@Component({
  selector: 'page-schedules',
  templateUrl: 'schedules.html'
})
export class PageSchedules {

  private Schedule: any = {
    MiniStadiumId: "",
    Price: "",
    Recurring: "0",
    Duration: "3",
    CustomerId: ""
  }

  public ListOfSchedules: any = [];

  public LoadingPage: any = false;

  public CurrentDate: any = new Date();

  public CurrentDate0: any = null;
  public CurrentDate1: any = null;
  public CurrentDate2: any = null;
  public CurrentDate4: any = null;
  public CurrentDate5: any = null;
  public CurrentDate6: any = null;

  public Dates: any = [
    {
      Date: null,
      Selected: false
    },
    {
      Date: null,
      Selected: false
    },
    {
      Date: null,
      Selected: false
    },
    {
      Date: null,
      Selected: true
    },
    {
      Date: null,
      Selected: false
    },
    {
      Date: null,
      Selected: false
    },
    {
      Date: null,
      Selected: false
    },
  ]

  constructor(public navCtrl: NavController, public _scheduleProvider: ProviderSchedules, public modalCtrl: ModalController) {
    this.GetMyBookings();
    this.RefreshDates();
  }

  GetMyBookings() {

    this.LoadingPage = true;
    this.ListOfSchedules = [];

    this._scheduleProvider.GetSchedulesByDate(new Date(this.CurrentDate)).subscribe((res: any) => {

      if (res != null && res.length != 0) {
        res.forEach(element => {
          element.MiniStadiumName = element.MiniStadium.Name;
          element.ScheduleDateFrom = new Date(element.ScheduleDateFrom);
        });
        this.ListOfSchedules = this.transformArray(res, 'MiniStadiumName');
        this.ListOfSchedules.forEach(element => {
          element.value = _.orderBy(element.value, ['ScheduleDateFrom'], ['asc']);
        });
        console.log(this.ListOfSchedules);
      }
      this.LoadingPage = false;

    }, err => {
      this.LoadingPage = false;
    })
  }

  EditBooking(Booking: any = null) {
    let modal = this.modalCtrl.create(PageEditBooking, { Booking: Booking });
    modal.present();
    modal.onDidDismiss(res => {
      if (res) {
        switch (res) {
          case 'edit':
            this.navCtrl.push(PageBookingAdd, { FromPage: this, Booking: Booking });
            break;
          case 'delete':
            this.GetMyBookings();
            break;
          default:
            break;
        }

      }
    })
  }

  AddBooking() {
    this.navCtrl.push(PageBookingAdd, { FromPage: this });
  }

  RefreshDates() {

    this.Dates[0].Date = new Date(this.CurrentDate).setDate(this.CurrentDate.getDate() - 3)
    this.Dates[1].Date = new Date(this.CurrentDate).setDate(this.CurrentDate.getDate() - 2)
    this.Dates[2].Date = new Date(this.CurrentDate).setDate(this.CurrentDate.getDate() - 1)
    this.Dates[3].Date = new Date(this.CurrentDate);
    this.Dates[4].Date = new Date(this.CurrentDate).setDate(this.CurrentDate.getDate() + 1)
    this.Dates[5].Date = new Date(this.CurrentDate).setDate(this.CurrentDate.getDate() + 2)
    this.Dates[6].Date = new Date(this.CurrentDate).setDate(this.CurrentDate.getDate() + 3)

  }

  transformArray(array: Array<any>, field) {
    if (array) {
      const groupedObj = array.reduce((prev, cur) => {
        if (!prev[cur[field]]) {
          prev[cur[field]] = [cur];
        } else {
          prev[cur[field]].push(cur);
        }
        return prev;
      }, {});
      return Object.keys(groupedObj).map(key => ({ key, value: groupedObj[key] }));
    }
    return [];
  }

  SelectDate(CDate: any) {

    this.ClearDates();
    CDate.Selected = true;

    this.CurrentDate = new Date(CDate.Date);
    this.GetMyBookings();

  }

  GoToday() {

    this.CurrentDate = new Date();
    this.GoToDate()
  }

  GoToDate() {
    this.ClearDates();
    this.Dates[3].Selected = true;
    this.RefreshDates();
    this.GetMyBookings();
  }

  ClearDates() {
    this.Dates.forEach(element => {
      element.Selected = false;
    });

  }

  swipeEvent(event: any) {


  }

}
