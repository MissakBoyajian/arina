import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


import { AlertClass } from '../../../helpers/AlertClass';
import { Settings } from '../../../app/settings';
import { PageSchedules } from '../../schedules/schedules';
import { UserProvider } from '../../../providers/user';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class PageLogin {

  // The account fields for the login form.
  // If you're using the username field with or without username, make
  // sure to add it to the type
  public account: { username: string, password: string } = {
    username: 'vatche@arina.com',
    password: 'arina@2018'
  };

  public isLoading: boolean = false;

  constructor(public navCtrl: NavController,
    public user: UserProvider,
    public customAlert: AlertClass,
    public _settings: Settings
  ) {
    //this.account.username = this._settings.getValue('username');
  }

  // Attempt to login in through our User service
  doLogin() {
    if (!this.isLoading) {

      this.isLoading = true;
      if (this.account.username.trim() != '' && this.account.password.trim() != '') {

        this.user.login(this.account).subscribe((resp: any) => {

          // IF LOGGED IN SUCCESSFULLY
          let token: any = resp.access_token;
          var loggedInMember = JSON.parse(resp.LoggedInMember);
          this._settings.CurrentUser = loggedInMember;

          this._settings.setValue('loggedInMemberId', loggedInMember.MemberId);
          this._settings.setValue('token', token);
          this._settings.setValue('tokenDate', new Date().toString());
          this._settings.setValue('username', this.account.username);
          this._settings.CurrentUser = loggedInMember;

          this._settings.reqOpts.headers['Authorization'] = 'Bearer ' + this._settings.getValue('token');

          //GO HOME PAGE
          this.navCtrl.setRoot(PageSchedules);
          this.isLoading = false;

        }, (err) => {
          this.isLoading = false;
          this.customAlert.showLongToast('Wrong username or password');
        });
      } else {
        this.isLoading = false;
        this.customAlert.showLongToast('Please fill all missing fields');
      }

      //this.navCtrl.setRoot(PageSchedules);
    }

  }

  OpenRegister() {

  }

  forgotPassword() {
    this.customAlert.showLongToast('Forget password clicked');
  }
}
