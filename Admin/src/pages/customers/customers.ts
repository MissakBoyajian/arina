import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { PageCustomersAdd } from './customersadd/customersadd';
import { ProviderCustomers } from '../../providers/customers';
import { AlertClass } from '../../helpers/AlertClass';

@Component({
  selector: 'page-customers',
  templateUrl: 'customers.html'
})
export class PageCustomers {

  private Customers: any = [];
  private PageLoaded: any = false;

  private FilterOptions: any = {
    PageNumber: 1,
    Criteria: ''
  }

  constructor(public alertCtrl: AlertController, public navCtrl: NavController, public _customerService: ProviderCustomers, public _alertClass: AlertClass) {

    this.GetMyCustomers();

  }



  GetMyCustomers() {

    this._customerService.Get(this.FilterOptions).subscribe((res: any) => {

      this.Customers = this.Customers.concat(res.Customers);

      if (res.CurrentPage == res.TotalPages) {
        this.PageLoaded = true;
      }

    }, err => {

    })
  }


  AddCustomer(item: any = null) {

    this.navCtrl.push(PageCustomersAdd, {
      Customer: item,
      PageFrom: this
    });

  }

  InitializePage() {
    this.Customers = [];
    this.FilterOptions.PageNumber = 1;
    this.GetMyCustomers();
  }

  DeleteCustomer(item: any) {

    let alert = this.alertCtrl.create({
      title: 'Are you sure you want to delete this customer?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {

            this._customerService.Delete(item.CustomerId).subscribe(res => {
              this._alertClass.showLongToast("Customer Deleted Successfully");
            }, err => {
              this._alertClass.showLongToast("Something Went Wrong");
            })
          }

        }
      ]
    });
    alert.present();
  }

  LoadMore() {
    this.FilterOptions.PageNumber++;
    this.GetMyCustomers();
  }

}
