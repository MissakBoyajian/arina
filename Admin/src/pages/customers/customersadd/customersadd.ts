import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { ProviderCustomers } from '../../../providers/customers';
import { AlertClass } from '../../../helpers/AlertClass';
import { PageCustomers } from '../customers';

@Component({
  selector: 'page-customersadd',
  templateUrl: 'customersadd.html'
})
export class PageCustomersAdd {

  public IsEditMode: boolean = false;
  public PageFrom: PageCustomers;

  public Customer: any = {
    MemberId: "",
    FirstName: "",
    Phone: ""
  };

  constructor(public navParams: NavParams,
    public navCtrl: NavController,
    public _alertController: AlertClass,
    public customersService: ProviderCustomers) {

    this.PageFrom = this.navParams.get('PageFrom');

    if (this.navParams.get('Customer') != null && this.navParams.get('Customer') != undefined) {
      this.IsEditMode = true;
      this.Customer.MemberId = this.navParams.get('Customer').MemberId;
      this.Customer.FirstName = this.navParams.get('Customer').FirstName;
      this.Customer.Phone = this.navParams.get('Customer').Phone;
    }
  }

  SaveCustomer() {

    if (this.Customer.Phone.length == 8) {
      this.customersService.Save(this.Customer).subscribe(res => {

        this._alertController.showVeryLongToast("Customer added successfully");
        this.navCtrl.pop();
        this.PageFrom.InitializePage();

      }, err => {

      })
    }
    else {
      this._alertController.showLongToast("Please Enter a Valid Phone Number")
    }



  }

}
