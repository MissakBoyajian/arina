import { Component } from '@angular/core';
import { NavController, ModalController, NavParams } from 'ionic-angular';
import { PageSelectCustomer } from './selectcustomer/selectcustomer';
import { ProviderStadiums } from '../../providers/stadiums';
import { AlertClass } from '../../helpers/AlertClass';
import { ProviderSchedules } from '../../providers/schedules';
import { PageNewCustomer } from './newcustomer/newcustomer';
import { ProviderCustomers } from '../../providers/customers';
import { LibraryService } from '../../helpers/LibraryService';

@Component({
  selector: 'page-bookingadd',
  templateUrl: 'bookingadd.html'
})
export class PageBookingAdd {

  private CurrentCustomer: any = {
    MemberId: null,
    FirstName: '',
    Phone: "",
    Cancelled: 0,
    DidNotShowUp: 0
  };

  private Schedule: any = {
    ScheduleDateFrom: new Date(),
    MemberId: "",
    MiniStadiumId: "",
    Recurring: 0,
    Price: 0,
    Duration: 90
  };

  private MyStadiums: any = [];
  private LoadingPage: any = false;

  private PageFrom: any = null;

  private simpleColumns: any;
  private DatesList = [];
  private DatePicker: any = "";

  private IsEditMode: Boolean = false;
  private LoadingCustomer: boolean = false;

  constructor(private stadiumService: ProviderStadiums,
    private schedulesService: ProviderSchedules,
    private navCtrl: NavController,
    private alertClass: AlertClass,
    private _libraryService: LibraryService,
    private navParams: NavParams,
    private customerService: ProviderCustomers,
    private modalCtrl: ModalController) {

    this.PageFrom = this.navParams.get('FromPage');


    this.GetMyStadiums();

    this.DatesList = this._libraryService.ConstructPicker();

    this.simpleColumns = [
      {
        name: 'col1',
        options: this.DatesList
      }, {
        name: 'col2',
        options: this._libraryService.PickerTimes
      }
    ];

    this.DatePicker = this.DatesList[1].value + ' at ' + this._libraryService.PickerTimes[0].value;

    if (this.navParams.get('Booking') != undefined || this.navParams.get('Booking') != null) {
      this.IsEditMode = true;
      this.FillForm(this.navParams.get('Booking'));
    };

  }

  FillForm(CurrentBooking: any = null) {

    this.DatePicker = this._libraryService.formatDate(CurrentBooking.ScheduleDateFrom) + ' at ' + CurrentBooking.ScheduleDateFrom.getHours().toString() + ':' + CurrentBooking.ScheduleDateFrom.getMinutes().toString()

    this.CurrentCustomer.MemberId = CurrentBooking.Member.MemberId;
    this.CurrentCustomer.Phone = CurrentBooking.Member.Phone;
    this.CurrentCustomer.FirstName = CurrentBooking.Member.FirstName;
    // this.CurrentCustomer.Cancelled = CurrentBooking.Member.MemberId;
    // this.CurrentCustomer.DidNotShowUp = CurrentBooking.Member.MemberId;
    this.Schedule.ScheduleDateFrom = new Date(CurrentBooking.ScheduleDateFrom);
    this.Schedule.MemberId = CurrentBooking.Member.MemberId;
    this.Schedule.MiniStadiumId = CurrentBooking.MiniStadiumId;
    this.Schedule.Recurring = CurrentBooking.Recurring;
    this.Schedule.Price = CurrentBooking.Price;
    this.Schedule.ScheduleId = CurrentBooking.ScheduleId;
    // this.Schedule.Duration =

  }

  GetMyStadiums() {

    this.LoadingPage = true;

    this.stadiumService.Get().subscribe(res => {
      if (res != null && res != undefined) {
        this.MyStadiums = res;

        if (!this.IsEditMode) {
          this.Schedule.MiniStadiumId = res[0].MiniStadiumId;
          this.Schedule.Price = res[0].Price;
        }
        this.LoadingPage = false;

      }
    }, err => {
      this.LoadingPage = false;
      console.log(err);
    })
  }

  SelectCustomer() {

    let profileModal = this.modalCtrl.create(PageSelectCustomer, { userId: 8675309 });
    profileModal.present();

    profileModal.onDidDismiss(customer => {

      if (customer) {
        this.CurrentCustomer.MemberId = customer.MemberId;
        this.CurrentCustomer.Phone = customer.Phone;
        this.CurrentCustomer.FirstName = customer.FirstName;

        this.Schedule.MemberId = customer.MemberId;

        //this.CurrentCustomer.Cancelled = customer.MemberId;
        //this.CurrentCustomer.DidNotShowUp = customer.MemberId;
      }

    })

  }

  FetchCustomer() {

    this.CurrentCustomer.MemberId = null;
    this.CurrentCustomer.Cancelled = 0;
    this.CurrentCustomer.DidNotShowUp = 0;
    this.Schedule.MemberId = null;

    if (this.CurrentCustomer.Phone.length == 8) {

      this.LoadingCustomer = true;

      this.customerService.GetByPhone(this.CurrentCustomer.Phone).subscribe((res: any) => {

        if (res != null && res != undefined) {
          this.CurrentCustomer.MemberId = res.MemberId;
          this.CurrentCustomer.Cancelled = res.Cancelled;
          this.CurrentCustomer.DidNotShowUp = res.DidNotShowUp;
          this.Schedule.MemberId = res.MemberId;
        }

        this.LoadingCustomer = false;

      }, err => {
        this.LoadingCustomer = false;
      })
    }
  }


  SaveBooking() {

    this.Schedule.ScheduleDateFrom = new Date(this.DatePicker.split('at')[0])
    this.Schedule.ScheduleDateFrom.setHours(Number(this.DatePicker.split('at')[1].split(':')[0].trim()))
    this.Schedule.ScheduleDateFrom.setMinutes(Number(this.DatePicker.split('at')[1].split(':')[1].trim()))

    if (this.Schedule.MemberId == "" || this.Schedule.MemberId == null) {
      this.alertClass.showAlert("Please Select a Customer");
    }
    else if (this.Schedule.MiniStadiumId == "" || this.Schedule.Price == "") {
      this.alertClass.showAlert("Please Fill All The Fields");
    }
    else {

      this.schedulesService.Save(this.Schedule).subscribe(res => {

        this.alertClass.showLongToast("Scheduled Saved Successfully");
        this.navCtrl.pop();
        this.PageFrom.GetMyBookings();

      }, err => {

        this.alertClass.showLongToast("Schedule Conflict. Please Select Another Time.");

      })
    }

  }


  AddNewCustomer() {

    let profileModal = this.modalCtrl.create(PageNewCustomer, {});
    profileModal.present();

    profileModal.onDidDismiss(customer => {

      if (customer) {
        this.Schedule.MemberId = customer.MemberId;
        this.CurrentCustomer.MemberId = customer.MemberId;
        this.CurrentCustomer.Phone = customer.Phone;
        this.CurrentCustomer.FirstName = customer.FirstName;
      }

    })
  }

}
