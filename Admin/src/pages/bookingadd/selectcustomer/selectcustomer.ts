import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { ProviderCustomers } from '../../../providers/customers';
import { FormControl } from '@angular/forms';

import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";

@Component({
  selector: 'page-selectcustomer',
  templateUrl: 'selectcustomer.html'
})
export class PageSelectCustomer {

  public Customers: any = [];

  public CustomersLoaded: boolean; // Pagination all loaded or yet
  public LoadingCustomers: boolean = false;

  public searchControl: FormControl;
  public FilterOptions: any = {
    PageNumber: 1,
    Criteria: "",
    MemberId: ""
  }

  public CustomerSubscriber: any;

  constructor(public _customerService: ProviderCustomers, public viewCtrl: ViewController) {

    this.GetMyCustomers();
    this.searchControl = new FormControl();
  }

  ngAfterViewInit() {



    this.searchControl.valueChanges.debounceTime(600).subscribe(search => {
      this.FilterOptions.PageNumber = 1;
      this.FilterOptions.Criteria = search;
      this.Customers = [];
      this.GetMyCustomers();
    })

  }

  GetMyCustomers() {

    this.CustomersLoaded = false;
    this.LoadingCustomers = true;

    if (this.CustomerSubscriber != undefined)
      this.CustomerSubscriber.unsubscribe();

    this.CustomerSubscriber = this._customerService.Get(this.FilterOptions).subscribe((res: any) => {

      if (res != null && res != [] && res != null && res != undefined) {

        if (res.length == 0 || res.length < 50) {
          this.CustomersLoaded = true;
        }

        if (this.Customers.length != 0) {
          res.forEach(element => {
            this.Customers.concat(res)
          });
        }
        else
          this.Customers = res;
      }
      else {
        this.CustomersLoaded = true;
      }

      this.LoadingCustomers = false;
    },
      err => { });

  }

  CloseScreen() {
    this.viewCtrl.dismiss();
  }

  SelectCustomer(item: any) {
    this.viewCtrl.dismiss(item);
  }

}
