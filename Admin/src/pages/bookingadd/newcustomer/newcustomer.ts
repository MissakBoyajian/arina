import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { ProviderCustomers } from '../../../providers/customers';
import { AlertClass } from '../../../helpers/AlertClass';

@Component({
  selector: 'page-newcustomer',
  templateUrl: 'newcustomer.html'
})
export class PageNewCustomer {

  public Customer: any = {
    FirstName: "",
    Phone: ""
  };

  public IsSaving: any = false;

  constructor(public _customerService: ProviderCustomers, public viewCtrl: ViewController, public _alertController: AlertClass) {


  }

  SaveCustomer() {

    this.IsSaving = true;

    if (this.Customer.Phone.length == 8) {

      this._customerService.Save(this.Customer).subscribe(res => {

        this._alertController.showShortToast("Customer added successfully");

        this.viewCtrl.dismiss(res);

      }, err => {
        this.IsSaving = false;
      })
    }
    else {
      this._alertController.showShortToast("Please Enter a Valid Phone Number");
      this.IsSaving = false;
    }

  }

  CloseScreen() {
    this.viewCtrl.dismiss(false);
  }

}
