import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProviderStadiums } from '../../providers/stadiums';

@Component({
  selector: 'page-stadiums',
  templateUrl: 'stadiums.html'
})
export class PageStadiums {

  public MyStadiums: any = [];

  public LoadingPage: any = false;

  constructor(public stadiumService: ProviderStadiums, public navCtrl: NavController) {

    this.GetMyStadiums();

  }

  GetMyStadiums() {

    this.LoadingPage = true;
    this.stadiumService.Get().subscribe(res => {
      if (res != null && res != undefined) {
        this.MyStadiums = res;
        this.LoadingPage = false;

      }
    }, err => {
      this.LoadingPage = false;
      console.log(err);
    })

  }

}
