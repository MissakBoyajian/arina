﻿import { AlertController, ToastController } from 'ionic-angular';
import { Injectable } from '@angular/core';

@Injectable()
export class AlertClass {
    constructor(public alertCtrl: AlertController, public toastCtrl: ToastController, ) { }

    showAlert(message, title = "Alert!") {
        let alert = this.alertCtrl.create({
            title: message,
            subTitle: '<br>' + message,
            buttons: ['OK']
        });
        alert.present();
    }

    showInfo(message, title = "Information") {
        let alert = this.alertCtrl.create({
            title: message,
            subTitle: '<br>' + message,
            buttons: ['OK']
        });
        alert.present();
    }

    showPrompt(message, title, yesMessage, cancelMessage, cssclass) {
        return new Promise((resolve, reject) => {
            let prompt = this.alertCtrl.create({
                title: title,
                message: message,
                buttons: [
                    {
                        text: cancelMessage,
                        cssClass: cssclass,
                        handler: data => {
                            reject();
                        }
                    },
                    {
                        text: yesMessage,
                        handler: data => {
                            resolve();
                        }
                    }
                ],
                cssClass: cssclass
            });
            prompt.present();
        });
    }

    showToast(Duration: number, Message: string) {
        let toast = this.toastCtrl.create({
            message: Message,
            duration: Duration
        });
        toast.present();
    }

    showShortToast(Message: any) {
        this.showToast(1000, Message);
    }

    ShowMediumToast(Message: any) {
        this.showToast(1500, Message);
    }

    showLongToast(Message: any) {
        this.showToast(2000, Message);
    }

    showVeryLongToast(Message: any) {
        this.showToast(4000, Message);
    }

}