import { Component, ViewChild } from '@angular/core';
import { ModalController, Events, MenuController, Nav, Platform } from 'ionic-angular';

import { Network } from '@ionic-native/network';


@Component({
    selector: 'network-mode',
    templateUrl: 'networkmode.html'
})
export class networkmode {

    @ViewChild(Nav) nav: Nav;

    Visibility: boolean;

    myStyle: any;

    ColorToolbar: String;
    Status: String;

    constructor(public modalCtrl: ModalController
        , public events: Events
        , public menuCtrl: MenuController
        //, public _AppConfig: AppConfig
        , public network: Network


    ) {

        this.Visibility = false;

        this.ColorToolbar = "randemred";
        this.Status = "";

        // watch network for a disconnect
        let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
            console.log('network was disconnected :-(');
            //this._AppConfig.IsOnline = false;
            this.Visibility = true;
        });

        // stop disconnect watch
        //disconnectSubscription.unsubscribe();


        // watch network for a connection
        let connectSubscription = this.network.onConnect().subscribe(() => {

            //this._AppConfig.IsOnline = true;
            console.log('network connected!');
            setTimeout(() => {

                this.Visibility = false;

            }, 3000);


        });


    }


}
