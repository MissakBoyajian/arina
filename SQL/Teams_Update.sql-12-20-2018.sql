USE [Arina]
GO
/****** Object:  StoredProcedure [dbo].[Teams_Update]    Script Date: 12/20/2018 12:11:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[Teams_Update]

  @TeamId UNIQUEIDENTIFIER,
  @Name nvarchar(50),
  @Available BIT,
  @Badge INT,
  @Comments NVARCHAR(50),
  @CreatedOn DATETIMEOFFSET(7),
  @FavStadium UNIQUEIDENTIFIER,
  @HomeJersey INT,
  @AwayJersey INT,
  @NumberOfGames INT,
  @Rank DECIMAL(18,0),
  @Rating DECIMAL(18,0),
  @ReviewRating DECIMAL(18,0),
  @Status INT,
  @Wins INT,
  @WinStreak INT,
  @YellowCard INT,
  @TeamOfSize INT,
  @TeamOfSize1 INT,
  @TeamOfSize2 INT,
  @AvailableMFrom INT,
  @AvailableMTo INT,
  @AvailableTFrom INT,
  @AvailableTTo INT,
  @AvailableWFrom INT,
  @AvailableWTo INT,
  @AvailableThFrom INT,
  @AvailableThTo INT,
  @AvailableFFrom INT,
  @AvailableFTo INT,
  @AvailableSaFrom INT,
  @AvailableSaTo INT,
  @AvailableSuFrom INT,
  @AvailableSuTo INT

  AS
  BEGIN

	UPDATE [dbo].[Teams]
	   SET [Available] = @Available
		,[Name] = @Name
		  ,[Badge] = @Badge
		  ,[Comments] = @Comments
		  ,[CreatedOn] = @CreatedOn
		  ,[FavStadium] = @FavStadium
		  ,[HomeJersey] = @HomeJersey
		  ,[AwayJersey] = @AwayJersey
		  ,[NumberOfGames] = @NumberOfGames
		  ,[Rank] = @Rank
		  ,[Rating] = @Rating
		  ,[ReviewRating] = @ReviewRating
		  ,[Status] = @Status
		  ,[Wins] = @Wins
		  ,[WinStreak] = @WinStreak
		  ,[YellowCard] = @YellowCard
		  ,[TeamOfSize] = @TeamOfSize
		  ,[TeamOfSize1] = @TeamOfSize1
		  ,[TeamOfSize2] = @TeamOfSize2
		  ,[AvailableMFrom] = @AvailableMFrom
		  ,[AvailableMTo] = @AvailableMTo
		  ,[AvailableTFrom] = @AvailableTFrom
		  ,[AvailableTTo] = @AvailableTTo
		  ,[AvailableWFrom] = @AvailableWFrom
		  ,[AvailableWTo] = @AvailableWTo
		  ,[AvailableThFrom] = @AvailableThFrom
		  ,[AvailableThTo] = @AvailableThTo
		  ,[AvailableFFrom] = @AvailableFFrom
		  ,[AvailableFTo] = @AvailableFTo
		  ,[AvailableSaFrom] = @AvailableSaFrom
		  ,[AvailableSaTo] = @AvailableSaTo
		  ,[AvailableSuFrom] = @AvailableSuFrom
		  ,[AvailableSuTo] = @AvailableSuTo
	 WHERE [TeamId] = @TeamId

		   END
