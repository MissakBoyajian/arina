USE [Arina]
GO
/****** Object:  StoredProcedure [dbo].[Teams_GetById]    Script Date: 12/20/2018 12:05:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[Teams_GetById]

@TeamId UNIQUEIDENTIFIER = NULL

AS
BEGIN
SELECT [TeamId]
,t.[Name]
      ,[Available]
      ,[Badge]
      ,[Comments]
      ,[CreatedOn]
      ,[FavStadium]
      ,[HomeJersey]
      ,[AwayJersey]
      ,[NumberOfGames]
      ,[Rank]
      ,t.[Rating]
      ,[ReviewRating]
      ,[Status]
      ,[Wins]
      ,[WinStreak]
      ,[YellowCard]
      ,[TeamOfSize]
      ,[TeamOfSize1]
	  ,[TeamOfSize2]
      ,[AvailableMFrom]
      ,[AvailableMTo]
      ,[AvailableTFrom]
      ,[AvailableTTo]
      ,[AvailableWFrom]
      ,[AvailableWTo]
      ,[AvailableThFrom]
      ,[AvailableThTo]
      ,[AvailableFFrom]
      ,[AvailableFTo]
      ,[AvailableSaFrom]
      ,[AvailableSaTo]
      ,[AvailableSuFrom]
      ,[AvailableSuTo]
	  ,s.[Name] as [StadiumName]
	  ,[NumberOfPlayers]
  FROM [dbo].[Teams] as t left join [Stadiums]as s on FavStadium = StadiumId
  WHERE TeamId = @TeamId
  END
