USE [Arina]
GO
/****** Object:  StoredProcedure [dbo].[Teams_Save]    Script Date: 12/20/2018 12:11:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[Teams_Save]

  @TeamId UNIQUEIDENTIFIER,
  @Name nvarchar(50),
  @Available BIT,
  @Badge INT,
  @Comments NVARCHAR(50),
  --@CreatedOn DATETIMEOFFSET(7),
  @FavStadium UNIQUEIDENTIFIER,
  @HomeJersey varchar(15),
  @AwayJersey varchar(15),
  @NumberOfGames INT,
  @Rank DECIMAL(18,0),
  @Rating DECIMAL(18,0),
  @ReviewRating DECIMAL(18,0),
  @Status INT,
  @Wins INT,
  @WinStreak INT,
  @YellowCard INT,
  @TeamOfSize INT,
  @TeamOfSize1 INT,
  @TeamOfSize2 INT,
  @AvailableMFrom INT,
  @AvailableMTo INT,
  @AvailableTFrom INT,
  @AvailableTTo INT,
  @AvailableWFrom INT,
  @AvailableWTo INT,
  @AvailableThFrom INT,
  @AvailableThTo INT,
  @AvailableFFrom INT,
  @AvailableFTo INT,
  @AvailableSaFrom INT,
  @AvailableSaTo INT,
  @AvailableSuFrom INT,
  @AvailableSuTo INT,
  @Captain uniqueidentifier

  AS
  BEGIN

  INSERT INTO [dbo].[Teams]
           ([TeamId]
		   ,[Name]
           ,[Available]
           ,[Badge]
           ,[Comments]
           --,[CreatedOn]
           ,[FavStadium]
           ,[HomeJersey]
           ,[AwayJersey]
           ,[NumberOfGames]
           ,[Rank]
           ,[Rating]
           ,[ReviewRating]
           ,[Status]
           ,[Wins]
           ,[WinStreak]
           ,[YellowCard]
           ,[TeamOfSize]
           ,[TeamOfSize1]
		   ,[TeamOfSize2]
           ,[AvailableMFrom]
           ,[AvailableMTo]
           ,[AvailableTFrom]
           ,[AvailableTTo]
           ,[AvailableWFrom]
           ,[AvailableWTo]
           ,[AvailableThFrom]
           ,[AvailableThTo]
           ,[AvailableFFrom]
           ,[AvailableFTo]
           ,[AvailableSaFrom]
           ,[AvailableSaTo]
           ,[AvailableSuFrom]
           ,[AvailableSuTo])
     VALUES
           (@TeamId, 
		   @Name,
           @Available, 
           @Badge, 
           @Comments,
           --@CreatedOn,
           @FavStadium, 
           @HomeJersey, 
           @AwayJersey, 
           @NumberOfGames,
           @Rank,
           @Rating,
           @ReviewRating,
           @Status, 
           @Wins, 
           @WinStreak, 
           @YellowCard, 
           @TeamOfSize, 
           @TeamOfSize1,
		   @TeamOfSize2,
           @AvailableMFrom, 
           @AvailableMTo, 
           @AvailableTFrom, 
           @AvailableTTo, 
           @AvailableWFrom, 
           @AvailableWTo,
           @AvailableThFrom,
           @AvailableThTo, 
           @AvailableFFrom, 
           @AvailableFTo, 
           @AvailableSaFrom, 
           @AvailableSaTo, 
           @AvailableSuFrom, 
           @AvailableSuTo)

		   exec [dbo].TeamPlayers_Save @TeamId,@Captain, 1;

		   END

