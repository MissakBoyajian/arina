USE [Arina]
GO
/****** Object:  StoredProcedure [dbo].[Teams_GetByMemberId]    Script Date: 12/20/2018 12:10:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Teams_GetByMemberId]

@MemberId UNIQUEIDENTIFIER = NULL

AS
BEGIN
SELECT t.[TeamId]
		,t.[Name]
      ,t.[Available]
      ,t.[Badge]
      ,t.[Comments]
      ,t.[CreatedOn]
      ,t.[FavStadium]
      ,t.[HomeJersey]
      ,t.[AwayJersey]
      ,t.[NumberOfGames]
      ,t.[Rank]
      ,t.[Rating]
      ,t.[ReviewRating]
      ,t.[Status]
      ,t.[Wins]
      ,t.[WinStreak]
      ,t.[YellowCard]
      ,t.[TeamOfSize]
      ,t.[TeamOfSize1]
	  ,t.[TeamOfSize2]
      ,t.[AvailableMFrom]
      ,t.[AvailableMTo]
      ,t.[AvailableTFrom]
      ,t.[AvailableTTo]
      ,t.[AvailableWFrom]
      ,t.[AvailableWTo]
      ,t.[AvailableThFrom]
      ,t.[AvailableThTo]
      ,t.[AvailableFFrom]
      ,t.[AvailableFTo]
      ,t.[AvailableSaFrom]
      ,t.[AvailableSaTo]
      ,t.[AvailableSuFrom]
      ,t.[AvailableSuTo]
	  ,s.[Name] as [StadiumName]
	  ,[NumberOfPlayers]
  FROM [dbo].[TeamPlayers] as tp 
  LEFT JOIN Teams as t ON tp.TeamId = t.TeamId
  LEFT JOIN [Stadiums]as s on FavStadium = StadiumId
  

  WHERE PlayerId = @MemberId

  END
