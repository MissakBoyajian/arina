USE [Arina]
GO
/****** Object:  StoredProcedure [dbo].[Teams_GetAll]    Script Date: 12/20/2018 12:08:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Teams_GetAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [TeamId]
	,t.[Name]
      ,[Available]
      ,[Badge]
      ,[Comments]
      ,[CreatedOn]
      ,[FavStadium]
      ,[HomeJersey]
      ,[AwayJersey]
      ,[NumberOfGames]
      ,[Rank]
      ,t.[Rating]
      ,[ReviewRating]
      ,[Status]
      ,[Wins]
      ,[WinStreak]
      ,[YellowCard]
      ,[TeamOfSize]
      ,[TeamOfSize1]
	  ,[TeamOfSize2]
      ,[AvailableMFrom]
      ,[AvailableMTo]
      ,[AvailableTFrom]
      ,[AvailableTTo]
      ,[AvailableWFrom]
      ,[AvailableWTo]
      ,[AvailableThFrom]
      ,[AvailableThTo]
      ,[AvailableFFrom]
      ,[AvailableFTo]
      ,[AvailableSaFrom]
      ,[AvailableSaTo]
      ,[AvailableSuFrom]
      ,[AvailableSuTo]
	  ,s.[Name] as [StadiumName]
	  ,[NumberOfPlayers]
  FROM [dbo].[Teams] as t left join [Stadiums]as s on FavStadium = StadiumId

END
