
import { FormControl } from '@angular/forms';

import * as _ from 'lodash';

export abstract class FormBaseClass {


    //Dealer Groups and Sites

    abstract GetPage(infiniteScroll?): void;
    abstract InitializeVariables(): void;


    //SEARCH DELAY
    public searchTerm: string = '';
    public searchControl: FormControl;

    public PageLoaded: boolean = false; //Menu Loading
    public PageLoading: boolean = false; //Menu Loading

    public PageNumber: number = 1;
    public Criteria: string = '';


    constructor() {


    }


    onKeyPageSearch() {
        this.Criteria = this.searchTerm.trim();
    }

    ngAfterViewInit() {
        // this.searchControl.valueChanges.debounceTime(this._AppConfig.SearchDelay).distinctUntilChanged().subscribe(search => {
        //     this.onKeyPageSearch();
        // });
        // this.GetPage();
    }

    LoadMorePage(infiniteScroll: any) {

        this.PageNumber++;
        this.GetPage(infiniteScroll);

    }


}


