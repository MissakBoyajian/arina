import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

@Component({
  selector: 'page-verifysms',
  templateUrl: 'verifysms.html'
})
export class PageVerifySMS {

  constructor(public viewCtrl: ViewController) {


  }

  CloseScreen()
  {
    this.viewCtrl.dismiss();
  }

}
