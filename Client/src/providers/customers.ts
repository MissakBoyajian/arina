import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { Api } from './api';

@Injectable()
export class ProviderCustomers {

    constructor(private api: Api) {

    }

    Get(FilterOptions: any) {
        return this.api.get('customers' + this.BuildQuery(FilterOptions));
    }

    Save(Customer: any) {
        return this.api.post('customers', Customer);
    }

    BuildQuery(FilterOptions: any) {

    }
}