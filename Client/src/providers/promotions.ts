import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { Api } from './api';

@Injectable()
export class ProviderPromotions {

    constructor(private api: Api) {

    }

    Get(FilterOptions: any) {
        return this.api.get('promotions');
    }

    Save(Promotion: any) {
        return this.api.post('promotions', Promotion);
    }

    BuildQuery(FilterOptions: any) {

    }
}