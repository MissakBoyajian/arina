import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { Api } from './api';
import { Settings } from '../app/settings';

@Injectable()
export class ProviderStadiums {

    constructor(private api: Api, private _settings: Settings) {

    }

    Get(FilterOptions: any = null) {
        return this.api.get('members/' + this._settings.CurrentUser.MemberId + '/ministadiums');
    }

    GetFree(FilterOptions: any = null) {
        return this.api.get('ministadiums/free' + this.BuildQuery(FilterOptions));
    }

    GetAllStadiums(FilterOptions: any = null){
      return this.api.get('stadiums' + this.BuildQuery(FilterOptions));
    }

    // Save(Promotion: any) {
    //     return this.api.post('promotions', Promotion);
    // }

    BuildQuery(FilterOptions: any = null) {
        return '?PageNumber=' + FilterOptions.PageNumber +
            '&SortBy=' + FilterOptions.SortBy +
            '&FloorType=' + FilterOptions.FloorType +
            '&StadiumType=' + FilterOptions.StadiumType +
            '&DistanceFrom=' + FilterOptions.Distance.lower +
            '&DistanceTo=' + FilterOptions.Distance.upper +
            '&PriceFrom=' + FilterOptions.Price.lower +
            '&PriceTo=' + FilterOptions.Price.upper +
            '&Lat=' + FilterOptions.Lat +
            '&Lon=' + FilterOptions.Lon +
            '&DateFrom=' + FilterOptions.DateFrom.toISOString()+
            '&NumPlayers=' + FilterOptions.NumPlayers;
    }
}
