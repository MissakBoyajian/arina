import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { Api } from './api';

@Injectable()
export class UserProvider {
  _user: any;
  token: any = '';

  constructor(public api: Api) { }

  login(accountInfo?: any) {
    let body = "username=" + encodeURIComponent(accountInfo.username) + "&password=" + encodeURIComponent(accountInfo.password) + "&grant_type=password";
    return this.api.PostCredentials('token', body);
  }

  signup(accountInfo: any) {
    let seq = this.api.post('signup', accountInfo);

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {

      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  logout() {
    this._user = null;
  }

  GetLoggedInMember() {
    return this.api.get("members/loggedIn");
  }

  _loggedIn(resp) {



  }
}
