import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { Api } from './api';
import { Settings } from '../app/settings';

@Injectable()
export class ProviderSchedules {

    simpleColumns: any;

    constructor(private api: Api, private _user: Settings) {


    }

    GetSchedulesByDate(Date: any) {
        return this.api.get('schedules?Date=' + Date.toISOString());
    }

    GetPreviousBookings(PageNumber: number) {
        return this.api.get('schedules/previous/' + this._user.CurrentUser.MemberId + '?PageNumber=' + PageNumber);
    }

    GetUpcomingBookings(PageNumber: number) {
        return this.api.get('schedules/current/' + this._user.CurrentUser.MemberId + '?PageNumber=' + PageNumber);
    }

    DeleteScheduleById(Id: string) {
        return this.api.delete('schedules/' + Id);
    }

    SaveSchedule(Schedule: any) {
        return this.api.post('schedules', Schedule);
    }
}