import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { Api } from './api';

@Injectable()
export class ProviderTeams {

    constructor(private api: Api) { }

    GetTeamsByMemberId(MemberId: string) {
        return this.api.get('members/'+ MemberId+'/teams');
    }

    GetTeamById(TeamId:string){
        return this.api.get('teams/' + TeamId);
    }

    Save(Team: any) {
        return this.api.post('teams', Team);
    }
}
