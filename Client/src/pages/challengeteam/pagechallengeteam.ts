import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { WheelSelector } from '@ionic-native/wheel-selector';

@Component({
  selector: 'page-challengeteam',
  templateUrl: 'pagechallengeteam.html'
})

export class PageChallengeTeam {

  jsonData: any;

  constructor(public navCtrl: NavController, private selector: WheelSelector) {

    this.jsonData = {
      numbers: [
        { description: "1" },
        { description: "2" },
        { description: "3" }
      ],
      fruits: [
        { description: "Apple" },
        { description: "Banana" },
        { description: "Tangerine" }
      ]
    };
  }



  // // basic selection, setting initial displayed default values: '3' 'Banana'
  // selectFruit() {
  //   this.selector.show({
  //     title: "How Much?",
  //     items: [
  //       this.jsonData.numbers, this.jsonData.fruits
  //     ],
  //     positiveButtonText: "Ok",
  //     negativeButtonText: "Nope",
  //     defaultItems: [
  //       { index: 0, value: this.jsonData.numbers[1].description },
  //       { index: 1, value: this.jsonData.fruits[1].description }
  //     ]
  //   }).then(
  //     result => {
  //       console.log(result[0].description + ' ' + result[1].description);
  //     },
  //     err => console.log('Error: ' + JSON.stringify(err))
  //   );
  // }

  // // more complex as overrides which key to display
  // // then retrieve properties from original data
  // selectNamesUsingDisplayKey() {
  //   this.selector.show({
  //     title: "Who?",
  //     items: [
  //       this.jsonData.firstNames, this.jsonData.lastNames
  //     ],
  //     displayKey: 'name',
  //     defaultItems: [
  //       { index: 0, value: this.jsonData.firstNames[2].name },
  //       { index: 0, value: this.jsonData.lastNames[3].name }
  //     ]
  //   }).then(
  //     result => {
  //       console.log(result[0].name + ' (id= ' + this.jsonData.firstNames[result[0].index].id + '), ' +
  //         result[1].name + ' (id=' + this.jsonData.lastNames[result[1].index].id + ')');
  //     },
  //     err => console.log('Error: ' + JSON.stringify(err))
  //   );
  // }

}
