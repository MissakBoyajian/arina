import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PageBookingAdd } from '../bookingadd/bookingadd';
import { AlertClass } from '../../helpers/AlertClass';
import { PagePlayerFinder } from '../playerfinder/playerfinder';
import { PageStadiums } from '../stadiums/stadiums';
import { PageChallengeTeam } from '../challengeteam/pagechallengeteam';
import { Settings } from '../../app/settings';
import { PageProfile } from '../profile/profile';

@Component({
  selector: 'page-homepage',
  templateUrl: 'homepage.html'
})
export class PageHomepage {

  constructor(public _settings: Settings, public navCtrl: NavController, public alertClass: AlertClass) {

  }


  AddBooking() {
    this.navCtrl.push(PageBookingAdd);
  }

  OpenPlayers() {
    this.alertClass.showInfo("OPPA", "TITLE");
    this.navCtrl.push(PagePlayerFinder);
  }

  OpenBookStadiums() {
    this.navCtrl.push(PageStadiums);

  }

  GoToProfile() {
    this.navCtrl.setRoot(PageProfile);
  }

  OpenChallengeTeams() {
    this.navCtrl.push(PageChallengeTeam);
  }
}
