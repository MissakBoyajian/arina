import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { ProviderSchedules } from '../../providers/schedules';
import { AlertClass } from '../../helpers/AlertClass';

@Component({
  selector: 'page-mybookings',
  templateUrl: 'mybookings.html'
})
export class PageMyBookings {

  public BookingType: string = "0"
  public Bookings: any = [];

  constructor(public alertCtrl: AlertController, public _alertClass: AlertClass, public navCtrl: NavController, public _schedulesProvider: ProviderSchedules) {

    this.GetBookings();

  }

  GetBookings() {

    this.Bookings = [];

    switch (this.BookingType) {

      case '0':
        this.GetCurrentBookings();
        break;

      case '1':
        this.GetPreviousBookings();
        break;

      default:
        break;
    }

  }

  GetPreviousBookings() {

    this._schedulesProvider.GetPreviousBookings(1).subscribe((res: any) => {

      res.forEach(element => {
        element.ScheduleDateFrom = new Date(element.ScheduleDateFrom)
      });

      this.Bookings = this.Bookings.concat(res);

    }, err => {

    })

  }

  GetCurrentBookings() {

    this._schedulesProvider.GetUpcomingBookings(1).subscribe((res: any) => {

      res.forEach(element => {
        element.ScheduleDateFrom = new Date(element.ScheduleDateFrom)
      });

      this.Bookings = this.Bookings.concat(res);

    }, err => {

    })

  }

  DeleteBooking(item: any) {

    let alert = this.alertCtrl.create({

      title: 'Are you sure you want to delete the booking??',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {

            this._schedulesProvider.DeleteScheduleById(item.ScheduleId).subscribe(res => {

              this._alertClass.showLongToast("Booking Deleted Successfully");

              this.GetBookings();

            }, err => {

              this._alertClass.showAlert(err);

            })
          }

        }
      ]
    });
    alert.present();

  }

}
