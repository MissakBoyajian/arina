import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


import { AlertClass } from '../../../helpers/AlertClass';
import { Settings } from '../../../app/settings';
import { UserProvider } from '../../../providers/user';


@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})

export class PageRegister {

  isLoading: boolean = false;

  constructor(public navCtrl: NavController,
    public user: UserProvider,
    public customAlert: AlertClass,
    public _settings: Settings
  ) {



  }


}
