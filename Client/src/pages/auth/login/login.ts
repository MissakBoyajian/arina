import { Component } from '@angular/core';

import { NavController, AlertController } from 'ionic-angular';

import { AlertClass } from '../../../helpers/AlertClass';

import { Settings } from '../../../app/settings';
import { UserProvider } from '../../../providers/user';
import { PageHomepage } from '../../homepage/homepage';
import { PageRegister } from '../register/register';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class PageLogin {
  // The account fields for the login form.
  // If you're using the username field with or without username, make
  // sure to add it to the type

  private account: { username: string, password: string } = {
    username: '',
    password: ''
  };

  private RegisterAccount: any = {
    username: '',
    password: ''
  };

  private isLoading: boolean = false;

  private IsLoginPage: any = true;

  constructor(private navCtrl: NavController,
    private user: UserProvider,
    private customAlert: AlertClass,
    private _settings: Settings,
    private alertCtrl: AlertController
  ) {
    this.account.username = this._settings.getValue('username');
  }

  // Attempt to login in through our User service
  // doLogin() {

  //   let alert = this.alertCtrl.create({
  //     title: 'Confirm purchase',
  //     message: 'Do you want to buy this book?',
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         role: 'cancel',
  //         handler: () => {
  //           console.log('Cancel clicked');
  //         }
  //       },
  //       {
  //         text: 'Buy',
  //         handler: () => {
  //           console.log('Buy clicked');
  //         }
  //       }
  //     ]
  //   });
  //   alert.present();

  // Attempt to login in through our User service
  doLogin() {
    if (!this.isLoading) {

      this.isLoading = true;
      if (this.account.username.trim() != '' && this.account.password.trim() != '') {

        this.user.login(this.account).subscribe((resp: any) => {

          // IF LOGGED IN SUCCESSFULLY
          let token: any = resp.access_token;
          var loggedInMember = JSON.parse(resp.LoggedInMember) ;
          this._settings.setValue('loggedInMemberId',loggedInMember.MemberId);
          this._settings.setValue('token', token);
          this._settings.setValue('tokenDate', new Date().toString());
          this._settings.setValue('username', this.account.username);
          this._settings.CurrentUser = loggedInMember;
          
          this._settings.reqOpts.headers['Authorization'] = 'Bearer ' + this._settings.getValue('token');

          //GO HOME PAGE
          this.navCtrl.setRoot(PageHomepage);
          this.isLoading = false;

        }, (err) => {
          this.isLoading = false;
          this.customAlert.showLongToast('Wrong username or password');
        });
      } else {
        this.isLoading = false;
        this.customAlert.showLongToast('Please fill all missing fields');
      }

      //this.navCtrl.setRoot(PageSchedules);
    }

  }

  FlipRegister() {

    this.IsLoginPage = !this.IsLoginPage
  }

  forgotPassword() {
    this.customAlert.showLongToast('Forget password clicked');
  }
}
