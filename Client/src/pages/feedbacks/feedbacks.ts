import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PageHomepage } from '../homepage/homepage';
import { ProviderFeedbacks } from '../../providers/feedbacks';
import { AlertClass } from '../../helpers/AlertClass';

@Component({
  selector: 'page-feedbacks',
  templateUrl: 'feedbacks.html'
})
export class PageFeedbacks {

  public FeedBack: any = "";

  constructor(public navCtrl: NavController, public _feedbackProvider: ProviderFeedbacks, public _alertClass: AlertClass) {

  }

  SaveFeedback() {

    if (this.FeedBack.trim().length != 0) {
      this._feedbackProvider.Save(this.FeedBack).subscribe(res => {
        this._alertClass.showLongToast("Feedback Saved Successfully");
        this.navCtrl.setRoot(PageHomepage);
      }, err => {
        this._alertClass.showLongToast("Something Went Wrong");
      })
    }

  }

}
