import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-stadiumfilters',
  templateUrl: 'stadiumfilters.html'
})
export class PageStadiumFilters {

  public FilterOptions: any = {
    SortBy: '0',
    StadiumType: '0',
    FloorType: '0',
    Distance:
    {
      upper: 30,
      lower: 0
    },
    Price:
    {
      upper: 300000,
      lower: 50000
    }
  }

  constructor(private navParams: NavParams, private viewCtrl: ViewController) {

    this.FilterOptions.SortBy = this.navParams.get('FilterOptions').SortBy;
    this.FilterOptions.StadiumType = this.navParams.get('FilterOptions').StadiumType;
    this.FilterOptions.FloorType = this.navParams.get('FilterOptions').FloorType;
    this.FilterOptions.Distance.upper = this.navParams.get('FilterOptions').Distance.upper;
    this.FilterOptions.Distance.lower = this.navParams.get('FilterOptions').Distance.lower;
    this.FilterOptions.Price.upper = this.navParams.get('FilterOptions').Price.upper;
    this.FilterOptions.Price.lower = this.navParams.get('FilterOptions').Price.lower;
  }

  ApplyFilter() {
    this.viewCtrl.dismiss(this.FilterOptions);
  }

  CancelFilter() {
    this.viewCtrl.dismiss();
  }

  CloseScreen() {
    this.viewCtrl.dismiss();
  }

}
