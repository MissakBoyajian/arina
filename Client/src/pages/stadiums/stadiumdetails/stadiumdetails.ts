import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';

declare var google;
@Component({
  selector: 'page-stadiumdetails',
  templateUrl: 'stadiumdetails.html'
})

export class PageStadiumDetails {

  @ViewChild('map') mapElement: ElementRef;
  public map: any;

  public FreeStadiums: any = [
    {
      StadiumName: "",
      MiniStadiumName: "",
      Rating: "",
      Price: 0,
      City: "",
      Distance: "",
      MediaUrl: "https://upload.wikimedia.org/wikipedia/commons/6/65/Saitama_stadium.png"
    }
  ]

  constructor(public navCtrl: NavController, public platform: Platform) {



  }

  ngAfterViewInit() {
    this.initMap();
  }

  initMap = () => {

    this.platform.ready().then(() => {

      let Origin = new google.maps.LatLng(0, 0)

      let mapOptions = {
        center: Origin,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        fullscreenControl: false
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      //CUSTOMER LOCATION
      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: this.map.getCenter()
      })

    })

  }


}
