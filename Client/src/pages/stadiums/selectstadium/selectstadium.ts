import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProviderStadiums } from '../../../providers/stadiums';
import { FormBaseClass } from '../../../Interfaces/FormBaseClass';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the SelectStadiumPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'pageselect-stadium',
  templateUrl: 'selectstadium.html',
})
export class PageSelectStadium extends FormBaseClass {

  constructor(private geolocation: Geolocation,
    public navCtrl: NavController,
    public navParams: NavParams,
    public _stadiumProvider: ProviderStadiums)
  {

    super();
    this.GetPage();
  }

  public FilterOptions: any = {

    SortBy: '0',
    StadiumType: '0',
    FloorType: '0',

    DistanceFrom: 0,
    DistanceTo: 0,

    PriceFrom: 100,
    PriceTo: 300000,

    Lat: 0,
    Lon: 0,
    DateFrom: new Date(),
    DateTo: new Date(),

    PageNumber: 0

  }

  InitializeVariables()
  {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectStadiumPage');
  }

  public Stadiums: any = [];

  private DatePicker: any = "";



  GetPage(infiniteScroll?: any): void {
    this.FetchAllStadiums(infiniteScroll);
  }

  GetAllStadiums() {
    this.geolocation.getCurrentPosition().then((resp) => {

      this.FilterOptions.Lat = resp.coords.latitude;
      this.FilterOptions.Lon = resp.coords.longitude;

      this.FetchAllStadiums();

    }).catch((error) => {

      this.FetchAllStadiums();

    });
  }

  FetchAllStadiums(infiniteScroll: any = null) {
    this.FilterOptions.PageNumber = this.PageNumber;
    this.PageLoading = true;

    this._stadiumProvider.GetAllStadiums(this.FilterOptions).subscribe((res: any) => {

      console.log(res);
      this.Stadiums = this.Stadiums.concat(res.Stadiums);

      if (res.TotalPages == res.CurrentPage) {
        this.PageLoaded = true;
      }

      this.PageLoading = false;

      if (infiniteScroll != null)
        infiniteScroll.complete();

      console.log(res);
    }, err => {

      this.PageLoading = false;

    })

  }

}
