import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PageSelectStadium } from './selectstadium';

@NgModule({
  declarations: [
    PageSelectStadium,
  ],
  imports: [
    IonicPageModule.forChild(PageSelectStadium),
  ],
})
export class SelectStadiumPageModule {}
