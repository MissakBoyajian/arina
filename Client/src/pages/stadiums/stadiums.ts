import { Component } from '@angular/core';
import { NavController, ModalController, AlertController } from 'ionic-angular';
import { PageStadiumDetails } from './stadiumdetails/stadiumdetails';
import { PageStadiumFilters } from './stadiumfilters/stadiumfilters';
import { PageVerifySMS } from '../../components/verifysms/verifysms';
import { LibraryService } from '../../helpers/LibraryService';
import { Geolocation } from '@ionic-native/geolocation';
import { Settings } from '../../app/settings';
import { ProviderSchedules } from '../../providers/schedules';
import { AlertClass } from '../../helpers/AlertClass';
import { PageMyBookings } from '../mybookings/mybookings';
import { ProviderStadiums } from '../../providers/stadiums';
import { FormBaseClass } from '../../Interfaces/FormBaseClass';

@Component({
  selector: 'page-stadiums',
  templateUrl: 'stadiums.html'
})
export class PageStadiums extends FormBaseClass {



  public FreeStadiums: any = [];

  private DatePicker: any = "";

  public FilterOptions: any = {

    SortBy: '3',
    StadiumType: '2',
    FloorType: '2',
    NumPlayers: '5',
    Distance:
    {
      upper: 30,
      lower: 0
    },
    Price:
    {
      upper: 300000,
      lower: 50000
    },
    Lat: 0,
    Lon: 0,
    DateFrom: new Date(),
    DateTo: new Date(),

    PageNumber: 0

  }

  simpleColumns: any;
  DatesList = [];

  constructor(private geolocation: Geolocation,
    public _user: Settings,
    public _schedulesProvider: ProviderSchedules,
    public _stadiumProvider: ProviderStadiums,
    private _alertClass: AlertClass,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public _libraryService: LibraryService
  ) {
    super();

    this.DatesList = this._libraryService.ConstructPicker();

    this.simpleColumns = [
      {
        name: 'col1',
        options: this.DatesList
      }, {
        name: 'col2',
        options: this._libraryService.PickerTimes
      }, {
        name: 'col3',
        options: [
          { text: '5 vs 5', value: '5' },
          { text: '6 vs 6', value: '6' },
          { text: '7 vs 7', value: '7' },
          { text: '8 vs 8', value: '8' },
          { text: '9 vs 9', value: '9' },
          { text: '10 vs 10', value: '10' },
          { text: '11 vs 11', value: '11' },
          { text: '12 vs 12', value: '12' },
        ]
      }
    ];

    this.DatePicker = this.DatesList[1].value + ' - ' + this._libraryService.PickerTimes[0].value + ' - 5';
    this.DateSelectionChanged();

    this.GetPage();

  }

  GetPage(infiniteScroll?: any): void {
    this.GetFreeStadiums();
  }

  InitializeVariables(): void {
    this.FreeStadiums = [];
    this.PageNumber = 1;
  }

  GetFreeStadiums() {
    this.geolocation.getCurrentPosition().then((resp) => {

      this.FilterOptions.Lat = resp.coords.latitude;
      this.FilterOptions.Lon = resp.coords.longitude;

      this.FetchFreeStadiums();

    }).catch((error) => {

      this.FetchFreeStadiums();

    });
  }

  FetchFreeStadiums(infiniteScroll: any = null) {

    this.FilterOptions.PageNumber = this.PageNumber;
    this.PageLoading = true;

    this._stadiumProvider.GetFree(this.FilterOptions).subscribe((res: any) => {

      this.FreeStadiums = this.FreeStadiums.concat(res.FreeStadiums);

      if (res.TotalPages == res.CurrentPage) {
        this.PageLoaded = true;
      }

      this.PageLoading = false;

      if (infiniteScroll != null)
        infiniteScroll.complete();

      console.log(res);
    }, err => {

      this.PageLoading = false;

    })

  }

  DateSelectionChanged() {

    this.FilterOptions.DateFrom = new Date(this.DatePicker.split(' - ')[0])
    this.FilterOptions.DateFrom.setHours(Number(this.DatePicker.split(' - ')[1].split(':')[0].trim()))
    this.FilterOptions.DateFrom.setMinutes(Number(this.DatePicker.split(' - ')[1].split(':')[1].trim()))

    this.FilterOptions.DateTo = new Date(this.FilterOptions.DateFrom);
    this.FilterOptions.DateTo = new Date(this.FilterOptions.DateTo.setMinutes(this.FilterOptions.DateTo.getMinutes() + 90));
    this.FilterOptions.NumPlayers = Number(this.DatePicker.split(' - ')[2].trim());

    this.InitializeVariables();
    this.FetchFreeStadiums();

  }

  GoDetails(item: any = []) {
    this.navCtrl.push(PageStadiumDetails);
  }

  OpenFilter() {
    let FilterModal = this.modalCtrl.create(PageStadiumFilters,
      {
        FilterOptions: this.FilterOptions
      });

    FilterModal.onDidDismiss(res => {
      if (res != null || res != undefined) { 
        this.FilterOptions.SortBy = res.SortBy;
        this.FilterOptions.StadiumType = res.StadiumType;
        this.FilterOptions.FloorType = res.FloorType;
        this.FilterOptions.Distance.upper = res.Distance.upper;
        this.FilterOptions.Distance.lower = res.Distance.lower;
        this.FilterOptions.Price.upper = res.Price.upper;
        this.FilterOptions.Price.lower = res.Price.lower;
        this.InitializeVariables();
        this.FetchFreeStadiums();

      }
    })
    FilterModal.present();

  }

  Reserve(MiniStadium: any = null) {

    //if (this._user.CurrentUser.IsSmsVerified) {

    if (true) {

      let alert = this.alertCtrl.create({
        title: 'Are you sure you want to book?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'Yes',
            handler: () => {

              var ScheduleData = {
                ScheduleDateFrom: this.FilterOptions.DateFrom,
                Price: this._user.CurrentUser.Price,
                Duration: 90,
                MemberId: this._user.CurrentUser.MemberId,
                MiniStadiumId: MiniStadium.MiniStadiumId
              }

              this._schedulesProvider.SaveSchedule(ScheduleData).subscribe(res => {

                this._alertClass.showLongToast("Scheduled Saved Successfully");
                this.navCtrl.setRoot(PageMyBookings);

              }, err => {

                this._alertClass.showLongToast("Schedule Conflict. Please Select Another Time.");

              });
            }

          }
        ]
      });
      alert.present();
    }
    else {

      // let VerifyModal = this.modalCtrl.create(PageVerifySMS,
      //   {
      //   });

      // VerifyModal.onDidDismiss(res => {
      //   if (res != null || res != undefined) {

      //   }
      // })
      // VerifyModal.present();
    }


  }

}
