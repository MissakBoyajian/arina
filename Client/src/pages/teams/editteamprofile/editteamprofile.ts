import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProviderTeams } from '../../../providers/team';

@Component({
  selector: 'page-editteamprofile',
  templateUrl: 'editteamprofile.html'
})
export class PageEditTeamProfile {

  public TeamProfile: any = {};

  constructor(public navCtrl: NavController, public navParam: NavParams, public _teamService: ProviderTeams) {

    this.TeamProfile = this.navParam.get("Team");

  }

  UpdateTeam() {

  }

}
