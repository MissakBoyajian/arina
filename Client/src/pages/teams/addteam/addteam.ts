import { ViewChild } from '@angular/core'
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Navbar } from 'ionic-angular';
import { ProviderTeams } from '../../../providers/team';
import { AlertClass } from '../../../helpers/AlertClass';
import { PageTeamProfile } from '../teamprofile/teamprofile';
import { Settings } from '../../../app/settings';
import { PageSelectStadium } from '../../stadiums/selectstadium/selectstadium';

@Component({
  selector: 'page-addteam',
  templateUrl: 'addteam.html'
})
export class PageAddTeam {
  @ViewChild(Navbar) navBar: Navbar;
  public matchSizes: any = [
    { name: 5, active: false, disabled: false },
    { name: 6, active: false, disabled: false },
    { name: 7, active: false, disabled: false },
    { name: 8, active: false, disabled: false },
    { name: 9, active: false, disabled: false },
    { name: 10, active: false, disabled: false },
    { name: 11, active: false, disabled: false },
  ];

  public MyTeam = {
    name: '',
    badge: null,
    favStadium: '',
    TeamOfSize: null,
    TeamOfSize1: null,
    TeamOfSize2: null,
    homeJersey: "red",
    awayJersey: "blue",
    captain: this._settings.getValue('loggedInMemberId')
  };
  public slidenumber: any = 0;
  public Badges: any = [];


  public colors: any = ['yellow', 'green', 'red', 'blue', 'white', 'black'];

  public countMatchSizesSelected: number = 0;
  public selectedBadge: any = { key: "logo", selected: false };
  private viewIndex: number;

  constructor(public navCtrl: NavController,
    private customAlert: AlertClass,
    private _teamService: ProviderTeams,
    private _settings: Settings) {
    //generate badges
    for (let index = 1; index < 61; index++) {
      this.Badges.push({
        key: index,
        selected: false
      })

    }
  }

  ionViewDidLoad() {
    this.navBar.backButtonClick = (e: UIEvent) => {
      // todo something
      if (this.slidenumber == 0)
        this.navCtrl.pop();
      else
        this.GoToPrevious();
    }
  }


  toggleSelected(matchSize) {
    //toggle selected
    matchSize.active = !matchSize.active;
    var count = 0;
    for (var i = 0; i < this.matchSizes.length; i++) {

      if (this.matchSizes[i].active) {
        count++;
      }
    }
    this.countMatchSizesSelected = count;
    // var selected = this.matchSizes.map(x=>x.name==true);
    // this.countMatchSizesSelected = this.matchSizes.map(x=>x.name==true).length;
    // console.log(this.countMatchSizesSelected);
    //check if more than 2 selected
    // if(this.matchSizes.map(x=>x.active==true).length==2)

    // {
    //   this.matchSizes.forEach(function (item){
    //     if(!item.active)

    //     {
    //       item.disabled = true;



    //     }
    //   });
    // }
  }

  selectBadge(badge) {
    this.MyTeam.badge = badge.key;

    badge.selected = true;
    this.selectedBadge.selected = false;
    this.selectedBadge = badge;
  }

  selectHomeJersey(jersey) {
    this.MyTeam.homeJersey = jersey;
  }

  selectAwayJersey(jersey) {
    this.MyTeam.awayJersey = jersey;
  }

  assignMatchSizes() {
    for (var i = 0; i < this.matchSizes.length; i++) {
      if (this.matchSizes[i].active) {
        if (this.MyTeam.TeamOfSize == null) {
          this.MyTeam.TeamOfSize = this.matchSizes[i].name;
        }
        else if (this.MyTeam.TeamOfSize1 == null) {
          this.MyTeam.TeamOfSize1 = this.matchSizes[i].name;
        }
        else if (this.MyTeam.TeamOfSize2 == null) {
          this.MyTeam.TeamOfSize2 = this.matchSizes[i].name;
        }
      }
    }
  }

  addTeam() {
    this.assignMatchSizes();
    this._teamService.Save(this.MyTeam).subscribe(
      (res: any) => {
        console.log(res.TeamId);
        this.navCtrl.push(PageTeamProfile, { TeamId: res.TeamId });
        this.navCtrl.remove(this.navCtrl.getActive().index);
      },
      error => { this.customAlert.showLongToast("Error creating team") });;
  }

  GoToNext() {
    if (this.slidenumber == 2) {
      this.addTeam();
    }
    else
      this.slidenumber++;
  }

  selectStadium()
  {
    this.navCtrl.push(PageSelectStadium);
  }

  GoToPrevious() {
    this.slidenumber--;
  }



}
