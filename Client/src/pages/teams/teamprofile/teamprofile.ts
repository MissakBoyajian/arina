import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ProviderTeams } from '../../../providers/team';
import { PageEditTeamProfile } from '../editteamprofile/editteamprofile';
import { Settings } from '../../../app/settings';
import { LibraryService} from '../../../helpers/LibraryService';

@Component({
  selector: 'page-teamprofile',
  templateUrl: 'teamprofile.html'
})
export class PageTeamProfile {

  public TeamId: any = null;
  public TeamProfile: any = {};

  constructor(public navCtrl: NavController, 
    public navParam: NavParams, 
    public _teamService: ProviderTeams,
    private _settings: Settings,
    private _helper:LibraryService) {

    this.TeamId = this.navParam.get("TeamId");

    if (this.TeamId != null || this.TeamId != undefined) {
      this.GetTeamById();
    }

  }

  GetTeamById() {

    // this._teamService.GetTeamsByMemberId(this._settings.getValue('loggedInMemberId')).subscribe(res => {
    //   this.TeamProfile = res[0];
    // })

    this._teamService.GetTeamById(this.TeamId).subscribe(res => {
      this.TeamProfile = res[0];
      this.TeamProfile.teamSizes = (this.TeamProfile.TeamOfSize!=null?this.TeamProfile.TeamOfSize + " vs " + this.TeamProfile.TeamOfSize: "");
      this.TeamProfile.TeamOfSize1!=null && this.TeamProfile.TeamOfSize1!=0?this.TeamProfile.teamSizes +=" and " + this.TeamProfile.TeamOfSize1 + " vs " + this.TeamProfile.TeamOfSize1 : "";
      this.TeamProfile.TeamOfSize2!=null && this.TeamProfile.TeamOfSize2!=0?this.TeamProfile.teamSizes +=" and " + this.TeamProfile.TeamOfSize2 + " vs " + this.TeamProfile.TeamOfSize2 : "";
      console.log(this.TeamProfile);
      this.TeamProfile.Rank = this._helper.numberToRank(this.TeamProfile.Rank);
    })

    
  }

  EditTeam() {
    this.navCtrl.push(PageEditTeamProfile)
  }

}
