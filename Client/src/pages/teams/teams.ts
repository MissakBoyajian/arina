import { Component } from '@angular/core';
import { LoadingController, NavController, Item, NavParams } from 'ionic-angular';
import { PageAddTeam } from './addteam/addteam';
import { PageTeamProfile } from './teamprofile/teamprofile';
import { PageChallengeTeam } from '../challengeteam/pagechallengeteam';
import { ProviderTeams } from '../../providers/team';
import { Observable } from 'rxjs/Observable';
import { Settings } from '../../app/settings';
import { LibraryService } from '../../helpers/LibraryService';

@Component({
  selector: 'page-teams',
  templateUrl: 'teams.html'
})
export class PageTeams {

  private MyTeams: any = [];// = this.teamProvider.GetMyTeams();



  // private MyTeams: any = [{
  //   TeamId: "",
  //   Name: "Superman",
  //   Rank: 4,
  //   Rating: 1500,
  //   NumberOfPlayers: 5,
  //   MediaUrl: "https://seeklogo.com/images/F/FC_Barcelona-logo-D941E13B46-seeklogo.com.png"
  // }, {
  //   TeamId: "",
  //   Name: "Aydz",
  //   Rank: 4,
  //   Rating: 1600,
  //   NumberOfPlayers: 5,
  //   MediaUrl: "https://seeklogo.com/images/F/FC_Barcelona-logo-D941E13B46-seeklogo.com.png"
  // }];

  private FromChallenge: any = false;

  constructor(public navParams: NavParams,
    public navCtrl: NavController,
    private _teamService: ProviderTeams,
    private _settings: Settings,
    public _helper:LibraryService) {
      this.GetMyTeams();
  }

  GetMyTeams()
  {
    this.FromChallenge = this.navParams.get('FromMenu') == 'Challenge Teams';
    this._teamService.GetTeamsByMemberId(this._settings.getValue('loggedInMemberId')).subscribe(res=>{this.MyTeams = res});
  }

  AddTeam() {
    this.navCtrl.push(PageAddTeam);
  }

  GoToTeamProfile(item: any) {

    if (!this.FromChallenge) {
      this.navCtrl.push(PageTeamProfile, {
        TeamId: item.TeamId
      })
    }
    else {
      this.navCtrl.push(PageChallengeTeam, {
        TeamId: item.TeamId
      })
    }
  }

  ionViewWillEnter() {
    this.GetMyTeams();
  }


}
