import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-playersfilter',
  templateUrl: 'playersfilter.html'
})
export class PagePlayersFilter {

  public FilterOptions: any = {
    SortBy: '0',
    StadiumType: '0',
    FloorType: '0',
    DistanceFrom: 0,
    DistanceTo: 0,
    PriceFrom: 100,
    PriceTo: 300000
  }

  constructor(private navParams: NavParams, private viewCtrl: ViewController) {

    // this.FilterOptions.SortBy = this.navParams.get('FilterOptions').SortBy;
    // this.FilterOptions.StadiumType = this.navParams.get('FilterOptions').StadiumType;
    // this.FilterOptions.FloorType = this.navParams.get('FilterOptions').FloorType;
    // this.FilterOptions.DistanceFrom = this.navParams.get('FilterOptions').DistanceFrom;
    // this.FilterOptions.DistanceTo = this.navParams.get('FilterOptions').DistanceTo;
    // this.FilterOptions.PriceFrom = this.navParams.get('FilterOptions').PriceFrom;
    // this.FilterOptions.PriceTo = this.navParams.get('FilterOptions').PriceTo;
  }

  ApplyFilter() {
    this.viewCtrl.dismiss(this.FilterOptions);
  }

  CancelFilter() {
    this.viewCtrl.dismiss();
  }

  CloseScreen() {
    this.viewCtrl.dismiss();
  }

}
