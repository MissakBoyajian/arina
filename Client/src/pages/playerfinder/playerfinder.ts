import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { WheelSelector } from '@ionic-native/wheel-selector';
import { PagePlayersFilter } from './playersfilters/playersfilter';

@Component({
  selector: 'page-playerfinder',
  templateUrl: 'playerfinder.html'
})

export class PagePlayerFinder {

  jsonData: any;

  constructor(public navCtrl: NavController, private selector: WheelSelector, private modalCtrl: ModalController) {

  }

  OpenFilter() {
    let FilterModal = this.modalCtrl.create(PagePlayersFilter,
      {

      });

    FilterModal.onDidDismiss(res => {
      if (res != null || res != undefined) {

      }
    })
    FilterModal.present();

  }

}
