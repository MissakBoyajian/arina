import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-leaderboard',
  templateUrl: 'leaderboard.html'
})
export class PageLeaderboard {

  public LeaderBoards: any = [
    {
      TeamName: "Team1",
      Rating: 2000,
      Badge: '1'
    },
    {
      TeamName: "Team2",
      Rating: 1950,
      Badge: '2'
    },
    {
      TeamName: "Team3",
      Rating: 1900,
      Badge: '3'
    },
    {
      TeamName: "Team4",
      Rating: 1850,
      Badge: '4'
    },
    {
      TeamName: "Team5",
      Rating: 1800,
      Badge: '5'
    }
  ];

  constructor(public navCtrl: NavController) {

    

  }



}
