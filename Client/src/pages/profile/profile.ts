import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PageEditProfile } from './editprofile/editprofile';
import { Settings } from '../../app/settings';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class PageProfile {

  public BottomToggle: any = "0";
  public CurrentUser: any = {};

  constructor(public navCtrl: NavController, public _settings: Settings) {
    this.CurrentUser = this._settings.CurrentUser;
    console.log(this.CurrentUser);
  }

  EditProfile() {
    this.navCtrl.push(PageEditProfile);
  }

}
