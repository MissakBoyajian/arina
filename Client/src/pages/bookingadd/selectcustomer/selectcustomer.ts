import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-selectcustomer',
  templateUrl: 'selectcustomer.html'
})
export class PageSelectCustomer {

  constructor(public navCtrl: NavController) {

  }

}
