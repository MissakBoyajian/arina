import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { PageSelectCustomer } from './selectcustomer/selectcustomer';

@Component({
  selector: 'page-bookingadd',
  templateUrl: 'bookingadd.html'
})
export class PageBookingAdd {

  private Schedule: any = {
    Date: new Date(),
    CustomerId: "",
    MiniStadiumId: "",
    Recurring: 0,
    Price: 0,
    Duration: 3
  };

  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {

  }

  SelectCustomer() {
    let profileModal = this.modalCtrl.create(PageSelectCustomer, { userId: 8675309 });
    profileModal.present();
  }

  SaveBooking() {

  }

}
