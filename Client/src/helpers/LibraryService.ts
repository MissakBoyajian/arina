import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';


@Injectable()
export class LibraryService {

    public numberToRank(num: number) {
        let rank: string;
        if (num % 10 == 1)
            rank = "1st";
        else if (num % 10 == 2)
            rank = "2nd";
        else if (num % 10 == 3)
            rank = "3rd";
        else
            rank = num + "th";

        return rank;
    }
    public weekday = ["Su,",
        "Mo,",
        "Tu,",
        "We,",
        "Th,",
        "Fr,",
        "Sa,"];

    public weekdayFull = ["Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"];

    public monthChar = ["Jan,",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
    ];

    public PickerTimes = [
        { text: '7:00 AM', value: '7:0' },
        { text: '7:30 AM', value: '7:30' },
        { text: '8:00 AM', value: '8:0' },
        { text: '8:30 AM', value: '8:30' },
        { text: '9:00 AM', value: '9:0' },
        { text: '9:30 AM', value: '9:30' },
        { text: '10:00 AM', value: '10:0' },
        { text: '10:30 AM', value: '10:30' },
        { text: '11:00 AM', value: '11:0' },
        { text: '11:30 AM', value: '11:30' },
        { text: '12:00 PM', value: '12:0' },
        { text: '12:30 PM', value: '12:30' },
        { text: '1:00 PM', value: '1:0' },
        { text: '1:30 PM', value: '1:30' },
        { text: '2:00 PM', value: '2:0' },
        { text: '2:30 PM', value: '2:30' },
        { text: '3:00 PM', value: '3:0' },
        { text: '3:30 PM', value: '3:30' },
        { text: '4:00 PM', value: '4:0' },
        { text: '4:30 PM', value: '4:30' },
        { text: '5:00 PM', value: '5:0' },
        { text: '5:30 PM', value: '5:30' },
        { text: '6:00 PM', value: '6:0' },
        { text: '6:30 PM', value: '6:30' },
        { text: '7:00 PM', value: '7:0' },
        { text: '7:30 PM', value: '7:30' },
        { text: '8:00 PM', value: '8:0' },
        { text: '8:30 PM', value: '8:30' },
        { text: '9:00 PM', value: '9:0' },
        { text: '9:30 PM', value: '9:30' },
        { text: '10:00 PM', value: '10:0' },
        { text: '10:30 PM', value: '10:30' },
        { text: '11:00 PM', value: '11:0' },
        { text: '11:30 PM', value: '11:30' },
    ];

    public dateArrayThingy: any = [];

    constructor() {

    }

    AddNumberSuffix(i) {
        var j = i % 10,
            k = i % 100;
        if (j == 1 && k != 11) {
            return i + "st";
        }
        if (j == 2 && k != 12) {
            return i + "nd";
        }
        if (j == 3 && k != 13) {
            return i + "rd";
        }
        return i + "th";
    }

    public b64toFile(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, { type: contentType });
        return this.blobToFile(blob, "newimage");
    }

    public blobToFile = (theBlob: Blob, fileName: string): File => {
        var b: any = theBlob;
        //A Blob() is almost a File() - it's just missing the two properties below which we will add
        b.lastModifiedDate = new Date();
        b.name = fileName;

        //Cast to a File() type
        return <File>theBlob;
    }


    public OpenResource(file) {
        window.open(file.MediaUrl, '_blank');
    }

    public GetDatesTwoYears() {


    }

    public getDistance(pos1: any, pos2: any) {
        var lat1 = pos1.latitude;
        var lon1 = pos1.longitude;

        var lat2 = pos2.latitude;
        var lon2 = pos2.longitude;


        var R = 6371; // km 
        //has a problem with the .toRad() method below.
        var x1 = lat2 - lat1;

        var dLat = this.toRad(x1);

        var x2 = lon2 - lon1;
        var dLon = this.toRad(x2);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km

        return d;
    }


    toRad(position) {
        return position * Math.PI / 180;
    }

    // public exportAsExcelFile(Array: any, FileName: any, Sheetname: any = 'data'): void {

    //     const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(JSON.parse(JSON.stringify(Array)));

    //     const workbook: XLSX.WorkBook =
    //     {
    //         Sheets: { 'data': worksheet },
    //         SheetNames: ['data']
    //     };

    //     const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });

    //     this.saveAsExcelFile(excelBuffer, FileName);
    // }

    // public saveAsExcelFile(buffer: any, fileName: string): void {
    //     const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    //     const EXCEL_EXTENSION = '.xlsx';
    //     const data: Blob = new Blob([buffer], {
    //         type: EXCEL_TYPE
    //     });
    //     FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    // }

    public List_To_Tree(list) {
        var map = {}, node, roots = [], i;
        for (i = 0; i < list.length; i += 1) {
            map[list[i].ResourceId] = i; // initialize the map
            list[i].children = []; // initialize the children
        }
        for (i = 0; i < list.length; i += 1) {
            node = list[i];
            if (node.HasParent) {
                // if you have dangling branches check that map[node.parentId] exists
                if (list[map[node.ParentResourceId]] == undefined) {
                    console.log(list[map[node.ParentResourceId]]);
                }
                else {
                    list[map[node.ParentResourceId]].children.push(node);
                }

            } else {
                roots.push(node);
            }
        }
        return roots;
    }

    // public RefetchMap(AddressString: any) {

    //     return this.http.get('https://maps.google.com/maps/api/geocode/json?address=' + AddressString + '&key=AIzaSyCI9fb2kV3dX02_dr8IX6zYBsyMe_-32Bk')
    //         .map(response => response.json());

    // }

    public ValidURL(str: string) {
        return str.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    }

    public validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    public GetHours(): any {
        return [
            { name: '12 am', value: 0 },
            { name: '01 am', value: 1 },
            { name: '02 am', value: 2 },
            { name: '03 am', value: 3 },
            { name: '04 am', value: 4 },
            { name: '05 am', value: 5 },
            { name: '06 am', value: 6 },
            { name: '07 am', value: 7 },
            { name: '08 am', value: 8 },
            { name: '09 am', value: 9 },
            { name: '10 am', value: 10 },
            { name: '11 am', value: 11 },
            { name: '12 pm', value: 12 },
            { name: '01 pm', value: 13 },
            { name: '02 pm', value: 14 },
            { name: '03 pm', value: 15 },
            { name: '04 pm', value: 16 },
            { name: '05 pm', value: 17 },
            { name: '06 pm', value: 18 },
            { name: '07 pm', value: 19 },
            { name: '08 pm', value: 20 },
            { name: '09 pm', value: 21 },
            { name: '10 pm', value: 22 },
            { name: '11 pm', value: 23 },

        ]

    }

    public GetMinutes(): string[] {
        let x = [];
        for (let index = 0; index <= 59; index++) {
            if (index < 10) {
                x.push('0' + index);
            } else {
                x.push(index + '');
            }
        }
        return x;
    }


    ConstructPicker() {

        var nesheDate = new Date();
        var dateArrayThingy = new Array();
        var DateNow = new Date();

        dateArrayThingy.push({ text: "Today", value: this.formatDate(DateNow) });
        dateArrayThingy.push({ text: "Tomorrow", value: this.formatDate(new Date(DateNow.setDate(DateNow.getDate() + 1))) });

        //alert(nesheDate.getDay());
        nesheDate.setDate(nesheDate.getDate() + 1);
        for (var i = 0; i < 5; i++) {
            nesheDate.setDate(nesheDate.getDate() + 1);
            dateArrayThingy.push({ text: this.weekdayFull[nesheDate.getDay()], value: this.formatDate(nesheDate) });
        }
        for (var i = 0; i < 100; i++) {
            nesheDate.setDate(nesheDate.getDate() + 1);
            //alert(weekday[nesheDate.getDay()]);
            var day = this.weekday[nesheDate.getDay()];
            var month = this.monthChar[nesheDate.getMonth()];
            var dayInMonth = nesheDate.getDate();
            dateArrayThingy.push({ text: day + " " + month + " " + dayInMonth, value: this.formatDate(nesheDate) });

        }

        return dateArrayThingy;

    }

    getDateFromDayName(selectedDay) {
        var selectedDate = new Date();
        if (selectedDay == "Tomorrow") {
            selectedDate.setDate(selectedDate.getDate() + 1);
            return this.weekday[selectedDate.getDay()] + " " + this.monthChar[selectedDate.getMonth()] + " " + selectedDate.getDate();
        }
        if (selectedDay == "Today") {
            selectedDate.setDate(selectedDate.getDate());
            return this.weekday[selectedDate.getDay()] + " " + this.monthChar[selectedDate.getMonth()] + " " + selectedDate.getDate();
        }
        for (var i = 0; i < 7; i++) {
            if (this.weekdayFull[selectedDate.getDay()] == selectedDay)
                return this.weekday[selectedDate.getDay()] + " " + this.monthChar[selectedDate.getMonth()] + " " + selectedDate.getDate();
            selectedDate.setDate(selectedDate.getDate() + 1);
        }
    }

    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }
}
