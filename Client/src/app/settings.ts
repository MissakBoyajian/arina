import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

/**
 * A simple settings/config class for storing key/value pairs with persistence.
 */
@Injectable()
export class Settings {

  public reqOpts = {
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    params: new HttpParams()
  };

  public CurrentUser: any = null;
  
  constructor() {
  }

  public getValueAtIndex(index: number) {
    let key = localStorage.key(index);
    return this.getValue(key);
  }

  public getKeyAtIndex(index: number){
    return localStorage.key(index);
  }

  public getValue(key: string) {
    return localStorage.getItem(key);
  }
  public setValue(key: string, value: string) {
    localStorage.setItem(key, value);
  }
  public getAll(){
    let localStorageArray: Array<myStorage> = [];
    for(let i = 0; i < localStorage.length; i++){
      localStorageArray.push({
        key: this.getKeyAtIndex(i),
        value: this.getValueAtIndex(i)
      });
    }
    return localStorageArray;
  }
  public clearStorage(){
    localStorage.clear();
  }

  public removeValueByKey(key: string){
    localStorage.removeItem(key);
  }
}

interface myStorage{
  key: string,
  value: string
}