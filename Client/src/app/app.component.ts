import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PageFeedbacks } from '../pages/feedbacks/feedbacks';
import { PageStadiums } from '../pages/stadiums/stadiums';
import { PageSelectStadium } from '../pages/stadiums/selectstadium/selectstadium';

import { PageLogin } from '../pages/auth/login/login';
import { PageSettings } from '../pages/settings/settings';
import { PageMyBookings } from '../pages/mybookings/mybookings';
import { PageProfile } from '../pages/profile/profile';
import { PageTeams } from '../pages/teams/teams';
import { PageLeaderboard } from '../pages/leaderboard/leaderboard';
import { PagePlayerFinder } from '../pages/playerfinder/playerfinder';
import { PageHomepage } from '../pages/homepage/homepage';
import { Settings } from './settings';
import { UserProvider } from '../providers/user';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: Array<{ title: string, component: any, icon: string }>;

  constructor(private _settings: Settings, private _user: UserProvider, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {

    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [


      { title: 'Homepage', component: PageHomepage, icon: 'ios-home-outline' },
      { title: 'Reserve Stadiums', component: PageStadiums, icon: 'ios-calendar-outline' },
      { title: 'My Contacts', component: PageProfile, icon: 'ios-contacts-outline' },
      { title: 'My Bookings', component: PageMyBookings, icon: 'calendar' },
      { title: 'Settings', component: PageSettings, icon: 'ios-settings-outline' },
      { title: 'Feedback', component: PageFeedbacks, icon: 'ios-call-outline' },
      { title: 'My Profile', component: PageProfile, icon: 'ios-person-outline' },
      { title: 'My Teams', component: PageTeams, icon: 'ios-shirt-outline' },
      { title: 'Challenge Teams', component: PageTeams, icon: 'pricetag' },
      { title: 'Player Finder', component: PagePlayerFinder, icon: 'pricetag' },
      { title: 'Leaderboard', component: PageLeaderboard, icon: 'ios-trophy-outline' },
      { title: 'Tournament', component: PageProfile, icon: 'pricetag' },
      { title: 'Log Out', component: PageLogin, icon: 'pricetag' },

      // { title: 'Log Out', component: PageLogin, icon: 'pricetag' },

    ];


    if (!this._settings.getValue('token')) {
      this.rootPage = PageLogin;
    } else {
      this._settings.reqOpts.headers['Authorization'] = 'Bearer ' + this._settings.getValue('token');

      this._user.GetLoggedInMember().subscribe(res => {

        this._settings.CurrentUser = res;

        if (res != null || res != undefined) {
          this.rootPage = PageHomepage;
        }

      }, err => {
        this.rootPage = PageLogin;
      })
    }

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component, {
      FromMenu: page.title
    });
  }

  Logout() {
    this._settings.removeValueByKey('token');
    this._settings.CurrentUser = null;
    this.rootPage = PageLogin;
  }
}
