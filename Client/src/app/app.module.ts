import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { WheelSelector } from '@ionic-native/wheel-selector';

import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';

import { PageHomepage } from '../pages/homepage/homepage';
import { PageBookingAdd } from '../pages/bookingadd/bookingadd';
import { PageStadiums } from '../pages/stadiums/stadiums';

import { PageFeedbacks } from '../pages/feedbacks/feedbacks';
import { PageSelectCustomer } from '../pages/bookingadd/selectcustomer/selectcustomer';
import { PageTeams } from '../pages/teams/teams';
import { PageAddTeam } from '../pages/teams/addteam/addteam';


import { LibraryService } from '../helpers/LibraryService';
import { AlertClass } from '../helpers/AlertClass';
import { PageLogin } from '../pages/auth/login/login';

import { Settings } from './settings';
import { PageLeaderboard } from '../pages/leaderboard/leaderboard';

import { PageSettings } from '../pages/settings/settings';
import { PageProfile } from '../pages/profile/profile';
import { PageEditProfile } from '../pages/profile/editprofile/editprofile';
import { PageMyBookings } from '../pages/mybookings/mybookings';
import { PageTeamProfile } from '../pages/teams/teamprofile/teamprofile';
import { PageChallengeTeam } from '../pages/challengeteam/pagechallengeteam';

import { PageEditTeamProfile } from '../pages/teams/editteamprofile/editteamprofile';
import { PageStadiumDetails } from '../pages/stadiums/stadiumdetails/stadiumdetails';
import { PageSelectStadium } from '../pages/stadiums/selectstadium/selectstadium';
import { PageStadiumFilters } from '../pages/stadiums/stadiumfilters/stadiumfilters';
import { PagePlayerFinder } from '../pages/playerfinder/playerfinder';
import { PagePlayersFilter } from '../pages/playerfinder/playersfilters/playersfilter';

//PROVIDERSS
import { ProviderTeams } from '../providers/team';
import { ProviderSchedules } from '../providers/schedules';
import { ProviderCustomers } from '../providers/customers';
import { ProviderFeedbacks } from '../providers/feedbacks';
import { ProviderPromotions } from '../providers/promotions';

import { UserProvider } from '../providers/user';
import { Api } from '../providers/api';


// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { PageRegister } from '../pages/auth/register/register';
import { menubutton } from '../components/menubutton/menubutton';
import { PageVerifySMS } from '../components/verifysms/verifysms';
import { MultiPickerModule } from 'ion-multi-picker';
import { Geolocation } from '@ionic-native/geolocation';
import { ProviderStadiums } from '../providers/stadiums';
@NgModule({
  declarations: [
    MyApp,
    PageHomepage,
    PageStadiums,
    PageStadiumDetails,
    PageSelectStadium,
    PageBookingAdd,
    PageFeedbacks,
    PageSelectCustomer,
    PageTeams,
    PageAddTeam,
    PageLogin,
    PageLeaderboard,
    PageSettings,
    PageProfile,
    PageEditProfile,
    PageMyBookings,
    PageTeamProfile,
    PageEditTeamProfile,
    PageStadiumFilters,
    PageChallengeTeam,
    PagePlayerFinder,
    PagePlayersFilter,
    PageRegister,
    PageVerifySMS,
    menubutton

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    Ionic2RatingModule,
    MultiPickerModule, //Import MultiPickerModule
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PageHomepage,
    PageStadiums,
    PageStadiumDetails,
    PageSelectStadium,
    PageBookingAdd,
    PageFeedbacks,
    PageSelectCustomer,
    PageTeams,
    PageAddTeam,
    PageLogin,
    PageLeaderboard,
    PageSettings,
    PageProfile,
    PageEditProfile,
    PageMyBookings,
    PageTeamProfile,
    PageEditTeamProfile,
    PageStadiumFilters,
    PageChallengeTeam,
    PagePlayerFinder,
    PagePlayersFilter,
    PageRegister,
    PageVerifySMS,
    menubutton

  ],
  providers: [
    StatusBar,
    SplashScreen,
    Api,
    ProviderSchedules,
    ProviderCustomers,
    ProviderFeedbacks,
    ProviderPromotions,
    ProviderTeams,
    ProviderStadiums,
    UserProvider,
    LibraryService,
    AlertClass,
    Settings,
    WheelSelector,
    Geolocation,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
